//
//  AppDelegate.h
//  MonkLibrary
//
//  Created by gree's apple on 17/2/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

