//
//  SQLiteViewController.m
//  MonkLibrary
//
//  Created by gree's apple on 26/10/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "SQLiteViewController.h"

@interface SQLiteViewController ()

@end

@implementation SQLiteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self testSQLite];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)openDatabase:(NSString *)dbname {
    NSString *directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) lastObject];
    NSString *filePath = [directory stringByAppendingPathComponent:dbname];
    int result = sqlite3_open(filePath.UTF8String,&_database);
    if (result == SQLITE_OK) {
        NSLog(@"open success");
    } else {
        NSLog(@"open fail");
    }
}

/* 执行没有返回值的SQL语句 */
- (void)executeNonQuery:(NSString *)sql
{
    if (!_database) {
        return;
    }
    char *error;
    //执行没有返回值的SQL语句
    int result = sqlite3_exec(_database, sql.UTF8String, NULL, NULL, &error);
    if (result == SQLITE_OK) {
        NSLog(@"Execute SQL Query Success");
    } else {
        NSLog(@"Execute SQL Query Fail error : %s",error);
    }
}

/* 执行有返回值的SQL语句 */
- (NSArray *)executeQuery:(NSString *)sql
{
    if (!_database) {
        return nil;
    }
    NSMutableArray *array = [NSMutableArray array];
    sqlite3_stmt *stmt; //保存查询结果
    //执行SQL语句，返回结果保存在stmt中
    int result = sqlite3_prepare_v2(_database, sql.UTF8String, -1, &stmt, NULL);
    if (result == SQLITE_OK) {
        //每次从stmt中获取一条记录，成功返回SQLITE_ROW，直到全部获取完成，就会返回SQLITE_DONE
        while( SQLITE_ROW == sqlite3_step(stmt)) {
            //获取一条记录有多少列
            int columnCount = sqlite3_column_count(stmt);
            //保存一条记录为一个字典
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            for (int i = 0; i < columnCount; i++) {
                //获取第i列的字段名称
                const char *name  = sqlite3_column_name(stmt, i);
                //获取第i列的字段值
                const unsigned char *value = sqlite3_column_text(stmt, i);
                //保存进字典
                NSString *nameStr = [NSString stringWithUTF8String:name];
                NSString *valueStr = [NSString stringWithUTF8String:(const char *)value];
                dict[nameStr] = valueStr;
            }
            [array addObject:dict];//添加当前记录的字典存储
        }
        sqlite3_finalize(stmt);//stmt需要手动释放内存
        stmt = NULL;
        NSLog(@"Query Stmt Success");
        return array;
    }
    NSLog(@"Query Stmt Fail");
    return nil;
}

/* 关闭数据库 */
- (void)closeDatabase
{
    //关闭数据库
    int result = sqlite3_close(_database);
    if (result == SQLITE_OK) {
        NSLog(@"Close Database Success");
        _database = NULL;
    } else {
        NSLog(@"Close Database Fail");
    }
}

- (void)testSQLite {
    [self openDatabase:@"sqlite3_database.db"];
    //varchar存的东西不会变，char：存xxx到长度为10的char型字段，取出来后面补空格。
//    [self executeNonQuery:@"create table mytable(num varchar(7),name varchar(7),sex char(1),primary key(num));"];
        //或者
    [self executeNonQuery:@"create table mytable(num INTEGER PRIMARY KEY NOT NULL UNIQUE,name TEXT NOT NULL,sex NOT NULL DEFAULT M);"];
    [self executeNonQuery:@"insert into mytable(num,name,sex) values(0,'liuting','f');"];
    [self executeNonQuery:@"insert into mytable(num,name,sex) values(1,'lisi','mm1');"];
    [self executeNonQuery:@"insert into mytable(num,name,sex) values(1,'lisi','mm22');"];

    NSArray *result = [self executeQuery:@"select * from mytable WHERE num=1;"];
    if (result) {
        for (NSDictionary *row in result) {
            NSString *num = row[@"num"];
            NSString *name = row[@"name"];
            NSString *sex = row[@"sex"];
            NSLog(@"Read Database: num=%@, name=%@, sex=%@",num,name,sex);
        }
    }
    
    [self closeDatabase];
}






@end
