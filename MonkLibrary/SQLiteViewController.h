//
//  SQLiteViewController.h
//  MonkLibrary
//
//  Created by gree's apple on 26/10/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sqlite3.h"

@interface SQLiteViewController : UIViewController

@property (nonatomic,assign) sqlite3 *database;

- (void)testSQLite;

@end
