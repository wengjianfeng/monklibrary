//
//  MM_PersonViewModel.m
//  MonkLibrary
//
//  Created by gree's apple on 25/3/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "MM_PersonViewModel.h"

@implementation MM_PersonViewModel

-(instancetype)initWithPerson:(MM_Person *)person {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _person = person;
    //下面这段代码本来是在c里面的，现在被提取到这；
    if (person.salutation.length>0) {
        _nameText = [NSString stringWithFormat:@"%@%@%@",person.salutation,person.firstName,person.lastName];
    } else {
        _nameText = [NSString stringWithFormat:@"%@%@",person.firstName,person.lastName];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE MMMM d, yyyy"];
    _birthdateText = [dateFormatter stringFromDate:person.birthdate];
    
    return self;
}

@end
