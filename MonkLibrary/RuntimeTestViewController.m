//
//  RuntimeTestViewController.m
//  MonkLibrary
//
//  Created by gree's apple on 15/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "RuntimeTestViewController.h"
#import "runtimeModel.h"
#import <objc/runtime.h>
#import "Associated.h"
#import "DeallocBlock.h"

@interface RuntimeTestViewController ()
{
    UIView *vv;
}
@end

@implementation RuntimeTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
/**
 成员变量 typedef struct objc_property *objc_property_t;
 **/
    unsigned int outCount = 0;
    // 获取所有成员变量
    Ivar *ivars = class_copyIvarList([runtimeModel class], &outCount);
    for (unsigned int i = 0; i < outCount; i++) {
        Ivar ivar = ivars[i];
        // 获取成员变量名
        const char *name = ivar_getName(ivar);
        // 获取成员变量类型编码
        const char *type = ivar_getTypeEncoding(ivar);
        NSLog(@"type is %s and name %s",type,name);
        // 获取某个对象成员变量的值  object_getIvar
        // 设置某个对象成员变量的值  object_setIvar
    }
    free(ivars);
    
/**
 属性 typedef struct objc_property *objc_property_t;
 **/
    //使用class_copyPropertyList并不会获取无@property声明的成员变量
    objc_property_t *vs = class_copyPropertyList([runtimeModel class], &outCount);
    for (unsigned int i = 0; i < outCount; i++) {
        objc_property_t v = vs[i];
        const char *name = property_getName(v);
        /**
         属性类型  name值：T  value：变化
         编码类型  name值：C(copy) &(strong) W(weak) 空(assign) 等 value：无
         非/原子性 name值：空(atomic) N(Nonatomic)  value：无
         变量名称  name值：V  value：变化
         **/
        const char *type = property_getAttributes(v);
        NSLog(@"%s !!%s",name,type);
    }
    free(vs);
    
/**
 关联属性  主要用于类别，因为类别不能定义属性。
 **/
    NSObject *obj = [[NSObject alloc] init];
    obj.associatedObject = @"我是关联属性啦!";
}

- (void)jsonToModelWithProperty {
    /**
     JSON--->Model 主要用property
     **/
    NSDictionary *dic = @{@"name":@"monk",
                          @"year":@"18",
                          @"sex" :@"male"
                          };
    
    runtimeModel *model = [[runtimeModel alloc] init];
    unsigned int count = 0;
    objc_property_t *ms = class_copyPropertyList([runtimeModel class], &count);
    for (int i = 0; i < count; i ++) {
        objc_property_t m = ms[i];
        const char *n = property_getName(m);
        NSString *name = [NSString stringWithUTF8String:n];
        NSLog(@"name %@",name);
        [model setValue:dic[name] forKey:name];
    }
    NSLog(@"%@",model);
    free(ms);
}

- (void)jsonToModelWithIvar {
    NSDictionary *dic = @{@"name":@"monk11",
                          @"year":@"18222",
                          @"sex" :@"male333"
                          };
    
    runtimeModel *model = [[runtimeModel alloc] init];
    
    unsigned int outCount = 0;
    // 获取所有成员变量(使用property定义的变量名会添加"_")
    Ivar *ivars = class_copyIvarList([runtimeModel class], &outCount);
    for (unsigned int i = 0; i < outCount; i++) {
        Ivar ivar = ivars[i];
        // 获取成员变量名
        const char *n = ivar_getName(ivar);
        NSString *name = [NSString stringWithUTF8String:n];
        NSLog(@"name 2 :%@",name);
        [model setValue:dic[name] forKey:name];
    }
    free(ivars);
    NSLog(@"%@",model);
    
}

- (void)testAddMethod {
    runtimeModel *model = [[runtimeModel alloc] init];
    //使用拦截功能去捕获未定义的方法，并添加方法
    [model performSelector:@selector(tar)];
    [model performSelector:@selector(tar2:) withObject:@"我是带参数的method"];
}

- (void)testAssociateDealloc {
    vv = [[UIView alloc] initWithFrame:CGRectMake(50, 50, 50, 50)];
    vv.backgroundColor = [UIColor redColor];
    vv.Map = @{@"111":@"22"};
    
    sleep(1);
    [self testAssociateNotification];
    sleep(1);
    [self testAssociateRemoveView];
}

- (void)testAssociateRemoveView {
    NSLog(@"remove");
    vv = nil;
}

- (void)testAssociateNotification {
    NSLog(@"associateNotification");
    [[NSNotificationCenter defaultCenter] postNotificationName:kEventDidChangeNotification object:nil userInfo:@{ @"BackgroundColor" : @"randomColor" }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
