//
//  runtimeModel.h
//  runtimeTest
//
//  Created by gree's apple on 14/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface runtimeModel : NSObject
{
    NSString *name;
}
//使用runtime获取时会得到_year／_sex参数
@property (nonatomic, strong) NSString *year;
@property NSString *sex;
@end
