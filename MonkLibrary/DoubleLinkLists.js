/*----->双向链表<-----*/‘
//每一个节增加一个前链表的指针

this.insert = function(data,key) {
    
    var newNode = new createNode(data);
    var current = findNode(headNode,key);
    newNode.next = current.next;
    newNode.previous = current;
    current.next = newNode;
}

this.remove = function(key) {
    var currNode = findNode(headNode,key);
    
    if(!(currNode.next == null)) {
        
        currNode.previous.next = currNode.next;
        currNode.next.previous = currNode.previous;
        currNode.next = null;
        currNode.previous = null;
    }
}