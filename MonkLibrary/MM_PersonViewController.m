//
//  MM_PersonViewController.m
//  MonkLibrary
//
//  Created by gree's apple on 25/3/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "MM_PersonViewController.h"

@interface MM_PersonViewController ()
@property (nonatomic,strong) UILabel *nameLabel;
@property (nonatomic,strong) UILabel *birthdateLabel;
@end

@implementation MM_PersonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //很简洁，数据的操作都移到vm中
    /*
     *Model在此例中是不变的；如果model是可变的，则需要进行绑定，一般这时会用RAC，但
     不强制；用kvo也行(属性1⃣️多就悲剧咯)
     */
    self.nameLabel.text = self.ViewModel.nameText;
    self.birthdateLabel.text = self.ViewModel.birthdateText;
    
    //原mvc架构下的代码
//    if (self.model.salutation.length>0) {
//        self.nameLabel.text = [NSString stringWithFormat:@"%@%@%@",self.model.salutation,self.model.firstName,self.model.lastName];
//    } else {
//        self.nameLabel.text = [NSString stringWithFormat:@"%@%@",self.model.firstName,self.model.lastName];
//    }
//    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"EEEE MMMM d,yyyy"];
//    self.birthdateLabel.text = [dateFormatter stringFromDate:self.model.birthdate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
