/*----->栈<-----*/
//先入后出，通过栈顶访问
function Stack() {
    this.dataStore = [];
    this.top = 0;
    this.push = push;
    this.pop = pop;
    this.peek = peek;
    this.length = length;
}

function push(element) {
    this.dataStore[this.top++] = element;
}

function peek(element) {
    return this.dataStore[this.top - 1];
}

function pop() {
    return this.dataStore[--this.top];
}

function clear() {
    this.top = 0;
}

function length() {
    return this.top;
}

//阶乘算法 5!=5*4*3*2*1
function fact(n) {
    var s = new Stack();
    while (n>1) {
        s.push(n--);//5,4...
    }
    var product = 1;
    while (s.length() > 0) {
        product *= s.pop();//通过出栈方式
    }
    
    return product;
}

fact (5);





