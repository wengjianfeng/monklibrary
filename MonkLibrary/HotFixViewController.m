//
//  HotFixViewController.m
//  MonkLibrary
//
//  Created by gree's apple on 1/7/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "HotFixViewController.h"
#import "JPEngine.h"
#import "UIView+Toast.h"
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kToastDuration 3.0f

@interface HotFixViewController ()
@property (nonatomic,strong) NSMutableArray *crashArray;

@end

@implementation HotFixViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _crashArray = [[NSMutableArray alloc] initWithObjects:@"0", nil];
    
    UIButton *CrlEvents = [[UIButton alloc] initWithFrame:CGRectMake(0, 100, [UIScreen mainScreen].bounds.size.width, 50)];
    [CrlEvents setTitle:@"HotFix_ErrorController" forState:UIControlStateNormal];
    [CrlEvents addTarget:self action:@selector(errorController:) forControlEvents:UIControlEventTouchUpInside];
    [CrlEvents setBackgroundColor:[UIColor redColor]];
    
    UIButton *FixCrl = [[UIButton alloc] initWithFrame:CGRectMake(0, 150, [UIScreen mainScreen].bounds.size.width, 50)];
    [FixCrl setTitle:@"HotFix_FixController" forState:UIControlStateNormal];
    [FixCrl addTarget:self action:@selector(fixController:) forControlEvents:UIControlEventTouchUpInside];
    [FixCrl setBackgroundColor:[UIColor blueColor]];
    
    [self.view addSubview:CrlEvents];
    [self.view addSubview:FixCrl];
//////////
    UIButton *CrashEvents = [[UIButton alloc] initWithFrame:CGRectMake(0, 250, [UIScreen mainScreen].bounds.size.width, 50)];
    [CrashEvents setTitle:@"HotFix_Crash" forState:UIControlStateNormal];
    [CrashEvents addTarget:self action:@selector(crash:) forControlEvents:UIControlEventTouchUpInside];
    [CrashEvents setBackgroundColor:[UIColor redColor]];
    
    UIButton *FixCrash = [[UIButton alloc] initWithFrame:CGRectMake(0, 300, [UIScreen mainScreen].bounds.size.width, 50)];
    [FixCrash setTitle:@"HotFix_FixCrash" forState:UIControlStateNormal];
    [FixCrash addTarget:self action:@selector(fixCrash:) forControlEvents:UIControlEventTouchUpInside];
    [FixCrash setBackgroundColor:[UIColor blueColor]];
    
    [self.view addSubview:CrashEvents];
    [self.view addSubview:FixCrash];
}

- (void)errorController:(id)sender
{
    CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
    [self.view makeToast:@"无事件" duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
}

- (void)fixController:(id)sender
{
    [JPEngine startEngine];
    NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"demo" ofType:@"js"];
    NSString *script = [NSString stringWithContentsOfFile:sourcePath encoding:NSUTF8StringEncoding error:nil];
    [JPEngine evaluateScript:script];
    CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
    [self.view makeToast:@"注入成功" duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
}

- (void)crash:(id)sender
{
    NSString *crashInfo = [NSString stringWithFormat:@"注入成功： %@",_crashArray[1]] ;

    CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
    [self.view makeToast:crashInfo duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
}

- (void)fixCrash:(id)sender
{
    [JPEngine startEngine];
    NSString *sourcePath = [[NSBundle mainBundle] pathForResource:@"FixCrash" ofType:@"js"];
    NSString *script = [NSString stringWithContentsOfFile:sourcePath encoding:NSUTF8StringEncoding error:nil];
    [JPEngine evaluateScript:script];
    CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
    [self.view makeToast:@"注入成功" duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
}


@end
























