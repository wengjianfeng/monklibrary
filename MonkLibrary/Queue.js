/*----->队列<-----*/
//先进先出 只允许在表的前端（front）进行删除操作，而在表的后端（rear）进行插入操作

function Queue() {
    this.dataStore = [];
    this.enqueue = enqueue;
    this.first = first;
    this.end = end;
    this.toString = toString;
    this.empty = empty;
}

//向队尾添加一个元素
function enqueue(element) {
    this.dataStore.push(element);//语言自带方法
}
//删除队首的元素
function dequeue() {
    return this.dataStore.shift();//语言自带方法吧
}
//读取队首和队尾元素
function first() {
    return this.dataStore[0];
}

function end() {
    return this.dataStore[this.dataStore.length - 1];
}
//显示队列内的所有元素
function toString() {
    var retStr = "";
    for (var i=0; i< this.dataStore.length; i++) {
        retStr += this.dataStore[i] + "\n";
    }
    return retStr;
}

//判断是否为空
function empty() {
    if(this.dataStore.length == 0) {
        return true;
    } else {
        return false;
    }
}

var q = new Queue();
q.enqueue("aron1");
q.enqueue("aron2");

console.log("队列头" + q.first());
console.log("队列尾" + q.end());













