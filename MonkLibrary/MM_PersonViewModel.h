//
//  MM_PersonViewModel.h
//  MonkLibrary
//
//  Created by gree's apple on 25/3/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MM_Person.h"
//在MVC基础上添加了此
@interface MM_PersonViewModel : NSObject

- (instancetype)initWithPerson:(MM_Person *)person;

@property (nonatomic,readonly) MM_Person *person;

@property (nonatomic,readonly) NSString *nameText;
@property (nonatomic,readonly) NSString *birthdateText;

@end
