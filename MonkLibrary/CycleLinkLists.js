/*----->循环链表<-----*/
function createNode(data) {
    this.data = data;
    this.next = null;
}


//初始化头部节
//从headNode开始形成一条链条
//通过next衔接
var headNode = this.headNode = new creatNode("head");
this.headNode.next = this.head;

var findNode = function createFindNode(currNode) {
    return function(key) {
        while (currNode.data != key) {
            currNode = currNode.next;
        }
        return currNode;
    }
}(headNode);

var findPrevious = function(currNode) {
    
    return function(key) {
        while(!(currNode.next == null) &&
              (currNode.next.data != key)) {
            currNode = currNode.next;
        }
        return currNode;
    }
}(headNode);

this.insert = function(data, key) {
    //创建一个新节
    var newNode = new createNode(data);
    //在链条中找到对应的数据节
    //然后把新加入的挂进去
    var current = findNode(key);
    //插入新的接，更改引用关系
    //1:a-b-c-d
    //2:a-b-n-c-d
    newNode.next = current.next;
    current.next = newNode;
};

//插入方法
this.remove = function(key) {
    var prevNode = findPrevious(key);
    if (!(prevNode.next == null)) {
        //修改链表关系
        prevNode.next = prevNode.next.next;
    }
};

this.display = function(fn) {
    var currNode = headNode;
    while (!(currNode.next == null) &&
           !(currNode.next.data == "head")) {
        fn(currNode.next.data)
        currNode = currNode.next;
    }
};
}
var cities = new createLinkList();
function create() {
    var text = '';
    cities.display(function(data) {
                   text += '-' + data;
                   });
    var div = document.createElement('div')
    div.innerHTML = text;
    document.body.appendChild(div)
}
document.getElementById("test1").onclick = function() {
    cities.insert("Conway", "head");
    cities.insert("Russellville", "Conway");
    cities.insert("Carlisle", "Russellville");
    cities.insert("Alma", "Carlisle");
    create();
}
document.getElementById("test2").onclick = function() {
    cities.remove("Russellville");
    create()
}




