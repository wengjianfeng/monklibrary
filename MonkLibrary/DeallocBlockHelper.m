//
//  DeallocBlockHelper.m
//  MonkLibrary
//
//  Created by gree's apple on 28/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "DeallocBlockHelper.h"

@interface DeallocBlockHelper()

@property (nonatomic, copy) DeallocExecutorBlock deallocExecutorBlock;

@end

@implementation DeallocBlockHelper

-(id)initWithBlock:(DeallocExecutorBlock)block {
    self = [super init];
    if (self) {
        _deallocExecutorBlock = [block copy];
    }
    
    return self;
}

-(void)dealloc {
    NSLog(@"DeallocExecutor Dealloc");
    if (_deallocExecutorBlock) {
        _deallocExecutorBlock();
        _deallocExecutorBlock = nil;
    }
}

@end
