//
//  Associated.m
//  MonkLibrary
//
//  Created by gree's apple on 17/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "Associated.h"
#import <objc/runtime.h>
@implementation NSObject (Associated)
@dynamic associatedObject;

-(void)setAssociatedObject:(id)object {
    //object为关联对象
    objc_setAssociatedObject(self, @selector(associatedObject), object, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(id)associatedObject {
    //大部分情况下，会使用SEL当作key
    return objc_getAssociatedObject(self, @selector(associatedObject));
}

@end
