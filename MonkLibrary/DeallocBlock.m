//
//  DeallocBlock.m
//  MonkLibrary
//
//  Created by gree's apple on 28/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "DeallocBlock.h"
#import <objc/runtime.h>
#import "DeallocBlockHelper.h"

static char *kDeallocHelper = "kDeallocHelper";

@implementation UIView (DeallocBlock)
//http://www.jianshu.com/p/ebd112cd8d41 换肤
- (void)setMap:(NSDictionary *)Map {
    objc_setAssociatedObject(self, @selector(Map), Map, OBJC_ASSOCIATION_COPY_NONATOMIC);
    if (Map) {
        // NOTE: need to be __unsafe_unretained because __weak var will be reset to nil in dealloc
        __unsafe_unretained typeof (self) weakSelf = self;
        // 将deallocHelper作为关联属性就是为了当view释放时，它也能调用到dealloc
        if (objc_getAssociatedObject(self, &kDeallocHelper) == nil) {
            id deallocHelper = [[DeallocBlockHelper alloc] initWithBlock:^{
                NSLog(@"Associate Dealloc");
                [[NSNotificationCenter defaultCenter] removeObserver:weakSelf];
            }];
            //此处的属性为OBJC_ASSOCIATION_RETAIN，换成其他的就直接结束了，因为deallocHelper是局部变量嘛
            objc_setAssociatedObject(self, &kDeallocHelper, deallocHelper, OBJC_ASSOCIATION_RETAIN);
        }
        //此处怕重复或者怎样，反正先remove掉
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:kEventDidChangeNotification
                                                      object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(themeChanged:)
                                                     name:kEventDidChangeNotification
                                                   object:nil];
        /*
            这里就可以实现你要的功能啦
         */
        [self doSomething];
    } else {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:kEventDidChangeNotification
                                                      object:nil];
    }
    
}

- (NSDictionary *)Map {
    return objc_getAssociatedObject(self, @selector(Map));
}

- (void)themeChanged:(NSNotification *)notification {
    if (notification.userInfo == nil) {
        return;
    }

    [self doSomething];
}

- (void)doSomething {
    NSLog(@"doSomething");
}

@end
