//
//  DeallocBlock.h
//  MonkLibrary
//
//  Created by gree's apple on 28/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString * const kEventDidChangeNotification = @"EventDidChangeNotification";

@interface UIView (DeallocBlock)

/*
    关联属性特点：当其父对象释放时，关联属性也会被释放。
    引入此类原因：如果有这么个情况，需要在catagary中监听，那么就涉及到dealloc，但catagary中无法复写dealloc()，
                也很难做到在每个view的dealloc中释放notification（别的成员不知道要释放），所以就有了这个类，自动去
                释放观察。
 */
@property (nonatomic, readwrite, copy) NSDictionary *Map;

@end
