/*----->单链表<-----*/
//(就是很单一的向下传递，每一个节点只记录下一个节点的信息，就跟无间道中的梁朝伟一样做卧底都是通过中间人上线与下线联系，一旦中间人断了，那么就无法证明自己的身份了，所以片尾有一句话："我是好人，谁知道呢？”)
function creatLinkList() {
    
    //creat Node
    function creatNode(data) {
        this.data = data;
        this.next = null;
    };
    
    //initial Head-Node
    var headNode = new creatNode("head");
    
    //find Node
    var findNode = function(currNode) {//由于传参为headNode，所以currNode即为headNode
        return function(key) {//key是因为方法参数就key，感觉参数范围是全局的。。
            //之所以能这么判断，是应为currNode到后面是链表结构了，next一级一级下去
            while (currNode.data != key) { //为何是data和key对比，见insert调用者
                currNode = currNode.next;
            }
            return currNode;
        }
    }(headNode);//(headNode)executor immediately传入值（第一次扫描完就执行）
    
    //find Previous Node
    var findPrevious = function(currNode) {
        return function(key) {
            while (!(currNode.next == null) &&
                   (currNode.next.data != key)){//注意是next.data，所以能够找到
                currNode = currNode.next;
            }
            return currNode;
        }
    }(headNode);
    
    //insert Node
    this.insert = function(data,key){
        var newNode = new creatNode(data);
        var current = findNode(key);
        newNode.next = current.next;
        current.next = newNode;
    };
    //-arron3-arron2-arron1-null
    
    //remove Node
    this.remove = function(key) {
        var prevNode = findPrevious(key);
        if(!(prevNode.next == null)) {
            prevNode.next = prevNode.next.next;//呵呵
        }
    };
    
    this.display = function(fn){
        var currNode = headNode;
        while (!(currNode.next == null)) {
            currNode = currNode.next;
            fn(currNode.data)//此处有点像黑魔法
        }
    };
    //simple
    var cities = new creatLinkList();
    
    function creat() {
        var text = '';
        cities.display(function(data) {
                       text += '-' + data;
                       });
        var div = document.creatElement('div');
        div.innerHTML = text;
        document.getElementById("listshow").appendChild(div);
    }
    document.getElementById("test1").onclick = function() {
        cities.insert("Conway", "head");
        cities.insert("Russellville", "Conway");
        cities.insert("Carlisle", "Russellville");
        cities.insert("Alma", "Carlisle");
        create();
    }
    document.getElementById("test2").onclick = function() {
        cities.remove("Russellville");
        create()
    }
}







/**参考文献**/
/*1.http://www.cnblogs.com/aaronjs/*/