//
//  runtimeModel.m
//  runtimeTest
//
//  Created by gree's apple on 14/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "runtimeModel.h"
#import <objc/runtime.h>

@implementation runtimeModel

void target(id self, SEL _cmd) {
    NSLog(@"target");
}
void target2(id self, SEL _cmd, id prame) {
    NSLog(@"target2 %@",prame);
}


+ (BOOL)resolveInstanceMethod:(SEL)sel {
    if (sel == @selector(tar)) {
        /**
         v:void
         @:对象
         ::SEL
         **/
        class_addMethod(self, sel, (IMP)target, "v@:");
        return YES;
    } else if (sel == @selector(tar2:)) {
        class_addMethod(self, sel, (IMP)target2, "v@:@");
        return YES;
    }
    
    return [super resolveInstanceMethod:sel];
}


@end
