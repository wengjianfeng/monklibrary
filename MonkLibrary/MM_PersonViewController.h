//
//  MM_PersonViewController.h
//  MonkLibrary
//
//  Created by gree's apple on 25/3/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MM_Person.h"
#import "MM_PersonViewModel.h"
@interface MM_PersonViewController : UIViewController

//@property (nonatomic,strong) MM_Person *model;//这是典型的mvc模式，c拥有了m，mvvm中已将此移到vm中
@property (nonatomic, strong) MM_PersonViewModel *ViewModel;//m替换成了vm

@end
