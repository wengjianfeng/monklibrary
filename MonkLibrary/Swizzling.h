//
//  Swizzling.h
//  runtimeTest
//
//  Created by gree's apple on 14/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Swizzling)

+ (void)swizzleOldSelector:(SEL)oldSel newSelector:(SEL)newSel;

@end
