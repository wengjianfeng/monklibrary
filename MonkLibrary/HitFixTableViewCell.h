//
//  HitFixTableViewCell.h
//  MonkLibrary
//
//  Created by gree's apple on 1/7/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HitFixTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@end
