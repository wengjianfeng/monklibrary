//
//  ButtonSwizzling.m
//  runtimeTest
//
//  Created by gree's apple on 14/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "ButtonSwizzling.h"
#import "Swizzling.h"

@implementation UIButton (ButtonSwizzling)
/*
 Swizzling应该在+load方法中实现，因为+load方法可以保证在类最开始加载时会调用。因为method swizzling的影响范围是全局的，所以应该放在最保险的地方来处理是非常重要的。+load能够保证在类初始化的时候一定会被加载，这可以保证统一性。试想一下，若是在实际时需要的时候才去交换，那么无法达到全局处理的效果，而且若是临时使用的，在使用后没有及时地使用swizzling将系统方法与我们自定义的方法实现交换回来，那么后续的调用系统API就可能出问题。
 
 类文件在工程中，一定会加载，因此可以保证+load会被调用。
 不要在+initialize中交换
 
 +initialize是类第一次初始化时才会被调用，因为这个类有可能一直都没有使用到，因此这个类可能永远不会被调用。
 
 类文件虽然在工程中，但是如果没有任何地方调用过，那么是不会调用+initialize方法的。
 */
+(void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SEL oldSEL = @selector(endTrackingWithTouch:withEvent:);
        SEL newSEL = @selector(newEndTrackingWithTouch:withEvent:);
       
        [self swizzleOldSelector:oldSEL newSelector:newSEL];
        
        SEL oldSEL1 = @selector(continueTrackingWithTouch:withEvent:);
        SEL newSEL1 = @selector(newContinueTrackingWithTouch:withEvent:);
        [self swizzleOldSelector:oldSEL1 newSelector:newSEL1];

    });
}

-(void)newEndTrackingWithTouch:(nullable UITouch *)touch withEvent:(nullable UIEvent *)event {
    NSLog(@"button touch");
    [self newEndTrackingWithTouch:touch withEvent:event];
}

-(void)newContinueTrackingWithTouch:(nullable UITouch *)touch withEvent:(nullable UIEvent *)event {
    NSLog(@"333");
    [self newContinueTrackingWithTouch:touch withEvent:event];
}

@end
