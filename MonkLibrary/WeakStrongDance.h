//
//  WeakStrongDance.h
//  MonkLibrary
//
//  Created by gree's apple on 29/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#ifndef WeakStrongDance_h
#define WeakStrongDance_h

//o##Weak为在对象后面添加Weak，即oWeak
#define WeakObj(o) __weak __typeof(o) o##Weak = o;
#define StrongObj(o) __strong __typeof(o) o = o##weak;

#endif /* WeakStrongDance_h */
