//
//  Swizzling.m
//  runtimeTest
//
//  Created by gree's apple on 14/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "Swizzling.h"
#import <objc/runtime.h>

@implementation NSObject (Swizzling)

+ (void)swizzleOldSelector:(SEL)oldSel newSelector:(SEL)newSel {

    Class class = [self class];
    
    Method oldMethod = class_getInstanceMethod(class, oldSel);
    Method newMethod = class_getInstanceMethod(class, newSel);
    
    //先尝试給源方法添加实现，这里是为了避免源方法没有实现的情况
    bool succ = class_addMethod(class, oldSel, method_getImplementation(newMethod), method_getTypeEncoding(newMethod));
    
    if (succ) {
        //添加成功：将源方法的实现替换到交换方法的实现
        class_replaceMethod(class, newSel, method_getImplementation(oldMethod), method_getTypeEncoding(oldMethod));
    } else {
        //添加失败：说明源方法已经有实现，直接将两个方法的实现交换即可
        method_exchangeImplementations(oldMethod, newMethod);
    }
}

@end
