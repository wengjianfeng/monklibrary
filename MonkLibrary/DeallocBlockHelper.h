//
//  DeallocBlockHelper.h
//  MonkLibrary
//
//  Created by gree's apple on 28/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^DeallocExecutorBlock)(void);

@interface DeallocBlockHelper : NSObject

- (id)initWithBlock:(DeallocExecutorBlock)block;

@end
