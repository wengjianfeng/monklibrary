//
//  RC_FlattenMap.m
//  MonkLibrary
//
//  Created by gree's apple on 28/3/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "RC_FlattenMap.h"
#import "ReactiveCocoa.h"
@implementation RC_FlattenMap

//flattenMap将两个signal变成了一个大的signal，任意一个signal sendError都会渠道error。两个都complete才行
//-(void)upload:(id)sender {
//    [[[self askFileNameSignal] flattenMap:^RACStream *(NSString *filename) {
//        return [self uploadFileSignal:filename];
//    }]subscribeError:^(NSError *error) {
//        NSLog(@"error");
//    } completed:^{
//        NSLog(@"complete");
//    }];
//}
// 
//-(RACSignal *)uploadFileSignal:(NSString *)filename {
//    RACSignal *signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
//        //这里可以发送网络请求，当然如果超时或错误，也可以sendError
//        NSLog(@"请求");
//        [subscriber sendCompleted];
//        return nil;
//    }];
//    
//    return signal;
//}
//
////取得档案的signal
//-(RACSignal *)askFileNameSignal {
//    RACSignal *signal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
//        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"File Name" message:@"输入名称" delegate:nil cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
//        [alertview.rac_buttonClickedSignal subscribeNext:^(NSNumber *buttonIndex) {
//            NSString *filename = [alertview textFieldAtIndex:0].text;
//            if ([buttonIndex integerValue] == alertview.cancelButtonIndex) {
//                NSError *error = [NSError errorWithDomain:@"file name error" code:1000 userInfo:@{NSLocalizedDescriptionKey:@"请输入"}];
//                [subscriber sendError:error];
//            } else {
//                [subscriber sendNext:filename];
//                [subscriber sendCompleted];
//            }
//        }];
//        [alertview show];
//        
//        return nil;//返回nil才会控制signal的生命周期
//    }];
//    
//    return signal;
//}
//
@end
