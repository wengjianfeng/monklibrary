//
//  MutableArraySwizzling.m
//  runtimeTest
//
//  Created by gree's apple on 14/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "MutableArraySwizzling.h"
#import "Swizzling.h"

@implementation NSMutableArray (MutableArraySwizzling)

+(void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
//        [objc_getClass("__NSPlaceholderArray") swizzleOldSelector:@selector(initWithObjects:count:) newSelector:@selector(new_initWithObjects:count:)];
        //因为类簇实际调用的不是NSArray这些类
//        [objc_getClass("__NSArrayM") swizzleOldSelector:@selector(addObject:) newSelector:@selector(new_addObject:)];
//        [objc_getClass("__NSArrayM") swizzleOldSelector:@selector(removeObject:) newSelector:@selector(new_removeObject:)];
//        [objc_getClass("__NSArrayM") swizzleOldSelector:@selector(insertObject:atIndex:) newSelector:@selector(new_insertObject:atIndex:)];
//        [objc_getClass("__NSArrayM") swizzleOldSelector:@selector(objectAtIndex:) newSelector:@selector(new_objectAtIndex:)];
//        [objc_getClass("__NSArrayM") swizzleOldSelector:@selector(removeObjectAtIndex:) newSelector:@selector(new_removeObjectAtIndex:)];
    });
}

- (instancetype)new_initWithObjects:(const id  _Nonnull __unsafe_unretained *)objects count:(NSUInteger)cnt {
    BOOL hasNilObject = NO;
    for (NSUInteger i = 0; i < cnt; i++) {
        if ([objects[i] isKindOfClass:[NSArray class]]) {
            NSLog(@"%@", objects[i]);
        }
        if (objects[i] == nil) {
            hasNilObject = YES;
            NSLog(@"%s object at index %lu is nil, it will be filtered", __FUNCTION__, i);
        }
    }
    
    // 因为有值为nil的元素，那么我们可以过滤掉值为nil的元素
    if (hasNilObject) {
        id __unsafe_unretained newObjects[cnt];
        
        NSUInteger index = 0;
        for (NSUInteger i = 0; i < cnt; ++i) {
            if (objects[i] != nil) {
                newObjects[index++] = objects[i];
            }
        }
        
        return [self new_initWithObjects:newObjects count:index];
    }
    
    return [self new_initWithObjects:objects count:cnt];
}


- (void)new_addObject:(id)obj {
    if (obj == nil) {
        NSLog(@"%s can add nil object into NSMutableArray", __FUNCTION__);
    } else {
        [self new_addObject:obj];
    }
}

- (void)new_removeObject:(id)obj {
    if (obj == nil) {
        NSLog(@"%s call -removeObject:, but argument obj is nil", __FUNCTION__);
        return;
    }
    
    [self new_removeObject:obj];
}

- (void)new_insertObject:(id)anObject atIndex:(NSUInteger)index {
    if (anObject == nil) {
        NSLog(@"%s can't insert nil into NSMutableArray", __FUNCTION__);
    } else if (index > self.count) {
        NSLog(@"%s index is invalid", __FUNCTION__);
    } else {
        [self new_insertObject:anObject atIndex:index];
    }
}

- (id)new_objectAtIndex:(NSUInteger)index {
    if (self.count == 0) {
        NSLog(@"%s can't get any object from an empty array", __FUNCTION__);
        return nil;
    }
    
    if (index > self.count) {
        NSLog(@"%s index out of bounds in array", __FUNCTION__);
        return nil;
    }
    
    return [self new_objectAtIndex:index];
}

- (void)new_removeObjectAtIndex:(NSUInteger)index {
    if (self.count <= 0) {
        NSLog(@"%s can't get any object from an empty array", __FUNCTION__);
        return;
    }
    
    if (index >= self.count) {
        NSLog(@"%s index out of bound", __FUNCTION__);
        return;
    }
    
    [self new_removeObjectAtIndex:index];
}

@end
