//
//  MM_Person.h
//  MonkLibrary
//
//  Created by gree's apple on 25/3/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MM_Person : NSObject

//典型的mvc结构啊，model提供这样的方法被c修改
- (instancetype)initwithSalutation:(NSString *)sa
                         firstName:(NSString *)fn
                          lastName:(NSString *)ln
                         birthdate:(NSDate *)bd;

@property (nonatomic, readonly) NSString *salutation;
@property (nonatomic, readonly) NSString *firstName;
@property (nonatomic, readonly) NSString *lastName;
@property (nonatomic, readonly) NSDate *birthdate;

@end
