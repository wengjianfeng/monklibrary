//
//  NSString+UnPacking.swift
//  VRF
//
//  Created by WJF_Monk on 2016/12/5.
//  Copyright © 2016年 WJF-Monk  330694. All rights reserved.
//
/************************************************************************************/
/*     针对粘包的拆包处理                                                              */
/************************************************************************************/

//        完整情况 (直接取完整情况，之后取<<<之前的以及>>>之后的，接着再进行截取。
//        开头完整 情况A： x<<<&&  >>>; B: <<<x>&& >>; C: <<<x>>&& >
//        开头不完整  情况A： x<&& <<y>>>; B: x<<&& <y>>>; C:x<&& <<y&& y>>>; D:x<<&& <x&& x>>>

import Foundation
extension NSString {
    
    func unpackWithInCompleteString(_ incompleteString:String) -> NSMutableArray {
        
        var incompleteString = incompleteString//浅复制的话就没问题  上一轮没有匹配的值
        var regex: NSRegularExpression?//正则表达式
        var oriString: String = self as String//等待处理的数据帧
        let pattern: String = "(<<<)((?!<<<|>>>).)*(>>>)"//((?!<<<|>>>).)去除中间的
        
        let completion: NSMutableArray = NSMutableArray.init()//拆完的数据包
        
        let rangeArray: NSMutableArray = NSMutableArray.init()
        
        oriString = incompleteString.appending(oriString)//将“缓冲的断包”跟“新收到的数据”合并（注意顺序）
        do {
            regex = try NSRegularExpression.init(pattern: pattern, options: .caseInsensitive)//生成正则表达式
        } catch {
            print("regex Error")
            return []
        }
        /***检索合法字符串***/
        regex?.enumerateMatches(in: oriString, options: .reportCompletion, range: NSMakeRange(0, oriString.characters.count), using: { (match, _, stop) in
            if match==nil {
                stop.initialize(to: true)
            } else {
                let range: NSRange = (match?.range)!
                rangeArray.add(range) //储存合法的字符串地址
                let substring = (oriString as NSString).substring(with: range)//截取合法字符串
                completion.add(substring)//添加到array中储存
            }
        })
        /***删除oriString中匹配的元素***/
        //遍历完后，最后一对合法数据前的数据皆抛弃，如：xxxx<<<ssda<<<aa>>><<<aaa;<<<aaa之前的不合法数据都丢弃
        if rangeArray.count != 0 {
            let lastRange: NSRange = rangeArray.lastObject as! NSRange
            incompleteString = (oriString as NSString).substring(from: (lastRange.location+lastRange.length))
        }
        
        return [completion,incompleteString]
    }
    
}
