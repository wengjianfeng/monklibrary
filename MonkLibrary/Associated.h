//
//  Associated.h
//  MonkLibrary
//
//  Created by gree's apple on 17/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 struct category_t {
 // 类名
 const char *name;
 // 类
 classref_t cls;
 // 实例方法
 struct method_list_t *instanceMethods;
 // 类方法
 struct method_list_t *classMethods;
 // 协议
 struct protocol_list_t *protocols;
 // 属性
 struct property_list_t *instanceProperties;
 };
 类别没有ivar类型，所以不能添加属性，只能通过property属性
 **/
@interface  NSObject (Associated)
//类别是不能添加成员变量，但可以通过runtime来添加。使用@dynamic
//类别只会生成setter和getter方法声明，不能生成setter／getter实现以及下划线成员变量
@property (nonatomic, strong) id associatedObject;

//事实上可以这么写
//@property id associatedObject;
@end
