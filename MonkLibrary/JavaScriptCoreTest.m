//
//  JavaScriptCoreTest.m
//  MonkLibrary
//
//  Created by gree's apple on 4/7/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "JavaScriptCoreTest.h"

@implementation JavaScriptCoreTest
{
    UIWebView *mWebView;
}

-(void)viewDidLoad {
    mWebView = [[UIWebView alloc] initWithFrame:self.view.frame];
    mWebView.delegate = self;
    [self.view addSubview:mWebView];
    
    [self LoadHtmlFile];
    
    [super viewDidLoad];
}

- (void)JavaScriptCoreTest {
    JSContext *context = [[JSContext alloc] init];
//    JSValue *jsVal = [context evaluateScript:@"21+7"];
//    int iVal = [jsVal toInt32];
//    NSLog(@"JSValue: %@, int: %d",jsVal, iVal);
//    //OS调用JavaScript
//    [context evaluateScript:@"function add(a,b) {return a+b;} function reduce(a,b) {return a-b;}"];
//    JSValue *add = context[@"add"];
//    JSValue *sum = [add callWithArguments:@[@(10),@(21)]];
//    NSLog(@"sum: %d",[sum toInt32]);
//    
//    JSValue *reduce = context[@"reduce"];
//    JSValue *sum2 = [reduce callWithArguments:@[@(10),@(2)]];
//    NSLog(@"sum2: %d",[sum2 toInt32]);
//    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"JavaScriptCoreTest"
                                                         ofType:@"js"];
    NSString *js = [NSString stringWithContentsOfFile:filePath
                                             encoding:NSUTF8StringEncoding
                                                error:nil];
    [context evaluateScript:js];
    //采用闭包方式则需要调用callwitharguments
    JSValue *testJS = context[@"testJS"];
    NSLog(@"v %@",[testJS toString]);
    JSValue *hello = [testJS callWithArguments:nil];
    NSLog(@"hello %@",[hello toString]);
    //
    JSValue *hello1 = [context evaluateScript:@"test2()"];
    NSLog(@"hello1 %@",[hello1 toString]);
}

- (void)LoadHtmlFile {
    NSString *filepath = [[NSBundle mainBundle]pathForResource:@"JavaScriptCoreTest" ofType:@"html"];
    NSURL *url = [NSURL fileURLWithPath:filepath];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [mWebView loadRequest:request];
}

#pragma mark -WebViewDelegate
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    JSContext *context = [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    context[@"text"] = ^() {
        NSLog(@"捕获test方法");
    };
    //给testParams绑定了block，当调用时会调用block
    context[@"testParams"] = ^(id data,id secondData) {
        NSLog(@"testParams方法");
        //avoid retain cycle
        NSArray *args = [JSContext currentArguments];
        for (id obj in args) {
            NSLog(@"遍历每一个参数%@",obj);
        }
    };
}

@end




























