//
//  JavaScriptCoreTest.h
//  MonkLibrary
//
//  Created by gree's apple on 4/7/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import <UIKit/UIKit.h>
@protocol JSPersonProtocol  <JSExport>

@property (nonatomic, copy) NSDictionary *data;
-(NSString *)whatYouName;

//JSExprot协议去把objc复杂对象转换成JSValue并暴露给js对象
//- (void)useJSExprot {
//    Person *p = [[Person alloc]init];
//    self.jsContext[@"person"] = p;
//    JSValue *value = [self.jsContext evaluateScript:@"person.whatYouName()"];
//}


@end

@interface JavaScriptCoreTest : UIViewController <UIWebViewDelegate>

- (void)JavaScriptCoreTest;
- (void)LoadHtmlFile;

@end
