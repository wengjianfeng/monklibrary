//
//  LazyAllocation.m
//  MonkLibrary
//
//  Created by gree's apple on 29/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "LazyAllocation.h"

@implementation LazyAllocation
{
    char *buf[100];
}

char *GetBuf() {
    static char *buffer;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        buffer = "11";
    });
    return buffer;
}

//JSPatch


@end
