//
//  ViewController.m
//  MonkLibrary
//
//  Created by gree's apple on 17/2/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "ViewController.h"
#import "HitFixTableViewCell.h"
@interface ViewController () <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *mTableView; // 设备列表

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView dataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0) {
        HitFixTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotFix"];
        if (!cell) {
            cell = [[HitFixTableViewCell alloc]init];
        }
        cell.backgroundColor = [UIColor grayColor];

        return cell;
    } else if(indexPath.row == 1){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JavaScriptCore"];
        if (!cell) {
            cell = [[UITableViewCell alloc]init];
        }
        cell.backgroundColor = [UIColor brownColor];
        
        return cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SQLite"];
        if (!cell) {
            cell = [[UITableViewCell alloc]init];
        }
        cell.backgroundColor = [UIColor purpleColor];
        
        return cell;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - segue
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[UITableViewCell class]]) {
        
    } else {
        return;
    }
}


@end
