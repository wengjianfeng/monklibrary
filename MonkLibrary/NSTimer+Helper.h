//
//  NSTimer+Helper.h
//  NewworkDemo
//
//  Created by gree's apple on 23/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
//在dealloc中释放，实际上已经造成了retaincycle
@interface  NSTimer (NSTimer_Helper)

+ (NSTimer *)scheduledTimerWithTimeInterval:(NSTimeInterval)inTimeInterval block:(void (^)())inBlock repeats:(BOOL)inRepeats;

+ (NSTimer *)timerWithTimeInterval:(NSTimeInterval)inTimeInterval block:(void (^)())inBlock repeats:(BOOL)inRepeats;

//+ (void)executeTimerBlock:(NSTimer *)inTimer;

@end
