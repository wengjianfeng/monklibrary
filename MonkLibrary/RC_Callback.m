//
//  RC_Callback.m
//  MonkLibrary
//
//  Created by gree's apple on 28/3/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import "RC_Callback.h"
//#import <ReactiveCocoa/ReactiveCocoa.h>
//#import "RACDelegateProxy.h"
#import <objc/runtime.h>

@implementation RC_Callback

- (void)bindViewModel {
    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [[button rac_signalForControlEvents:UIControlEventTouchUpInside]
//     subscribeNext:^(id sender) {
//         
//         UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"xx"
//                                                                 delegate:nil
//                                                        cancelButtonTitle:@"1"
//                                                   destructiveButtonTitle:@"2"
//                                                        otherButtonTitles:nil, nil];
//         @weakify(actionSheet);//与strong必须成对出现
//         [actionSheet.rac_buttonClickedSignal
//          subscribeNext:^(NSNumber *x) {
//              @strongify(actionSheet);
//              NSInteger index = [x integerValue];
//              NSString *title = [actionSheet buttonTitleAtIndex:index];//retain cycle
//              NSLog(@"x %@",title);
//          }];
//         
//         [actionSheet.rac_willDeallocSignal
//          subscribeNext:^(id x) {
//              NSLog(@"xxx");
//          }];
//         
//         NSLog(@"%@",button);
//     }];
//    
//    //delegateProxy (有很多个textField需要resign，将导致textFieldShouldReturn有很多if用于逻辑区分，所以就弄在这里啦)
//    RACDelegateProxy *delegateProxy = [[RACDelegateProxy alloc] initWithProtocol:@protocol(UITextFieldDelegate)];
//    [[delegateProxy rac_signalForSelector:@selector(textFieldShouldReturn:)]
//     subscribeNext:^(RACTuple *args) {//RACTuple是参数的组合，如arg1/arg2等等
//         UITextField *field = [args first];
//         [field resignFirstResponder];
//     }];
//    
//    UITextField *tf= [[UITextField alloc]init];
//    tf.delegate = (id<UITextFieldDelegate>)delegateProxy;
//    objc_setAssociatedObject(tf, @"delegateProxy", delegateProxy, OBJC_ASSOCIATION_RETAIN_NONATOMIC);//因为delegate属性是assign，不会被保存，所以需要人为keep；一：在perproty中添加此属性。二：利用runtime手动添加一个strong；
}

@end
