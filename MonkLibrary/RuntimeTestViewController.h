//
//  RuntimeTestViewController.h
//  MonkLibrary
//
//  Created by gree's apple on 15/6/16.
//  Copyright © 2016年 WJF. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RuntimeTestViewController : UIViewController
//测试属性
- (void)jsonToModelWithProperty;
//测试成员
- (void)jsonToModelWithIvar;
//测试添加方法
- (void)testAddMethod;
//测试关联属性
- (void)testAssociateDealloc;
@end
