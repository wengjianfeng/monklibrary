//
//  OfflineDetect.h
//  G-Life_NewUI
//
//  Created by gree's apple on 5/11/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OfflineDetect : NSObject

-(void)startDetectOfflineWithMac:(NSString *)mac;

@end
