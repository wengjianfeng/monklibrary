//
//  AppsGatherViewController.m
//  G-Life_NewUI
//
//  Created by gree's apple on 15/12/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "AppsGatherViewController.h"

@interface AppsGatherViewController ()

@end

@implementation AppsGatherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)appsTarget:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSString *url;
    switch (btn.tag) {
        case 1://格力官方商城
            url = @"https://itunes.apple.com/cn/app/ge-li-guan-fang-shang-cheng/id962646876?mt=8";
            break;
        case 2://格力智能家电
            url = @"https://itunes.apple.com/cn/app/ge-li-zhi-neng-jia-dian/id652018836?mt=8";
            break;
        case 3://G-Life
            url = @"https://itunes.apple.com/cn/app/g-life/id945948992?mt=8";
            break;
        case 4://格力官方商城HD
            url = @"https://itunes.apple.com/cn/app/ge-li-guan-fang-shang-chenghd/id966754651?mt=8";
            break;
        case 5://格力选型
            url = @"https://itunes.apple.com/cn/app/ge-li-xuan-xing/id908653148?mt=8";
            break;
        default:
            break;
    }
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
