//
//  ChangePasswordViewController.m
//  Remote
//
//  Created by Zhang Hanying on 14/11/14.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "AccountHelper.h"
#import "CombinedInputView.h"
#import "AccountHelper.h"
#import "AppContext.h"
#import "NotificationLists.h"

@interface ChangePasswordViewController ()
@end

@implementation ChangePasswordViewController
{
    UITextField *originalPwdTF;
    UITextField *newPwdTF;
    UITextField *confirmNewPwdTF;
    
    AccountHelper *_helper;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 标题
    self.navigationItem.title = @"修改密码";
    
    // 背景图片
    CGRect ViewBounds = [[UIScreen mainScreen] bounds];
    UIImageView *backGroundImage = [[UIImageView alloc]initWithFrame:ViewBounds];
    backGroundImage.image = [UIImage imageNamed:@"1-33"];
    [self.view addSubview:backGroundImage];
    
    // 原密码View
    CGPoint passwordViewOrigin;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        passwordViewOrigin = CGPointMake(0, 64);
    }else{
        passwordViewOrigin = CGPointMake(0, 0);
    }
    UIView *passwordView = [[CombinedInputView alloc]initWithOriginPoint:passwordViewOrigin imageName:@"1-31" textFieldPlaceHolder:@"原密码" textFieldIsSecureTextEntry:YES];
    [self.view addSubview:passwordView];
    originalPwdTF = (UITextField*)[passwordView viewWithTag:1003];
    originalPwdTF.delegate = self;
    
    // 新密码View
    CGPoint newPwdViewOrigin = CGPointMake(0, CGRectGetMaxY(passwordView.frame)+1);
    UIView *newPwdView = [[CombinedInputView alloc]initWithOriginPoint:newPwdViewOrigin imageName:@"1-31" textFieldPlaceHolder:@"新密码" textFieldIsSecureTextEntry:YES];
    [self.view addSubview:newPwdView];
    newPwdTF = (UITextField*)[newPwdView viewWithTag:1003];
    newPwdTF.delegate = self;
    
    //确认新密码View
    CGPoint confirmPwdViewOrigin = CGPointMake(0, CGRectGetMaxY(newPwdView.frame)+1);
    UIView *confirmPwdView = [[CombinedInputView alloc]initWithOriginPoint:confirmPwdViewOrigin imageName:@"1-31" textFieldPlaceHolder:@"确认新密码" textFieldIsSecureTextEntry:YES];
    [self.view addSubview:confirmPwdView];
    confirmNewPwdTF = (UITextField*)[confirmPwdView viewWithTag:1003];
    confirmNewPwdTF.delegate = self;
    
    // 修改密码按钮
    UIButton *changePwdButton = [[UIButton alloc]init];
    CGFloat changePwdButtonWidth = 270;
    CGFloat changePwdButtonHeight = 42*kScreenHeight/568;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        changePwdButton.frame = CGRectMake((kScreenWidth-changePwdButtonWidth)*0.5, 270*ViewBounds.size.height/568, changePwdButtonWidth, changePwdButtonHeight);
    }else{
        changePwdButton.frame = CGRectMake((kScreenWidth-changePwdButtonWidth)*0.5, (270-64)*ViewBounds.size.height/568, changePwdButtonWidth, changePwdButtonHeight);
    }
    [changePwdButton setBackgroundImage:[UIImage imageNamed:@"1-34"] forState:UIControlStateNormal];
    [changePwdButton setTitle:@"修改密码" forState:UIControlStateNormal];
    changePwdButton.titleLabel.textColor = [UIColor blackColor];
    changePwdButton.titleLabel.font = [UIFont systemFontOfSize:22];
    changePwdButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    changePwdButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:changePwdButton];
    [changePwdButton addTarget:self action:@selector(changePassword) forControlEvents:UIControlEventTouchUpInside];
    
    if (!([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)) {
        UILabel *TitleLabel = [[UILabel alloc]initWithFrame:CGRectMake((kScreenWidth - 120) * 0.5, 7, 120, 30)];
        TitleLabel.text = @"修改密码";
        TitleLabel.textAlignment = NSTextAlignmentCenter;
        TitleLabel.font = [UIFont boldSystemFontOfSize:20];
        TitleLabel.adjustsFontSizeToFitWidth = YES;
        TitleLabel.textColor = [UIColor colorWithRed:89.0f/255.0f green:87.0f/255.0f blue:87.0f/255.0f alpha:1.0f];
        TitleLabel.backgroundColor = [UIColor clearColor];
        self.navigationItem.titleView = TitleLabel;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([AppContext shareAppContext].connectSuccess == NO) {
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
        [self.view makeToast:@"无法连接服务器,请确保能接入互联网!" duration:10.0f position:[NSValue valueWithCGPoint:position]];
    }
}

#pragma action
- (IBAction)cancelLogin:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        nil;
    }];
}

-(void)changePassword
{
    //退出键盘，防止阻挡toast
    [self resignTextFields];
    
    if (!_helper) {
        _helper = [[AccountHelper alloc] init];
    }
    [_helper changePasswordWithOriginPassword:originalPwdTF.text newPassword:newPwdTF.text comparePassword:confirmNewPwdTF.text withBlock:^(NSString *info, NSError *error) {
        if (!error) {
            [AppContext shareAppContext].loginSuccess = NO;
            //修改成功后，需要发送账号退出帧，并转移到登陆页面重新登陆
            [[AppContext shareAppContext] clearUserInfo];
            NSLog(@"修改成功");
            CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 120/568.0*kScreenHeight);
            [self.view makeToast:@"修改成功，请重新登录" duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
            //重新刷新列表
            [[NSNotificationCenter defaultCenter] postNotificationName:Flow_becomeActive object:nil];
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        } else {
            CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 120/568.0*kScreenHeight);
            [self.view makeToast:@"修改失败" duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
        }
        _helper = nil;
    }];
}

-(void)resignTextFields
{
    [originalPwdTF resignFirstResponder];
    [newPwdTF resignFirstResponder];
    [confirmNewPwdTF resignFirstResponder];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!textField.window.isKeyWindow) {
        [textField.window makeKeyAndVisible];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}

-(void)BackToSuperView
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
