//
//  LeftMenuTableViewCell.m
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/4/2.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "ImageLabelOnlyTableViewCell.h"

@implementation ImageLabelOnlyTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
