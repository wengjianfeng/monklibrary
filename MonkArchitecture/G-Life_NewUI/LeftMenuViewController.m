//
//  LeftMenuViewController.m
//  G-Life_NewUI
//
//  Created by Hanying Zhang on 15/4/1.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "LeftMenuViewController.h"
#import "UIViewController+RESideMenu.h"
#import "ImageLabelOnlyTableViewCell.h"
#import "UIViewController+RESideMenu.h"
#import "ChangePasswordViewController.h"
#import "UIView+Toast.h"
#import "AppContext.h"
#import "VirtualServerBL.h"
#import "AccountHelper.h"

#import "GEDeviceBLRepository.h"
#import "VirtualDeviceBL.h"
#import "ChangePasswordViewController.h"
#import "NotificationLists.h"

@interface LeftMenuViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;           // 侧边栏的tableview
@property (weak, nonatomic) IBOutlet UILabel *cityNameLabel;           // 当前城市名称
@property (weak, nonatomic) IBOutlet UILabel *weatherDescriptionLabel; // 当前天气状况
@property (nonatomic,assign) BOOL weatherUpdateSuccess;                // 是否成功获取了天气信息
@property (weak, nonatomic) IBOutlet UIButton *userIconButton;         // 用户头像按钮
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;           // 用户名
@property (weak, nonatomic) IBOutlet UIButton *yahooLogoButton;
@end

@implementation LeftMenuViewController
{
    NSArray *_titles;       // tableView项的名称
    NSArray *_images;       // tableView项的图标
    
    double _currentTemp;                              // 气温
    NSString *_cityName;                              // 城市名称
    NSString *_weatherDescription;                    // 天气状况描述
    NSMutableDictionary *weatherDic;                  // 天气状况字典
    
    AccountHelper *_helper;   // 用户登出
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    NSString *verson = [NSString stringWithFormat:@"帮助V%@",[[AppContext shareAppContext] appv]];
//    _titles = @[@"备份", @"共享", verson, @"设置", @"推送历史", @"功能界面", @"退出当前账号"];
//    _images = @[@"icon_backup", @"icon_share", @"icon_help", @"icon_set", @"icon_push", @"icon_funcation", @"icon_leftMenu_quit"];
    _titles = @[@"设备界面", @"帮助", @"设置", @"客户服务", @"退出当前账号"];
    _images = @[@"icon_funcation", @"icon_help", @"icon_set", @"icon_backup", @"icon_leftMenu_quit"];
    weatherDic = [NSMutableDictionary dictionary];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView = nil;
    self.tableView.bounces = NO;
    self.tableView.scrollsToTop = NO;
    
    [self.view addSubview:self.tableView];
    
    
    self.weatherUpdateSuccess = NO;
    
    CGFloat insetLength = self.view.frame.size.width / 17.0;
    self.userIconButton.imageEdgeInsets = UIEdgeInsetsMake(insetLength, insetLength, insetLength, insetLength);

}

-(void)refreshUserName
{
    self.userNameLabel.text = [[AppContext shareAppContext] userName];
}

-(void)regainUserName
{
    [[AppContext shareAppContext] clearUserInfo];
    self.userNameLabel.text = @"未登录";
}

#pragma mark - UITableView Delegate
// 点击侧边栏切换到相应的VC
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //    _titles = @[@"备份", @"共享", verson, @"设置", @"推送历史", @"功能界面", @"退出当前账号"];
    //      @[@"设备界面", @"帮助", @"设置", @"修改密码", @"退出当前账号"];
    switch (indexPath.row) {
        case 0:
            [self switchToContentViewContorllerWithStoryBoardIdentifier:@"contentViewController" isNavigationController:YES];
            break;
//        case 1:
//            [self switchToContentViewContorllerWithStoryBoardIdentifier:@"DevicesUpdateViewController" isNavigationController:NO];
//            break;
        case 1:
            [self switchToContentViewContorllerWithStoryBoardIdentifier:@"helpViewController" isNavigationController:NO];
            break;
        case 2:
            [self switchToContentViewContorllerWithStoryBoardIdentifier:@"settingsViewController" isNavigationController:NO];
            break;
        case 3:
            //客户服务
            [self switchToContentViewContorllerWithStoryBoardIdentifier:@"customerServiceViewController" isNavigationController:NO];
            break;
        case 4:
        {
            //退出登录
            if (!_helper) {
                _helper = [[AccountHelper alloc] init];
            }
            [_helper logoutWithBlock:^(NSString *info, NSError *error) {
                if (!error) {    //登出成功
                    [self regainUserName];
                    //移除数据
                    NSMutableDictionary *dic = [[GEDeviceBLRepository shareInstance] findAllVirtualDevice];
                    for (VirtualDeviceBL *bl in [dic allValues]) {
                        //1.找到广域网的设备 2.剔除局域网设备 3.剔除16进制的设备
                        if ( (!bl._context.mWiFiNetWorkStatus.isLAN) &&
                            (bl.identify != [AppContext shareAppContext].servermac) &&
                            (bl._mType != hex)) {
                            [[GEDeviceBLRepository shareInstance] removeByMac:bl.identify];
                            NSLog(@"广域网移除设备ID : %@",bl.identify);
                        }
                        [[NSNotificationCenter defaultCenter] postNotificationName:Flow_updataSuccess object:nil];
                    }
                    NSLog(@"Logout Success");
                } else {
                    NSLog(@"Logout Error");
                }
                _helper = nil;
            }];
        }
            break;
        default:
            break;
    }
    
}

//更改主显示控制器
- (void)switchToContentViewContorllerWithStoryBoardIdentifier:(NSString*)identifier isNavigationController:(BOOL)isNavigationController
{
    if (!isNavigationController) { // 如果不是navigationController，先添加到navigationController中
        UINavigationController *naviVC = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:identifier]];
        naviVC.navigationBar.tintColor = [UIColor whiteColor];
        [self.sideMenuViewController setContentViewController:naviVC animated:YES];
    }else{ // 如果是navigationController，就直接设置
        [self.sideMenuViewController setContentViewController:[self.storyboard instantiateViewControllerWithIdentifier:identifier] animated:YES];
    }
    // 隐藏侧边栏
    [self.sideMenuViewController hideMenuViewController];
}

#pragma mark - UITableView Datasource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex {
    return _titles.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1.0/MAXFLOAT;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"leftMenuTableViewCell";
    ImageLabelOnlyTableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.backgroundColor = [UIColor clearColor];
    cell.cellLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18];
    cell.cellLabel.textColor = [UIColor whiteColor];
    cell.cellLabel.highlightedTextColor = [UIColor lightGrayColor];
    cell.cellLabel.text = _titles[indexPath.row];
    cell.cellImageView.image = [UIImage imageNamed:_images[indexPath.row]];
    cell.cellImageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}
- (IBAction)yahooLogoClicked {
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"https://www.yahoo.com/?ilc=401"]];
}

@end
