//
//  ActivateBySMSViewController.h
//  Remote
//
//  Created by Zhang Hanying on 14/10/21.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivateBySMSViewController : UIViewController <UIAlertViewDelegate,UITextFieldDelegate>

@property (nonatomic,assign) int secondsLeft;

-(void)updateCounter:(NSTimer *)theTimer;

@end
