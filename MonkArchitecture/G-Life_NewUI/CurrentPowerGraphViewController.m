//
//  CurrentPowerGraphViewController.m
//  SmartSocket
//
//  Created by workMac on 15/3/5.
//  Copyright (c) 2015年 Gree Inc. All rights reserved.
//

#import "CurrentPowerGraphViewController.h"
#import "CurrentPowerGraph.h"

@interface CurrentPowerGraphViewController ()

@property (weak, nonatomic) IBOutlet CurrentPowerGraph *baseView;
@property (weak, nonatomic) IBOutlet UILabel *currentPowerLabel;

@end

@implementation CurrentPowerGraphViewController
{
    NSTimer *timer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self startPowerAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)startPowerAnimating
{
    if (timer) {
        [timer invalidate];
    }
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(powerAnimate) userInfo:nil repeats:YES];
    [timer fire];
}

-(void)powerAnimate
{
    //更新实时功率数组
    [self.baseView.powers removeObjectAtIndex:0];
    double power = arc4random() % 85;
    power /= 10.0;
    [self.baseView.powers addObject:[NSNumber numberWithDouble:power]];
    //更新横轴时间数组
    [self.baseView.horizontalLabelTexts removeObjectAtIndex:0];
    [self.baseView.horizontalLabelTexts addObject:[self getCurrentTime]];
    //更新功率图标显示
    [self.baseView setNeedsDisplay];
    //更新label显示
    self.currentPowerLabel.text = [[NSString alloc]initWithFormat:@"实时功率：%.1fW",power];
}

//获取当前时间
-(NSString*)getCurrentTime
{
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *now=[NSDate date];
    NSInteger unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger hour = [comps hour];
    NSInteger min = [comps minute];
    NSInteger second = [comps second];
    return [[NSString alloc]initWithFormat:@"%02ld:%02ld:%02ld",(long)hour,(long)min,(long)second];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
