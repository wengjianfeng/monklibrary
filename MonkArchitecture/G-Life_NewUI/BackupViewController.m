//
//  BackupViewController.m
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/4/8.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "BackupViewController.h"
#import "ImageLabelOnlyTableViewCell.h"

@interface BackupViewController ()

@end

@implementation BackupViewController {
    NSArray *_imageNames;
    NSArray *_titles;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _imageNames = [[NSArray alloc]initWithObjects:@"backup_cloud",@"backup_list", nil];
    _titles = [[NSArray alloc]initWithObjects:@"备份到云端",@"云端备份列表", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImageLabelOnlyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"leftMenuTableViewCell"];
    if (!cell) {
        cell = [[ImageLabelOnlyTableViewCell alloc]init];
    }
    
    cell.cellImageView.image = [UIImage imageNamed:_imageNames[indexPath.section]];
    cell.cellImageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.cellLabel.text = _titles[indexPath.section];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return CGFLOAT_MIN;
    }else {
        return 10;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
