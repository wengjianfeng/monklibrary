//
//  ShareViewController.m
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/4/8.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "ShareViewController.h"
#import "ImageLabelOnlyTableViewCell.h"

@interface ShareViewController ()

@end

@implementation ShareViewController{
    NSArray *_imageNames;
    NSArray *_titles;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _imageNames = [[NSArray alloc]initWithObjects:@"share_out",@"share_in",@"share_cloud", nil];
    _titles = [[NSArray alloc]initWithObjects:@"本地分享",@"本地导入",@"云分享", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }else{
        return 1;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ImageLabelOnlyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"leftMenuTableViewCell"];
    if (!cell) {
        cell = [[ImageLabelOnlyTableViewCell alloc]init];
    }
    if (indexPath.section == 0) {
        cell.cellImageView.image = [UIImage imageNamed:_imageNames[indexPath.row]];
        cell.cellLabel.text = _titles[indexPath.row];
    }else{
        cell.cellImageView.image = [UIImage imageNamed:_imageNames[indexPath.section + 1]];
        cell.cellLabel.text = _titles[indexPath.section + 1];
    }
    cell.cellImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return CGFLOAT_MIN;
    }else {
        return 10;
    }
}

@end
