//
//  OverridesPickerView.h
//  OverridesPickerView
//
//  Created by 翁 健峰 on 13-6-18.
//  Copyright (c) 2013年 翁 健峰. All rights reserved.
//
//此处代码有待优化，项目节点关系，先用此方法顶着。。。
#import <UIKit/UIKit.h>

@class OverridesPickerView;

@protocol OverridesPickerDelegate <NSObject>
@optional
- (NSInteger)numberOfComponentsInPickerView:(OverridesPickerView *)pickerView;
- (NSInteger)pickerView:(OverridesPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
- (NSString *)pickerView:(OverridesPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
- (void)pickerView:(OverridesPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component;
- (CGFloat)pickerView:(OverridesPickerView *)pickerView widthForComponent:(NSInteger)component;
- (CGFloat)pickerView:(OverridesPickerView *)pickerView rowHeightForComponent:(NSInteger)component;

@end

@interface OverridesPickerView : UIView<UIScrollViewDelegate>
{
    //视图对应滑动完成数据
    NSInteger SubScrollValueOne;
    NSInteger SubScrollValueTwo;
    NSInteger SubScrollValueThree;
    
    //Type 0为普通选取器，1是温度选取器
    NSInteger PickerType;
        
    id<OverridesPickerDelegate>OverridesDelegate;
}
@property (strong,nonatomic) id<OverridesPickerDelegate>OverridesDelegate;

@property (nonatomic) NSInteger SubScrollValueOne;
@property (nonatomic) NSInteger SubScrollValueTwo;
@property (nonatomic) NSInteger SubScrollValueThree;

@property (nonatomic) NSInteger PickerType;

- (id)initWithFrame:(CGRect)frame WithDelegate:mDelegate AndType:(NSInteger)Type;
- (void)setComponentImageView:(UIImageView *)ComponentImageView WithRow:(NSInteger)Row;
- (void)reloadAllComponents;
- (void)reloadComponent:(NSInteger)component;
- (void)selectRow:(NSInteger)row inComponent:(NSInteger)component animated:(BOOL)animated;  // scrolls the specified row to center.
- (NSInteger)selectedRowInComponent:(NSInteger)component;                                   // returns selected row. -1 if nothing selected
- (NSInteger)numberOfRowsInComponent:(NSInteger)component;


@end
