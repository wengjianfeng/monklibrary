 //
//  main.m
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/3/23.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
