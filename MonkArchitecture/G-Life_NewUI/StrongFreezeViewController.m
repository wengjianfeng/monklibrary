//
//  StrongFreezeViewController.m
//  TableViewRowAnimationDemo
//
//  Created by workMac on 15/10/26.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "StrongFreezeViewController.h"

@interface StrongFreezeViewController ()

@end

@implementation StrongFreezeViewController
{
    NSMutableArray *_btns;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"瞬冷冻室";
    _btns = [NSMutableArray array];

    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            [_btns addObject:subView];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//瞬冷冻室状态切换
- (IBAction)changeState:(UIButton *)sender {
    if (sender.selected == NO) {
        sender.selected = YES;
    }
    for (UIButton *btn in _btns) {
        if (btn != sender) {
            btn.selected = NO;
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
