//
//  RootViewController.h
//  G-Life_NewUI
//
//  Created by Hanying Zhang on 15/4/1.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "RESideMenu.h"

@interface RootViewController : RESideMenu <RESideMenuDelegate>

@end
