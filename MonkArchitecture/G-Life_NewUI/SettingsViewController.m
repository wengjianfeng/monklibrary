//
//  SettingsViewController.m
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/4/8.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "SettingsViewController.h"
#import "LabelSwitchTableViewCell.h"
#import "SecurityViewController.h"
@interface SettingsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *settingsTableView;
@end

@implementation SettingsViewController{
    NSArray *_section0Titles;
    NSArray *_section1Titles;
    NSArray *_section2Titles;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /* section
     账号与安全 >  用户名 >
                  邮箱  >
                 手机号 >
                 
                 密码   >  修改密码 >
                          忘记密码 >
                 修改绑定手机 >
                 修改绑定邮箱 >
     
     通用      >  多语言   >
                 换肤     >
     
     关于      > 评分   >
                反馈   >
                相关应用 >
                新产品  >
                产品选购 >
     */
    
    _section0Titles = [[NSArray alloc]initWithObjects:@"账号与安全", nil];
    _section1Titles = [[NSArray alloc]initWithObjects:@"通用", nil];//暂时屏蔽
    _section2Titles = [[NSArray alloc]initWithObjects:@"关于", nil];
    
    self.settingsTableView.delegate = self;
    self.settingsTableView.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - TableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [_section0Titles count];
    } else {
        return [_section2Titles count];
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"defaultCell"];

    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"defaultCell"];
    }
    
    if (indexPath.section == 0) {
        cell.textLabel.text = _section0Titles[indexPath.row];
    } else {
        cell.textLabel.text = _section2Titles[indexPath.row];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        [self showViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SecurityView"] sender:self];
    } else {
        [self showViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"applicationInfoViewController"] sender:self];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return CGFLOAT_MIN;
    }else {
        return 10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return 10;
    }else {
        return CGFLOAT_MIN;
    }
}

@end
