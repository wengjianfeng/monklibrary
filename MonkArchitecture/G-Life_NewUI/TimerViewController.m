//
//  TimerViewController.m
//  SmartSocket
//
//  Created by workMac on 15/3/9.
//  Copyright (c) 2015年 Gree Inc. All rights reserved.
//

#import "TimerViewController.h"

@interface TimerViewController ()
@property (weak, nonatomic) IBOutlet UIButton *timerTypeOn;
@property (weak, nonatomic) IBOutlet UIButton *timerTypeOff;
@property (weak, nonatomic) IBOutlet UIButton *timerRepeatMonBtn;
@property (weak, nonatomic) IBOutlet UIButton *timerRepeatTueBtn;
@property (weak, nonatomic) IBOutlet UIButton *timerRepeatWedBtn;
@property (weak, nonatomic) IBOutlet UIButton *timerRepeatThuBtn;
@property (weak, nonatomic) IBOutlet UIButton *timerRepeatFriBtn;
@property (weak, nonatomic) IBOutlet UIButton *timerRepeatSatBtn;
@property (weak, nonatomic) IBOutlet UIButton *timerRepeatSunBtn;
@end

@implementation TimerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.timerTypeOff.selected = YES;
    self.timerTypeOn.selected = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)timerRepeatBtnPressed:(UIButton *)sender {
    sender.selected = ! sender.selected;
}

- (IBAction)timerTypeBtnPressed:(UIButton *)sender {
    if (sender.selected == NO) {
        self.timerTypeOn.selected = ! self.timerTypeOn.selected;
        self.timerTypeOff.selected = ! self.timerTypeOff.selected;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
