//
//  FeedBackViewController.swift
//  G-Life_NewUI
//
//  Created by workMac on 15/12/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

import UIKit

@objc(FeedBackViewController)
class FeedBackViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var feedbackCategorySegmentControl: UISegmentedControl!
    
    var identify = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        //设置textview圆角
        textView.layer.cornerRadius = 5.0
        textView.layer.masksToBounds = true
        //
        identify = AppContext.shareAppContext().uiIdentify;
        //设置segmentControl默认选中“其他”
        feedbackCategorySegmentControl.selectedSegmentIndex = 4
    }

    override func viewDidAppear(animated: Bool) {
        textView.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //发送
    @IBAction func sendFeedBack() {
        let bl = GEDeviceBLRepository.shareInstance().findVirtualDeviceByMac(AppContext.shareAppContext().servermac) as! VirtualServerBL
        
        let dicToSend:NSMutableDictionary = ["feedbackType":feedbackCategorySegmentControl.selectedSegmentIndex, "feedbackInfo":textView.text]
        bl.stuffFormatMutableDictionary(dicToSend)
        bl.excuteWithTcpCmd(KJsonUserFeedbackSubmitFC)
    }
    //点击屏幕其他地方取消键盘
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if textView.isFirstResponder() {
            textView.resignFirstResponder()
        }
    }
}
