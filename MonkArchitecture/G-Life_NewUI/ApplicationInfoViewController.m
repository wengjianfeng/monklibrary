//
//  ApplicationInfoViewController.m
//  G-Life_NewUI
//
//  Created by gree's apple on 14/12/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "ApplicationInfoViewController.h"
#import "AppContext.h"
#import "G_Life_NewUI-Swift.h"
#import "GreeMallWebViewController.h"

@interface ApplicationInfoViewController () <UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic)IBOutlet UITableView *tableView;
@property (weak, nonatomic)IBOutlet UIView *headerView;
@property (weak, nonatomic)IBOutlet UIView *footerView;
@property (weak, nonatomic)IBOutlet UILabel *versionLabel;
@end

@implementation ApplicationInfoViewController
{
    NSArray *_titleArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _titleArray = [[NSArray alloc] initWithObjects:@"去评分(上线才有此功能)",@"反馈",@"相关应用",@"格力商城", nil];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _versionLabel.text = [[NSString alloc] initWithFormat:@"格力智能家电 G-Life %@",[AppContext shareAppContext].appv];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _footerView.frame = CGRectMake(0, _tableView.frame.size.height - _footerView.frame.size.height, _footerView.frame.size.width, _footerView.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - tableView DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_titleArray count];
}

#pragma mark - tableview Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = _titleArray[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
        {
#warning 上线时要改为新的
            NSString *str = [NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=xxxxxx" ];
            if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0)){
                str = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/idxxxxxxx"];
            }
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
        }
            break;
        case 1://反馈
        {
            FeedBackViewController *feedbackVC = [[FeedBackViewController alloc]initWithNibName:@"FeedBackViewController" bundle:[NSBundle mainBundle]];
            [self.navigationController showViewController:feedbackVC sender:nil];
        }
            break;
        case 2://相关应用
        {
            [self showViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"appsGatherViewController"] sender:self];
        }
            break;
        case 3://格力商城
        {
            GreeMallWebViewController *web = [[GreeMallWebViewController alloc] init];
            [self showViewController:web sender:self];
        }
            break;
        default:
            break;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return _headerView;
}

@end
