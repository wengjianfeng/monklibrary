//
//  NetWorkDetailViewController.h
//  Remote
//
//  Created by 翁 健峰 on 13-4-26.
//  Copyright (c) 2013年 WJF-Monk  330694. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "OverridesPickerView.h"

@interface NetWorkDetailViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UIAlertViewDelegate,OverridesPickerDelegate>
{
    UITableView *mTableView;
    UIPickerView *mPickerView;
    
    NSDictionary *mResourceData;    //外部传入数据
    
    NSMutableArray *mDataSource;    //数据
    NSMutableArray *mPicker_Data;   //拾取器数据

    NSMutableArray *mNetWork_ApType;  //AP网络类型
    NSMutableArray *mNetWork_ClientType;    //client网络类型
    NSMutableArray *mNetWork_Mode;  //安全模式
    NSMutableArray *mTextSource;    //文本框
    
    UITextField *mTextField;
    
    NSUInteger currentTag;
    
    BOOL isEditing;
    BOOL ShowPicker;
}

@property (nonatomic, strong) NSDictionary *mResourceData;    //外部传入数据
@property (nonatomic, strong) NSDictionary *statusDictionary;   //设备所有信息

@end
