//
//  UIImage+ImageWithColor.h
//  RoomEdit
//
//  Created by apple on 14-7-31.
//  Copyright (c) 2014年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageWithColor)

+(UIImage*)imagewithColor:(UIColor*)color;

@end
