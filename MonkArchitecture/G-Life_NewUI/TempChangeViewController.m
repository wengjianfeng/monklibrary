//
//  TempChangeViewController.m
//  TableViewRowAnimationDemo
//
//  Created by workMac on 15/10/26.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "TempChangeViewController.h"

#import "AppContext.h"
//冰箱相关head
#import "ResourceCenter.h"
#import "GERFDeviceInfoBL.h"
#import "GERFFeedDAO.h"
#import "GERFFeed.h"
#import "GERFErrorFeed.h"
#import "GERFErrorLists.h"
#import "VirtualDeviceBL.h"
#import "ControlJsonFormatLists.h"
@interface TempChangeViewController ()

@property (nonatomic, weak) IBOutlet UILabel *TemperatureLabel;

@end

@implementation TempChangeViewController
{
    NSMutableArray *_btns;
    
    NSString *_identify;
    
    ResourceCenter *_resource;
    NSDictionary *deviceInfoDictionary;//设备所有状态
    GERFFeed *_rfFeed;
    VirtualDeviceBL *_bl;
    
    int step;//温度步进
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"变温室";
    
    _btns = [NSMutableArray array];
    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            [_btns addObject:subView];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _identify = [[AppContext shareAppContext] uiIdentify];
    [self refreshView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IB
//变温室状态切换
- (IBAction)changeStateTo:(UIButton *)sender {
        sender.selected = !sender.selected;

    for (UIButton *btn in _btns) {
        if (btn != sender) {
            btn.selected = NO;
        }
    }
    
    NSString *cmd = nil;
    if (sender.tag == 100) {
        cmd = @"Fresh";
    } else if (sender.tag == 101) {
        cmd = @"SoftFrz";
    } else if (sender.tag == 102) {
        cmd = @"mLossLessFreez";
    } else if (sender.tag == 103) {
        cmd = @"mDeodorize";//??
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",sender.selected],cmd, nil];
    [_bl stuffFormatMutableDictionary:dic];
    [_bl excuteWithTcpCmd:KJsonDevicesControlsFC];
}

-(IBAction)TemperatureReduce:(id)sender
{
    //step 0~12
    if (step < 0) {
        step = 0;
        return;
    }
    
    if (step != 0) {
        step -= 1;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%x",step],@"ChgRmSG", nil];
    [_bl stuffFormatMutableDictionary:dic];
    [_bl excuteWithTcpCmd:KJsonDevicesControlsFC];
    
    
    self.TemperatureLabel.text = [_rfFeed.tempRmMap objectAtIndex:step];
}

-(IBAction)TemperaturePlus:(id)sender
{    
    if (step >= 12) {
        return;
    }
    if (step != 12) {
        step += 1;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%x",step],@"ChgRmSG", nil];
    [_bl stuffFormatMutableDictionary:dic];
    [_bl excuteWithTcpCmd:KJsonDevicesControlsFC];
    
    
    self.TemperatureLabel.text = [_rfFeed.tempRmMap objectAtIndex:step];
}

#pragma mark refreshView
- (void)refreshView
{
    _resource = [[ResourceCenter alloc] init];
    deviceInfoDictionary = [_resource resourceFromIdentify:_identify];
    
    _rfFeed = deviceInfoDictionary[@"GERFFeed"];
    _bl = deviceInfoDictionary[@"VirtualDeviceBL"];
    
    step = [_rfFeed mTempRmSetTemp];
    self.TemperatureLabel.text = [_rfFeed.tempRmMap objectAtIndex:step];

}

@end
