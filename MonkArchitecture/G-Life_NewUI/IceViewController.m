//
//  IceViewController.m
//  TableViewRowAnimationDemo
//
//  Created by workMac on 15/10/26.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "IceViewController.h"

#import "AppContext.h"
//冰箱相关head
#import "ResourceCenter.h"
#import "GERFDeviceInfoBL.h"
#import "GERFFeedDAO.h"
#import "GERFFeed.h"
#import "GERFErrorFeed.h"
#import "GERFErrorLists.h"
#import "VirtualDeviceBL.h"
#import "ControlJsonFormatLists.h"

@interface IceViewController ()
{
    NSString *_identify;

    ResourceCenter *_resource;
    NSDictionary *deviceInfoDictionary;//设备所有状态
    GERFFeed *_rfFeed;
    VirtualDeviceBL *_bl;
}
@end

@implementation IceViewController
{
    NSMutableArray *_btns;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"制冰室";
    
    _btns = [NSMutableArray array];
    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            [_btns addObject:subView];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _identify = [[AppContext shareAppContext] uiIdentify];
    [self refreshView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//制冰状态切换
- (IBAction)changeState:(UIButton *)sender {
    if (sender.selected == NO) {
        sender.selected = YES;
        for (UIButton *btn in _btns) {
            if (btn != sender) {
                btn.selected = NO;
            }
        }
        if (sender.tag == 100) {
            
        } else if (sender.tag == 101) {
            
        } else if (sender.tag == 102) {
            
        }
        //标准？快速？停止？
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",sender.selected],@"IceMake", nil];
        [_bl stuffFormatMutableDictionary:dic];
        [_bl excuteWithTcpCmd:KJsonDevicesControlsFC];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark refreshView
- (void)refreshView
{
    _resource = [[ResourceCenter alloc] init];
    deviceInfoDictionary = [_resource resourceFromIdentify:_identify];
    
    _rfFeed = deviceInfoDictionary[@"GERFFeed"];
    _bl = deviceInfoDictionary[@"VirtualDeviceBL"];
}

@end
