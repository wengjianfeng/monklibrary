//
//  LabelSwitchTableViewCell.h
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/4/9.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LabelSwitchTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@property (weak, nonatomic) IBOutlet UISwitch *cellSwitch;


@end
