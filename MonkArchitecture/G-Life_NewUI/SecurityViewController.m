//
//  SecurityViewController.m
//  G-Life_NewUI
//
//  Created by gree's apple on 10/12/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "SecurityViewController.h"
#import "SecurityInfoTableViewCell.h"
#import "AppContext.h"

@interface SecurityViewController () <UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *mTableView;

@end

@implementation SecurityViewController
{
    NSArray *_info;
    NSArray *_psw;
    NSArray *_changBind;
    
    NSArray *_userInfo;//用户信息
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _mTableView.delegate = self;
    _mTableView.dataSource = self;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([AppContext shareAppContext].connectSuccess == NO) {
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
        [self.view makeToast:@"无法连接服务器,请确保能接入互联网!" duration:10.0f position:[NSValue valueWithCGPoint:position]];
    }
    
    _info = [NSArray arrayWithObjects:@"用户名:",@"手机号:",@"邮箱:", nil];
    _psw = [NSArray arrayWithObjects:@"修改密码",@"忘记密码", nil];
    //暂时屏蔽
    //    _changBind = [NSArray arrayWithObjects:@"修改绑定手机",@"修改绑定邮箱",@"绑定手机",@"绑定邮箱", nil];
    
    AppContext *app = [AppContext shareAppContext];
    //隐藏部分
    NSString *secureTele = app.userTele;
    if (secureTele.length >= 8) {
        NSString *t = [secureTele substringWithRange:NSMakeRange(4, 4)];
        secureTele = [secureTele stringByReplacingOccurrencesOfString:t withString:@"****"];
    }
    
    
    NSString *secureMail = app.userMail;
    if (secureMail.length != 0) {
        NSRange range = [secureMail rangeOfString:@"@"];
        NSString *m = [secureMail substringWithRange:NSMakeRange(range.location-2, 2)];
        secureMail = [secureMail stringByReplacingOccurrencesOfString:m withString:@"**"];
    }
    
    _userInfo = [NSArray arrayWithObjects:app.userName,secureTele,secureMail, nil];

    [_mTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return [_info count];
    } else if (section == 1) {
        return [_psw count];
    } else {
        return [_changBind count];
    }
}

#pragma mark - tableView Delegate
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        //用户信息
        SecurityInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"securityCell"];
        if (!cell) {
            cell = [[SecurityInfoTableViewCell alloc] init];
        }
        cell.title.text = _info[indexPath.row];
        cell.info.text = _userInfo[indexPath.row];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    } else if (indexPath.section == 1){
        //密码修改
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[UITableViewCell alloc] init];
        }
        cell.textLabel.text = _psw[indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
    } else {
        //修改绑定
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (!cell) {
            cell = [[UITableViewCell alloc] init];
        }
        cell.textLabel.text = _changBind[indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            //忘记密码
            [self.navigationController showViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"changePasswordView"] sender:self];
        } else {
            //修改密码
            [self.navigationController showViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"ForgetPwdView"] sender:self];
        }
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            //修改绑定手机
        } else if (indexPath.row == 1) {
            //修改绑定邮箱
        } else if (indexPath.row == 2) {
            //绑定手机
        } else if (indexPath.row == 3) {
            //绑定邮箱
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}








@end
