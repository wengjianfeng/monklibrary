//
//  RootViewController.m
//  G-Life_NewUI
//
//  Created by Hanying Zhang on 15/4/1.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "RootViewController.h"
#import "LeftMenuViewController.h"

#define kWeatherUpdateTimeInterval 3600

@interface RootViewController ()
@end

@implementation RootViewController
{
    NSDate *_lastWeatherUpdateTime;   // 最近一次更新天气的时间
}

- (void)awakeFromNib
{
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
    
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.backgroundImage = [UIImage imageNamed:@"left_menu_bg_2"];
    
    self.delegate = self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _lastWeatherUpdateTime = nil;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //如果不这样处理，进入登陆页登录完成后无法刷新
    LeftMenuViewController *leftMenuController = (LeftMenuViewController*)self.leftMenuViewController;
    //刷新登录用户名
    [leftMenuController refreshUserName];
}

// 获取自上次更新天气之后的时间间隔
- (NSTimeInterval)timeIntervalSinceLastUpdate
{
    NSDate *now = [NSDate date];

    if (!_lastWeatherUpdateTime) {
        return 0;
    }else{
        NSTimeInterval interval = [now timeIntervalSinceDate:_lastWeatherUpdateTime];
        return interval;
    }
}

#pragma mark - RESideMenu Delegate
- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController {
//    NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
    LeftMenuViewController *leftMenuController = (LeftMenuViewController*)menuViewController;
    //刷新登录用户名
    [leftMenuController refreshUserName];
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController {
//    NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
    
    //刷新天气状况
    NSTimeInterval timeSinceLastWeatherUpdate = [self timeIntervalSinceLastUpdate];
    LeftMenuViewController *leftMenuController = (LeftMenuViewController*)menuViewController;
    // 如果时间间隔为0或者已经超过了预定值，就更新天气
    if (timeSinceLastWeatherUpdate == 0 || timeSinceLastWeatherUpdate >= kWeatherUpdateTimeInterval || [leftMenuController weathGetWeatherInformation] == NO) {
        [leftMenuController refreshWeather];
        // 更新天气就更新时间
        _lastWeatherUpdateTime = [NSDate date];
    }
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController {
//    NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController {
//    NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

@end
