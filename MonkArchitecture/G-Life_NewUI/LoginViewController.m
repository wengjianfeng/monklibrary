//
//  LoginViewController.m
//  G-Life_NewUI
//
//  Created by workMac on 15/5/12.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginHelper.h"
#import "AppContext.h"
#import "VirtualServerBL.h"
#import "GEDeviceBLRepository.h"
#import "ActivateBySMSViewController.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (strong, nonatomic) NSString *name;
@property (nonatomic) LoginHelper *helper;
@end

@implementation LoginViewController
#pragma mark - lifeCycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.userNameTextField.delegate = self;
    self.pwdTextField.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([AppContext shareAppContext].connectSuccess == NO) {
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
        [self.view makeToast:@"无法连接服务器,请确保能接入互联网!" duration:10.0f position:[NSValue valueWithCGPoint:position]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
}

#pragma action
- (IBAction)cancelLogin:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        nil;
    }];
}

- (IBAction)login:(id)sender{
    [self loginAction];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - loginAction
- (void)loginAction
{
    if (!self.helper) {
        self.helper = [[LoginHelper alloc] init];
    }
    self.name = self.userNameTextField.text;
    [self.helper loginVerifyHelperName:self.userNameTextField.text
                         password:self.pwdTextField.text
                        withblock:^(NSString *info, NSError *error) {
                            if (error) {
                                if ([info isEqualToString:@"ActivateBySMS"]) {
                                        //激活
                                        ActivateBySMSViewController *vc = [[ActivateBySMSViewController alloc]init];
                                        [self showViewController:vc sender:self];
                                }
                                //提示错误
                                CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
                                [self.view makeToast:info duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
                                
                                NSLog(@"Login Info: %@",info);
                            } else {
                                VirtualServerBL *bl = (VirtualServerBL *)[[GEDeviceBLRepository shareInstance]
                                                                          findVirtualDeviceByMac:[[AppContext shareAppContext] servermac]];
                                [bl statusRequest];
                                
                                CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
                                [self.view makeToast:info duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
                                //关闭界面
                                [self cancelLogin:nil];
                            }
                            
                            self.helper = nil;
    }];
    
}

#pragma mark - textField resign
- (void)textfieldResign
{
    [self.userNameTextField resignFirstResponder];
    [self.pwdTextField resignFirstResponder];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self textfieldResign];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
