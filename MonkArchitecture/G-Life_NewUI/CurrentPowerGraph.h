//
//  CurrentPowerGraph.h
//  SmartSocket
//
//  Created by workMac on 15/3/3.
//  Copyright (c) 2015年 Gree Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CurrentPowerGraph : UIView

@property (nonatomic,strong) NSMutableArray *powers;
@property (nonatomic,strong) NSMutableArray *horizontalLabelTexts;

@end
