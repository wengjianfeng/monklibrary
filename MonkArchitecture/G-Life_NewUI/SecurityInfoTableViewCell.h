//
//  SecurityInfoTableViewCell.h
//  G-Life_NewUI
//
//  Created by gree's apple on 10/12/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecurityInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *info;

@end
