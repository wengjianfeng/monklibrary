//
//  FreezeViewController.m
//  TableViewRowAnimationDemo
//
//  Created by workMac on 15/10/26.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "FreezeViewController.h"

#import "AppContext.h"
//冰箱相关head
#import "ResourceCenter.h"
#import "GERFDeviceInfoBL.h"
#import "GERFFeedDAO.h"
#import "GERFFeed.h"
#import "GERFErrorFeed.h"
#import "GERFErrorLists.h"
#import "VirtualDeviceBL.h"
#import "ControlJsonFormatLists.h"

@interface FreezeViewController ()
{
    NSString *_identify;

    ResourceCenter *_resource;
    NSDictionary *deviceInfoDictionary;//设备所有状态
    GERFFeed *_rfFeed;
    VirtualDeviceBL *_bl;
    
    int step;//温度步进
}
@property (nonatomic, weak) IBOutlet UILabel *TemperatureLabel;
@end

@implementation FreezeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"冷冻室";
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    _identify = [[AppContext shareAppContext] uiIdentify];
    [self refreshView];
}

#pragma mark - IB
-(IBAction)TemperatureReduce:(id)sender
{
    //step 1~9 -26°C只能通过模式键开启
    if (step < 1) {
        step = 1;
        return;
    }
    
    if (step != 1) {
        step -= 1;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%x",step],@"FrzRmSG", nil];
    [_bl stuffFormatMutableDictionary:dic];
    [_bl excuteWithTcpCmd:KJsonDevicesControlsFC];

    self.TemperatureLabel.text = [_rfFeed.freezMap objectAtIndex:step];
}

-(IBAction)TemperaturePlus:(id)sender
{
    if (step >= 9) {
        step = 9;
        return;
    }
    if (step != 9) {
        step += 1;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%x",step],@"FrzRmSG", nil];
    [_bl stuffFormatMutableDictionary:dic];
    [_bl excuteWithTcpCmd:KJsonDevicesControlsFC];
    
    self.TemperatureLabel.text = [_rfFeed.freezMap objectAtIndex:step];
}

-(void)FreezeButton:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",btn.selected],@"FstFrzRunStat", nil];
    [_bl stuffFormatMutableDictionary:dic];
    [_bl excuteWithTcpCmd:KJsonDevicesControlsFC];
}

#pragma mark refreshView
- (void)refreshView
{
    _resource = [[ResourceCenter alloc] init];
    deviceInfoDictionary = [_resource resourceFromIdentify:_identify];
    
    _rfFeed = deviceInfoDictionary[@"GERFFeed"];
    _bl = deviceInfoDictionary[@"VirtualDeviceBL"];
    
    step = [_rfFeed mFreezRmSetTemp];
    self.TemperatureLabel.text = [_rfFeed.freezMap objectAtIndex:step];
}

@end
