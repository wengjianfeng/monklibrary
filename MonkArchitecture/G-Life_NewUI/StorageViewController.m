//
//  StorageViewController.m
//  TableViewRowAnimationDemo
//
//  Created by workMac on 15/10/26.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "StorageViewController.h"

#import "AppContext.h"
//冰箱相关head
#import "ResourceCenter.h"
#import "GERFDeviceInfoBL.h"
#import "GERFFeedDAO.h"
#import "GERFFeed.h"
#import "GERFErrorFeed.h"
#import "GERFPropertyLists.h"
#import "GERFErrorLists.h"
#import "VirtualDeviceBL.h"
#import "ControlJsonFormatLists.h"

#import "NotificationLists.h"

@interface StorageViewController ()
{
    NSString *_identify;
    ResourceCenter *_resource;
    NSDictionary *deviceInfoDictionary;//设备所有状态
    GERFFeed *_rfFeed;
    VirtualDeviceBL *_bl;
    
    int step;//温度步进
}

@property (nonatomic, weak) IBOutlet UILabel *TemperatureLabel;
@property (nonatomic, weak) IBOutlet UIButton *HolidayButton;
@end

@implementation StorageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"冷藏室";
    
    _identify = [[AppContext shareAppContext] uiIdentify];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView) name:Flow_updataSuccess object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshView];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:Flow_updataSuccess object:nil];
}

//Holiday功能
-(void)HolidayButton:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    btn.selected = !btn.selected;
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%d",btn.selected],@"Holiday", nil];
    [_bl stuffFormatMutableDictionary:dic];
    [_bl excuteWithTcpCmd:KJsonDevicesControlsFC];
}

-(void)TemperatureReduce:(id)sender
{
    //step 0~6 14°C只能通过假日按钮点出
    if (step < 0) {
        step = 0;
        return;
    }
    
    if (step != 0) {
        step -= 1;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%x",step],@"CldRmSG", nil];
    [_bl stuffFormatMutableDictionary:dic];
    [_bl excuteWithTcpCmd:KJsonDevicesControlsFC];
    
    self.TemperatureLabel.text = [_rfFeed.coldMap objectAtIndex:step];
}

-(void)TemperaturePlus:(id)sender
{
    if (step >= 6) {
        return;
    }
    if (step != 6) {
        step += 1;
    }
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%x",step],@"CldRmSG", nil];
    [_bl stuffFormatMutableDictionary:dic];
    [_bl excuteWithTcpCmd:KJsonDevicesControlsFC];

    self.TemperatureLabel.text = [_rfFeed.coldMap objectAtIndex:step];
}

#pragma mark refreshView
- (void)refreshView
{
    _resource = [[ResourceCenter alloc] init];
    deviceInfoDictionary = [_resource resourceFromIdentify:_identify];
    
    _rfFeed = deviceInfoDictionary[@"GERFFeed"];
    _bl = deviceInfoDictionary[@"VirtualDeviceBL"];
    
    //状态stuff
    step = [_rfFeed mColdRmSetTemp];
    _HolidayButton.selected = [_rfFeed mHoliday];
    self.TemperatureLabel.text = [_rfFeed.coldMap objectAtIndex:step];
}
@end
