//
//  HelpViewController.m
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/4/8.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "HelpViewController.h"
#import "FAQTableViewController.h"
#import "OnlineDocumentsTableViewController.h"
#import "OnlineVideosTableViewController.h"

@interface HelpViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *FAQButton;              // 常见问题按钮
@property (weak, nonatomic) IBOutlet UIButton *onlineDocumentsButton;  // 在线文档按钮
@property (weak, nonatomic) IBOutlet UIButton *onlineVideosButton;     // 在线视频按钮
@property (weak, nonatomic) IBOutlet UIView *contentView;              // 显示内容的view
@property (weak, nonatomic) IBOutlet UIImageView *FAQSelectionIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *onlineDocuSelectionIndicator;
@property (weak, nonatomic) IBOutlet UIImageView *onlineVideoSelectionIndicator;
@end

@implementation HelpViewController{
    NSArray *_buttonArray;                            // 按钮数组
    FAQTableViewController *FAQTVC;                   // “常见问题”子控制器
    OnlineDocumentsTableViewController *onlineDocuVC; // “在线文档”子控制器
    OnlineVideosTableViewController *onlineVideoVC;   // “在线视频”子控制器
    UITableViewController *currentVC;                 // 当前显示的自控制器
    
    UIWebView *webView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    FAQTVC = [[FAQTableViewController alloc]init];
//    onlineDocuVC = [[OnlineDocumentsTableViewController alloc]init];
//    onlineVideoVC = [[OnlineVideosTableViewController alloc]init];
//    
//    _buttonArray = [NSArray arrayWithObjects:self.FAQButton,self.onlineDocumentsButton,self.onlineVideosButton, nil];
//    
//    self.FAQSelectionIndicator.hidden = NO;
//    self.onlineDocuSelectionIndicator.hidden = YES;
//    self.onlineVideoSelectionIndicator.hidden = YES;
    
    webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    webView.scalesPageToFit = YES;
    [self.view addSubview:webView];
}

- (void)viewDidAppear:(BOOL)animated{
    // 默认显示“常见问题”子控制器
//    self.FAQButton.selected = YES;
//    currentVC = FAQTVC;
//    // 添加子控制器的view
//    [self addChildViewController:currentVC];
//    FAQTVC.view.frame = self.contentView.frame;
//    [self.view addSubview:FAQTVC.view];
    
    NSString *resourcePath =  [[NSBundle mainBundle] pathForResource:@"GreeHelp_ZH" ofType:@"html"];

    NSString *htmlString = [NSString stringWithContentsOfFile:resourcePath encoding:NSUTF8StringEncoding error:nil];
    
    [webView loadHTMLString:htmlString baseURL:[NSURL URLWithString:resourcePath]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

// 点击三个主题按钮之后的处理
- (IBAction)buttonsClicked:(UIButton *)sender
{
    // 保证三个按钮只有一个可以被同时选中
    if (sender.selected == NO) {
        for (UIButton *button in _buttonArray) {
            if (button.selected == YES) {
                button.selected = NO;
                [self selectionIndicatorOfButton:button].hidden = YES; // 隐藏原来选中按钮的蓝色指示条
            }
        }
        
        sender.selected = YES;
        [self selectionIndicatorOfButton:sender].hidden = NO; // 现实新选中按钮的蓝色指示条
        
        // 显示相应被选中的VC的内容
        [self showButtonContentViewWithButtonTag:sender.tag];
    }
}

// 得到按钮是否被选中的蓝色指示条
- (UIView*)selectionIndicatorOfButton:(UIButton*)button
{
    UIView *selectionIndicator;
    for (UIView *view in button.superview.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            selectionIndicator = view;
        }
    }
    return selectionIndicator;
}

// 显示相应被选中的按钮的内容
-(void)showButtonContentViewWithButtonTag:(NSUInteger)tag
{
    switch (tag) {
        case 100:
            [self switchToVC:FAQTVC];
            break;
        case 101:
            [self switchToVC:onlineDocuVC];
            break;
        case 102:
            [self switchToVC:onlineVideoVC];
            break;
        default:
            break;
    }
}

// 切换子控制器
- (void)switchToVC:(UITableViewController*)toVC
{
    [self addChildViewController:toVC];
    [currentVC willMoveToParentViewController:nil];
    toVC.view.frame = self.contentView.frame;
    
    __weak id weakSelf = self;
    [self transitionFromViewController:currentVC
                      toViewController:toVC
                              duration:0.5
                               options:UIViewAnimationOptionTransitionNone
                            animations:^{ }
                            completion:^(BOOL finished) {
                                [currentVC.view removeFromSuperview];
                                [currentVC removeFromParentViewController];
                                [toVC didMoveToParentViewController:weakSelf];
                                currentVC = toVC;
                            }];
}

@end
