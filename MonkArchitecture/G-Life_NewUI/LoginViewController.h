//
//  LoginViewController.h
//  G-Life_NewUI
//
//  Created by workMac on 15/5/12.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@end
