//
//  ForgetPwdViewController.h
//  G-Life_NewUI
//
//  Created by workMac on 15/5/29.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPwdViewController : UIViewController <UITextFieldDelegate>

@end
