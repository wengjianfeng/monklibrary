//
//  FAQTableViewController.m
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/4/10.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "FAQTableViewController.h"

#define kHeaderHeight 30              // header高度
#define kHeaderTitleLeftBorder 20     // header跟左边的距离
#define kRowHeight 55                 // 行高

@interface FAQTableViewController ()

@end

@implementation FAQTableViewController
{
    NSArray *_titles;     // 主题数组
    NSArray *_contents;   // 内容数组
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _titles = [NSArray arrayWithObjects:@"怎样配置空调",@"你猜",@"你再猜", nil];
    _contents = [NSArray arrayWithObjects:
                 @"查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助",
                 @"查看帮助，查看帮助，查看帮助",
                 @"查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助，查看帮助。", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _titles.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kHeaderHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kRowHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kHeaderHeight)];
    view.backgroundColor = [UIColor colorWithRed:243/255.0 green:243/255.0 blue:243/255.0 alpha:1.0];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(kHeaderTitleLeftBorder, 0, self.view.frame.size.width - kHeaderTitleLeftBorder, kHeaderHeight)];
    label.backgroundColor = [UIColor colorWithRed:243/255.0 green:243/255.0 blue:243/255.0 alpha:1.0];
    label.text = _titles[section];
    [view addSubview:label];
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reuseIdentifier"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reuseIdentifier"];
    }
    
    cell.textLabel.text = _contents[indexPath.section];
    cell.textLabel.numberOfLines = 0;
    
    return cell;
}

@end
