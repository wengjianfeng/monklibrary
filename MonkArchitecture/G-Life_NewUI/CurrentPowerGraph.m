//
//  CurrentPowerGraph.m
//  SmartSocket
//
//  Created by workMac on 15/3/3.
//  Copyright (c) 2015年 Gree Inc. All rights reserved.
//

#import "CurrentPowerGraph.h"
# define kVerticalLabelHeight 20           //垂直方向上Label的高度
# define kVerticalLabelWidth 30            //垂直方向上Label的宽度
# define kUnitLabelSpaceToVerticalLabel 20 //功率单位label距离第一个Y轴Label的距离
# define kHorizontalLabeiWidth 50          //水平方向上Label的宽度
# define kHorizontalLabeiHeight 40         //水平方向上Label的高度
# define kGraphLeftBorder 50               //图表左边距离superview的距离
# define kGraphRightBorder 20              //图表右边距离superview的距离
# define kVerticalLabelLeftBorder 20       //Y轴label左边距离superview的距离
# define kNumberOfVerticalSegments 7       //竖直方向上被分为了几个，坐标区间数目加上上下各一个
# define kNumberOfHorizontalSegments 7     //水平方向上被分为了几个，坐标区间数目加上左右各一个border
# define kHorizontalPointsBorderToGraph 15 //X轴标签点距离Graph两侧的距离
# define kCurvePointDiameter 8             //圆点直径
# define kCurveLineWidth 1.5               //曲线宽度

@interface CurrentPowerGraph ()
@property (nonatomic,strong) NSMutableArray *horizontalLabels;          // X轴的标签
@property (nonatomic,strong) NSMutableArray *horizontalLabelXPositions; // X轴标签的位置
@property (nonatomic,strong) UIBezierPath *horizontalLinePath;          // 水平横线
@property (nonatomic,strong) NSMutableArray *verticalLabelYPositions;   // Y轴标签坐标
@property (nonatomic,strong) NSMutableArray *verticalLabels;            // Y轴标签
@property (nonatomic,strong) NSArray *verticalLabelTexts;               // Y轴标签文字
@property (nonatomic,strong) NSMutableArray *pointImageViews;           // 功率点上的原点
@end

IB_DESIGNABLE
@implementation CurrentPowerGraph
{
    UIBezierPath *_curvePath;        //功率曲线
    UIBezierPath *_verticalLinePath; //功率对应的竖线
    BOOL isFirstTimeDraw;            //是否是第一次加载
}

#pragma mark - 数据懒加载
-(NSArray *)powers{
    if (!_powers) {
        _powers = [[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
    }
    return _powers;
}
-(NSMutableArray *)horizontalLabelTexts{
    if (!_horizontalLabelTexts) {
        _horizontalLabelTexts = [[NSMutableArray alloc]initWithObjects:@"00:00:00",@"00:00:00",@"00:00:00",@"00:00:00",@"00:00:00",@"00:00:00",@"00:00:00",@"00:00:00", nil];
    }
    return _horizontalLabelTexts;
}
-(NSMutableArray *)horizontalLabels{
    if (!_horizontalLabels) {
        _horizontalLabels = [NSMutableArray array];
    }
    return _horizontalLabels;
}
-(NSMutableArray *)horizontalLabelXPositions{
    if (!_horizontalLabelXPositions) {
        _horizontalLabelXPositions = [NSMutableArray array];
    }
    return _horizontalLabelXPositions;
}
-(UIBezierPath *)horizontalLinePath{
    if (!_horizontalLinePath) {
        _horizontalLinePath = [UIBezierPath bezierPath];
    }
    return _horizontalLinePath;
}
-(UIBezierPath *)verticalLinePath{
    if (!_verticalLinePath) {
        _verticalLinePath = [UIBezierPath bezierPath];
    }
    return _verticalLinePath;
}
-(NSMutableArray *)verticalLabelYPositions{
    if (!_verticalLabelYPositions) {
        _verticalLabelYPositions = [NSMutableArray array];
    }
    return _verticalLabelYPositions;
}
-(NSMutableArray *)verticalLabels{
    if (!_verticalLabels) {
        _verticalLabels = [NSMutableArray array];
    }
    return _verticalLabels;
}
-(NSArray *)verticalLabelTexts{
    if (!_verticalLabelTexts) {
        _verticalLabelTexts = [NSArray arrayWithObjects:@"8.5",@"6.8",@"5.1",@"3.4",@"1.7",@"0.0", nil];
    }
    return _verticalLabelTexts;
}
-(NSMutableArray *)pointImageViews{
    if (!_pointImageViews) {
        _pointImageViews = [NSMutableArray array];
        for (NSUInteger i = 1; i <= 8; i ++) {
            UIImageView *point = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"5-47"]];
            point.frame = CGRectMake(0, 0, 5, 5);
            [_pointImageViews addObject:point];
        }
    }
    return _pointImageViews;
}

#pragma mark - 初始化
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        isFirstTimeDraw = YES;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        isFirstTimeDraw = YES;
    }
    return self;
}

#pragma mark - 绘图
- (void)drawRect:(CGRect)rect
{
    [self drawBaseInRect:rect];
    [self drawPowerCurveInRect:rect];
}

//显示坐标系和label
-(void)drawBaseInRect:(CGRect)rect
{
    NSUInteger lineHeight = CGRectGetHeight(rect) / kNumberOfVerticalSegments;
    
    //第一次建立
    if (isFirstTimeDraw) {
        //Y轴标签
        for (NSUInteger i = 1; i <= kNumberOfVerticalSegments - 1; i ++) {
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(kVerticalLabelLeftBorder, lineHeight*i - kVerticalLabelHeight/2, kVerticalLabelWidth, kVerticalLabelHeight)];
            label.text = [self.verticalLabelTexts objectAtIndex:i-1];
            label.textAlignment = NSTextAlignmentCenter;
            [self addSubview:label];
            [self.verticalLabels addObject:label];
            [self.verticalLabelYPositions addObject:[[NSNumber alloc]initWithUnsignedLong:lineHeight*i]];
        }
        //功率单位标签
        UILabel *firstVerticalLabel = (UILabel*)[self.verticalLabels firstObject];
        UILabel *unitLabel = [[UILabel alloc]initWithFrame:CGRectMake(kVerticalLabelLeftBorder, CGRectGetMinY(firstVerticalLabel.frame) - kUnitLabelSpaceToVerticalLabel, kVerticalLabelWidth, kVerticalLabelHeight)];
        unitLabel.textAlignment = NSTextAlignmentCenter;
        unitLabel.text = @"W";
        [self addSubview:unitLabel];
        //横标签建立
        NSUInteger graphWidth = CGRectGetWidth(rect) - kGraphLeftBorder - kGraphRightBorder; //图表宽度
        NSUInteger horizontalSeparaterWidth = (graphWidth - kHorizontalPointsBorderToGraph * 2) / kNumberOfHorizontalSegments;
        for (NSUInteger i = 1; i <= kNumberOfHorizontalSegments + 1; i ++) {
            if ( i%2 == 1) {
                UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(kGraphLeftBorder + kHorizontalPointsBorderToGraph + horizontalSeparaterWidth * (i-1) - kHorizontalLabeiWidth/2, CGRectGetHeight(rect)-lineHeight, kHorizontalLabeiWidth, kHorizontalLabeiHeight)];
                label.text = [self.horizontalLabelTexts objectAtIndex:i-1];
                label.font = [UIFont systemFontOfSize:12];
                [self addSubview:label];
                [self.horizontalLabels addObject:label];
            }
            [self.horizontalLabelXPositions addObject:[[NSNumber alloc]initWithUnsignedLong:(kGraphLeftBorder + kHorizontalPointsBorderToGraph + horizontalSeparaterWidth * (i-1))]];
        }
        //将是否为第一次置否
        isFirstTimeDraw = NO;
    }

    //横线更新
    for (NSUInteger i = 1; i <= kNumberOfVerticalSegments - 1; i ++) {
        [self.horizontalLinePath moveToPoint:CGPointMake(kGraphLeftBorder, lineHeight * i)];
        [self.horizontalLinePath addLineToPoint:CGPointMake(CGRectGetMaxX(rect)- kGraphRightBorder, lineHeight * i)];
        [[UIColor darkGrayColor]setStroke];
        self.horizontalLinePath.lineWidth = 1.5;
        [self.horizontalLinePath stroke];
    }
    //横标签更新
    for (NSUInteger i = 1; i <= 4; i ++) {
        UILabel *label = (UILabel*)self.horizontalLabels[i-1];
        label.text = [self.horizontalLabelTexts objectAtIndex:2*(i-1)];
    }
}

//显示曲线和对应的竖线
-(void)drawPowerCurveInRect:(CGRect)rect
{
    _curvePath = [UIBezierPath bezierPath];
    _verticalLinePath = [UIBezierPath bezierPath];
    
    //曲线移动到第一个点
    CGPoint firstCurcePoint = CGPointMake([self.horizontalLabelXPositions[0] integerValue], [self.verticalLabelYPositions[5] integerValue] - [self.powers[0] doubleValue]/8.5*(CGRectGetHeight(rect)*5/7.0));
    [_curvePath moveToPoint:firstCurcePoint];
    //第一个点的竖线
    [_verticalLinePath moveToPoint:CGPointMake([self.horizontalLabelXPositions[0] integerValue], [self.verticalLabelYPositions[5] integerValue])];
    [_verticalLinePath addLineToPoint:firstCurcePoint];
    //第一个圆点
    UIImageView *firstPoint = self.pointImageViews.firstObject;
    firstPoint.frame = CGRectMake(firstCurcePoint.x-kCurvePointDiameter*0.5, firstCurcePoint.y-kCurvePointDiameter*0.5, kCurvePointDiameter, kCurvePointDiameter);
    [self addSubview:firstPoint];
    //后续的点
    for (NSUInteger i = 1; i <= 7; i ++) {
        CGFloat basePointX = [self.horizontalLabelXPositions[i] integerValue];
        CGFloat basePointY = [self.verticalLabelYPositions[5] integerValue];
        //后续的曲线
        CGPoint curvePoint = CGPointMake(basePointX, basePointY - [self.powers[i] doubleValue]/8.5*(CGRectGetHeight(rect)*5/7.0));
        [_curvePath addLineToPoint:curvePoint];
        //后续点的竖线
        [_verticalLinePath moveToPoint:CGPointMake(basePointX, basePointY)];
        [_verticalLinePath addLineToPoint:curvePoint];
        //后续的圆点
        UIImageView *point = [self.pointImageViews objectAtIndex:i];
        point.frame = CGRectMake(curvePoint.x-kCurvePointDiameter*0.5, curvePoint.y-kCurvePointDiameter*0.5, kCurvePointDiameter, kCurvePointDiameter);
        [self addSubview:point];
    }
    
    [[UIColor lightGrayColor] setStroke];
    [_verticalLinePath stroke];
    
    _curvePath.lineWidth = kCurveLineWidth;
    [[UIColor colorWithRed:68/255.0 green:179/255.0 blue:231/255.0 alpha:1] setStroke];
    [_curvePath stroke];
}

@end
