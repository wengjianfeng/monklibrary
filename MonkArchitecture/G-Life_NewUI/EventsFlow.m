//
//  EventsFlow.m
//  G-Life_NewUI
//
//  Created by gree's apple on 15/10/15.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "EventsFlow.h"
#import "AppContext.h"
#import "NotificationLists.h"

#import "VirtualDeviceForBroadcastBL.h"
#import "VirtualDeviceBL.h"
#import "VirtualServerBL.h"

#import "GEDeviceBLRepository.h"

@implementation EventsFlow
{
    VirtualDeviceForBroadcastBL *_bcBL;
    VirtualDeviceForBroadcastBL *_bchexBL;
    VirtualServerBL *_svBL;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(getNetworkStatus)
                                                     name:Flow_networkNotification
                                                   object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(refresh)
                                                     name:Flow_becomeActive
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(clear)
                                                     name:Flow_resignActive
                                                   object:nil];
    }
    
    return self;
}

-(void)dealloc
{
    NSLog(@"EventsFlow dealloc");
    [[NSNotificationCenter defaultCenter] removeObserver:self name:Flow_networkNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:Flow_becomeActive object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:Flow_resignActive object:nil];

}

- (void)startFlow
{
    /*-----------------------------------------------------------------------------------*/
    /********串行广播*********/
    
    AppContext *app = [AppContext shareAppContext];
    
    /***************************/
    /*  Step1、HEX近程广播开始       */
    /*  自动建立TCP并完成数据请求  */
    /*  能够UDP广播接收并发送同步数据了*/
    /***************************/
    //UDP广播BL
    if (!_bchexBL) {
        _bchexBL = [[VirtualDeviceForBroadcastBL alloc] init];
    }
    [_bchexBL creatUdpWithIpAndPortContext:@{
                                        @"IP" : app.bch,
                                        @"Port" : [NSNumber numberWithInteger:app.bcp],
                                        @"LocPort" : [NSNumber numberWithInteger:app.hexlp]
                                       }];
    [_bchexBL broadcast:^{
        NSLog(@"HEX Client broadcast Success");
    } fail:^{
        NSLog(@"HEX Client broadcast Fail");
    }];
    
    /******************************以上为0X7E7E流程模式********************/
    /******************************以下为JSON流程模式**********************/
        
    /******************************/
    /* Step2、Json近程广播开始       */
    /* 能够UDP广播接收并发送同步数据了 */
    /******************************/
    if (!_bcBL) {
        _bcBL = [[VirtualDeviceForBroadcastBL alloc] initWithType:json];
    }
    [_bcBL creatUdpWithIpAndPortContext:@{
                                       @"IP" : app.bch,
                                       @"Port" : [NSNumber numberWithInteger:app.bcJSp],
                                       @"LocPort" : [NSNumber numberWithInteger:app.jslp]
                                       }];
    [_bcBL broadcast:^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        NSLog(@"JSON Client broadcast Success");
    } fail:^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:Flow_searchDeviceStatus object:@"fault"];
        NSLog(@"JSON Client broadcast Fail");
    }];
}

- (void)start
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    /******************************/
    /* Step3、网络监控。            */
    /* 涉及与服务器的交互            */
    /******************************/
    //开始前要通知界面刷新一次
    [[NSNotificationCenter defaultCenter] postNotificationName:Flow_updataSuccess object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:Flow_searchDeviceStatus object:@"start"];


    [self startFlow];
}

- (void)refresh
{
    [self clearWithoutServer];
    //step1 查找近程设备
    [self start];      
    //step2 查找远程设备
    if ([AppContext shareAppContext].loginSuccess == YES) {
        //已经连接上服务器且登录完成
        [_svBL statusRequest];
    } else {
        if ([AppContext shareAppContext].connectSuccess == NO) {
            //如果与服务器存在连接失败，刷新需要重新走流程
            //连接不上服务器或者密钥交互失败
            [[GEDeviceBLRepository shareInstance] clear];
            _svBL= nil;
            //重新开启服务器连接
            [[NSNotificationCenter defaultCenter] postNotificationName:Flow_networkNotification object:nil];
        }
    }
}

- (void)clear
{
    [[GEDeviceBLRepository shareInstance] clear];
    [[AppContext shareAppContext] clearUserInfo];
    _svBL= nil;
}

- (void)clearWithoutServer
{
    [[GEDeviceBLRepository shareInstance] clearWithOutObjectName:@"server"];
}

#pragma mark-
#pragma mark Reachability Notification
- (void)getNetworkStatus
{
    /*********************/
    /* Json远程搜索开始    */
    /*********************/
    if ([AppContext shareAppContext].networkStatus == -1) {
        //等待近程搜索完毕再通知，否则提示“搜索不到设备”太快
//        [[NSNotificationCenter defaultCenter] postNotificationName:Flow_searchDeviceStatus object:nil];
        return;
    }
    
    AppContext *app = [AppContext shareAppContext];
    
    if (!_svBL) {
        if ([AppContext shareAppContext].connectSuccess == NO)
        {
            _svBL = [[VirtualServerBL alloc] initWithType:json];
            [_svBL creatTcpWithIpAndPortContext:@{
                                                  @"IP" : app.sh,
                                                  @"Port" : [NSNumber numberWithInteger:app.sp],
                                                  }];
        }
    }

}

@end
