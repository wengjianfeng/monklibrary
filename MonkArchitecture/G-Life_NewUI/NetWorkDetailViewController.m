//
//  NetWorkDetailViewController.m
//  Remote
//
//  Created by 翁 健峰 on 13-4-26.
//  Copyright (c) 2013年 WJF-Monk  330694. All rights reserved.
//

#import "NetWorkDetailViewController.h"
#import "VirtualDeviceBL.h"
#import "ControlJsonFormatLists.h"

@interface NetWorkDetailViewController ()
{
    NSArray *keys;
    NSArray *DefineData;
    NSMutableArray *mContentObject;     //装cell中的控件
    
    NSString *Remote_name;
    NSString *Remote_key;
    NSString *Loca_name;
    NSString *loca_key;
    
    UIButton *bindNWBtn;
}
-(void)tapToSuperView:(id)sender;

@end

@implementation NetWorkDetailViewController
@synthesize mResourceData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        currentTag = 0;
        
        mContentObject = [[NSMutableArray alloc]initWithCapacity:6];
        mTextSource = [[NSMutableArray alloc]initWithCapacity:8];
        mPicker_Data = [[NSMutableArray alloc]initWithCapacity:10];
        mResourceData = [[NSDictionary alloc]init];
        
        //默认数值
//        mNetWork_Mode = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"LocShort Range Control", @"近程模式"),NSLocalizedString(@"LocLong Range Control", @"远程模式"), nil];
        mNetWork_Mode = [[NSMutableArray alloc]initWithObjects:@"近程模式",@"远程模式", nil];
        
//        DefineData = [[NSArray alloc]initWithObjects:NSLocalizedString(@"LocNetwork type", @"网络类型"),NSLocalizedString(@"LocNetwork name", @"网络名称"),NSLocalizedString(@"LocKey", @"安全密钥"),NSLocalizedString(@"LocRoom name", @"空调名称"), nil];
        DefineData = [[NSArray alloc]initWithObjects:@"网络类型",@"网络名称",@"安全密钥",@"空调名称", nil];
        
        isEditing = NO;
        ShowPicker = NO;
        
        Remote_name = @"";
        Remote_key = @"";
        Loca_name = @"";
        loca_key = @"";
        
        mDataSource = [[NSMutableArray alloc]initWithCapacity:10];  //中间变量，最后点击确定之后才将真正数据传出去
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[[UIDevice currentDevice]systemVersion]floatValue]>=7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    UIBarButtonItem *Rbutton = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonSystemItemSave target:self action:@selector(tapToSuperView:)];
    Rbutton.width = 24;
    Rbutton.style = UIBarButtonSystemItemSave;
    self.navigationItem.rightBarButtonItem = Rbutton;
    
    CGRect ViewBounds = [[UIScreen mainScreen] bounds];
    
    mTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ViewBounds.size.width, ViewBounds.size.height) style:UITableViewStylePlain];
    mTableView.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"单元格底色.png"]];
    mTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mTableView.dataSource = self;
    mTableView.delegate = self;
    mTableView.rowHeight = 38;
    [self.view addSubview:mTableView];
    
    mPickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0,ViewBounds.size.height, ViewBounds.size.width, 260)];
    mPickerView.dataSource = self;
    mPickerView.delegate = self;
    mPickerView.backgroundColor = [UIColor whiteColor];
    mPickerView.showsSelectionIndicator = YES;
    [self.view addSubview:mPickerView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    NSUInteger NetWorkTypeData  = [[mResourceData objectForKey:@"NetWorkType"]intValue];//AP/Client
    
    NSString *NetWorkName  = [mResourceData objectForKey:@"aNetWorkName"];
    
    NSString *NetWorkKey = [mResourceData objectForKey:@"NetWorkKey"];
    
    NSString *DeviceName   = [mResourceData objectForKey:@"DeviceName"];
    
    if (NetWorkTypeData == 1)  //0为STA,1为AP模式
    {
        Loca_name = NetWorkName;
        loca_key = NetWorkKey;
    }
    else
    {
        Remote_name = NetWorkName;
        Remote_key = NetWorkKey;
    }
    
    mDataSource = [[NSMutableArray alloc]initWithObjects:[NSString stringWithFormat:@"%lu",(unsigned long)NetWorkTypeData],NetWorkName,NetWorkKey,DeviceName, nil];//全部转换为字符串，数组不能放数值。
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)tapToSuperView:(id)sender
{
    NSString *errorInfo = @"";
    //用户设置错误
    if ([[mDataSource objectAtIndex:1] length] == 0 || [[mDataSource objectAtIndex:3] length] == 0)
    {
        errorInfo = @"用户设置错误";
    } else {
        errorInfo = @"是否确定修改!";
    }
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示"  message:errorInfo delegate:nil cancelButtonTitle:@"放弃" otherButtonTitles:@"确定", nil];
    alert.delegate = self;
    [alert show];
}

-(void)ShowOrHiddenPicker
{
    CGRect ViewBounds = [[UIScreen mainScreen] bounds];
    
    if (ShowPicker == YES)
    {
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^()
         {
             [mPickerView setFrame:CGRectMake(mPickerView.frame.origin.x, ViewBounds.size.height, mPickerView.frame.size.width, mPickerView.frame.size.height)];
         }completion:^(BOOL finished) {}];
    }
    else
    {
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^()
         {
             [mPickerView setFrame:CGRectMake(mPickerView.frame.origin.x, ViewBounds.size.height - 260, mPickerView.frame.size.width, mPickerView.frame.size.height)];
         }completion:^(BOOL finished) {}];
    }
    
    ShowPicker = !ShowPicker;
}

#pragma mark --- TextFieldDelegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!textField.window.isKeyWindow)
    {
        [textField.window makeKeyAndVisible];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    NSInteger row = 0;
    NSInteger section = 0;
    
    CGRect ViewBounds = [[UIScreen mainScreen] bounds];
    
    if ( ShowPicker == YES )    //已经显示了
    {
        [self ShowOrHiddenPicker];  //隐藏
        [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^()
         {
             [mTableView setFrame:CGRectMake(mTableView.frame.origin.x, mTableView.frame.origin.y, mTableView.frame.size.width, ViewBounds.size.height)];
             
         }completion:^(BOOL finished) {}];
        return NO;
    }
    
    if (isEditing == YES)  //键盘已经显示
    {
        if (textField.tag == 100)
        {
            currentTag = textField.tag;
            [[mTextSource objectAtIndex:(currentTag - 100)] resignFirstResponder];
            
            UITextField *TempField = [mTextSource objectAtIndex:(currentTag - 100)];
            [TempField resignFirstResponder];
        }
    }
    
    isEditing = YES;
    
    mTextField = textField;
    currentTag = mTextField.tag;
    
    if (mTextField.tag <= 103)
    {
        section = 0;
        row = mTextField.tag - 100;
    }
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:row inSection:section];
    //滚动tableview
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^()
	 {
		 [mTableView setFrame:CGRectMake(mTableView.frame.origin.x, mTableView.frame.origin.y, mTableView.frame.size.width, ViewBounds.size.height - 260)];
	 } completion:^(BOOL finished)	{}];
    
    [mTableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    [mTableView selectRowAtIndexPath:path animated:NO scrollPosition:UITableViewScrollPositionNone];
    [mTableView deselectRowAtIndexPath:path animated:NO];
    
    NSUInteger NetWorkTypeData  = [[mDataSource objectAtIndex:0]intValue];
    
    [mPickerView reloadComponent:0];    //不知为何要在这里reload才能生效？？？
    if (mTextField.tag == 100)
    {
        if (mTextField.tag == 100)
        {
            if (NetWorkTypeData == 1)   //AP模式
            {
                [mPickerView selectRow:0 inComponent:0 animated:YES];
            }
            else if (NetWorkTypeData == 0)
            {
                [mPickerView selectRow:1 inComponent:0 animated:YES];
            }
            
        }
        [self ShowOrHiddenPicker];  //展示
        
        return NO;
    }
    
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //  显示的长度跟字符少1
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    if (textField.tag == 103)//空调名
    {
        if ( strlen([textField.text UTF8String]) >= 15)
        {
            textField.text = @"";
            
//            MainRoomViewController *MainRoom = [MainRoomViewController ShareInstance];
//            [MainRoom.mWindowsAlartShowView AppearAlertWithMessage:NSLocalizedString(@"LocReminderFour", @"请输入4个中文或14个英文数字") WithType:0 AndTimeInterval:5.0f];
        }
        else if(strlen([textField.text UTF8String]) == 0)
        {
//            MainRoomViewController *MainRoom = [MainRoomViewController ShareInstance];
//            [MainRoom.mWindowsAlartShowView AppearAlertWithMessage:NSLocalizedString(@"LocReminderThi", @"请输入至少1个字母或数字") WithType:0 AndTimeInterval:5.0f];
        }
        [mDataSource replaceObjectAtIndex:(textField.tag - 100) withObject:textField.text];
        
        return YES;
    }
    
    if (textField.tag == 100)
    {
        //判断是AP还是client
        if ( [[mDataSource objectAtIndex:4] isEqualToString:@"2"] ) //AP模式
        {
            if ( strlen( [textField.text UTF8String]) >= 10 && strlen( [textField.text UTF8String]) <= 15 )   //AP模式
            {
                NSString *text = nil;
                text = [textField.text substringToIndex:5];
                if ([text isEqualToString:@"gree-"])
                {
                    [mDataSource replaceObjectAtIndex:(textField.tag - 100) withObject:textField.text];
                    Loca_name = textField.text;
                    
                    return YES;
                }
                else
                {
                    textField.text = @"";
                }
                
//                MainRoomViewController *MainRoom = [MainRoomViewController ShareInstance];
//                [MainRoom.mWindowsAlartShowView AppearAlertWithMessage:NSLocalizedString(@"LocReminderFive", @"请输入3个中文或10个英文数字") WithType:0 AndTimeInterval:5.0f];
                
                
                [mDataSource replaceObjectAtIndex:(textField.tag - 100) withObject:textField.text];
                
                Loca_name = textField.text;
                
                return YES;
            }
            else if( strlen([textField.text UTF8String]) <= 4)
            {
                textField.text = [NSString stringWithFormat:@"gree-%@",textField.text];
                
                [mDataSource replaceObjectAtIndex:(textField.tag - 100) withObject:textField.text];
                Loca_name = textField.text;
                
                return YES;
            }
            else if ( strlen( [textField.text UTF8String]) < 10 )
            {
                NSString *text = nil;
                text = [textField.text substringToIndex:5];
                if ([text isEqualToString:@"gree-"])
                {
                    [mDataSource replaceObjectAtIndex:(textField.tag - 100) withObject:textField.text];
                    Loca_name = textField.text;
                    
                    return YES;
                }
                else
                {
                    textField.text = [NSString stringWithFormat:@"gree-%@",textField.text];
                    
                    [mDataSource replaceObjectAtIndex:(textField.tag - 100) withObject:textField.text];
                    Loca_name = textField.text;
                    
                    return YES;
                }
                
            }
            
            
        }
        else if ( [[mDataSource objectAtIndex:4] isEqualToString:@"0"] )    //client
        {
            if ( strlen([textField.text UTF8String]) <= 15 )
            {
                [mDataSource replaceObjectAtIndex:(textField.tag - 100) withObject:textField.text];
                Remote_name = textField.text;
                
                return YES;
            }
            else
            {
//                MainRoomViewController *MainRoom = [MainRoomViewController ShareInstance];
//                [MainRoom.mWindowsAlartShowView AppearAlertWithMessage:NSLocalizedString(@"LocReminderFir", @"请输入4个中文或15个英文数字") WithType:0 AndTimeInterval:5.0f];
                
            }
        }
        
        
        return YES;
    }
    
    if (textField.tag == 102)
    {
        if (textField.text.length < 8)
        {
            NSString *proText = [mDataSource objectAtIndex:(textField.tag - 100)];
            textField.text = proText;
//            MainRoomViewController *MainRoom = [MainRoomViewController ShareInstance];
//            [MainRoom.mWindowsAlartShowView AppearAlertWithMessage:NSLocalizedString(@"LocEightPassword", @"密码长度应为X位ASCII码字符") WithType:0 AndTimeInterval:5.0f];
            NSLog(@"必须大于8位密码");
        }
       
    }
    
    [mDataSource replaceObjectAtIndex:(textField.tag - 100) withObject:textField.text];
    
    if ( [[mDataSource objectAtIndex:0] isEqualToString:@"1"] ) //AP模式
    {
        loca_key = textField.text;
    }
    else
    {
        Remote_key = textField.text;
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    isEditing = NO;
    mTextField = textField;
    [mTextField resignFirstResponder];
    CGRect ViewBounds = [[UIScreen mainScreen] bounds];
    [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^()
     {
         [mTableView setFrame:CGRectMake(mTableView.frame.origin.x, mTableView.frame.origin.y, mTableView.frame.size.width, ViewBounds.size.height)];
         
     }completion:^(BOOL finished) {}];
    
    return YES;
}

#pragma mark --- tableView DataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;   //两个表
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [DefineData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifer = [NSString stringWithFormat:@"%ld,%ld",(long)indexPath.section,(long)indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifer];
    
    
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifer];
        cell.backgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"单元格分隔.png"]];
        
//        mTextField = [[UITextField alloc]initWithFrame:CGRectMake(cell.frame.origin.x + 150, cell.frame.origin.y, cell.frame.size.width - 100, cell.frame.size.height -5)];
        mTextField = [[UITextField alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 130, cell.frame.origin.y, [UIScreen mainScreen].bounds.size.width - 190, cell.frame.size.height -5)];

//        mTextField.backgroundColor = [UIColor grayColor];
        mTextField.tintColor = [UIColor grayColor];
        mTextField.textAlignment = NSTextAlignmentLeft;
        mTextField.delegate = self;
        mTextField.tag = indexPath.row + 100;    //为了辨别不同section的row
        mTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        mTextField.returnKeyType = UIReturnKeyDone;
        
        mTextField.text = [mDataSource objectAtIndex:indexPath.row];
        
        if (indexPath.row == 0)
        {
            NSUInteger UnChangeData = [[mDataSource objectAtIndex:indexPath.row]intValue];
            if (UnChangeData == 1) //AP模式
            {
                mTextField.text = [mNetWork_Mode objectAtIndex:0];
            }
            else if (UnChangeData == 0) //client模式
            {
                mTextField.text = [mNetWork_Mode objectAtIndex:1];
            }
        }
        
        if (indexPath.row == 2) {
            [mTextField setSecureTextEntry:YES];
        }
        
        [mTextSource addObject:mTextField];
        [cell addSubview:mTextField];
        
    }
    cell.textLabel.text = [DefineData objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark --- tableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CGRect ViewBounds = [[UIScreen mainScreen] bounds];
    NSUInteger aRow = indexPath.row;
    
    if (isEditing == NO)
    {
        isEditing = YES;
        [mTextField resignFirstResponder];
        mTextField = [mTextSource objectAtIndex:aRow];
        
        [mTextField becomeFirstResponder];
    }
    else
    {
        if (ShowPicker == YES)
        {
            [self ShowOrHiddenPicker];
            [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^()
             {
                 [mTableView setFrame:CGRectMake(mTableView.frame.origin.x, mTableView.frame.origin.y, mTableView.frame.size.width, ViewBounds.size.height)];
                 
             }completion:^(BOOL finished) {}];
        }
        else
        {
            [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^()
             {
                 [mTableView setFrame:CGRectMake(mTableView.frame.origin.x, mTableView.frame.origin.y, mTableView.frame.size.width, ViewBounds.size.height-260)];
                 
             }completion:^(BOOL finished) {}];
        }
        
        if (isEditing == YES)
        {
            isEditing = NO;
            [mTextField resignFirstResponder];
            [UIView animateWithDuration:0.3f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^()
             {
                 [mTableView setFrame:CGRectMake(mTableView.frame.origin.x, mTableView.frame.origin.y, mTableView.frame.size.width, ViewBounds.size.height)];
                 
             }completion:^(BOOL finished) {}];
        }
        
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [keys objectAtIndex:section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}


#pragma mark --- PickerDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (currentTag == 100)
    {
        return [mNetWork_Mode count];
    }
    else
    {
        return 0;
    }
    
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (currentTag == 100)
    {
        return [mNetWork_Mode objectAtIndex:row];
    }
    else
    {
        return 0;
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (currentTag == 100)
    {
        NSInteger SelectRow = [pickerView selectedRowInComponent:0];
        if (SelectRow == 0) //AP模式
        {
            SelectRow = 1;
        }
        else if (SelectRow == 1)    //Client模式
        {
            SelectRow = 0;
        }
        NSInteger BeforeSelect = [[mDataSource objectAtIndex:0]intValue];
        if (BeforeSelect == SelectRow)
        {
            //无需改变
            return;
        }
        
        //网络类型
        NSString *Name = @"";   //清空
        NSString *key = @"";    //清空
        
        if (row == 0)   //协议中，AP值为1，client为0，故需要转换
        {
            [mDataSource replaceObjectAtIndex:0 withObject:@"1"];
            Name = Loca_name;
            key = loca_key;
        }
        else if (row == 1)  //client模式
        {
            [mDataSource replaceObjectAtIndex:0 withObject:@"0"];
            Name = Remote_name;
            key = Remote_key;
        }
        NSString *text = [mNetWork_Mode objectAtIndex:row];
        mTextField.text = text; //更新文字
        
        //网络名称
        [mDataSource replaceObjectAtIndex:1 withObject:Name];
        UITextField *nameText = [mTextSource objectAtIndex:1];
        nameText.text = [mDataSource objectAtIndex:1];
        
        //密钥
        [mDataSource replaceObjectAtIndex:2 withObject:key];
        UITextField *keyText = [mTextSource objectAtIndex:2];
        keyText.text = [mDataSource objectAtIndex:2];
        
    }
    
}

#pragma mark --- UIAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (![[mDataSource objectAtIndex:1] length] == 0) {
        if (buttonIndex == 1) {
            NSMutableDictionary *stuff = [[NSMutableDictionary alloc] initWithCapacity:1];
            
            if ([[mResourceData objectForKey:@"NetWorkType"] isEqualToString:[mDataSource objectAtIndex:0]]&&
                [[mResourceData objectForKey:@"aNetWorkName"] isEqualToString:[mDataSource objectAtIndex:1]]&&
                [[mResourceData objectForKey:@"NetWorkKey"] isEqualToString:[mDataSource objectAtIndex:2]]) {
                
            } else {
                [stuff setObject:[mDataSource objectAtIndex:0] forKey:@"networkType"];
                [stuff setObject:[mDataSource objectAtIndex:1] forKey:@"networkName"];
                [stuff setObject:[mDataSource objectAtIndex:2] forKey:@"key"];
            }
            
            [stuff setObject:[mDataSource objectAtIndex:3] forKey:@"productName"];
            
//            [_network sendDataWithDictionary:stuff functionCode:@"0D"  sequenceNumber:@"1"];
            
            VirtualDeviceBL *bl = _statusDictionary[@"VirtualDeviceBL"];
            [bl stuffFormatMutableDictionary:stuff];
            [bl excuteWithTcpCmd:KJsonDevicesManageFC];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];

}

@end












