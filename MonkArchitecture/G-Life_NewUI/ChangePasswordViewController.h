//
//  ChangePasswordViewController.h
//  Remote
//
//  Created by Zhang Hanying on 14/11/14.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate>

@end
