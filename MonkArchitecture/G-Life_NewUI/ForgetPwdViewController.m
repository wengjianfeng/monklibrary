//
//  ForgetPwdViewController.m
//  G-Life_NewUI
//
//  Created by workMac on 15/5/29.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "ForgetPwdViewController.h"
#import "GEDeviceBLRepository.h"
#import "VirtualServerBL.h"
#import "AppContext.h"
#import "ControlJsonFormatLists.h"
#import "NotificationLists.h"
#import "NSObject+Helper.h"
#import "LoginHelper.h"
#import "CommunicateErrorData.h"
@interface ForgetPwdViewController ()
@property (weak, nonatomic) IBOutlet UITextField *userNameTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneCheckNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *confirmNewPwdTF;
@property (weak, nonatomic) IBOutlet UITextField *freshPasswordTF;

@property (weak, nonatomic) IBOutlet UIView *findModeView;//查找方式屏蔽
@property (weak, nonatomic) IBOutlet UIView *checkNumberView;//验证码屏蔽
@property (weak, nonatomic) IBOutlet UIView *confirmView;//确认栏屏蔽

@property (weak, nonatomic) IBOutlet UIButton *findByPhoneBtn;//通过手机找回
@property (weak, nonatomic) IBOutlet UIButton *findByMailBtn;//通过邮箱找回
@property (weak, nonatomic) IBOutlet UIButton *regainCheckNumBtn;//重新获取校验码
@property (weak, nonatomic) IBOutlet UIButton *confirmCheckNumBtn;//确认校验码
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;//确认
@end

@implementation ForgetPwdViewController
{
    VirtualServerBL *_bl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.userNameTF.delegate = self;
    self.phoneCheckNumberTF.delegate = self;
    self.confirmNewPwdTF.delegate = self;
    self.freshPasswordTF.delegate = self;
    
    _bl = (VirtualServerBL *)[[GEDeviceBLRepository shareInstance]
                                              findVirtualDeviceByMac:[[AppContext shareAppContext] servermac]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([AppContext shareAppContext].connectSuccess == NO) {
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
        [self.view makeToast:@"无法连接服务器,请确保能接入互联网!" duration:10.0f position:[NSValue valueWithCGPoint:position]];
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(dispatchNotifications:) name:Flow_serverFeedbackNormalFrame object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:Flow_serverFeedbackNormalFrame object:nil];
}
// 用户名是否合法（包括全部是数字的情况）
-(BOOL)isUserNameValidIncludeAllDigit:(NSString *)userName
{
    NSString *userNameRegex = @"^[a-zA-Z0-9][A-Za-z0-9_]{7,15}";
    NSPredicate *userNameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", userNameRegex];
    return [userNameTest evaluateWithObject:userName];
}
//密码合法性
-(BOOL)isValidPassword:(NSString *)password
{
    NSString *passwordRegex = @"^[A-Z0-9a-z_]{8,16}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [emailTest evaluateWithObject:password];
}
#pragma mark - ibAction
-(IBAction)findBackByPhone:(id)sender//手机找回
{
    [_userNameTF resignFirstResponder]; //退出键盘，防止其阻挡toast
    
    BOOL userNameValid = [self isUserNameValidIncludeAllDigit:_userNameTF.text];
    if(!userNameValid){
        CGPoint messageCenter = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 120/568.8*kScreenHeight);
        [self.view makeToast:@"用户名格式不正确" duration:kToastDuration position:[NSValue valueWithCGPoint:messageCenter]];
    }else{
        [_bl stuffFormatMutableDictionary:[[NSMutableDictionary alloc] initWithObjectsAndKeys:_userNameTF.text,[self gainUserNameKey:_userNameTF.text],@"0",@"getbackStyle", nil]];
        [_bl excuteWithTcpCmd:KJsonRetrievePwdRequestFC];
    }
}

-(IBAction)findBackByMail:(id)sender//邮箱找回
{
    [_userNameTF resignFirstResponder]; //退出键盘，防止其阻挡toast
    
    BOOL userNameValid = [self isUserNameValidIncludeAllDigit:_userNameTF.text];
    if(!userNameValid){
        CGPoint messageCenter = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 120/568.8*kScreenHeight);
        [self.view makeToast:@"用户名格式不正确" duration:kToastDuration position:[NSValue valueWithCGPoint:messageCenter]];
    }else{
        [_bl stuffFormatMutableDictionary:[[NSMutableDictionary alloc] initWithObjectsAndKeys:_userNameTF.text,[self gainUserNameKey:_userNameTF.text],@"1",@"getbackStyle", nil]];
        [_bl excuteWithTcpCmd:KJsonRetrievePwdRequestFC];
    }
}

-(IBAction)regainCheckCode:(id)sender//重新获取验证码
{
    [_bl stuffFormatMutableDictionary:[[NSMutableDictionary alloc] initWithObjectsAndKeys:_userNameTF.text,[self gainUserNameKey:_userNameTF.text],@"0",@"getbackStyle", nil]];
    [_bl excuteWithTcpCmd:KJsonRetrievePwdRequestFC];
}

-(IBAction)confrimCheckCode:(id)sender//确认验证码
{
    [_bl stuffFormatMutableDictionary:[[NSMutableDictionary alloc] initWithObjectsAndKeys:_userNameTF.text,[self gainUserNameKey:_userNameTF.text],_phoneCheckNumberTF.text,@"identifyCode", nil]];
    [_bl excuteWithTcpCmd:KJsonRetrivePwdCheckIdentifyCodeRequestFC];
}

- (IBAction)confirmSetNewPwd:(UIButton *)sender//确定
{
    [_confirmNewPwdTF resignFirstResponder];
    [_freshPasswordTF resignFirstResponder];

    BOOL passwordValid = [self isValidPassword:_confirmNewPwdTF.text]; // 密码符合要求
    BOOL hasTwoSameAndNotEmptyPasswords = [_confirmNewPwdTF.text isEqualToString:_freshPasswordTF.text];// 密码一致并且非空
    NSString *message;
    if (!(passwordValid && hasTwoSameAndNotEmptyPasswords)) {
        if (!passwordValid) {
            message = @"注册密码格式不符合要求";
        }else if (!hasTwoSameAndNotEmptyPasswords){
            message = @"两次输入的注册密码不一致";
        }
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 120/568.0*kScreenHeight);
        [self.view makeToast:message duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
    }else{
        NSString *psw_md5 = [NSObject md5FromString:_confirmNewPwdTF.text];

        [_bl stuffFormatMutableDictionary:[[NSMutableDictionary alloc] initWithObjectsAndKeys:_userNameTF.text,[self gainUserNameKey:_userNameTF.text],psw_md5,@"userPassword", nil]];
        [_bl excuteWithTcpCmd:KJsonResetPwdFC];
    }
}

- (NSString *)gainUserNameKey:(NSString *)name
{
    LoginHelper *lh = [[LoginHelper alloc] init];
    return [lh loginUserNameKeyFromChain:name];
}

#pragma mark - alertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
        [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - textField Delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!textField.window.isKeyWindow) {
        [textField.window makeKeyAndVisible];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (textField == _userNameTF) {
        _findModeView.hidden = YES;//一输入用户名就开启选择
    }
    
    return YES;
}

#pragma mark - dispatch
-(void)dispatchNotifications:(NSNotification*)notification
{
    NSArray *array = [notification object];
    NSString *statusCode = [array objectAtIndex:1];
    NSString *functionCode = [array objectAtIndex:0];
    
    CommunicateErrorData *errorData  = [[CommunicateErrorData alloc] init];
    NSString *statusCode_PhoneCheckCodeSent = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocPhoneCheckCodeSent", @"手机验证码已发送")];        //@“5”
    NSString *statusCode_ActivationLinkSent = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocEmailActivationLinkSent", @"邮箱激活链接已发送")]; //@"6"
    NSString *statusCode_communicateSucess = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"locCommunicateSucess",@"通信成功")]; //@"1"

    if ([functionCode isEqualToString:KJsonRetrievePwdRespondFC]){ // "手机找回"或"邮箱找回"或"重新获取验证码"
        if ([statusCode isEqualToString:statusCode_ActivationLinkSent]) { // 邮箱
            [self retrivePwdByMail:array];
        }else if([statusCode isEqualToString:statusCode_PhoneCheckCodeSent]){ // 手机
            [self retrivePwdByPhone:array];
        }else if ([statusCode isEqualToString:statusCode_communicateSucess]) { //重新获取验证码
            [self identifyCodeRequestResponded:array];
        }else{ // 返回的错误数据
            NSString *errorMessage = [errorData errorDetailWithStatusCode:statusCode];
            CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 100/568.0*kScreenHeight);
            [self.view makeToast:errorMessage duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
        }
    } else if ([functionCode isEqualToString:KJsonRetrivePwdCheckIdentifyCodeRespondFC]){//确认验证码
        [self identifyCodeCheckResponded:array];
    } else if ([functionCode isEqualToString:KJsonResetPwdRespondFC]){//重设
        [self identifyCodeResetResponded:array];
    }
}
#pragma mark - handler
-(void)retrivePwdByMail:(NSArray*)array
{
    UIAlertView *mainAlert = [[UIAlertView alloc]initWithTitle:nil message:@"找回密码邮件已经发送到注册的邮箱" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
    [mainAlert show];
}

-(void)retrivePwdByPhone:(NSArray*)array
{
    //接收到应答就开启验证码
    _checkNumberView.hidden = YES;
    CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 100/568.0*kScreenHeight);
    [self.view makeToast:@"已发送验证码" duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
}

// 接收到“请求验证码应答帧”处理
-(void)identifyCodeRequestResponded:(NSArray*)array
{
    NSString *statusCode = [array objectAtIndex:1];
    
    CommunicateErrorData *errorData  = [[CommunicateErrorData alloc] init];
    NSString *statusCode_PhoneCheckCodeSent = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocPhoneCheckCodeSent", @"手机验证码已发送")];        //@“5”
    NSString *statusCode_ActivationLinkSent = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocEmailActivationLinkSent", @"邮箱激活链接已发送")]; //@"6"
    
    if ([statusCode isEqualToString:statusCode_PhoneCheckCodeSent] || [statusCode isEqualToString:statusCode_ActivationLinkSent] ) { //验证码成功发送
        NSLog(@"验证码成功发送");
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 100/568.0*kScreenHeight);
        [self.view makeToast:@"验证码成功发送" duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
    }else { // 验证码发送失败
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 100/568.0*kScreenHeight);
        [self.view makeToast:@"请求验证码错误" duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
        //因为验证码没有发送成功，所以重新发送按钮可用
//            resendButton.enabled = YES;
    }
}

// 接收到“验证码校验帧”处理
-(void)identifyCodeCheckResponded:(NSArray*)array
{
    NSString *statusCode = [array objectAtIndex:1];
    CommunicateErrorData *errorData  = [[CommunicateErrorData alloc] init];
    NSString *statusCode_communicateSucess = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"locCommunicateSucess",@"通信成功")]; //@"1"
   
    if([statusCode isEqualToString:statusCode_communicateSucess]){ // 验证码校验成功
        _confirmView.hidden = YES;
    }else{
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
        [self.view makeToast:@"验证码错误" duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
    }
}

//重置密码
-(void)identifyCodeResetResponded:(NSArray *)array
{
    NSString *statusCode = [array objectAtIndex:1];

    CommunicateErrorData *errorData  = [[CommunicateErrorData alloc] init];
    NSString *statusCode_communicateSucess = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"locCommunicateSucess",@"通信成功")]; //@"1"

    if ([statusCode isEqualToString:statusCode_communicateSucess]) { // 重置成功
        UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:nil message:@"密码已成功重置" delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
        [alertV show];
    }else{ // 重置失败
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 120/568.0*kScreenHeight);
        [self.view makeToast:[errorData errorDetailWithStatusCode:statusCode] duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
    }
}

@end
