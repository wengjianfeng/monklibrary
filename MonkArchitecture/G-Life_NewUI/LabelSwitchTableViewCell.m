//
//  LabelSwitchTableViewCell.m
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/4/9.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "LabelSwitchTableViewCell.h"

@implementation LabelSwitchTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)CellSwitch:(id)sender {
    UISwitch *swic = (UISwitch *)sender;
    switch (swic.tag) {
        case 0: //推送通知
        {
            if (swic.on) {
                UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge|UIUserNotificationTypeSound|UIUserNotificationTypeAlert categories:nil];
                [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
            } else {
                [[UIApplication sharedApplication] unregisterForRemoteNotifications];
            }
        }
            break;
        default:
            break;
    }
}

@end
