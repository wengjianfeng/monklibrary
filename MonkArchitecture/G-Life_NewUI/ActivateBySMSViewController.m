//
//  ActivateBySMSViewController.m
//  Remote
//
//  Created by Zhang Hanying on 14/10/21.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.

#define isiOS7OrLater [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height


#import "ActivateBySMSViewController.h"
#import "ActivateBySMSWithNavigationBarView.h"
#import "VerifyHelper.h"
#import "LoginHelper.h"
#import "KeychainItemWrapper.h"
#import "ControlJsonFormatLists.h"
#import "AppContext.h"

#define kSeconds 60  // 预设的倒计时时间

@interface ActivateBySMSViewController ()
@end

@implementation ActivateBySMSViewController
{
    NSTimer *timer;
    UIButton *resendButton;  // 重新发送按钮
    UIView *activateBySMSV;
    UITextField *identifyCodeTF;
    
    VerifyHelper *helper;
    NSString *userName;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self userName];
    
    activateBySMSV = [[ActivateBySMSWithNavigationBarView alloc]initWithFrame:self.view.frame];
    self.view = activateBySMSV;
    UIButton *confirmButton = (UIButton*)[self.view viewWithTag:1001];
    [confirmButton addTarget:self action:@selector(activateAccount) forControlEvents:UIControlEventTouchUpInside];
    resendButton = (UIButton*)[self.view viewWithTag:1002];
    resendButton.enabled = NO;
    [resendButton addTarget:self action:@selector(resendRequest) forControlEvents:UIControlEventTouchUpInside];
    
    identifyCodeTF = (UITextField*)[activateBySMSV viewWithTag:1003];
    identifyCodeTF.delegate = self;
    
    // 倒计时
    self.secondsLeft = kSeconds;
    [self countdown];

    // 退出按钮
    UIButton *backToLoginButton = [[UIButton alloc]init];
    CGFloat backToLoginButtonWidth = 80;
    CGFloat backToLoginButtonHeight = 42*kScreenHeight/568.0;
    if (isiOS7OrLater) {
        backToLoginButton.frame = CGRectMake((kScreenWidth - backToLoginButtonWidth)*0.5, 520*kScreenHeight/568.0, backToLoginButtonWidth, backToLoginButtonHeight);
    }else{
        backToLoginButton.frame = CGRectMake((kScreenWidth - backToLoginButtonWidth)*0.5, (520-20)*kScreenHeight/568.0, backToLoginButtonWidth, backToLoginButtonHeight);
    }
    
    [backToLoginButton setTitle:@"退出" forState:UIControlStateNormal];
    [backToLoginButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:backToLoginButton];
    [backToLoginButton addTarget:self action:@selector(backToLoginVC) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([AppContext shareAppContext].connectSuccess == NO) {
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
        [self.view makeToast:@"无法连接服务器,请确保能接入互联网!" duration:10.0f position:[NSValue valueWithCGPoint:position]];
    }
}

#pragma mark - View按钮处理
//确认激活
- (void)activateAccount
{
    [identifyCodeTF resignFirstResponder]; //退出键盘，防止其阻挡toast
    
    __block BOOL isDone = NO;
    
    helper = [[VerifyHelper alloc] init];
    [helper verifyWithActivateCode:identifyCodeTF.text withVerifyBlock:^(NSString *verifyInfo, NSError *error) {
        if (!error) {
            isDone = YES;
        } else {
            CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
            [self.view makeToast:verifyInfo duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
        }
    }];
    
    if (isDone) {
        LoginHelper *lh = [[LoginHelper alloc] init];
        NSString *key = [lh loginUserNameKeyFromChain:userName];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:userName,key,identifyCodeTF.text,@"identifyCode", nil];
        [helper verifyWithUserInfoMutaDictionary:dic withCmd:KJsonCheckIdentifyCodeRequestFC withVerifyBlock:^(NSString *verifyInfo, NSError *error) {
            if (!error) {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"LocRegisterFinish", "注册完成，请登录") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"LocOK", "确定"), nil];
                [alertView show];
            } else {
                CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 120/568.0*kScreenHeight);
                [self.view makeToast:verifyInfo duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
            }

        }];
    }
    
}
//重新发送验证码请求
- (void)resendRequest
{
    LoginHelper *lh = [[LoginHelper alloc] init];
    NSString *key = [lh loginUserNameKeyFromChain:userName];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:userName,key, nil];
    [helper verifyWithUserInfoMutaDictionary:dic withCmd:KJsonRequestCheckCodeFC withVerifyBlock:^(NSString *verifyInfo, NSError *error) {
        if (!error) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"LocRegisterFinish", "注册完成，请登录") delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"LocOK", "确定"), nil];
            [alertView show];
        } else {
            CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 120/568.0*kScreenHeight);
            [self.view makeToast:verifyInfo duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
        }
    }];

    // 60秒倒计时
    self.secondsLeft = kSeconds;
    [self countdown];
    // 重新发送请求之后，按钮不再可用
    resendButton.enabled = NO;
}

- (void)userName
{
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc]initWithIdentifier:@"password" accessGroup:nil];
    
    NSString *name = [wrapper objectForKey:(__bridge id)kSecAttrAccount];
    userName = name;
}

#pragma mark - 倒计时相关
// 倒计时
-(void)countdown
{
    if([timer isValid]){
        [timer invalidate];
    }
    if (self.secondsLeft) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateCounter:) userInfo:nil repeats:YES];
    }
}

// 更新倒计时显示
- (void)updateCounter:(NSTimer *)theTimer {
    NSString *resendMessage = @"重新发送验证码";
    if(self.secondsLeft > 1 ){
        self.secondsLeft -- ;
        [resendButton setTitle:[resendMessage stringByAppendingString:[NSString stringWithFormat:@"(%02d)",self.secondsLeft]]  forState:UIControlStateDisabled];
    }else{
        //设置重新发送按钮可交互
        resendButton.enabled = YES;
        [resendButton setTitle:[resendMessage stringByAppendingString:[NSString stringWithFormat:@"(%02d)",kSeconds]]  forState:UIControlStateDisabled];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)backToLoginVC
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (0 == buttonIndex) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

@end
