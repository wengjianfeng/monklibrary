//
//  StorageViewController.h
//  TableViewRowAnimationDemo
//
//  Created by workMac on 15/10/26.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StorageViewController : UIViewController

-(IBAction)HolidayButton:(id)sender;
-(IBAction)TemperatureReduce:(id)sender;
-(IBAction)TemperaturePlus:(id)sender;

@end
