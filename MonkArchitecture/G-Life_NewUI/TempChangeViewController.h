//
//  TempChangeViewController.h
//  TableViewRowAnimationDemo
//
//  Created by workMac on 15/10/26.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TempChangeViewController : UIViewController

-(IBAction)TemperatureReduce:(id)sender;
-(IBAction)TemperaturePlus:(id)sender;

@end
