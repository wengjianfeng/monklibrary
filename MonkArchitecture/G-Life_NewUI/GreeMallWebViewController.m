//
//  GreeMallWebView.m
//  G-Life_NewUI
//
//  Created by gree's apple on 15/12/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "GreeMallWebViewController.h"

@implementation GreeMallWebViewController
{
    UIWebView *webView;
}
- (void)viewDidLoad {
    [super viewDidLoad];

    webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    webView.scalesPageToFit = YES;
    webView.delegate = self;
    [self.view addSubview:webView];
    
    NSURL *url = [[NSURL alloc] initWithString:@"http://mall.gree.com/webapp/wcs/stores/servlet/zh-CN/greestore"];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [webView loadRequest:request];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - web Delegata
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)w
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)w
{
    
}

- (void)webView:(UIWebView *)w didFailLoadWithError:(nullable NSError *)error
{
    
}

@end
