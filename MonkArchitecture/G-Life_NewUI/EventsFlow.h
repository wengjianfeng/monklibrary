//
//  EventsFlow.h
//  G-Life_NewUI
//
//  Created by gree's apple on 15/10/15.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventsFlow : NSObject

- (void)start;
- (void)refresh;
- (void)clear;

@end
