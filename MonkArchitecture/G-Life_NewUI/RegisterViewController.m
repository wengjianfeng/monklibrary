//
//  RegisterViewController.m
//  G-Life_NewUI
//
//  Created by workMac on 15/5/29.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "RegisterViewController.h"
#import "VerifyHelper.h"
#import "WrapperHelper.h"
#import "AppContext.h"
#import "NSObject+Helper.h"
#import "ActivateBySMSViewController.h"
#import "ControlJsonFormatLists.h"
#import "UIView+Toast.h"
#import "CommunicateErrorData.h"

@interface RegisterViewController ()
{
    VerifyHelper *helper;
    CommunicateErrorData *_errorData;
}
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *userNameTF;
@property (weak, nonatomic) IBOutlet UITextField *pwdTF;
@property (weak, nonatomic) IBOutlet UITextField *confirmPwdTF;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.phoneNumberTF.delegate = self;
    self.userNameTF.delegate = self;
    self.pwdTF.delegate = self;
    self.confirmPwdTF.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([AppContext shareAppContext].connectSuccess == NO) {
        CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
        [self.view makeToast:@"无法连接服务器,请确保能接入互联网!" duration:10.0f position:[NSValue valueWithCGPoint:position]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)register:(UIButton *)sender {

    __block BOOL isDone = YES;
    __block NSString *info = @"";
    
    helper = [[VerifyHelper alloc] init];
    //验证手机号/邮箱及用户名
    [helper verifyWithEmailORPhone:self.phoneNumberTF.text userName:self.userNameTF.text withVerifyBlock:^(NSString *verifyInfo, NSError *error) {
        if(!error) {
            info = verifyInfo;
        } else {
            isDone = NO;
            CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
            [self.view makeToast:[NSString stringWithFormat:@"%@",verifyInfo] duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
        }
    }];
    
    //验证密码及重复密码
    [helper verifyWithPassword:self.pwdTF.text comparePassword:self.confirmPwdTF.text withVerifyBlock:^(NSString *verifyInfo, NSError *error) {
        if(!error) {
            
        } else {
            isDone = NO;
            CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
            [self.view makeToast:[NSString stringWithFormat:@"%@",verifyInfo] duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
        }
    }];
    
    // 全部合法
    if (isDone) {

        // 获取系统时间
        NSString *psw_md5 = [NSObject md5FromString:self.pwdTF.text];
        NSString *BindDate = [self getCurrentSystemTime];
        NSString *phoneOrEmail = [[NSString alloc]init];

        if([info isEqualToString:@"phoneNumberValid"])
        {
            phoneOrEmail =@"userMobile";
        }else
            if([info isEqualToString:@"eMailAddressValid"])
        {
            phoneOrEmail =@"userMail";

        }
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    self.userNameTF.text,@"userName",
                                    psw_md5,@"userPassword",
                                    self.phoneNumberTF.text,phoneOrEmail,
                                    BindDate,@"registeDate", nil];
        
        [helper verifyWithUserInfoMutaDictionary:dic withCmd:KJsonUserRegisterRequestFC withVerifyBlock:^(NSString *verifyInfo, NSError *error) {
            if(!error) {
                if ([verifyInfo isEqualToString:@"ActivateBySMS"]){
                    //激活
                    ActivateBySMSViewController *vc = [[ActivateBySMSViewController alloc]init];
                    [self showViewController:vc sender:self];
                } else {
                    UIAlertView *alertV = [[UIAlertView alloc]initWithTitle:nil message:verifyInfo delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
                    [alertV show];
                }
                WrapperHelper *wrpHelper = [[WrapperHelper alloc] init];
                [wrpHelper wrapperName:self.userNameTF.text Password:self.pwdTF.text];
                
            } else {
                //显示错误//代隽杰添加
                CGPoint position = CGPointMake(kScreenWidth * 0.5, kScreenHeight - 110/568.0*kScreenHeight);
                [self.view makeToast:verifyInfo duration:kToastDuration position:[NSValue valueWithCGPoint:position]];
            }
        }];
    }
}

//获取当前系统时间
-(NSString *)getCurrentSystemTime
{
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *now=[NSDate date];
    NSInteger unitFlags =  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger year = [comps year];
    NSInteger month = [comps month];
    NSInteger day = [comps day];
    NSInteger hour = [comps hour];
    NSInteger min = [comps minute];
    NSInteger sec = [comps second];
    return [NSString stringWithFormat:@"%ld-%ld-%ld %ld:%ld:%ld",(long)year,(long)month,(long)day,(long)hour,(long)min,(long)sec];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.phoneNumberTF resignFirstResponder];
    [self.userNameTF resignFirstResponder];
    [self.pwdTF resignFirstResponder];
    [self.confirmPwdTF resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
