//
//  CombinedInputView.h
//  Remote
//
//  Created by Zhang Hanying on 14/12/8.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CombinedInputView : UIView

-(instancetype)initWithOriginPoint:(CGPoint)origin imageName:(NSString *)imageName textFieldPlaceHolder:(NSString *)placeHolder textFieldIsSecureTextEntry:(BOOL)isSecureTextEntry;

@end
