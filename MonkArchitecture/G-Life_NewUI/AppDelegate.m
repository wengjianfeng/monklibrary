//
//  AppDelegate.m
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/3/23.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import "AppDelegate.h"
#import "NotificationLists.h"
#import "AppContext.h"
//#import "APService.h"  //极光推送

@interface AppDelegate ()
{
}
@end

@implementation AppDelegate
@synthesize flow;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstLaunch"])
    {
        NSString *oldPhoneUUID = [[[UIDevice currentDevice]identifierForVendor]UUIDString];
        NSString *newPhoneUUID = [oldPhoneUUID stringByReplacingOccurrencesOfString:@"-" withString:@""];//去除UUID中的“-”
        [[NSUserDefaults standardUserDefaults]setObject:newPhoneUUID forKey:@"UUID"];
    }
    
    [[UINavigationBar appearance]setBackgroundImage:[UIImage imageNamed:@"title_bg_2"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance]setBarStyle:UIBarStyleBlack];
    [[UINavigationBar appearance]setTranslucent:NO];
    
//    [UIViewController load];
    /*!
        flow
     */
    flow = [[EventsFlow alloc] init];
//    
    //取消红色按钮显示的个数
    UIApplication *app = [UIApplication sharedApplication];
    app.applicationIconBadgeNumber = 0;

    if ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0) {
        // 1.注册UserNotification,以获取推送通知的权限
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge categories:nil];
        [application registerUserNotificationSettings:settings];
        
        // 2.注册远程推送
        [application registerForRemoteNotifications];
    } else {
        [application registerForRemoteNotificationTypes:UIRemoteNotificationTypeNewsstandContentAvailability | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    }
    
    if (launchOptions[UIApplicationLaunchOptionsLocalNotificationKey]) {
        // 当被杀死状态收到本地通知时执行的跳转代码
        // [self jumpToSession];
        UILabel *redView = [[UILabel alloc] init];
        redView.backgroundColor = [UIColor redColor];
        redView.frame = CGRectMake(0, 100, 300, 400);
        redView.numberOfLines = 0;
        redView.text = [NSString stringWithFormat:@"%@", launchOptions];
        [self.window.rootViewController.view addSubview:redView];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [[NSNotificationCenter defaultCenter] postNotificationName:Flow_resignActive object:nil];

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.

    /*！
        _flow clear
    */
    [flow clear];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[NSNotificationCenter defaultCenter] postNotificationName:Flow_becomeActive object:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - register
-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(nonnull NSData *)deviceToken
{
    NSLog(@"Token: %@", deviceToken.description);
    
    NSString *Token = [deviceToken description];
    NSString *pushToken = [[[Token
                              stringByReplacingOccurrencesOfString:@"<" withString:@""]
                             stringByReplacingOccurrencesOfString:@">" withString:@""]
                            stringByReplacingOccurrencesOfString:@" " withString:@""];  //整理Token格式，去除空格和括号
    NSLog(@"pushToken:%@",pushToken);

    NSDictionary *myDeviceToken = [NSDictionary dictionaryWithObject:pushToken forKey:@"myToken"];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"pushTokenNotification"
                                                       object:self
                                                     userInfo:myDeviceToken];   //发送推送通知
    
    //获取终端设备标识，这个标识需要通过接口发送到服务器端，服务器端推送消息到APNS时需要知道终端的标识，APNS通过注册的终端标识找到终端设备。

}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSString *error_Token = [NSString stringWithFormat: @"%@", error];
    NSLog(@"--------------Failed to get token, error:%@", error_Token);
    //添加注册失败（未成功获取deviceToken）时的处理；
    
//    NSDictionary *myError = [NSDictionary dictionaryWithObject: error_Token forKey:@"tokenError"];
//    [[NSNotificationCenter defaultCenter]postNotificationName:@"pushTokenNotification"
//                                                       object:self
//                                                     userInfo:nil];     //发送推送通知失败
    
    
}

/*
 1.开启后台模式
 2.调用completionHandler,告诉系统你现在是否有新的数据更新
 3.userInfo添加一个字段:"content-available" : "1" : 只要添加了该字段,接受到通知都会在后台运行
 */
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    NSLog(@"RemoteNotification: %@", userInfo);
//    UIView *redView = [[UIView alloc] init];
//    redView.backgroundColor = [UIColor redColor];
//    redView.frame = CGRectMake(100, 100, 100, 100);
//    [self.window.rootViewController.view addSubview:redView];
    NSString *message = [[userInfo objectForKey:@"aps"]objectForKey:@"alert"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:message delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定",nil];
    [alert show];
    
    completionHandler(UIBackgroundFetchResultNewData);
}

@end
