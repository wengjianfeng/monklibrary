//
//  LeftMenuViewController.h
//  G-Life_NewUI
//
//  Created by Hanying Zhang on 15/4/1.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"
#import <CoreLocation/CoreLocation.h>

@interface LeftMenuViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,RESideMenuDelegate,CLLocationManagerDelegate>

- (BOOL)weathGetWeatherInformation;
- (void)refreshWeather;
- (void)refreshUserName;
@end
