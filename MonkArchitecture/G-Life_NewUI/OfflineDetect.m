//
//  OfflineDetect.m
//  G-Life_NewUI
//
//  Created by gree's apple on 5/11/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "OfflineDetect.h"
#import "NSTimer+Helper.h"
#import "NotificationLists.h"
@implementation OfflineDetect
{
    NSMutableDictionary *deviceLists;//设备列表
    NSTimer *timer;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        deviceLists = [[NSMutableDictionary alloc] initWithCapacity:1];
        //每包间隔20秒发送，所以理论上，第一包临界没收到，第2包也会20秒收到，预留4秒，30秒没收到认为丢包
        timer = [NSTimer scheduledTimerWithTimeInterval:45 block:^{
            //每隔45秒，所有信号量都减一
            [self detecting];
        } repeats:YES];
    }
    return self;
}

-(void)dealloc
{
    NSLog(@"%s dealloc",__FILE__);
    [timer invalidate];
    timer = nil;
}

-(void)startDetectOfflineWithMac:(NSString *)mac
{
    [deviceLists setObject:[NSNumber numberWithInt:3] forKey:mac];
}

-(void)detecting
{
    [deviceLists enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        int semaphore = [[deviceLists objectForKey:key] intValue];
        //NSLog(@"没收到设备第%d次回复",semaphore);
        semaphore -= 1;
        [deviceLists setObject:[NSNumber numberWithInt:semaphore] forKey:key];
        if (semaphore == 0) {
            [deviceLists removeObjectForKey:key];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:Flow_deviceOffline object:key];
            //认为丢失,离线通知出去
            NSLog(@"设备丢失了...............");
        }
    }];
}

@end
