//
//  OverridesPickerView.m
//  OverridesPickerView
//
//  Created by 翁 健峰 on 13-6-18.
//  Copyright (c) 2013年 翁 健峰. All rights reserved.
//

#import "OverridesPickerView.h"

@interface OverridesPickerView()
{
    NSMutableArray *Componet_DataSource;  //放所有Scroll
    NSMutableArray *Componet_LabelDataSource;   //放所有component的文字
    NSMutableArray *Componet_Page;  //放所有component位置，即数据
    NSMutableArray *Componet_Label; //放置所以uilabel
    
    NSInteger PicketOneValue;//同个值时，第二个不变
    
    BOOL isScrolling;
}
@property (nonatomic) UIScrollView *SubDataScroll;

@end

@implementation OverridesPickerView
@synthesize SubScrollValueOne,SubScrollValueTwo,SubScrollValueThree;
@synthesize PickerType;

@synthesize OverridesDelegate;
@synthesize SubDataScroll;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {

    }
    return self;
}

-(id)initWithFrame:(CGRect)frame WithDelegate:mDelegate AndType:(NSInteger)Type
{
    self = [super initWithFrame:frame];
    if (self)
    {
        Componet_DataSource = [[NSMutableArray alloc]initWithCapacity:10];
        Componet_LabelDataSource = [[NSMutableArray alloc]initWithCapacity:10];
        Componet_Page = [[NSMutableArray alloc]initWithCapacity:10];
        Componet_Label = [[NSMutableArray alloc]initWithCapacity:10];
        
        isScrolling = NO;
        
        OverridesDelegate = mDelegate;
        PickerType = Type;
        [self CreatPickerRow];
    }

    return self;
}

-(void)CreatPickerRow
{
    NSUInteger Component;
    Component = [OverridesDelegate numberOfComponentsInPickerView:self];//获得几个子控件
    
    for (int i = 0; i < Component; i++)
    {
        CGFloat float_x = 0;
        float_x = [OverridesDelegate pickerView:self widthForComponent:i];
        UIScrollView *DataScroll = [[UIScrollView alloc]init];;
        if (i == 0)
        {
            if (PickerType == 1)//温度选取器
            {
                DataScroll.frame = CGRectMake( 0, 2*self.frame.size.height/5, float_x,  self.frame.size.height/5);
            }
            else if (PickerType == 0)//普通选取器
            {
                DataScroll.frame = CGRectMake( 0, 2*self.frame.size.height/5, float_x,  self.frame.size.height/5 );
            }
        }
        else
        {
            DataScroll.frame = CGRectMake( self.frame.size.width/2 - 11, 2*self.frame.size.height/5, self.frame.size.width/2 - 10,  self.frame.size.height/5);
        }
        
        DataScroll.delegate = self;
        DataScroll.tag = 100 + i;
        [self addSubview:DataScroll];
        [self SetLabelToScroll:DataScroll];

        [Componet_DataSource addObject:DataScroll];
        
    }

    self.clipsToBounds = YES;   //裁剪整个视图多余的部分
}

-(void)setComponentImageView:(UIImageView *)aComponentImageView WithRow:(NSInteger)Row
{
    // 背景
    UIImageView *backGroundImage = aComponentImageView;
    //防错
    if (Row < 0 || Row >= 4)
    {
        return;
    }
    [self addSubview:backGroundImage];
    [self sendSubviewToBack:backGroundImage];
}

-(void)SetLabelToScroll:(UIScrollView *)scrollView
{
    //返回每个部件的row个数
    
    NSInteger NumberOfcomponent = [OverridesDelegate pickerView:self numberOfRowsInComponent:scrollView.tag-100];
    NSMutableArray *TitleString = [[NSMutableArray alloc]initWithCapacity:10];
    for (int i = 0; i < NumberOfcomponent; i ++) //多个滚轮时有用
    {
        NSString *str = [OverridesDelegate pickerView:self titleForRow:i forComponent:scrollView.tag-100];
        [TitleString addObject:str];    
    }
    [Componet_LabelDataSource addObject:TitleString];
    
    UILabel* label;
    NSMutableArray *TempArray;
    TempArray = [[NSMutableArray alloc]initWithCapacity:10];
    for (int i = 0; i < [[Componet_LabelDataSource objectAtIndex:scrollView.tag-100] count]; i++)
    {
        label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 70, scrollView.bounds.size.height)];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = [[Componet_LabelDataSource objectAtIndex:scrollView.tag-100] objectAtIndex:i];
        CGRect frame = label.frame;
        label.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        if (self.PickerType == 1)
        {
           frame.origin.y = (frame.size.height * i) - 5;   //需要偏移一点
            label.textAlignment = NSTextAlignmentLeft;
        }
        else if (self.PickerType == 0)
        {
            frame.origin.y = (frame.size.height * i);   //需要偏移一点
            frame.origin.x = scrollView.bounds.origin.x;
            frame.size.width = scrollView.bounds.size.width + 20;
        }

        label.frame = frame;
        [label setFont:[UIFont boldSystemFontOfSize:20.0f]];
        label.adjustsFontSizeToFitWidth = YES;

        [scrollView addSubview:label];
        [TempArray addObject:label];
    }
    [Componet_Label addObject:TempArray];
    
    scrollView.contentSize = CGSizeMake(scrollView.bounds.size.width, scrollView.bounds.size.height*[[Componet_LabelDataSource objectAtIndex:scrollView.tag-100] count]);

    scrollView.pagingEnabled = YES;
    scrollView.clipsToBounds = NO;  //不裁剪多余的
    scrollView.showsVerticalScrollIndicator = NO;

}

-(void)scrollviewCreatLabel:(UIScrollView *)scrollView
{
    NSUInteger Component;
    Component = [OverridesDelegate numberOfComponentsInPickerView:self];
    if (Component > 1)
    {
        UIScrollView *Scroll = [[UIScrollView alloc]init];
        Scroll = [Componet_DataSource objectAtIndex:1];
        
        for (int i = 0; i < [[Componet_Label objectAtIndex:1] count]; i++)
        {
            UILabel *label = [[Componet_Label objectAtIndex:1] objectAtIndex:i];
            [label removeFromSuperview];
        }
        [Componet_Label removeObjectAtIndex:1];
        
        NSInteger NumberOfcomponent = [OverridesDelegate pickerView:self numberOfRowsInComponent:Scroll.tag-100];
        NSMutableArray *TitleString = [[NSMutableArray alloc]initWithCapacity:10];
        [Componet_LabelDataSource removeAllObjects];
        for (int i = 0; i < NumberOfcomponent; i ++) //多个滚轮时有用
        {
            NSString *str = [OverridesDelegate pickerView:self titleForRow:i forComponent:Scroll.tag-100];
            [TitleString addObject:str];
        }
        [Componet_LabelDataSource addObject:TitleString];

        UILabel* label;
        NSMutableArray *TempArray;
        TempArray = [[NSMutableArray alloc]initWithCapacity:10];
        for (int i = 0; i < [[Componet_LabelDataSource objectAtIndex:0] count]; i++)
        {
            label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 56, Scroll.bounds.size.height)];
            label.backgroundColor = [UIColor clearColor];
            
            label.text = [[Componet_LabelDataSource objectAtIndex:0] objectAtIndex:i];
            CGRect frame = label.frame;
            label.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
            if (self.PickerType == 1)
            {
                frame.origin.y = (frame.size.height * i) - 5;   //需要偏移一点
                label.textAlignment = NSTextAlignmentLeft;

            }
            else if (self.PickerType == 0)
            {
                frame.origin.y = (frame.size.height * i);   //需要偏移一点
                frame.origin.x = Scroll.bounds.origin.x;
                frame.size.width = Scroll.bounds.size.width;
                label.textAlignment = NSTextAlignmentCenter;
            }
            
            label.frame = frame;
            [label setFont:[UIFont boldSystemFontOfSize:20.0f]];
            label.adjustsFontSizeToFitWidth = YES;

            [Scroll addSubview:label];
            [TempArray addObject:label];
        }
        [Componet_Label addObject:TempArray];
        
        CGFloat TempFrame;
        TempFrame = [[Componet_LabelDataSource objectAtIndex:0] count] ;
        Scroll.contentSize = CGSizeMake(Scroll.bounds.size.width, Scroll.bounds.size.height * TempFrame);
        [Componet_DataSource replaceObjectAtIndex:1 withObject:Scroll];
        
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    isScrolling = YES;

    if (scrollView.tag == 0)
    {
        PicketOneValue = [self selectedRowInComponent:0];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView //应该一出去就变颜色
{
    if (isScrolling)
    {
        [self returnOrChangeColor:NO withComp:(scrollView.tag -100)];
    }

}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView  //减速的时候执行
{
    NSInteger page = 0;
    page = [self selectedRowInComponent:(scrollView.tag - 100)];
    
    [OverridesDelegate pickerView:self didSelectRow:page inComponent:scrollView.tag - 100];
    [self scrollviewCreatLabel:scrollView];
    if (PickerType == 0)
    {
        if (scrollView.tag - 100 == 0)
        {
            PicketOneValue = page;
        }
    }

    if (isScrolling)
    
    [self returnOrChangeColor:YES withComp:(scrollView.tag - 100)];
    
    isScrolling = NO;
    
}

-(void)returnOrChangeColor:(BOOL)changeColor withComp:(NSUInteger)index
{
    NSArray *DataArray = [[NSArray alloc]initWithArray:[self getScrollSubView:[Componet_DataSource objectAtIndex:index]]];
    if (PickerType == 1)
    {
        for (int i = 0; i < [DataArray count]; i++)
        {
            UILabel *label;
            label = (UILabel *)[DataArray objectAtIndex:i];
            label.textColor = [UIColor grayColor];
        }
        
        if (changeColor == YES)
        {
            UILabel *label = [DataArray objectAtIndex:self.SubScrollValueOne];
            label.textColor = [UIColor whiteColor];
        }
    }
    else
    {
        for (int i = 0; i < [DataArray count]; i++)
        {
            UILabel *label;
            label = (UILabel *)[DataArray objectAtIndex:i];
            label.textColor = [UIColor grayColor];
        }
        if (index == 0)
        {
            if (changeColor == YES)
            {
                UILabel *label;
                label = [DataArray objectAtIndex:self.SubScrollValueOne];
                label.textColor = [UIColor blackColor];
                
                NSArray *DataArray = [[NSArray alloc]initWithArray:[self getScrollSubView:[Componet_DataSource objectAtIndex:1]]];
                for (int i = 0; i < [DataArray count]; i++)
                {
                    UILabel *label;
                    label = (UILabel *)[DataArray objectAtIndex:i];
                    label.textColor = [UIColor grayColor];
                }
                label = [DataArray objectAtIndex:self.SubScrollValueTwo];
                label.textColor = [UIColor blackColor];
            }
        }
        else
        {
            if (changeColor == YES)
            {
                UILabel *label;
                NSLog(@"aaaaaaaa %ld",(long)self.SubScrollValueTwo);
                label = [DataArray objectAtIndex:self.SubScrollValueTwo];
                
                label.textColor = [UIColor blackColor];
                //第一次进入，左栏全黑问题
                NSArray *DataArray = [[NSArray alloc]initWithArray:[self getScrollSubView:[Componet_DataSource objectAtIndex:0]]];
                for (int i = 0; i < [DataArray count]; i++)
                {
                    UILabel *label;
                    label = (UILabel *)[DataArray objectAtIndex:i];
                    label.textColor = [UIColor grayColor];
                }
                label = [DataArray objectAtIndex:self.SubScrollValueOne];
                label.textColor = [UIColor blackColor];
            }
        }

        

    }

}

-(NSArray *)getSelfViewSubView //获得子元素
{
    NSMutableArray *array = [[NSMutableArray alloc]initWithCapacity:10];
    
    for (id obj in [self subviews])
    {
        if ( [(UILabel *)obj isKindOfClass:[UILabel class]] )
        {
            [array addObject:obj];
        }
    }
    
    return array;
}

-(NSArray *)getScrollSubView:(UIScrollView *)scroll //获得子元素
{
    NSMutableArray *array = [[NSMutableArray alloc]initWithCapacity:10];
    
    for (id obj in [scroll subviews])
    {
        if ( [(UILabel *)obj isKindOfClass:[UILabel class]] )
        {
            [array addObject:obj];
        }
    }
    
    return array;
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event   //点击响应（全屏事件）
{
    CGFloat x = point.x;
    CGFloat y = point.y;

    if ( (y < 0 || y > self.frame.size.height) || (x < 0 || x > self.frame.size.width) )//全局点击事件
    {
        return nil;
    }
    else
    {
        int i = 0;
        while (i < [Componet_DataSource count])
        {
            //返回对应点击控件
            UIScrollView *view = [Componet_DataSource objectAtIndex:i];
            
            i += 1;
            
            if (self.PickerType == 1)
            {
                if ( (y > view.frame.origin.y && (y < view.frame.origin.y + view.frame.size.height) ) || (x > view.frame.origin.x && x < (view.frame.origin.x + view.frame.size.width) ) )
                {
                    return view;
                }
            }
            else if (self.PickerType == 0)
            {
                if ( (y >= self.bounds.origin.y && (y <= self.bounds.origin.y + self.bounds.size.height) ) && (x >= view.frame.origin.x && x <= (view.frame.origin.x + view.frame.size.width) ) )
                {
                    return view;
                }
                
            }
        }
    }
    
    return nil;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.*/

//- (void)drawRect:(CGRect)rect
//{
//    
//}

- (void)reloadAllComponents
{
    for (int i = 0; i < [Componet_DataSource count]; i++)
    {
        UIScrollView *scroll;
        scroll = [Componet_DataSource objectAtIndex:i];
        [self selectRow:[[Componet_Page objectAtIndex:i] intValue] inComponent:i animated:YES];
    }
}

- (void)reloadComponent:(NSInteger)component
{
    UIScrollView *scroll;
    scroll = [Componet_DataSource objectAtIndex:component];
    [self selectRow:[[Componet_Page objectAtIndex:component]intValue] inComponent:component animated:YES];
}

- (void)selectRow:(NSInteger)row inComponent:(NSInteger)component animated:(BOOL)animated  // scrolls the specified row to center.
{
    UIScrollView *scroll = [[UIScrollView alloc]init];
    
    scroll = [Componet_DataSource objectAtIndex:component];

    if (component == 1)
    {
        [self scrollviewCreatLabel:scroll];
    }
    
    CGRect frame = scroll.frame;
    frame.origin.y = frame.size.height * row;
    if (component == 0)
    {
        self.SubScrollValueOne = row;
    }
    else if (component == 1)
    {
        self.SubScrollValueTwo = row;
    }
    NSLog(@"row ...com:%d %d",(int)component,(int)row);
    [scroll scrollRectToVisible:frame animated:animated];
    
    [self returnOrChangeColor:YES withComp:(scroll.tag - 100)];
}

- (NSInteger)selectedRowInComponent:(NSInteger)component//实现查看当前选择的row
{
    UIScrollView *scroll;
    NSInteger page = 0;
    
    if (component == 0)
    {
        scroll = [Componet_DataSource objectAtIndex:0];
        
        page = scroll.contentOffset.y / scroll.bounds.size.height;
        self.SubScrollValueOne = page;
    }
    else if (component == 1)
    {
        scroll = [Componet_DataSource objectAtIndex:1];
        page = scroll.contentOffset.y / scroll.bounds.size.height;
        self.SubScrollValueTwo = page;
        NSLog(@"222row ...com:%d %d",(int)component,(int)page);

    }
    
    return page;
}

- (NSInteger)numberOfRowsInComponent:(NSInteger)component   //暂且不知作何处理
{
    
    return 0;
}

@end



