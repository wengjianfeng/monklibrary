//
//  CombinedInputView.m
//  Remote
//
//  Created by Zhang Hanying on 14/12/8.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "CombinedInputView.h"

@implementation CombinedInputView

#define kBorderWidth 30 //头像和TextField距离屏幕两边的距离

-(instancetype)initWithOriginPoint:(CGPoint)origin imageName:(NSString *)imageName textFieldPlaceHolder:(NSString *)placeHolder textFieldIsSecureTextEntry:(BOOL)isSecureTextEntry
{
    if (self = [super init]) {
        self.frame = (CGRect){origin, {kScreenWidth, kScreenHeight*0.085} };
        self.backgroundColor = [UIColor whiteColor];
        
        //image
        UIImage *iconImage = [UIImage imageNamed:imageName];
        CGFloat iconImageWidth = iconImage.size.width * 0.5;
        CGFloat iconImageHeight = iconImage.size.height * 0.5;
        //imageView
        UIImageView *iconView = [[UIImageView alloc]init];
        CGFloat iconViewWidth = 25 / 320.0 * kScreenWidth;
        CGFloat iconViewHeight = 25 / 568.0 * kScreenHeight;
        //使图片保持原比例，较长的一边值为25
        if (iconImageWidth > iconImageHeight) {
            iconViewHeight = iconImageHeight * iconViewWidth / iconImageWidth;
        }else if (iconImageWidth < iconImageHeight){
            iconViewWidth = iconImageWidth * iconViewHeight / iconImageHeight;
        }
        //设置ImageView大小和图片
        iconView.frame = CGRectMake(kBorderWidth/320.0*kScreenWidth, 0.5*(self.frame.size.height-iconViewHeight), iconViewWidth, iconViewHeight);
        iconView.image = iconImage;
        [self addSubview:iconView];
        //TextField
        UITextField *textField = [[UITextField alloc]init];
        textField.borderStyle = UITextBorderStyleRoundedRect;
        CGFloat textFieldHeight = 25;
        CGFloat textzFieldOriginX = 60/320.0*kScreenWidth;
        textField.frame = CGRectMake(textzFieldOriginX, CGRectGetMidY(iconView.frame)-textFieldHeight*0.5, kScreenWidth-textzFieldOriginX-kBorderWidth+5, textFieldHeight);
        textField.font = [UIFont systemFontOfSize:14];
        textField.textAlignment = NSTextAlignmentLeft;
        textField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        textField.textColor = [UIColor blackColor];
        textField.placeholder = placeHolder;
        textField.returnKeyType = UIReturnKeyDone;
        if (isSecureTextEntry) {
            [textField setSecureTextEntry:YES];
        }
        textField.tag = 1003;
        [self addSubview:textField];
    }
    return self;
}

@end
