//
//  AppDelegate.h
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/3/23.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventsFlow.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) EventsFlow *flow;

@end

