//
//  LeftMenuTableViewCell.h
//  G-Life_NewUI
//
//  Created by Zhang Hanying on 15/4/2.
//  Copyright (c) 2015年 Gree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageLabelOnlyTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellImageView;
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@end
