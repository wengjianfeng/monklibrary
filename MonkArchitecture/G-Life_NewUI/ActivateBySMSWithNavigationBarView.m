//
//  ActivateBySMSWithNavigationBarView.m
//  Remote
//
//  Created by Zhang Hanying on 14/11/19.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#define isiOS7OrLater [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height


#import "ActivateBySMSWithNavigationBarView.h"
#import "UIImage+ImageWithColor.h"
#import "CombinedInputView.h"

@implementation ActivateBySMSWithNavigationBarView

-(instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1.0];
        
        //验证码输入
        CGPoint identifyCodeViewOrigin;
        if (isiOS7OrLater) {
            identifyCodeViewOrigin = CGPointMake(0, 64);
        }else{
            identifyCodeViewOrigin = CGPointMake(0, 0);
        }
        UIView *identifyCodeView = [[CombinedInputView alloc]initWithOriginPoint:identifyCodeViewOrigin imageName:@"1-14" textFieldPlaceHolder:@"验证码" textFieldIsSecureTextEntry:NO];
        [self addSubview:identifyCodeView];
        UITextField *identifyCodeTF = (UITextField*)[identifyCodeView viewWithTag:1003];
        identifyCodeTF.tag = 1003;
        
        // 确定按钮
        UIButton *confirmButton = [[UIButton alloc]init];
        CGFloat confirmButtonWidth = 270;
        CGFloat confirmButtonHeight = 42*kScreenHeight/568;
        if (isiOS7OrLater) {
            confirmButton.frame = CGRectMake((kScreenWidth - confirmButtonWidth)*0.5, 167*kScreenHeight/568, confirmButtonWidth, confirmButtonHeight);
        }else{
            confirmButton.frame = CGRectMake((kScreenWidth - confirmButtonWidth)*0.5, (167-64)*kScreenHeight/568, confirmButtonWidth, confirmButtonHeight);
        }
        [confirmButton setTitle:@"确认" forState:UIControlStateNormal];
        confirmButton.titleLabel.font = [UIFont boldSystemFontOfSize:22];
        [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [confirmButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        UIImage *image = [UIImage imageNamed:@"1-34"];
        [confirmButton setBackgroundImage:image forState:UIControlStateNormal];
        [self addSubview:confirmButton];
        confirmButton.tag = 1001;
           
        // 重新发送按钮
        UIButton *resendButton = [[UIButton alloc]init];
        CGFloat resendButtonWidth = 270;
        CGFloat resendButtonHeight = 42*kScreenHeight/568;
        if (isiOS7OrLater) {
            resendButton.frame = CGRectMake((kScreenWidth - resendButtonWidth)*0.5, 222*kScreenHeight/568, resendButtonWidth, resendButtonHeight);
        }else{
            resendButton.frame = CGRectMake((kScreenWidth - resendButtonWidth)*0.5, (222-64)*kScreenHeight/568, resendButtonWidth, resendButtonHeight);
        }
        
        [resendButton setTitle:@"重新发送验证码" forState:UIControlStateNormal];
        resendButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [resendButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateDisabled];
        [resendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [resendButton setImageEdgeInsets:UIEdgeInsetsMake(10, 107, 10, 107)];
        [resendButton setBackgroundImage:[UIImage imageNamed:@"1-34"] forState:UIControlStateNormal];
        [resendButton setBackgroundImage:[UIImage imageNamed:@"1-35"] forState:UIControlStateDisabled];
        [self addSubview:resendButton];
        resendButton.tag = 1002;
    }
    return self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end

