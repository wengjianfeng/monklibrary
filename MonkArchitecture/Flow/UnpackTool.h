//
//  UnpackTool.h
//  NewworkDemo
//
//  Created by gree's apple on 21/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
/************************************************************************************/
/*     针对粘包的拆包处理                                                              */
/************************************************************************************/
@interface UnpackTool : NSObject

+ (id)shareInstance;

- (NSMutableArray *)unpackWithData:(NSData *)data;

@end
