//
//  AcJsonData_LAN.m
//  NewworkDemo
//
//  Created by gree's apple on 12/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "AcJsonData_Search.h"

#import "AppContext.h"
#import "VirtualDeviceForBroadcastBL.h"
#import "VirtualDeviceBL.h"
#import "GEDeviceBLRepository.h"

@implementation AcJsonData_Search
{
    AppContext *_ctx;
    VirtualDeviceForBroadcastBL *_bl;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _ctx = [AppContext shareAppContext];
    }
    return self;
}

-(id)initWithBL:(id<VirtualDeviceProtocol>)BL
{
    self = [super init];
    if (self) {
        _bl = (VirtualDeviceForBroadcastBL *)BL;//这里其实有问题，也存在为VirtualDeviceBL对象
        
        _ctx = [AppContext shareAppContext];
    }
    return self;
}

-(void)parseFromJson:(NSDictionary *)json
{
    id vd = [json objectForKey:@"validData"];
    
    if ([vd isKindOfClass:[NSArray class]]) {
        //JSON远程多台空调的情况
        [self remote:json];
    } else if ([[vd allKeys] containsObject:@"productIP"])
    {
        //JSON近程反馈
        NSDictionary *deviceinfo = [json objectForKey:@"validData"];
        NSString *mac = [json objectForKey:@"originDeviceUUID"];
        /////
        NSString *ip = [deviceinfo objectForKey:@"productIP"];
        NSString *port = [deviceinfo objectForKey:@"productPort"];
        NSString *mask = [deviceinfo objectForKey:@"productMask"];
        NSString *gw = [deviceinfo objectForKey:@"productGW"];
        NSString *dns = [deviceinfo objectForKey:@"productDNS"];
        
        NSString *deviceType = [json objectForKey:@"deviceType"];

        NSString *nameStr = [deviceinfo objectForKey:@"productName"];
        if ([nameStr isEqualToString:@"卧室"])
        {
            NSArray *languages = [NSLocale preferredLanguages];
            NSString *currentLanguage = [languages objectAtIndex:0];
            if(![currentLanguage isEqualToString:@"zh-Hans"])
            {
                nameStr = NSLocalizedString(@"LocBedroom", @"卧室");
            }
        }
        /////
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:10];
        [dic setValue:ip forKey:@"mWifiInfo.ip"];
        [dic setValue:port forKey:@"mWifiInfo.port"];
        [dic setValue:mask forKey:@"mWifiInfo.mask"];
        [dic setValue:gw forKey:@"mWifiInfo.gate"];
        [dic setValue:dns forKey:@"mWifiInfo.dns"];
        [dic setValue:mac forKey:@"mWifiInfo.mac"];
        [dic setValue:nameStr forKey:@"deviceName"];
        [dic setValue:deviceType forKey:@"deviceType"];
        [dic setValue:[NSNumber numberWithBool:YES] forKey:@"mWiFiNetWorkStatus.isLAN"];
        
        [_bl configContextWithDictionary:dic];   //修改GEWiFiContext
        [_bl sendMessage:BLMessageEvt_BroadcastDone];
    } else {
        //JSON远程反馈
        [self remote:json];
    }
    
}

- (void)remote:(NSDictionary *)ajson
{
    NSArray *arr = [ajson objectForKey:@"validData"];
    if ([[ajson allKeys] containsObject:@"statusCode"])
    {
        NSLog(@"解析数据错误");
        return;
    }
    
    for (int i = 0; i < [arr count]; i++)
    {
        NSDictionary *deviceinfo = [arr objectAtIndex:i];
        
        NSString *mac = [deviceinfo objectForKey:@"productUUID"];
        NSString *nameStr = [deviceinfo objectForKey:@"productName"];
        NSString *deviceType = [deviceinfo objectForKey:@"productType"];
        BOOL online = [[deviceinfo objectForKey:@"online"] isEqualToString:@"true"]?YES:NO;
        
        if ([nameStr isEqualToString:@"卧室"])
        {
            NSArray *languages = [NSLocale preferredLanguages];
            NSString *currentLanguage = [languages objectAtIndex:0];
            if(![currentLanguage isEqualToString:@"zh-Hans"])
            {
                nameStr = NSLocalizedString(@"LocBedroom", @"卧室");
            }
        }
        
        ///
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:3];
        [dic setValue:mac forKey:@"mWifiInfo.mac"];
        [dic setValue:nameStr forKey:@"deviceName"];
        [dic setValue:deviceType forKey:@"deviceType"];
        [dic setValue:[NSNumber numberWithBool:online] forKey:@"mWiFiNetWorkStatus.isOnline"];
        [dic setValue:[NSNumber numberWithBool:YES] forKey:@"isBind"];
        
        VirtualDeviceBL *vdbl = (VirtualDeviceBL *)[[GEDeviceBLRepository shareInstance] findVirtualDeviceByMac:mac];
        if (!vdbl) {
            vdbl = [[VirtualDeviceBL alloc] initWithType:json];
        }
        
        [vdbl configContextWithDictionary:dic];
        [vdbl sendMessage:BLMessageEvt_DeviceCreatDone];
    }
}

@end
