//
//  AcJsonData_LAN.h
//  NewworkDemo
//
//  Created by gree's apple on 12/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//  局域网JSON格式通信数据包

#import <Foundation/Foundation.h>
#import "AcJsonData.h"

@interface AcJsonData_Search : AcJsonData

-(id)initWithBL:(id<VirtualDeviceProtocol>)BL;

-(void)parseFromJson:(NSDictionary *)json;

@end
