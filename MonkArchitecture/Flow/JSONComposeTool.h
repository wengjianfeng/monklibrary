//
//  JSONCompose.h
//  Remote
//
//  Created by Zhang Hanying on 14-9-20.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONComposeTool : NSObject

+(id)JSONComposedWithDictionary:(NSDictionary*)dictionary;       // 压缩validData
+(id)JSONStringComposedWithDictionary:(NSDictionary*)dictionary; // 整体压缩
+(id)JSONStringComposedWithArray:(NSArray *)array;

@end
