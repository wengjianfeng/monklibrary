//
//  NetWorkData_RmFactory.h
//  Remote
//
//  Created by gree's apple on 18/9/14.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "NetWorkData_Factory.h"

typedef void (^RSAExchangeBlockComple) (void);//RSA交互完成
typedef void (^DeviceSearchComple) (NSString *Mac);//设备搜索
typedef void (^DeviceBindConfig) (void);//查询绑定并绑定

@interface NetWorkData_RmFactory : NetWorkData_Factory
{
//    typeID NWid;
    
}
-(void)setBlock:(void (^)(void))RSACompleBlock;
-(void)setDeviceSearchComple:(void (^)(NSString *))deviceSearchCompleBlock;
-(void)setDeviceBindConfig:(void (^)(void))deviceBindConfigBlock;

-(id)DataFormJSON:(id)json;

@end
