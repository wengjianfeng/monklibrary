//
//  VirtualDevice.m
//  NewworkDemo
//
//  Created by gree's apple on 24/8/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "VirtualDeviceBL.h"
#import "GEDeviceBLRepository.h"
#import "ControlHexFormatLists.h"
#import "ControlJsonFormatLists.h"
#import "GEHexFactory.h"
#import "GEJsonFactory.h"
#import "AppContext.h"
#import "VirtualServerBL.h"
#import "DeviceType.h"

//#import "GEAirCDeviceInfoBL.h"
//#import "GERFDeviceInfoBL.h"

#import "NotificationLists.h"

#import "OfflineDetect.h"
@implementation VirtualDeviceBL
{
    GETCPSocket *tcp;
    GEUDPSocket *udp;
    RTTCPDAO *tcpDao;
    RTUDPDAO *udpDao;
    
    OfflineDetect *_detect;
    BOOL isRemote;
}

@synthesize _context,_nw,_command,identify;

#pragma mark-
#pragma mark lift cycle
-(instancetype)initWithType:(protocolType)type
{
    self = [self init];
    if (self) {
        self._mType = type;
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _nw = [GENetworking shareInstance];
        _context = [[GEWiFiContext alloc] init];
        _command = [[Command alloc] init];
        
        tcpDao = [RTTCPDAO shareInstance];
        udpDao = [RTUDPDAO shareInstance];
        
    }
    
    return self;
}

-(void)dealloc
{
    NSLog(@"%s dealloc",__FILE__);
}

#pragma mark-
#pragma mark message
-(void)sendMessage:(BLMessageEvt)msg
{
    NSString *mac = [NSString stringWithFormat:@"%s",_context.mWifiInfo.mac];

    if (msg == BLMessageEvt_DeviceCreatDone) {
        /****************************************/
        /*     进到这里，说明是走JSON远程服务器流程  */
        /****************************************/

        /****************创建设备*****************/
        GEDeviceBLRepository *rp = [GEDeviceBLRepository shareInstance];
        id<VirtualDeviceProtocol> obj = [rp findVirtualDeviceByMac:mac];
        
        self.identify = mac;    //需要手动设置关键的MAC

        if (!obj) {
            isRemote = YES;

            [rp addVirtualDeviceObj:self];
            
            /*
                @end 如果是离线状态，那就没必要请求状态
             */
            if (self._context.mWiFiNetWorkStatus.isOnline == YES) {

                //共享远程socket
                [_nw set_tcpDao:tcpDao];
                _nw.identify = identify;//读取通过"SV" + "MAC"方式
                tcp = [_nw cloneTcpSocketMap:[[AppContext shareAppContext] servermac] andType:GETcpTypeOfPTS];
            }
            
        }else {
            //只有远程才到这
            [obj._context setValue:[NSNumber numberWithBool:self._context.mWiFiNetWorkStatus.isOnline] forKey:@"mWiFiNetWorkStatus.isOnline"];//在线
            [obj._context setValue:[NSNumber numberWithBool:self._context.isBind] forKey:@"isBind"];//绑定
        }
        
        if (self._context.mWiFiNetWorkStatus.isOnline) {
            //请求设备控制状态 (在线才发送状态查询)
            NSLog(@"状态查询 %@",mac);
            //当远程在线时也查询，担心TCP建立失败！如果后续有要求，添加条件
//            if (!self._context.mWiFiNetWorkStatus.isLAN) {
                [self stuffFormatMutableDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"01",@"lookupType", nil]];
                [self excuteWithTcpCmd:KJsonStateEnquiryFC];
//            }
            /*
             @function 添加设备的状态数据（近程TCP连接后也构建）
             */
            [self storeDeviceInfoBL];
        }

        /****************创建设备*****************/
    } else if (msg == BLMessageEvt_BroadcastDone) {
        /****************************************/
        /*   这肯定是TCP发送的入网宣告，无意义       */
        /****************************************/
    } else if (msg == BLMessageEvt_UpdataSuccess) {
        NSLog(@"%s 数据解析完成/n",__FILE__);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:Flow_updataSuccess object:nil];

    //STEP2:断网检测；如果秒内（检测板20/s发一次）没收到则认为设备丢失！**仅对近程(JSON)有效**
    if ((self._mType == json) && !isRemote) {
//        [self detectDeviceOfflineWithMac:mac];
    }

}

#pragma mark-
#pragma mark GEWiFiContext
-(GEWiFiContext *)configContextWithDictionary:(NSDictionary *)dic
{
    [_context setValuesForKeysWithDictionary:dic];
    
    return _context;
}

#pragma mark-
#pragma mark Network
-(void)creatTcpWithIpAndPortContext:(NSDictionary *)context
{
    [_nw set_tcpDao:tcpDao];
    _nw.identify = identify;
    
    tcp = [_nw creatTcpWithDAO:tcpDao
          withIpAndPortContext:context
                      withType:GETcpTypeOfPTP
                  withDelegate:self];

    tcp._isAlive = NO;
}

-(void)creatUdpWithIpAndPortContext:(NSDictionary *)context
{
    [_nw set_udpDao:udpDao];
    
    udp = [_nw creatUdpWithDAO:udpDao withIpAndPortContext:@{
                                                              @"IP" : @"255.255.255.0",
                                                              @"Port" : [NSNumber numberWithInt:9090]
                                                              }
                                              withDelegate:self];
}

#pragma mark-
#pragma mark command
-(void)excuteWithTcpCmd:(NSString *)cmd
{
    [AppContext shareAppContext].tempMac = identify; //设置mac
    
    id<GEFormatFactory> fac;
    NSData *data = nil;
    if (self._mType == hex) {
        fac = [[GEHexFactory alloc] init];
        data = [_command excuteWithIdentify:cmd withFactory:fac withBL:self];
    }else if (self._mType == json) {
        
        [self stuffNecessaryKey:cmd];
        
        if (!isRemote) {
            NSLog(@"近程JSON");
            fac = [[GEJsonFactory alloc] init];
            data = [_command excuteWithIdentify:cmd withFactory:fac withContext:sendingBuffer];
        }else {
            NSLog(@"远程JSON");
            //取出VirtualServerBL,然后数据通道通过它处理，包括加密、解密、数据通道等，此类制作状态存储
            if ([cmd isEqualToString:KJsonControlFC] || [cmd isEqualToString:KJsonStateEnquiryFC]) {
                [sendingBuffer setObject:self._context.deviceType forKey:@"productType"];
            }
            VirtualServerBL *bl = (VirtualServerBL *)[[GEDeviceBLRepository shareInstance]
                                                      findVirtualDeviceByMac:[[AppContext shareAppContext] servermac]];
            bl.sendingBuffer = sendingBuffer;
            [bl excuteWithTcpCmd:cmd];
            return;
        }

    }
    
    [tcp writeData:data];
    NSLog(@"excuteWithTcpCmd %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
}

-(void)excuteWithUdpCmd:(NSString *)cmd
{
    
}

#pragma mark-
#pragma mark Stuff
- (void)stuffFormatMutableDictionary:(NSMutableDictionary *)dictionary
{
    [sendingBuffer removeAllObjects];
    sendingBuffer = dictionary;
}

#pragma mark-
#pragma mark TcpDelegate
- (BOOL)GEonSocketWillConnect:(GETCPSocket *)sock
{
    NSLog(@"%s sock WillConnect %@",__FILE__,sock.identify);
    return YES;
}

- (void)GEonSocket:(GETCPSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    NSLog(@"%s sock didConnect %@",__FILE__,sock.identify);
    
    if (self._mType == hex) {
        [self excuteWithTcpCmd:KHexDataSync];//需要发送同步帧
    } else if (self._mType == json) {
        
        [self stuffFormatMutableDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"01",@"lookupType",identify,@"objectUUID", nil]];
        [self excuteWithTcpCmd:KJsonQueryDeviceStatus];//查询设备状态
    }
}

- (void)GEonSocket:(GETCPSocket *)sock didReadData:(NSData *)data
{
    unsigned char *buf;
    buf = (unsigned char *)[data bytes];
    
    /********创建设备实体********/
    [self storeDeviceInfoBL];
    [AppContext shareAppContext].tempMac = [self identify];//每个对象只保留了一个mac，故无需映射(以为需通过IP找到对应储存的mac)
    /********创建设备实体********/
    
    if (buf[0] == 0x7E && buf[1] == 0x7E) {
        NSLog(@"%s sock didReadData %@",__FILE__,data);

        GEHexFactory *fac = [[GEHexFactory alloc] init];
        [_command parseWithData:data withFactory:fac withBL:self];
    }else{
        //可能是加密的json or other
        NSLog(@"%s sock didReadData %@",__FILE__,[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
        
        GEJsonFactory *fac = [[GEJsonFactory alloc] init];
        [_command parseWithData:data withFactory:fac withBL:self];
    }
}

- (void)GEonSocketDidDisconnect:(GETCPSocket *)sock
{
//    //如果近程建立失败，还原对应标志
//    isRemote = YES;

    [[GEDeviceBLRepository shareInstance] removeByMac:sock.identify];
    NSLog(@"重新连接tcp连接不上的设备 %@",sock.identify);
}

#pragma mark-
#pragma mark UdpDelegate
- (BOOL)onUdpSocket:(GEUDPSocket *)sock didReceiveData:(NSData *)data fromHost:(NSString *)host port:(UInt16)port
{
    NSLog(@"Udp didReceiveData %@",data);
    return YES;
}

#pragma mark-
#pragma mark Setter and Getter


#pragma mark-
#pragma mark private
- (void)storeDeviceInfoBL
{
    GEDeviceBLRepository *rp = [GEDeviceBLRepository shareInstance];
    //搜索到只需创建一次，之后都是修改其内容
    id <GEDeviceInfoProtocol> obj = [rp findDeviceInfoDBByMac:[self identify]];/////XXXXXYYYY
    if (!obj) {
        obj = [self deviceInfoWithType:self._context.deviceType];
        //创建ID的状态实体，并初始化
//        [obj createByMac:[self identify]];
        //保存BL
        [rp addDeviceInfoDBObj:obj byMac:[self identify]];
    }
}

- (void)stuffNecessaryKey:(NSString *)cmd
{
    //自动添加uuid
    if (![[sendingBuffer allKeys] containsObject:@"objectUUID"]) {
        [sendingBuffer setValue:identify forKey:@"objectUUID"];
    }

    if ([cmd isEqualToString:KJsonDevicesControlsFC]) {
        if (![[sendingBuffer allKeys] containsObject:@"Status_Number"]) {
            [sendingBuffer setValue:[NSString stringWithFormat:@"%ld",(long)self._context.statusNumber] forKey:@"Status_Number"];
        }
    }
}

- (id <GEDeviceInfoProtocol>)deviceInfoWithType:(NSString *)type
{
    id<GEDeviceInfoProtocol> obj = nil;
    if ([type isEqualToString:kACDeviceType]) {
//        obj = [[GEAirCDeviceInfoBL alloc] init];
    } else if ([type isEqualToString:kRFDeviceType]) {
//        obj = [[GERFDeviceInfoBL alloc] init];
    }
    return obj;
}

#pragma mark-
#pragma mark deviceOffline
- (void)detectDeviceOfflineWithMac:(NSString *)mac {
    if (!_detect) {
        _detect = [[OfflineDetect alloc] init];
    }
    [_detect startDetectOfflineWithMac:mac];
}

- (void)detectDeviceOfflineNotificate:(NSNotification *)noti
{
    NSString *mac = [noti object];
    
    [[GEDeviceBLRepository shareInstance] removeByMac:mac];
    NSLog(@"由于离线删除的mac：%@",mac);
    [[NSNotificationCenter defaultCenter] postNotificationName:Flow_updataSuccess object:nil];
}
@end




