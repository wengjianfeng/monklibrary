//
//  JSONProcessing.h
//  Remote
//
//  Created by Zhang Hanying on 14-9-23.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONParseTool.h"
#import "JSONComposeTool.h"
#import "PackDictionary.h"

@interface JSONProcessing : NSObject

/* 传入ValidData字典和功能码，返回要发送的json数组。
 * 若有效数据长度较小，只需发送一个json包，则此json包作为数组是唯一元素。
 * 功能码用来确定vallidData以外的其他参数
 */
+(NSArray*)JSONWithValidData:(id)validData functionCode:(NSString*)functionCode sequenceNum:(NSString *)seqNum;

// 把收到的Json数组解析成一个字典
+(NSDictionary*)dictionaryParsedFromJsonArray:(NSArray*)jsonArray;

@end
