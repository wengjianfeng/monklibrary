//
//  GEDeviceInfoProtocol.h
//  NewworkDemo
//
//  Created by gree's apple on 6/10/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageLists.h"
@protocol GEFeedDAO;

@protocol GEDeviceInfoProtocol <NSObject>
/********************************************************/
/* 以下的增删查改均是构建数据访问对象DAO的操作                 */
/* 1、所谓的设备列表，其实就是设备的状态数据                   */
/* 2、由于状态数据结构不暴露在外部，故操作状态数据均通过DAO      */
/* 3、故以下的方法均是处理DAO                               */
/********************************************************/
@optional

-(void) sendMessage:(BLMessageEvt)msg;

-(void) createByMac:(NSString *)mac;

-(void) removeByMac:(NSString *)mac;

-(void) modifyByMac:(NSString *)mac andContext:(NSString *)context;

-(id <GEFeedDAO>) findByMac:(NSString *)mac;

//-(NSMutableDictionary *) findAllDevice;


- (void) setStuff:(id)stuff forKey:(id)key;

@end
