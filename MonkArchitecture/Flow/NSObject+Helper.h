//
//  NSObject+Helper.h
//  NewworkDemo
//
//  Created by gree's apple on 23/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NSObject_Helper)

// 生成字符串的md5值
+ (NSString *)md5FromString:(NSString *)string;



@end
