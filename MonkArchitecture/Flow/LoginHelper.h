//
//  LoginHelper.h
//  G-Life_NewUI
//
//  Created by gree's apple on 21/10/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^loginVerifyBlock)(NSString *info, NSError *error);

@interface LoginHelper : NSObject

- (NSString *)loginUserNameKeyFromChain:(NSString *)userInfo; //自动登录判断账号信息
- (void)loginVerifyHelperName:(NSString *)name password:(NSString *)psw withblock:(loginVerifyBlock)block;  //账号登录

@end
