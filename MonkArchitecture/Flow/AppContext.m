//
//  GetIpV4.m
//  RemoteController
//
//  Created by WJF-Monk  330694 on 13/11/12.
//  Copyright (c) 2012 WJF-Monk  330694. All rights reserved.
//

#import "AppContext.h"
#import "IPAdress.h"
#import <ifaddrs.h>
#import <arpa/inet.h>
#import <sys/socket.h>
#import <net/if_dl.h>
#import <sys/xattr.h>

#import "Reachability.h"
#import "NotificationLists.h"

#define VERSION_INFO @"1.0.5" //软件版本

@implementation AppContext
{
    NSInteger nwStatus;
    Reachability *reachability;
}
@synthesize userName = _userName;

+(AppContext *)shareAppContext
{
    static AppContext *ctx = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ctx = [[self alloc] init];
    });
    return ctx;
}

-(instancetype)init
{
    self = [super init];
    if (self) {

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startReachability) name:Flow_becomeActive object:nil];
        // 监听通知
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNetworkStatus) name:kReachabilityChangedNotification object:nil];
        
        nwStatus = -1;
        [self startReachability];

        [self clearUserInfo];
    }
    return self;
}

-(NSString *)lh
{
    NSString *ip = @"192.168.1.0";
    
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    success = getifaddrs(&interfaces);
    if (success == 0) {
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    ip = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    freeifaddrs(interfaces);

    return ip;
}

-(NSInteger)hexlp
{
    return 2048;
}

-(NSInteger)jslp
{
    return 1024;
}

-(NSString *)bch
{
    struct in_addr ip;
    struct in_addr mask;
    ip.s_addr = inet_addr([self.lh cStringUsingEncoding:NSASCIIStringEncoding]);
    mask.s_addr = inet_addr([@"0.0.0.255" cStringUsingEncoding:NSASCIIStringEncoding]);
    ip.s_addr =  mask.s_addr | ip.s_addr;
    return [NSString stringWithUTF8String:inet_ntoa(ip)];
}

-(NSInteger)bcp
{
    return 988;
}

-(NSInteger)bcJSp
{
    return 988; //文档写着998，实际是988....wtf!!!
}

-(NSInteger)slp
{
    return 6000;
}

-(NSString *)sh
{
    return @"125.88.19.72";
}

-(NSInteger)sp
{
    return 9090;
}

-(NSString *)servermac
{
    return @"ffffffffffffffffffffffffffffffff";
}

-(NSString *)appv
{
    return VERSION_INFO;
}

-(NSString *)osv
{
    return [[UIDevice currentDevice] systemVersion];
}

-(NSString *)osn
{
    return [[UIDevice currentDevice] systemName];
}

-(NSString *)teln
{
    return [[UIDevice currentDevice] name];
}

-(NSString *)uuid
{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

-(NSInteger)networkStatus
{
    if (nwStatus == 1 || nwStatus == -1) {
        return nwStatus;
    }else{
        return 0;
    }
}

-(void)setUserMail:(NSString *)userMail
{
    if (![userMail isEqual:[NSNull null]]) {
        _userMail = userMail;
    }
}

-(void)setUserTele:(NSString *)userTele
{
    if (![userTele isEqual:[NSNull null]]) {
        _userTele = userTele;
    }
}

-(void)setTempMac:(NSString *)tempMac
{
    _currentMac = tempMac;
    _tempMac = tempMac;
}

-(void)setUiIdentify:(NSString *)g
{
    _currentMac = g;
    _uiIdentify = g;
}

-(void)clearUserInfo
{
    _userName = @"未登录";
    _userTele = @"";
    _userMail = @"";
    
    _loginSuccess = NO;
    _connectSuccess = NO;
}

#pragma mark-
#pragma mark Reachability
- (void)startReachability
{
    reachability = [Reachability reachabilityWithHostName:@"www.baidu.com"];
    [reachability startNotifier];
}

- (void)getNetworkStatus
{
    // 判断当前网络是否是WiFi
    if ([Reachability reachabilityForLocalWiFi].currentReachabilityStatus != NotReachable) {
        NSLog(@"是wifi且能够链接");
        nwStatus = 1;
        // 判断当前网络是否是手机自带网络
    } else if ([Reachability reachabilityForInternetConnection].currentReachabilityStatus != NotReachable) {
        NSLog(@"是手机自带网络");
        nwStatus = 0;
    } else {
        NSLog(@"网络有问题");
        nwStatus = -1;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:Flow_networkNotification object:nil];
}

@end





