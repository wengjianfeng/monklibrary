//
//  NSObject+Helper.m
//  NewworkDemo
//
//  Created by gree's apple on 23/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "NSObject+Helper.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSObject (NSObject_Helper)

// 生成字符串的md5值
+ (NSString*)md5FromString:(NSString*)string
{
    const char *cStr = [string UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}


@end
