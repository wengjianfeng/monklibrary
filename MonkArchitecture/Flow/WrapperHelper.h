//
//  WrapperHelper.h
//  G-Life_NewUI
//
//  Created by gree's apple on 24/10/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WrapperHelper : NSObject

- (void)wrapperName:(NSString *)name Password:(NSString *)psw;
- (void)clearWrapper;

@end
