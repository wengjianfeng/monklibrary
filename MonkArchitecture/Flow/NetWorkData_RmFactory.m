//
//  NetWorkData_RmFactory.m
//  Remote
//
//  Created by gree's apple on 18/9/14.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "NetWorkData_RmFactory.h"
#import "DeviceNetWork.h"
#import "DeviceData.h"
#import "AirCondDB.h"
#import "MainRoomViewController.h"
#import "RSA.h"
#import "NSData+Base64.h"
#import "NSString+Base64.h"
#import "AESCrypt.h"

#import "GetLocalIPV4.h"

#import "JSONProcessing.h"

#import "RSACryptor.h"

#import "AppDelegate.h"
#import "LoginVerifyViewController.h"
#import "FindBackPwdBySMSViewController.h"
#import "ActivateBySMSViewController.h"

@interface NetWorkData_RmFactory()
{
    RSAExchangeBlockComple _rsaExchangeBlock;
    DeviceSearchComple _deviceSearchComple;
    DeviceBindConfig _deviceBindConfig;
    
    id SendingData;
    
    DeviceNetWork *NW;
}
@end

@implementation NetWorkData_RmFactory

-(void)setBlock:(void (^)(void))RSACompleBlock
{
    _rsaExchangeBlock = RSACompleBlock;
}

-(void)setDeviceSearchComple:(void (^)(NSString *))deviceSearchCompleBlock
{
    _deviceSearchComple = deviceSearchCompleBlock;
}

-(void)setDeviceBindConfig:(void (^)(void))deviceBindConfigBlock
{
    _deviceBindConfig = deviceBindConfigBlock;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        NW = [DeviceNetWork shareInstance];
    }
    
    return self;
}

/*
 * 远程服务器回复数据基本一致，故无需单独列出每个方法
 * 近程模式可能需要分开，故单独处理，此处不做统一
 * 功能码列表
 */
-(id)DataFormJSON:(id)json
{
    /*
     *添加粘包处理
     *json是nsdata
     */
    {
        /*
         *用于特殊处理的json
         */
        id tjson = json;

        // 统一成Array进行处理
        if ([json isKindOfClass:[NSArray class]]) {
            
        }else{
            json = [[NSArray alloc]initWithObjects:json, nil];
        }
        
        // 将json数组解析出来成字典
        NSDictionary *jsonObj = [JSONProcessing dictionaryParsedFromJsonArray:json];
        
        TSLog(@"%@",[NSString stringWithFormat:@"Remote DataFormJSON %@",jsonObj]);
         
        NSString *Funcod =    [jsonObj objectForKey:@"functionCode"];
        NSDictionary *VaData = [jsonObj objectForKey:@"validData"];
        GELog(@"%@",[NSString stringWithFormat:@"Remote Funcod %@",Funcod]);
        /*以下内容服务器回复0、1或相关错误码，可统一处理
         *登录30
         *注册3B. 注册手机激活验证码请求3D. 注册手机激活验证码验证41. 注册邮件激活请求 3F.
         *登录31. 退出33. 修改密码43. 找回密码请求45. 手机找回密码验证4D "找回密码新密码输入47. 邮箱有问题"
         *绑定手机49. 绑定手机验证码校验4D 修改绑定手机51. 修改绑定手机校验4D 绑定邮箱53. 修改绑定邮箱55.
         *绑定空调37.  解绑空调39.
         *设备描述文件1. 事件订阅0B. 事件消息03.
         */
        if ([Funcod isEqualToString:kUserRegisterInfoVerifyRespondFC] || [Funcod isEqualToString:kUserLoginRespondFC] || [Funcod isEqualToString:kUserLogoutRespondFC] || [Funcod isEqualToString:kNewDeviceCombineAccountRespondFC] || [Funcod isEqualToString:kDeviceDecombineAccountRespondFC] || [Funcod isEqualToString:kRequestCheckCodeRespondFC] || [Funcod isEqualToString:kRequestActivateEmailRespondFC] || [Funcod isEqualToString:kPasswordChangeRespondFC] || [Funcod isEqualToString:kRetrievePwdRespondFC] || [Funcod isEqualToString:kResetPwdRespondFC] || [Funcod isEqualToString:kCombinePhoneRespondFC] || [Funcod isEqualToString:kChangeCombinedPhoneRespondFC] || [Funcod isEqualToString:kCombineEmailRespondFC] || [Funcod isEqualToString:kUserRegisterRespondFC] || [Funcod isEqualToString:kChangeCombinedPhoneVerifyRespondFC] || [Funcod isEqualToString:kCheckIdentifyCodeRespondFC] || [Funcod isEqualToString:kRetrivePwdCheckIdentifyCodeRespondFC] || [Funcod isEqualToString:kRetrivePwdIdentifyCodeRespondFC] || [Funcod isEqualToString:kChangeEmailRespondFC] || [Funcod isEqualToString:kPhoneCombineCheckIdentityCodeResbondFC] )
        {
            NSString *statusCode = [VaData valueForKey:@"statusCode"];
            // 2014-12-02添加，手机号码和邮箱登录时，需要返回用户名
            NSString *userName = [VaData valueForKey:@"userName"];
            NSArray *data = [[NSArray alloc]initWithObjects:Funcod,statusCode,userName, nil];
            CommunicateErrorData *errorData = [[CommunicateErrorData alloc]init];
            NSString *errorInfo = [errorData errorDetailWithStatusCode:[VaData valueForKey:@"statusCode"]];
            GELog(@"%@",[NSString stringWithFormat:@"Remote错误码： %@",errorInfo]);
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerFeedbackNormalFrame" object:data];
    //        return [VaData valueForKey:@"statusCode"];
            return nil;
        }
        
        //文件传输相关，把整个字典传递回去
        // A6 A8 A5
        if ([Funcod isEqualToString:kFileTransferFetchLatestVersionFC] || [Funcod isEqualToString:kFileTransferFetchLatestVersionRespondFC] || [Funcod isEqualToString:kFileTransferDoneRespond]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerFeedBackForFileTransfer" object:jsonObj];
        }
        
        /*返回的数据需要特殊处理
         *获取设备注册表请求 35.
         *validData:{
         *"originIP"="xxx",
         *"originPort"="6000"
         *}
         *说明：广域网请求时一次会返回多个空调信息
         *部分数据无用
         */
        else if ([Funcod isEqualToString:kAcquireDeviceRegistryRespondFC])
        {
            NSArray *vdArr = [jsonObj objectForKey:@"validData"];
    //        [NW.mDeviceDBDic removeAllObjects];
            
            for (int i = 0; i < [vdArr count]; i++)
            {
                DeviceData *device = [[DeviceData alloc]init];

                NSDictionary *deviceinfo = [vdArr objectAtIndex:i];

                device->DeviceMac = [deviceinfo objectForKey:@"productUUID"];
                device->DeviceName = [deviceinfo objectForKey:@"productName"];
                device->isOnline = [[deviceinfo objectForKey:@"online"] isEqualToString:@"true"]?YES:NO;
                device->isLAN = NO;//非局域网设备
                
                if ([device->DeviceName isEqualToString:@"卧室"])
                {
                    NSArray *languages = [NSLocale preferredLanguages];
                    NSString *currentLanguage = [languages objectAtIndex:0];
                    if(![currentLanguage isEqualToString:@"zh-Hans"])
                    {
                        device->DeviceName = NSLocalizedString(@"LocBedroom", @"卧室");
                    }
                }
                device->isBind = YES;

                /****************创建设备*****************/
                /*
                 *远程数据过来，则认为是绑定的
                 */
                BOOL DeviceBool = NO;

                if ([[NW.mDeviceDBDic allKeys] containsObject:device->DeviceMac] == YES)
                {
                    /*
                     *能够进到这里，证明远程能够搜索到，那么肯定绑定了
                     */
                    DeviceData *TheData = [[DeviceData alloc]init];
                    TheData = [NW.mDeviceDBDic objectForKey:device->DeviceMac];
                    NSLog(@"is SameMAC,%d,%d",TheData->isBind,TheData->isLAN);
                    NSLog(@"is SameMAC device222,%d,%d",device->isBind,device->isLAN);
                    GELog(@"%@",[NSString stringWithFormat:@"Remote is SameMAC,%d,%d,%d",TheData->isBind,TheData->isLAN,TheData->isOnline]);
                    GELog(@"%@",[NSString stringWithFormat:@"Remote is SameMAC device222,%d,%d,%d",device->isBind,device->isLAN,device->isOnline]);
                    
                    if ((TheData->isLAN == device->isLAN) && (TheData->isBind == device->isBind))//如果原来保存的是局域网网数据，无需再次保存数据
                    {
                        DeviceBool = YES;
                        NSLog(@"is LAN & Bind");
                        GELog(@"%@",[NSString stringWithFormat:@"Remote%@",@"is LAN & Bind"]);
                        if (TheData->isOnline != device->isOnline)//如果突然掉线
                        {
                            TheData->isOnline = device->isOnline;//修改isOnline值,深复制
                        }
                    }
                    else//如果已经保存有近程的数据，则不改变IP之类的，只修改online和LAN数据
                    {
                        NSLog(@"is LAN no & Bind no");
                        GELog(@"%@",[NSString stringWithFormat:@"Remote%@",@"is LAN no & Bind no"]);
                        TheData->isOnline = device->isOnline;
                        TheData->isBind = YES;
                        DeviceBool = YES;
                    }
                }
                else
                {
                    NSLog(@"is no Same ");
                    GELog(@"%@",[NSString stringWithFormat:@"Remote%@",@"Remote is no Same "]);
                    device->isBind = YES;
                }
                
                /*
                 *如果远程未上线，则不需要发送状态查询
                 *远近程时是否需要发送状态查询？？？服务器与近程状态是否一致？？？
                 */
                if (DeviceBool == NO)
                {
                    //填充离线数组，用来判断是否没查到状态。
                    [NW->mOfflineDeviceDic setObject:device forKey:[NSString stringWithFormat:@"%@",device->DeviceMac]];
                    NSLog(@"daaaaaaaaaaa %d",device->isLAN);
                    [NW.mDeviceDBDic setObject:device forKey:[NSString stringWithFormat:@"%@",device->DeviceMac]];/*保存查找到的设备*/
                    NW->searchConnectMac = device->DeviceMac;
                    NW->CurrentDeviceData = [NW->mDeviceDBDic objectForKey:device->DeviceMac];/*将当前的设备更新*/
                    [NW->mRemoteDeviceMacDB setObject:device->DeviceMac forKey:device->DeviceMac];
                }
                if (device->isOnline == YES)
                {//是否需要判断近程是否已经查询到状态了，查询到了就不查询了？？？
                    AirCondDB *TDB = [NW->mACDeviceContrDB objectForKey:device->DeviceMac];
                    if (TDB == nil)
                    {
                        if (_deviceSearchComple)//发送设备状态查询
                        {
                            _deviceSearchComple(device->DeviceMac);
                        }
                    }
                }
                NSLog(@"xxxxxxxxxNW.mDeviceDBDic Count %lu",(unsigned long)[NW.mDeviceDBDic count]);
                GELog(@"%@",[NSString stringWithFormat:@"xxxxxxxxxNW.mDeviceDBDic Count %lu",(unsigned long)[NW.mDeviceDBDic count]]);
                /****************创建设备*****************/
            }
            /*
             *此时可以检测设备是否被绑定
             */
            if (_deviceBindConfig)
            {
                _deviceBindConfig();
            }
            return nil;
        }
        /*状态查询08.
         *"On_off"="1"
         *"Run_mode"="1"
         *设备控制06.
         *"On_off"="1"
         *"Run_mode"="1"
         *事件消息帧03.
         *"空调上线"
         *"状态反馈，内机状态"
         */
        else if ([Funcod isEqualToString:kStateEnquiryRespondFC] || [Funcod isEqualToString:kControlRespondFC] || [Funcod isEqualToString:kEventMessageFC])
        {
            /*
             *由于以下内容需要用到nsmutabledictionary，但json库返回的是dictionary类，导致
             *NSMutableDictionary *vaildArr = [jsonObj objectForKey:@"validData"];
             *vaildArr为不可变NSMutableDictionary，导致移除错误
             */
            id tempJSONData = [NSJSONSerialization JSONObjectWithData:tjson options:NSJSONReadingMutableContainers error:nil];
            NSString *sHostMac = [tempJSONData objectForKey:@"originDeviceUUID"];
            NSMutableDictionary *vdArr = [tempJSONData objectForKey:@"validData"];
            
            NSString *evtKeyStr = @"";
            
            /*
             *statusCode = 67;
             */
            if ([tempJSONData objectForKey:@"statusCode"] != nil)
            {
                return nil;
            }
            
            /*
             *切换到该MAC对应所有空调控制数据库，否则新建一个基本的数据库。
             */
            AirCondDB *TDB = [NW->mACDeviceContrDB objectForKey:sHostMac];
            
            if (TDB == nil)
            {
                /*
                 *清除离线列表
                 */
                [NW->mOfflineDeviceDic removeObjectForKey:sHostMac];
                
                NW->mAcDB = [[AirCondDB alloc]init];
                NW->mAcDB->mTemMac = sHostMac;//连接sock、控制数据、终端数据的关键
                [NW->mAcDB LoadDeviceIcon];
                [NW->mACDeviceContrDB setObject:NW->mAcDB forKey:sHostMac];
            }
            else
            {
                NW->mAcDB = TDB;
            }

            /*
             *队列处理
             */
            if ([Funcod isEqualToString:kControlRespondFC])
            {
                vdArr = [self queueHandler:tempJSONData];
                
                int statusNum = [[vdArr valueForKey:@"Status_Number"] intValue];
                TSLog(@"Status_Number2222 %d",statusNum);
                
                DeviceData *TheData = [NW->mDeviceDBDic objectForKey:sHostMac];
                TheData.Status_Number = statusNum;
                
                if (vdArr == nil)
                {
                    return nil;
                }
            }
            /*
             *事件消息帧
             */
            if ([Funcod isEqualToString:kEventMessageFC])
            {
                evtKeyStr = [VaData objectForKey:@"eventType"];
                
                DeviceData *TheData = [NW->mDeviceDBDic objectForKey:sHostMac];
                if (TheData == nil)
                {
                    return nil;
                }
                [NW ChangeCurrentDevice:sHostMac];
                /*
                 *事件消息回复04
                 */
                TheData->tempSeqNum = [tempJSONData objectForKey:@"sequenceNumber"];
                [NW->mSendingBuffer removeAllObjects];
                NW->mSendingBuffer = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"null",@"null", nil];
                [NW SendTcpDataWithMode:RemoteNetWork WithModeID:BtSendEventSubscribeActive WithData:nil WithEnctyp:YES];
                
                /*
                 *空调上线帧(服务器)
                 *eventType:04空调上下线通知帧
                 *eventContent:01上线 02下线
                 */
                if ([evtKeyStr isEqualToString:@"04"])
                {
                    //            NSString *mac = [VaData objectForKey:@"slaveDeviceUUID"];
//                    id tempJSONData = [NSJSONSerialization JSONObjectWithData:tjson options:NSJSONReadingMutableContainers error:nil];
                    NSString *mac = [tempJSONData objectForKey:@"originDeviceUUID"];
                    /*
                     *一般情况下，进入这里基本证明在注册表返回过程已经有此数据。但不排除异常情况(有漏洞，BUG)，后续需要完善
                     */
                    DeviceData *TheData = [[DeviceData alloc]init];
                    TheData = [NW.mDeviceDBDic objectForKey:mac];
                    //            TheData->isOnline = [[VaData objectForKey:@"eventContent"] isEqualToString:@"01"]?YES:NO;
                    if (TheData == nil)
                    {
                        return nil;//如果不存在则离开，防错。
                    }
                    if ([[VaData objectForKey:@"eventContent"] isEqualToString:@"01"])
                    {
                        NW->searchConnectMac = mac;
                        NW->CurrentDeviceData = [NW->mDeviceDBDic objectForKey:mac];/*将当前的设备更新*/
                        TheData->isOnline = YES;
                        
                        if (_deviceSearchComple)//发送设备状态查询
                        {
                            _deviceSearchComple(mac);
                            
                            MainRoomViewController *MainRoom = [MainRoomViewController ShareInstance];
                            [MainRoom.mWindowsAlartShowView AppearAlertWithMessage:[NSString stringWithFormat:@"%@%@",TheData->DeviceName,NSLocalizedString(@"LocDeviceInline", @"设备上线")] WithType:0 AndTimeInterval:5.0f];//中文
                        }
                    }
                    else//下线
                    {
                        NSString *name = [NSString stringWithFormat:@"%@",TheData->DeviceName];
                        if ([[NW->mACDeviceContrDB allKeys] containsObject:mac] == YES)
                        {
                            [NW->mACDeviceContrDB removeObjectForKey:mac];
                        }
                        
                        [NW->mOfflineDeviceDic setObject:TheData forKey:[NSString stringWithFormat:@"%@",mac]];

                        if ([NW.NetWorkDelegate respondsToSelector:@selector(RoomRenew:)])
                        {
                             TSLog(@"空调离线");
                            GELog(@"%@",[NSString stringWithFormat:@"Remote空调离线%@",mac]);
                            MainRoomViewController *MainRoom = [MainRoomViewController ShareInstance];
                            [MainRoom.mWindowsAlartShowView AppearAlertWithMessage:[NSString stringWithFormat:@"%@%@",name,NSLocalizedString(@"LocDeviceOffline", @"设备离线")] WithType:0 AndTimeInterval:5.0f];//中文
                            
                            AppDelegate *app = [[UIApplication sharedApplication] delegate];
                            UIViewController *lastVC = app.ViewController.navController.topViewController;
                            if ([lastVC isKindOfClass:[LoginVerifyViewController class]] || [lastVC isKindOfClass:[FindBackPwdBySMSViewController class]] || [lastVC isKindOfClass:[ActivateBySMSViewController class]])
                            {
                                GELog(@"%@",[NSString stringWithFormat:@"%@",@"界面不跳转"]);
                            }
                            else
                            {
                                [app.ViewController.navController popToRootViewControllerAnimated:YES];
                                GELog(@"%@",[NSString stringWithFormat:@"%@",@"界面跳转"]);
                            }

                            [NW.NetWorkDelegate performSelector:@selector(RoomRenew:) withObject:nil];//状态查询到后，更新设备列表
                        }
                    }
                    
                    /*
                     *事件消息回复04
                     */
                    [NW->mSendingBuffer removeAllObjects];
                    NW->mSendingBuffer = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"null",@"null", nil];
                    [NW SendTcpDataWithMode:RemoteNetWork WithModeID:BtSendEventSubscribeActive WithData:nil WithEnctyp:YES];
                    
                    return nil;
                }
                /*
                 *空调管理事件
                 *eventType:05
                 *productName 设备名称
                 *productReset 设备修改网络配置 0设备不重启 1设备准备重启
                 */
                else if([evtKeyStr isEqualToString:@"05"])
                {
                    DeviceData *TheData = [NW->mDeviceDBDic objectForKey:sHostMac];
                    if ([[VaData objectForKey:@"productReset"] isEqualToString:@"0"])
                    {
                        TheData->DeviceName = [VaData objectForKey:@"productName"];
                    }
                    else
                    {
                        //重新刷新列表？？？还是等待设备离线？？？
                    }
                    /*
                     *事件消息回复04
                     */
                    [NW->mSendingBuffer removeAllObjects];
                    NW->mSendingBuffer = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"null",@"null", nil];
                    [NW SendTcpDataWithMode:RemoteNetWork WithModeID:BtSendEventSubscribeActive WithData:nil WithEnctyp:YES];
                    
                    return nil;
                }
                /*
                 *空调状态改变(服务器)
                 *eventType:02
                 */
                else if([evtKeyStr isEqualToString:@"02"])
                {
                    /*
                     *如果sendingbuffer有数据，肯定是没发送完，没发完，不处理事件帧
                     */
                    NSMutableDictionary *dic = [NW->mSendingBufferOverflow objectForKey:sHostMac];
                    if (dic)
                    {
                        [NW->mSendingBufferOverflow removeObjectForKey:sHostMac];
                        //~暂时无法判断是否在发送期间接收到的事件帧，故无法不处理事件帧~
                        //continue;
                    }

                    /*
                     *如果事件消息来的num不连续，则全查状态
                     */
                    DeviceData *TheData = [NW->mDeviceDBDic objectForKey:sHostMac];
                    int statusNum = [[vdArr valueForKey:@"Status_Number"] intValue];
                    
                    if((TheData.Status_Number + 1)%256 == statusNum)
                    {
                        TheData.Status_Number = statusNum;
                    }
                    else if(((statusNum - TheData.Status_Number +256)%256 < 128) &&statusNum!= TheData.Status_Number)//全查
                    {
                        if (_deviceSearchComple)//发送设备状态查询
                        {
                            TSLog(@"%@",[NSString stringWithFormat:@"Remote%@",@"eventType02:设备状态改变 tcp"]);

                            _deviceSearchComple(sHostMac);
                        }
                        return nil;
                    }
                    else
                    {
                        TSLog(@"%@",[NSString stringWithFormat:@"Status_Number:%d TheData.Status_Number:%ld",statusNum,(long)TheData.Status_Number]);
                        return nil;
                    }
                    
                    //继续往下执行刷新状态
                }
                /*
                 *空调状态反馈(近程)服务器是否也会回？？？
                 *
                 */
                else
                {
                    //待处理
                }
                
            }
            else if ([Funcod isEqualToString:kStateEnquiryRespondFC])
            {
                DeviceData *TheData = [NW->mDeviceDBDic objectForKey:sHostMac];
                TheData.Status_Number = [[vdArr valueForKey:@"Status_Number"] intValue];
            }
            
            /*
             *说明：只有控制数据才涉及切换数据库，故只需在对应的帧前切换即可
             *     提取控制公共数据及模式私有数据
             *     以下仅限用于空调设备
             * DB数组格式为[arr1,arr2]，现在的空调分两个数据库，但也可能只有一个数据库的其他设备，需要区分
             * arr1为public数据，arr2为mode数据
             */
            NSArray *DB = [DeviceRulesFactory MatchDBWithSigns:@"01" AndDeviceMAC:sHostMac];//根据设备类型，提取设备控制数据数据库
            [vdArr removeObjectForKey:@"lookupType"];//使用完后即可删除，避免后续搜索key值浪费内存
            
            NSArray *keys = [vdArr allKeys];
            
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"OriginalAndNewDescriptionForACState" ofType:@"plist"];
            NSDictionary *plistDic = [[NSDictionary alloc]initWithContentsOfFile:filePath];//取出网络协议与本地键值对照表
            NSMutableArray *sleepArr = [[NSMutableArray alloc] initWithObjects:@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
            float temperature = 0.0f;
            float decimal = 0.0f;
            for (id key in keys)
            {
                NSString *nwMapLocKey = [plistDic valueForKey:key];//取出网络协议对应的本地键值
                
                int value = (int)strtoul([[vdArr objectForKey:key] UTF8String], 0, 16);

                /***/
                if ([key isEqualToString:@"Timer_off_week"] || [key isEqualToString:@"Timer_on_week"])//广域网此key对应了本软件多个key，需要对应到本地key
                {
                    /*
                     *字典中的定时key数据顺序为：日、一、二、三、四、五、六
                     *取出plist里面的本地array，关键字"Timer_off_week"内含多个key
                     */
                    NSArray *keyArrs = [plistDic valueForKey:key];
                    NSLog(@"keyArrs %@",keyArrs);
                    
                    /*
                     *从网络协议数据取出值
                     *内容为：0xxxxxxx ，依次为 无、六、五、四、三、二、一、日
                     *更改：去除0xxxxxxx方式，改用二进制
                     */
//                    NSString *nwValueData = [vdArr valueForKey:key];
                    
//                    int timeChip = (int)strtoul([nwValueData UTF8String], 0, 16);
                    
                    for (int i = 0;i < [keyArrs count];i++)
                    {
                        unsigned char ttemp = (value>>i)&0x01;
                        //                        因为DB的空调顺序为：[公共数据，私有数据],定时为公共数据，所以此处直接setobject即可
                        [[DB objectAtIndex:0] setObject:[NSNumber numberWithInt:ttemp] forKey:[keyArrs objectAtIndex:i]];//公共数据中改变定时关工作日相关值
                    }


                }
                else if ([key isEqualToString:@"Sleep_mode3_temp1"] || [key isEqualToString:@"Sleep_mode3_temp2"] || [key isEqualToString:@"Sleep_mode3_temp3"] || [key isEqualToString:@"Sleep_mode3_temp4"] || [key isEqualToString:@"Sleep_mode3_temp5"] || [key isEqualToString:@"Sleep_mode3_temp6"] || [key isEqualToString:@"Sleep_mode3_temp7"] || [key isEqualToString:@"Sleep_mode3_temp8"])
                {
                    value = [[vdArr objectForKey:key] intValue] * 10;
                    
                    //因为键值可能不是按temp1、2、3进入，所以需要排序
                    unichar sleepChip = [key characterAtIndex:([key length]-1)];
                    int sleepChipData = sleepChip - 49;//根据key值最后的数字排序..数组下标
                    NSLog(@"key %@ , sleepChipData %d",key,sleepChipData);
                    [sleepArr replaceObjectAtIndex:sleepChipData withObject:[NSNumber numberWithInt:value]];
                    
                    [[DB objectAtIndex:1] setObject:sleepArr forKey:@"KDefaultSleepModeDIY"];//模式私有数据
                }
                else if ([key isEqualToString:@"Set_temp_integer"] || [key isEqualToString:@"Set_temp_decimal"])
                {
    //                int value = [[vdArr objectForKey:key] intValue];
//                    int value = (int)strtoul([[vdArr objectForKey:key] UTF8String], 0, 16);

                    float tep = [[[DB objectAtIndex:1] objectForKey:@"KDefaultTemperature"] floatValue];//模式私有数据
                    temperature = tep;//放错机制导致某些字段不存在
                    
                    if ([key isEqualToString:@"Set_temp_integer"])//整数
                    {
                        temperature = value;
                    }
                    else//小数
                    {
                        decimal += value*0.1;
                    }
                    
                    [[DB objectAtIndex:1] setObject:[NSNumber numberWithFloat:(temperature + decimal)] forKey:@"KDefaultTemperature"];//模式私有数据
                    
                }
                else if ([key isEqualToString:@"Noise_setting_cold"] || [key isEqualToString:@"Noise_setting_heat"])
                {
//                    int value = (int)strtoul([[vdArr objectForKey:key] UTF8String], 0, 16);
                    
                    if ([key isEqualToString:@"Noise_setting_cold"])
                    {
                        [[DB objectAtIndex:0] setObject:[NSNumber numberWithInt:value] forKey:@"KDefaultCoolNoise"];//公共数据中改变定时关工作日相关值
                    }
                    else
                    {
                        [[DB objectAtIndex:0] setObject:[NSNumber numberWithInt:value] forKey:@"KDefaultHotNoise"];//公共数据中改变定时关工作日相关值
                    }
                }
                else if ([key isEqualToString:@"Up_down_swing"] || [key isEqualToString:@"Left_right_swing"])
                {
//                    int value = (int)strtoul([[vdArr objectForKey:key] UTF8String], 0, 16);
                    [[DB objectAtIndex:1] setObject:[NSNumber numberWithInt:value] forKey:nwMapLocKey];//模式私有数据
                }
                else
                {
                    //其它非特殊键值均在此处理
                    for (int j = 0; j < [DB count]; j ++)
                    {
                        for (id value in [[DB objectAtIndex:j] allKeys])//枚举空调公共数据及私有数据键值
                        {
                            if ([value isEqualToString:nwMapLocKey])
                            {
                                if([value isEqualToString:@"AirCondition_mode"])
                                {
                                    NSLog(@"aaaaa ...");
                                }
                                if([value isEqualToString:@"E_heater"])
                                {
                                    NSLog(@"aaaaa ...");
                                }
                                id nwValue = [vdArr objectForKey:key];//从网络协议数据中读取数据
                                [[DB objectAtIndex:j] setObject:nwValue forKey:nwMapLocKey];//采用的是深复制，所以不需替换，只需修改值即可。
                                [vdArr removeObjectForKey:key];//删除已找到的，避免浪费工作
                                break;
                            }
                            //
                        }
                    }
                    
                }
            }
            
            AirCondDB *mAcDB = [NW->mACDeviceContrDB objectForKey:sHostMac]; //控制数据，需要将数据库切换到对应device
            
            [mAcDB->mModeDefaultDB setValuesForKeysWithDictionary:[DB objectAtIndex:0]];
            
            NSNumber *mode = [mAcDB->mModeDefaultDB objectForKey:@"KDefaultMode"];//根据模式，将模式中的数据提取出来
            NSString *modeKeyWord = [mAcDB->mModeKey objectAtIndex:[mode intValue]];
            [mAcDB->mModeDefaultDB setObject:[DB objectAtIndex:1] forKey:modeKeyWord];
            [mAcDB UpDataForCurrentDB];//更新数据库内容
            [mAcDB UpDataDBDidEnd];//通知界面修改

            if ([NW.NetWorkDelegate respondsToSelector:@selector(RoomRenew:)])
            {
                [NW.NetWorkDelegate performSelector:@selector(RoomRenew:) withObject:nil];//状态查询到后，更新设备列表
            }
        }
        /*
         *RSA交互
         */
        else if ([Funcod isEqualToString:kClientPostPubKeyRespondFC])
        {
            NSString *RSA_String = [jsonObj objectForKey:@"validData"];
            
            if ([RSA_String isEqualToString:NW.RSAReqRandom])//与发送的random做对比
            {
                if (_rsaExchangeBlock) {
                    _rsaExchangeBlock();
                };
                
                TSLog(@"%@",[NSString stringWithFormat:@"%s,%@",__func__,@"完成RSA交互"]);
            }
        }
        /*AES交互F3.
         *"On_off"="1"
         *"Run_mode"="1"
         */
        else if ([Funcod isEqualToString:kClientRequestSessionKeyRespondFC])
        {
            NSDictionary *DecryptData = [jsonObj objectForKey:@"validData"];
           {
//                NSData *base64FromString = [RSA_String dataUsingEncoding:NSUTF8StringEncoding];
               
//                id DecryptData = [NSJSONSerialization JSONObjectWithData:base64FromString options:NSJSONReadingMutableContainers error:nil];
//                TSLog(@"%@",[NSString stringWithFormat:@"Remote AES 接收数据..... %@",DecryptData]);
                //注册自身公钥
                NSString * random = [DecryptData objectForKey:@"identifyCode"];
               
                if ([random isEqualToString:NW.AESReqRandom])//与发送的random做对比
                {
                    NSLog(@"收到AES");
                    NW.RmSessKey = [DecryptData objectForKey:@"sessKey"];
                    NW.AESExchangeComple = YES;
                }
            }
        }
    
    }
    return nil;
}

/*
 *返回YES则代表“需处理这包数据”
 *只有控制应答帧需要处理
 */
-(NSMutableDictionary *)queueHandler:(NSDictionary *)jsonDic
{
    BOOL need = NO;
    
    NSString *seqc = [jsonDic objectForKey:@"sequenceNumber"];
    
    NSString *uuid = [jsonDic objectForKey:@"originDeviceUUID"];
    NSMutableDictionary *dic = [NW->mSendingBufferOverflow objectForKey:uuid];
    //    if (!dic) {
    //        return [jsonDic objectForKey:@"validData"];
    //    }
    
    NSDictionary *value = [dic objectForKey:seqc];
    //如果发现控制帧中间丢失，则不认为断网，但状态需全查!
    if([dic count] >= 1)
    {
        for (NSString *tempseqc in [dic allKeys])
        {
            //~~~~需要考虑翻转情况~~~~
            //如果存在中间seqc丢了，但statusNum仍然连续的情况，需全查。
            if( ([seqc intValue] - [tempseqc intValue] +256)%256 < 128 &&[seqc intValue]!= [tempseqc intValue])//全查
            {
                need = YES;
                [dic removeObjectForKey:tempseqc];//小于的全删除，因为后面的数据收到代表OK
            }
        }
        
        if (need)
        {
            [dic removeObjectForKey:seqc];
            
            if (_deviceSearchComple)//发送设备状态查询
            {
                TSLog(@"%@",[NSString stringWithFormat:@"Remote%@",@"eventType02:设备状态改变 tcp"]);
                
                _deviceSearchComple(uuid);
            }
            return nil;//全查了就不用处理这包了
        }
        
    }
    
    if (value)
    {
        [dic removeObjectForKey:seqc];
        
        TSLog(@"disOverflow11 %@\n",dic);
        TSLog(@"disOverflow %@\n",NW->mSendingBufferOverflow);
        
        /*
         *判断对应值是否一致
         *是否键值不匹配即可认为是不一样？
         *匹配不一致时需要将一致的数据去除，只同步不一致数据~
         */
        //        if ([[value allKeys] count] == [[[jsonDic objectForKey:@"validData"] allKeys] count])
        {
            for (NSString *keyname in [value allKeys])
            {
                //                if ([[value objectForKey:keyname] isEqualToString:[[jsonDic objectForKey:@"validData"] objectForKey:keyname]])
                if([[value objectForKey:keyname]intValue] == [[[jsonDic objectForKey:@"validData"] objectForKey:keyname] intValue])
                {
                    [[jsonDic objectForKey:@"validData"] removeObjectForKey:keyname];
                }
                else
                {
                    need = YES;
                }
            }
            
            if (need)
            {
                return [jsonDic objectForKey:@"validData"];
            }
            else
            {
                return nil;
            }
        }
        //        else
        //        {
        //            //键值对都不匹配，则认为是不一样的，需要处理数据
        //            return nil;
        //        }
        
    }
    
    //    [deviceDic setObject:kDevicesControlsFC forKey:[NSString stringWithFormat:@"%d",(DVNW->DeviceSeqc+1)]];
    //    [DVNW->mSendingBufferOverflow setObject:deviceDic forKey:DVNW->objectUUID];
    return [jsonDic objectForKey:@"validData"];
}

#pragma mark --- ControlMaths

-(id)BtMasterDeclare:(BOOL)enctyp//宣告入网 UDP方式？？？一直发？？？
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
    
//    GetLocalIPV4 *info = [GetLocalIPV4 sharelocalIPV4];

//    NSString *ip = info.localHost;
//    NSInteger port = info.localPort;
//    NSString *mask = @"255.255.255.0";
//    NSString *GW = @"192.168.1.1";
//    NSString *DNS = @"192.168.1.1";
//    
//    NSString *softwareVersion = @"1.0.1";

//    NW->mSendingBuffer = [[NSMutableDictionary alloc]initWithObjectsAndKeys:ip,@"originIP",[NSString stringWithFormat:@"%ld",(long)port],@"originPort",mask,@"originMask",GW,@"originGW",DNS,@"originDNS",softwareVersion,@"softwareEdition", nil];
    NW->mSendingBuffer = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"null",@"null", nil];
    
    return [self getValidDataFromBufferWithFunctionCode:kPhoneInNetworkAnnouncementFC];
}

-(id)BtMasterQuit:(BOOL)enctyp//手机退网 UDP方式？？？
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kPhoneQuitNetworkAnnouncementFC];
}

-(id)BtSendRegisterForEnctyp:(BOOL)enctyp//注册
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kUserRegisterRequestFC];
}

//-(id)BtSendActiveForEnctyp:(BOOL)enctyp//激活
//{
//    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
//
//    return [self getValidDataFromBufferWithFunctionCode:kRequestCheckCodeFC];
//}

-(id)BtSendActiveMailForEnctyp:(BOOL)enctyp // 发送邮件激活
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kRequestActivateEmailFC];
}

-(id)BtSendVerifyForEnctyp:(BOOL)enctyp//验证码校验
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kCheckIdentifyCodeRequestFC];
}

-(id)BtSendLoginForEnctyp:(BOOL)enctyp//登录
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kUserLoginRequestFC];
}

-(id)BtSendSearchDeviceForEnctyp:(BOOL)enctyp//搜索帧(设备注册表请求)  获得的是设备信息，设备状态呢？？设备描述呢？？
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    SendingData = @" ";//无需传值
    return [self getValidDataFromBufferWithFunctionCode:kAcquireDeviceRegistryFC];
}

-(id)BtSendSearchDeviceStatusForEnctyp:(BOOL)enctyp//状态查询帧
{
//    NW->objectUUID = NW->currentMac;//目标地址为空调,外部已经传值
    NSLog(@"状态查询帧MAC1 : %@",NW->currentMac);
    NSLog(@"状态查询帧MAC1 : %@",NW->objectUUID);
    SendingData = @" ";//无需传值
    return [self getValidDataFromBufferWithFunctionCode:kStateEnquiryFC];
}

-(id)BtSendControlForEnctyp:(BOOL)enctyp//控制帧
{
    NW->objectUUID = NW->currentMac;//目标地址为空调
    //控制帧数据需添加statusNumber
    DeviceData *TheData = [NW->mDeviceDBDic objectForKey:NW->objectUUID];
    [NW->mSendingBuffer setObject:[NSString stringWithFormat:@"%d",(int)TheData->Status_Number] forKey:@"Status_Number"];
    
    /*
     *添加队列控制
     */
    NSMutableDictionary *deviceDic = [NSMutableDictionary dictionaryWithDictionary:[NW->mSendingBufferOverflow objectForKey:NW->objectUUID]];
    if ([deviceDic count] >= 3)
    {
        //关闭用户接口
        AppDelegate *app = [[UIApplication sharedApplication] delegate];
        app.window.userInteractionEnabled = NO;
        
        return nil;
    }
    
    NSMutableDictionary *tempDic = [NSMutableDictionary dictionaryWithDictionary:NW->mSendingBuffer];
    [deviceDic setObject:tempDic forKey:[NSString stringWithFormat:@"%d",(NW->DeviceSeqc+1)%256]];
    [NW->mSendingBufferOverflow setObject:deviceDic forKey:NW->objectUUID];
    TSLog(@"sendingOverflow %@\n",NW->mSendingBufferOverflow);

    
    return [self getValidDataFromBufferWithFunctionCode:kControlFC];
}

-(id)BtSendLogoutForEnctyp:(BOOL)enctyp//账号退出
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kUserLogoutRequestFC];
}

-(id)BtSendUserPwChangeForEnctyp:(BOOL)enctyp//修改密码
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kPasswordChangeRequestFC];
}

-(id)BtSendUserPwFindForEnctyp:(BOOL)enctyp//找回密码
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kRetrievePwdRequestFC];
}

-(id)BtSendSesetPwdForEnctyp:(BOOL)enctyp//重置密码
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kResetPwdFC];
}

-(id)BtSendUserPwChangeVerifyForEnctyp:(BOOL)enctyp//发送验证码（密码找回）
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
    
    return [self getValidDataFromBufferWithFunctionCode:kRetrivePwdCheckIdentifyCodeRequestFC];
}

-(id)BtSendUserPwchangeNewForEnctyp:(BOOL)enctyp//发送新密码
{
     NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
    
    return [self getValidDataFromBufferWithFunctionCode:kResetPwdFC];
}

-(id)BtSendBindTelpForEnctyp:(BOOL)enctyp//绑定手机号
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kCombinePhoneRequestFC];
}

-(id)BtSendBindTelpChangeForEnctyp:(BOOL)enctyp//修改绑定手机
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kChangeCombinedPhoneRequestFC];
}

-(id)BtSendBindTelpCodeRequestForEnctyp:(BOOL)enctyp//请求绑定手机验证码
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
    
    return [self getValidDataFromBufferWithFunctionCode:kChangeCombinedPhoneVerifyFC];
}

-(id)BtSendBindTelpCodeVerifyForEnctyp:(BOOL)enctyp//绑定校验
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kPhoneCombineCheckIdentityCodeRequestFC];
}

-(id)BtSendBindEmailForEnctyp:(BOOL)enctyp//绑定邮箱
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kCombineEmailRequestFC];
}

-(id)BtSendBindEmailChangeForEnctyp:(BOOL)enctyp//修改绑定邮箱
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
    
    return [self getValidDataFromBufferWithFunctionCode:kChangeEmailRequestFC];
}

-(id)BtSendBindDeviceForEnctyp:(BOOL)enctyp//用户绑定
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kNewDeviceCombineAccountRequestFC];
}

-(id)BtSendBindDeviceCancelForEnctyp:(BOOL)enctyp//用户解绑
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kDeviceDecombineAccountRequestFC];
}

-(id)BtSendEventSubscribe:(BOOL)enctyp//事件订阅
{
    return [self getValidDataFromBufferWithFunctionCode:kDevicesEventSubscribeRenewFC];
}

-(id)BtSendEventSubscribeActiveForEnctyp:(BOOL)enctyp//事件消息响应
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
    return [self getValidDataFromBufferWithFunctionCode:kEventMessageRespondFC];
}

-(id)BtSendManageCommand:(BOOL)enctyp//管理帧
{
    NW->objectUUID = NW->currentMac;//目标地址为服务器地址

    return [self getValidDataFromBufferWithFunctionCode:kDevicesManageFC];
}

-(id)BtSendDeviceLoginCommand:(BOOL)enctyp//设备登陆帧
{
    
    return [self getValidDataFromBufferWithFunctionCode:kDeviceLoginFC];
}

-(id)BtSendRSAExchangeCommand:(NSString *)data Enctyp:(BOOL)enctyp//请求服务器公钥
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址

    NSLog(@"RSA 发送交互请求");
    SendingData = data;
    return [self getValidDataFromBufferWithFunctionCode:kClientPostPubKeyFC];
}

-(id)BtSendAESExchangeCommand:(NSString *)data Enctyp:(BOOL)enctyp
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
    
    NSLog(@"AES 发送交互请求");
    SendingData = data;
    return [self getValidDataFromBufferWithFunctionCode:kClientRequestSessionKeyFC];
}

-(id)BtResendIdentifyCodeForEnctyp:(BOOL)enctyp
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
    return [self getValidDataFromBufferWithFunctionCode:kRequestCheckCodeFC];
}

-(id)BtResendUserPwdIdentifyCodeForEnctyp:(BOOL)enctyp
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
    return [self getValidDataFromBufferWithFunctionCode:kRetrivePwdIdentifyCodeRequestFC];
}

-(id)BtFileTransferFetchLatestVersionForEnctyp:(BOOL)enctyp
{
    NW->objectUUID = NW->currentMac;//目标地址为服务器地址
    return [self getValidDataFromBufferWithFunctionCode:kFileTransferFetchLatestVersionFC];
}

-(id)BtFileTransferWhetherUpgradeByServerForEnctyp:(BOOL)enctyp
{
    NW->objectUUID = HostObjectUUID;//目标地址为服务器地址
    return [self getValidDataFromBufferWithFunctionCode:kFileTransferExecuteUpgradeFC];
}

-(NSArray*)getValidDataFromBufferWithFunctionCode:(NSString*)functionCode
{
    NSString *seqnum;
    
    if ([functionCode isEqualToString:kClientPostPubKeyFC] || [functionCode isEqualToString:kClientRequestSessionKeyFC] || [functionCode isEqualToString:kAcquireDeviceRegistryFC])
    {
       
    }else
    {
        SendingData = NW->mSendingBuffer;
    }
    
    if ([functionCode isEqualToString:kDevicesEventMessageRespondFC])
    {
        DeviceData *TheData = [NW->mDeviceDBDic objectForKey:NW->currentMac];
        
        seqnum = TheData->tempSeqNum;
    }
    else
    {
        NW->DeviceSeqc = (NW->DeviceSeqc + 1)%256;
        
        seqnum = [NSString stringWithFormat:@"%d",NW->DeviceSeqc];
    }
    
    NSLog(@"远程");
    NSArray *jsonArray = [JSONProcessing JSONWithValidData:SendingData functionCode:functionCode sequenceNum:seqnum];
    return jsonArray;
}

@end
