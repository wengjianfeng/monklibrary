//
//  AcJsonData.h
//  NewworkDemo
//
//  Created by gree's apple on 12/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VirtualDeviceProtocol;

@interface AcJsonData : NSObject

-(id)initWithBL:(id<VirtualDeviceProtocol>)BL;
/*
 解析数据
 @param json 通过解密或其他操作后的数据，非socket原始回调数据
 */
-(void)parseFromJson:(NSDictionary *)json;

/*
 从数据库打包需要的原始通信包
 @return 原始通信包
 */
-(NSData *)nwkJsonFormatFromDictionary:(NSMutableDictionary *)dic;

@end