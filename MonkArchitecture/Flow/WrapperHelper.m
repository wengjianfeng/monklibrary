//
//  WrapperHelper.m
//  G-Life_NewUI
//
//  Created by gree's apple on 24/10/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "WrapperHelper.h"
#import "KeychainItemWrapper.h"

@implementation WrapperHelper

#pragma mark - wrapper
- (void)wrapperName:(NSString *)name Password:(NSString *)psw
{
    //自动保存用户名密码
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc]initWithIdentifier:@"password" accessGroup:nil];
    [wrapper setObject:name forKey:(__bridge id)kSecAttrAccount];
    [wrapper setObject:psw forKey:(__bridge id)kSecValueData];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES] forKey:@"autoAccountLogin"];
}

- (void)clearWrapper
{
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc]initWithIdentifier:@"password" accessGroup:nil];
    [wrapper setObject:@"" forKey:(__bridge id)kSecValueData];//无论何时都只清空密码
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:@"autoAccountLogin"];
}

@end
