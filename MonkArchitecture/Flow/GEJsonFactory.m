//
//  GEJsonFactory.m
//  NewworkDemo
//
//  Created by gree's apple on 26/8/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "GEJsonFactory.h"

#import "AcJsonData.h"
#import "AcJsonData_Search.h"
#import "AcJsonData_StateEnquiry.h"
#import "AcJsonData_ServerLists.h"

#import "ControlJsonFormatLists.h"
#import "JSONProcessing.h"

#import "NotificationLists.h"
@implementation GEJsonFactory

-(NSData *)creatWithIdentify:(NSString *)identify withDictionary:(NSMutableDictionary *)dic
{
    //构建json包不需要区分LAN/WAN(BL去区分)
    AcJsonData_Search *frame = [[AcJsonData_Search alloc] init];
    [dic setObject:identify forKey:@"functionCode"];
    
    NSData *data = [frame nwkJsonFormatFromDictionary:dic];
    
    return data;
}

-(id)parseWithData:(NSData *)data withBL:(id)bl;
{
    //在此解析数据包并分发
    NSArray *json = [[NSArray alloc]initWithObjects:data, nil];
    
    // 将json数组解析出来成字典
    NSDictionary *jsonObj = [JSONProcessing dictionaryParsedFromJsonArray:json];
    NSString *Funcod = [jsonObj objectForKey:@"functionCode"];
    AcJsonData *frame = [[AcJsonData_ServerLists alloc] init];
/***********************设备相关***************Start**********/
    /*!
        @创建设备相关  --------->done
    */
    NSArray *arr1 = @[
                      KJsonInLocalAnnouncement,                 // 设备入网宣告帧 11
                      KJsonSearchDeviceResponse,                // 设备搜索应答帧 14
                      KJsonRequestDeviceRegistryRespondFC,      // 获取设备注册表请求响应帧(远) 35
                      ];
    if ([arr1 containsObject:Funcod]){
        //设备入网宣告/广播搜索应答
        frame = [[AcJsonData_Search alloc] initWithBL:bl];
    }
    
    /*!
        @状态解析
        eventType02:设备状态改变
        eventType05:空调名称或网络改变
     */
    NSArray *arr2 = @[
                      KJsonEventMessageFC,          //事件消息 03
                      KJsonControlRespondFC,        //设备控制 06
                      KJsonStateEnquiryRespondFC,   //状态查询 08
                      ];
    if ([arr2 containsObject:Funcod]) {
        frame = [[AcJsonData_StateEnquiry alloc] initWithBL:bl];
    }
/***********************设备相关***************End**********/
    
/***********************文传管理相关***************Start**********/
    /*!
        @文件传输
     */
    NSArray *arr3 = @[
                      KJsonFileTransferFetchLatestVersionFC,          // 向服务器请求最新的升级文件版本 A6
                      KJsonFileTransferFetchLatestVersionRespondFC,   // 向服务器请求最新的升级文件版本响应 A8
                      KJsonFileTransferActivateRespondFC,             // 文件传输请求响应帧 A1
                      KJsonFileTransferPropertyRespondFC,             // 发送传输文件属性应答帧 A3
                      KJsonFileTransferDoneRespond,                   // 发送文件传输完毕应答帧 A5
                      ];
    if ([arr3 containsObject:Funcod]) {
        //        [[NSNotificationCenter defaultCenter] postNotificationName:@"LocFileTransferRespond" object:jsonObj];
    }
    
    /*!
        @设备登陆应答帧 24
     */
    if ([Funcod isEqualToString:KJsonDeviceLoginRespondFC]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:Flow_deviceLoginDone object:jsonObj];//为了兼容HEX，所以所有数据回传
    }
    
    /*!
        @暂无处理
     */
    NSArray *arr4 = @[
                      KJsonQuitLocalAnnouncement,    // 设备退网宣告帧 12
                      KJsonManagementRespondFC, //管理帧 0D
                      ];
    if ([arr4 containsObject:Funcod]){
        NSLog(@"不处理");
        return nil;
    }
    
/***********************文传管理相关***************End**********/

    [frame parseFromJson:jsonObj];
    
    return nil;
}

@end
