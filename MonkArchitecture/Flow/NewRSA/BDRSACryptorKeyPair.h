//
//  Created by Patrick Hogan on 10/12/12.
//

#import <Foundation/Foundation.h>

@interface BDRSACryptorKeyPair : NSObject

@property (nonatomic, strong) NSString *publicKey;
@property (nonatomic, strong) NSString *privateKey;
@property (nonatomic) SecKeyRef serverPublicKey;

- (id)initWithPublicKey:(NSString *)publicKey
             privateKey:(NSString *)privateKey
        serverPublicKey:(SecKeyRef )serverPublicKey;

@end
