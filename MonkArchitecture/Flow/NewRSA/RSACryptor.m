//
//  RSACryptor.m
//  Remote
//
//  Created by gree's apple on 20/10/14.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "RSACryptor.h"
#import "AESCrypt.h"

static RSACryptor *_rsa;

@implementation RSACryptor
//@synthesize publicKey,privateKey,ServerPublicKey;
@synthesize _RSACryptor,_RSAKeyPair;

+(id)shareInstance
{
    if (_rsa == nil)
    {
        _rsa = [[RSACryptor alloc]init];
    }
    
    return _rsa;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        [self generateKeyPairRSA];
    }
    
    return self;
}

- (void)generateKeyPairRSA
{
    self->_RSACryptor = [[BDRSACryptor alloc] init];

    if ([self GetRSAPublicKey] == NO)
    {
        self->_RSAKeyPair = [_RSACryptor generateKeyPairWithKeyIdentifier:@"key_for_GREE" error:nil];
                
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *mFileName = [documentsDirectory stringByAppendingPathComponent:@"PublicKey.txt"];
        [self._RSAKeyPair.publicKey writeToFile:mFileName atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        NSString *mPriFileName = [documentsDirectory stringByAppendingPathComponent:@"Example.txt"];
//        TSLog(@"----------Before privateKey \n %@ \n",self._RSAKeyPair.privateKey);
        NSString *AESkey = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UUID"] substringWithRange:NSMakeRange(0, 16)];
        NSString *encryptString = [AESCrypt encryptAES128:self._RSAKeyPair.privateKey forKey:AESkey forIv:nil];
        [encryptString writeToFile:mPriFileName atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
}

-(SecKeyRef)GetServerRSAPublicKey
{
    SecCertificateRef myCertificate;
    SecPolicyRef myPolicy;
    SecTrustRef myTrust;
    SecTrustResultType trustResult;
    
    NSString *priKeyPath = [[NSBundle mainBundle] pathForResource:@"rsa_public_key" ofType:@"der"];
    
    NSData *priKeyFileContent = [NSData dataWithContentsOfFile:priKeyPath];
    
    myCertificate = nil;
    
    myCertificate = SecCertificateCreateWithData(kCFAllocatorDefault, (__bridge CFDataRef)priKeyFileContent);
    myPolicy = SecPolicyCreateBasicX509();
    OSStatus status = SecTrustCreateWithCertificates(myCertificate,myPolicy,&myTrust);
    if (status == noErr) {
        status = SecTrustEvaluate(myTrust, &trustResult);
    }

    SecKeyRef pubkey = SecTrustCopyPublicKey(myTrust);
        
    return pubkey;
}

- (BOOL)GetRSAPublicKey
{
    BOOL status = NO;
    NSString *publicKey;
    NSString *privateKey;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filename = [documentsDirectory stringByAppendingPathComponent:@"PublicKey.txt"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filename])
    {
         publicKey = [[NSString alloc]initWithContentsOfFile:filename encoding:NSUTF8StringEncoding error:nil];
        status = YES;
    }
    
    NSString *prifilename = [documentsDirectory stringByAppendingPathComponent:@"Example.txt"];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:prifilename])
    {
        NSString *AES_DeCryptString = [[NSString alloc]initWithContentsOfFile:prifilename encoding:NSUTF8StringEncoding error:nil];
        NSString *AESkey = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UUID"] substringWithRange:NSMakeRange(0, 16)];
        privateKey = [AESCrypt decryptAES128:AES_DeCryptString forKey:AESkey forIv:nil];
//        TSLog(@"----------privateKey \n %@ \n",privateKey);
        
        status = YES;
    }
    
   SecKeyRef serverpublickey = [self GetServerRSAPublicKey];
    
    self._RSAKeyPair = [[BDRSACryptorKeyPair alloc]initWithPublicKey:publicKey privateKey:privateKey serverPublicKey:serverpublickey];
    
//    NSLog(@"/**************************/\n Private Key:\n%@\n\nPublic Key:\n%@\n Server:%@\n/***********************/", _RSAKeyPair.privateKey, _RSAKeyPair.publicKey,self._RSAKeyPair);

    return status;
}

-(NSString *)RSA_EncryptUsingPublicKeyWithData:(NSData *)data
{
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *cipherText = [self->_RSACryptor encrypt:str key:self._RSAKeyPair.publicKey error:nil];
    
    return cipherText;
}

- (NSString *)RSA_DecryptUsingPrivateKeyWithData:(NSData *)data//用本地公钥加密
{
//    NSLog(@"RSA_DecryptUsingPrivateKeyWithData %@",self._RSAKeyPair.privateKey);
    NSString *str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    NSString *recoveredText = [self->_RSACryptor decrypt:str key:self._RSAKeyPair.privateKey error:nil];
    
    return recoveredText;
}

-(NSString *)RSA_EncryptUsingServerPublicKeyWithData:(NSData *)data
{
    [self GetRSAPublicKey];
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

    NSString *cipherText = [self->_RSACryptor encryptWithServerPublicKey:str key:self._RSAKeyPair.serverPublicKey error:nil];
    
    return cipherText;
}

@end
