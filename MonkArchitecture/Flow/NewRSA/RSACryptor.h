//
//  RSACryptor.h
//  Remote
//
//  Created by gree's apple on 20/10/14.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BDRSACryptor.h"
#import "BDRSACryptorKeyPair.h"

@interface RSACryptor : NSObject
{
    BDRSACryptor *_RSACryptor;
    BDRSACryptorKeyPair *_RSAKeyPair;

}
@property (nonatomic) BDRSACryptor *_RSACryptor;
@property (nonatomic) BDRSACryptorKeyPair *_RSAKeyPair;

+(id)shareInstance;

- (void)generateKeyPairRSA;//生成密钥，并保存到本地
- (SecKeyRef)GetServerRSAPublicKey;//获得服务器
- (BOOL)GetRSAPublicKey;

- (NSString *)RSA_EncryptUsingPublicKeyWithData:(NSData *)data;//用本地公钥加密
- (NSString *)RSA_DecryptUsingPrivateKeyWithData:(NSData*)data;//用本地公钥解密
- (NSString *)RSA_EncryptUsingServerPublicKeyWithData:(NSData*)data;//用服务器公钥加密

@end
