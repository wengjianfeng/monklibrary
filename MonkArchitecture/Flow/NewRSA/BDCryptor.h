//
//  Created by Patrick Hogan on 10/12/12.
//


#import <Foundation/Foundation.h>


////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - Public Interface
////////////////////////////////////////////////////////////////////////////////////////////////////////////
@interface BDCryptor : NSObject

- (NSString *)encrypt:(NSString *)plainText
                  key:(NSString *)key
                error:(NSError *)error;

- (NSString *)decrypt:(NSString *)cipherText
                  key:(NSString *)key
                error:(NSError *)error;

@end