//
//  ACData2.m
//  Remote
//
//  Created by apple on 14-9-17.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.

//  OriginDeviceUUID在本地没有变动，不需要每次传入，在初始化时定义好。同样的还有protocolEdition.
//  如果考虑frameData复用问题，那么OriginDeviceUUID需要变动
//  protocolEdition可以写在userDefaults里面

#import "FrameData.h"
#import "JSONComposeTool.h"
#define kProtocolEdition @"40" // 协议版本
#define kDeviceType @"F0"      // 设备类型

@implementation FrameData

- (id)initWithObjectDeviceUUID:(NSString *)objectDeviceUUID functionCode:(NSString*)functionCode sequenceNumber:(NSString*)sequenceNumber
{
    self = [super init];
    if (self) {
        // 常量，有变动时更改宏定义
        self.protocolEdition = kProtocolEdition;
        self.deviceType = kDeviceType;
        self.originDeviceUUID = kOriginDeviceUUID;
        // 变量
        self.objectDeviceUUID = objectDeviceUUID;
        self.functionCode = functionCode;
        self.sequenceNumber = sequenceNumber;
        // 需要后续传入
        self.validData = nil;
        // 需要根据包内已有数据来确定
        self.sign = nil;
        self.subPackage = nil;
        self.dataLength = nil;
    }
    return self;
}

@end
