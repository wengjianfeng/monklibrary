//
//  NSObject+AcJsonData.m
//  NewworkDemo
//
//  Created by gree's apple on 18/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "AcJsonData.h"
#import "JSONProcessing.h"

@implementation AcJsonData

-(id)initWithBL:(id<VirtualDeviceProtocol>)BL
{
    return nil;
}

-(void)parseFromJson:(NSDictionary *)json
{
    
}

-(NSData *)nwkJsonFormatFromDictionary:(NSMutableDictionary *)dic
{
    NSString *func = [dic objectForKey:@"functionCode"];
    //functionCode不涉及打包
    [dic removeObjectForKey:@"functionCode"];
    
    //之所以是array,那是因为分包，当然貌似永远用不上。呵呵~
    NSArray *jsonArray = [JSONProcessing JSONWithValidData:dic functionCode:func sequenceNum:@"1"];
    
    return [jsonArray objectAtIndex:0];
}

@end
