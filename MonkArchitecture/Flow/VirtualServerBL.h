//
//  VirtualServerBL.h
//  NewworkDemo
//
//  Created by gree's apple on 15/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VirtualDeviceProtocol.h"
/************************************************************************************/
/*     WAN模式下，虽然可能会有N台设备在服务器上，但此BL始终只有这一个                         */
/*     实际创建多个DeviceBL用来储存数据，链接实际用的还是本类的                              */
/*     由此类去分发记挂在服务器上的设备指令及发送设备的指令                                  */
/*     封装与服务器所有的交互                                                           */
/*     与服务器相关的交流用此类，与服务器中的设备交流用其他类                                 */
/************************************************************************************/

@interface VirtualServerBL : NSObject<GETCPSocketDelegate,GEUDPSocketDelegate,VirtualDeviceProtocol>

// 用于通过client构建指令（非数据库构建）的数据帧数据,交互类不算包括RSA/AES/KEEPALIVE/AUTOLOGIN,JSON格式数据帧就是这种方式
@property (nonatomic, strong, readwrite) NSMutableDictionary *sendingBuffer;

@property (nonatomic, strong) GEWiFiContext *_context;
@property (nonatomic, strong) GENetworking *_nw;
@property (nonatomic, strong) Command *_command;
@property (nonatomic, strong) NSString *identify;           //标识 可以认为是MAC
@property (nonatomic, assign) protocolType _mType;


-(instancetype)initWithType:(protocolType)type;

//由于是BL，所以应当在此处理时间
-(void)sendMessage:(BLMessageEvt)msg;

//Context
//- (GEWiFiContext *) configContextWithDictionary:(NSDictionary *)dic;

//Command
- (void)excuteWithTcpCmd:(NSString *)cmd;

- (void)excuteWithUdpCmd:(NSString *)cmd;

//NW
- (void)creatTcpWithIpAndPortContext:(NSDictionary *)context;

- (void)creatUdpWithIpAndPortContext:(NSDictionary *)context;

//Stuff
- (void)stuffFormatMutableDictionary:(NSMutableDictionary *)dictionary;

//登录
- (void)autoAccountLoginStuff;

//状态查询
- (void)statusRequest;

//产生token
-(void) creatToken:(NSNotification *)notification;

@end
