//
//  NetWorkData_LocFactory.m
//  Remote
//
//  Created by gree's apple on 18/9/14.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "NetWorkData_LocFactory.h"
#import "DeviceNetWork.h"
#import "DeviceData.h"
#import "AirCondDB.h"

#import "GetLocalIPV4.h"

#import "JSONProcessing.h"

#import "RSACryptor.h"

#import "AppDelegate.h"
@interface NetWorkData_LocFactory()
{
    LocDeviceSearchComple _searchBlock;
    LocDeviceStatusSearchComple _statusSearchBlock;
    
    DeviceNetWork *DVNW;
//    int tempSeqNum;
}

@end

@implementation NetWorkData_LocFactory

-(id)init
{
    self = [super init];
    if (self)
    {
        DVNW = [DeviceNetWork shareInstance];
    }
    
    return self;
}

-(void)setLocDeviceSearchComple:(void (^)(NSString *))deviceSearchCompleBlock
{
    _searchBlock = deviceSearchCompleBlock;
}

-(void)setLocDeviceStatusSearchComple:(void (^)(NSString *))deviceSearchCompleBlock
{
    _statusSearchBlock = deviceSearchCompleBlock;
}

/*
 * 远程服务器回复数据基本一致，故无需单独列出每个方法
 * 近程模式可能需要分开，故单独处理，此处不做统一
 * 功能码列表
 */
-(id)DataFormJSON:(id)json
{
    /*
     *添加粘包处理
     *json是nsdata
     *此正则表达式无法区分[{},{}]这种情况
     */
//    NSString *handlejsonObj = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
//    
//    NSString *content;
//    NSRange range = [handlejsonObj rangeOfString:@"[{"];
//    
//    NSArray *listItems;
//    if (range.location != NSNotFound)//判断是否有粘包情况，没有无需处理
//    {
//        NSString *phoneRegex = @"\\}[^}](?!,?\"([a-zA-Z0-9]*)\").*(?<!\":)\\{";
//        NSRegularExpression *regularExpression = [NSRegularExpression regularExpressionWithPattern:phoneRegex options:0 error:nil];
//        content  = [regularExpression stringByReplacingMatchesInString:handlejsonObj options:0 range:NSMakeRange(0, handlejsonObj.length) withTemplate:@"}{"];//去除乱码部分
//    }
//    else
//    {
//        NSString *phoneRegex = @"\\}[^,\\]]\\{";
//        NSRegularExpression *regularExpression = [NSRegularExpression regularExpressionWithPattern:phoneRegex options:0 error:nil];
//        content  = [regularExpression stringByReplacingMatchesInString:handlejsonObj options:0 range:NSMakeRange(0, handlejsonObj.length) withTemplate:@"}{"];//去除乱码部分
//    }
//
//    NSRange range2 = [handlejsonObj rangeOfString:@"}{"];
//    if (range2.location != NSNotFound)//判断是否有粘包情况，没有无需处理
//    {
//        NSMutableString *bbb = [[NSMutableString alloc]initWithString:handlejsonObj];
//        [bbb replaceOccurrencesOfString:@"}{" withString:@"}}{{" options:NSLiteralSearch range:NSMakeRange(0, [bbb length])];
//        listItems = [bbb componentsSeparatedByString:@"}{"];
//    }
//    else
//    {
//        listItems = [[NSArray alloc] initWithObjects:handlejsonObj, nil];
//    }
    
//    handlejsonObj = [handlejsonObj stringByReplacingOccurrencesOfString:@"} {" withString:@"}{"];
//    handlejsonObj = [handlejsonObj stringByReplacingOccurrencesOfString:@"}/t{" withString:@"}{"];
//    handlejsonObj = [handlejsonObj stringByReplacingOccurrencesOfString:@"}/n{" withString:@"}{"];
//    
//    NSRange range = [handlejsonObj rangeOfString:@"}{"];
//    
//    NSArray *listItems;
//    if (range.location != NSNotFound)//判断是否有粘包情况，没有无需处理
//    {
//        NSMutableString *bbb = [[NSMutableString alloc]initWithString:handlejsonObj];
//        [bbb replaceOccurrencesOfString:@"}{" withString:@"}}{{" options:NSLiteralSearch range:NSMakeRange(0, [bbb length])];
//        listItems = [bbb componentsSeparatedByString:@"}{"];
//    }
//    else
//    {
//        listItems = [[NSArray alloc] initWithObjects:handlejsonObj, nil];
//    }
//    GELog(@"%@",[NSString stringWithFormat:@"Local List %@",listItems]);
    
    /*
     *业务处理
     */
//    for (int i = 0; i < [listItems count]; i++)
    {
//        json = [[listItems objectAtIndex:i] dataUsingEncoding:NSUTF8StringEncoding];
    
        /*
         *用于特殊处理的json
         */
        id tjson = json;
        TSLog(@"local %@",tjson);

        // 统一成Array进行处理
        if ([json isKindOfClass:[NSArray class]]) {
            
        }else{
            json = [[NSArray alloc]initWithObjects:json, nil];
        }
        
        // 将json数组解析出来成字典
        NSDictionary *jsonObj = [JSONProcessing dictionaryParsedFromJsonArray:json];

        /*
         *事务处理
         */
        NSString *Funcod = [jsonObj objectForKey:@"functionCode"];
        

        //收到文件传输相关帧
        if ([Funcod isEqualToString:kFileTransferFetchLatestVersionFC] || [Funcod isEqualToString:kFileTransferFetchLatestVersionRespondFC] || [Funcod isEqualToString:kFileTransferActivateRespondFC] || [Funcod isEqualToString:kFileTransferPropertyRespondFC] || [Funcod isEqualToString:kFileTransferDoneRespond]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LocFileTransferRespond" object:jsonObj];
        }
        
        
        if ([Funcod isEqualToString:kDevicesQuitLocalAnnouncementFC])//退网宣告
        {
            
        }
        else if ([Funcod isEqualToString:kDeviceLoginRespondFC])//设备登录
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DeviceLoginRespond" object:[jsonObj objectForKey:@"validData"]];
        }
        /*返回的数据需要特殊处理
         *获取设备注册表请求 35.
         *validData:{
         *"originIP"="xxx",
         *"originPort"="6000"
         *}
         *说明：广域网请求时一次会返回多个空调信息
         *部分数据无用
         *入网宣告
         *TCP
         */
        else if ([Funcod isEqualToString:kDevicesSearchingRespondFC] || [Funcod isEqualToString:kDevicesInLocalAnnouncementFC])
        {        
            NSDictionary *deviceinfo = [jsonObj objectForKey:@"validData"];//可能有嵌套

    //        for (int i = 0; i < [vdArr count]; i++)
            {
                DeviceData *device = [[DeviceData alloc]init];
                
                //远程无这些数据
                device->DeviceIP = [deviceinfo objectForKey:@"productIP"];
                device->DevicePort = [deviceinfo objectForKey:@"productPort"];
                device->DeviceMask = [deviceinfo objectForKey:@"productMask"];
                device->DeviceGate = [deviceinfo objectForKey:@"productGW"];
                device->DeviceDNS = [deviceinfo objectForKey:@"productDNS"];
                device->DeviceMac = [jsonObj objectForKey:@"originDeviceUUID"];//从数据源读取UUID
                device->DeviceName = [deviceinfo objectForKey:@"productName"];
                
                device->isLAN = YES;//非局域网设备
                
                if ([device->DeviceName isEqualToString:@"卧室"])
                {
                    NSArray *languages = [NSLocale preferredLanguages];
                    NSString *currentLanguage = [languages objectAtIndex:0];
                    if(![currentLanguage isEqualToString:@"zh-Hans"])
                    {
                        device->DeviceName = NSLocalizedString(@"LocBedroom", @"卧室");
                    }
                }
                GELog(@"%@",[NSString stringWithFormat:@"Local device->isLAN:%d; device->isBind:%d; device->isOnline:%d",device->isLAN,device->isBind,device->isOnline]);
                /****************创建设备*****************/
                BOOL DeviceBool = NO;

                if ([[DVNW.mDeviceDBDic allKeys] containsObject:device->DeviceMac] == YES)
                {
                    /*
                     *能够进到这里，证明远程能够搜索到，那么肯定绑定了
                     */
                    DeviceData *TheData = [[DeviceData alloc]init];
                    TheData = [DVNW.mDeviceDBDic objectForKey:device->DeviceMac];
    //                TheData->isLAN = device->isLAN;
                    TSLog(@"TheData->isLAN %d %d",TheData->isLAN,device->isLAN);
                    GELog(@"%@",[NSString stringWithFormat:@"Local TheData->isLAN %d %d",TheData->isLAN,device->isLAN]);
                    /*
                     *近程搜索会关闭TCP，关闭时会清界面，若此时远程已有列表，将会无界面显示。
                     */
                    if (TheData->isLAN == device->isLAN)//如果原来保存的是局域网网数据，无需再次保存数据
                    {
                        DeviceBool = YES;
                    }
                    else
                    {
                        TheData->isLAN = device->isLAN;
                    }
                }
                
                if (DeviceBool == NO)
                {
                    
                    TSLog(@"近程设置空调列表");
                    GELog(@"%@",[NSString stringWithFormat:@"Local %@",@"近程设置空调列表"]);
                    //填充离线数组，用来判断是否没查到状态。
                    [DVNW->mOfflineDeviceDic setObject:device forKey:[NSString stringWithFormat:@"%@",device->DeviceMac]];
                    
                    [DVNW.mDeviceDBDic setObject:device forKey:[NSString stringWithFormat:@"%@",device->DeviceMac]];/*保存查找到的设备*/
                    DVNW->searchConnectMac = device->DeviceMac;
                    DVNW->CurrentDeviceData = [DVNW->mDeviceDBDic objectForKey:device->DeviceMac];/*将当前的设备更新*/
                    
                    if ([[DVNW->mRemoteDeviceMacDB allKeys] containsObject:device->DeviceMac])
                    {
                        [DVNW->mRemoteDeviceMacDB removeObjectForKey:device->DeviceMac];
                    }
                    
                    if (_searchBlock)//发送设备状态查询
                    {
                        TSLog(@"AAAA tcp");
                        GELog(@"%@",[NSString stringWithFormat:@"%@",@"AAAA tcp"]);
                        _searchBlock(device->DeviceMac);
                    }
                    else
                    {
                        TSLog(@"回调未准备完善");
                        [DVNW.mDeviceDBDic removeObjectForKey:[NSString stringWithFormat:@"%@",device->DeviceMac]];
                    }
                }
                /****************创建设备*****************/
            }
            
        }
        /*状态查询08.
         *"On_off"="1"
         *"Run_mode"="1"
         *设备控制06.
         *"On_off"="1"
         *"Run_mode"="1"
         *事件消息03
         *eventType02:设备状态改变
         *eventType05:空调名称或网络改变
         *product
         */
        else if ([Funcod isEqualToString:kDevicesStatusRespondFC] || [Funcod isEqualToString:kDevicesControlsRespondFC] || [Funcod isEqualToString:kDevicesEventMessageFC])
        {
            /*
             *由于以下内容需要用到nsmutabledictionary，但json库返回的是dictionary类，导致        
             *NSMutableDictionary *vaildArr = [jsonObj objectForKey:@"validData"];
             *vaildArr为不可变NSMutableDictionary，导致移除错误
             */
            id tempJSONData = [NSJSONSerialization JSONObjectWithData:tjson options:NSJSONReadingMutableContainers error:nil];
            NSString *sHostMac = [tempJSONData objectForKey:@"originDeviceUUID"];
            
            NSMutableDictionary *vaildArr = [tempJSONData objectForKey:@"validData"];
            GELog(@"Loc vaildArr%@",vaildArr);


            /*
             *切换到该MAC对应所有空调控制数据库，否则新建一个基本的数据库。
             */
            AirCondDB *TDB = [DVNW->mACDeviceContrDB objectForKey:sHostMac];
            
            if (TDB == nil)
            {
                /*
                 *清除离线列表
                 */
                [DVNW->mOfflineDeviceDic removeObjectForKey:sHostMac];
                
                DVNW->mAcDB = [[AirCondDB alloc]init];
                DVNW->mAcDB->mTemMac = sHostMac;//连接sock、控制数据、终端数据的关键
                [DVNW->mAcDB LoadDeviceIcon];
                [DVNW->mACDeviceContrDB setObject:DVNW->mAcDB forKey:sHostMac];
            }
            else
            {
                DVNW->mAcDB = TDB;
            }
            
            /*
             *队列处理
             */
            if ([Funcod isEqualToString:kDevicesControlsRespondFC])
            {
                vaildArr = [self queueHandler:tempJSONData];
                
                int statusNum = [[vaildArr valueForKey:@"Status_Number"] intValue];
                TSLog(@"Status_Number2222 %d",statusNum);
                
                DeviceData *TheData = [DVNW->mDeviceDBDic objectForKey:sHostMac];
                TheData.Status_Number = statusNum;
                
                if (vaildArr == nil)
                {
//                    continue;
                    return nil;
                }
            }

            /*
             *状态查询及事件需要判断Status_Number
             */
            if ([Funcod isEqualToString:kDevicesStatusRespondFC])
            {
                DeviceData *TheData = [DVNW->mDeviceDBDic objectForKey:sHostMac];
                TheData.Status_Number = [[vaildArr valueForKey:@"Status_Number"] intValue];
                TSLog(@"%@",[NSString stringWithFormat:@"saaaStatus_Number %ld",(long)TheData.Status_Number]);
            }
            /*
             *事件消息帧
             */
            else if ([Funcod isEqualToString:kDevicesEventMessageFC])
            {
                DeviceData *TheData = [DVNW->mDeviceDBDic objectForKey:sHostMac];
                [DVNW ChangeCurrentDevice:sHostMac];
                /*
                 *事件消息回复04
                 */
                TheData->tempSeqNum = [tempJSONData objectForKey:@"sequenceNumber"];
                TSLog(@"tempSeqNum %s",TheData->tempSeqNum);
                [DVNW->mSendingBuffer removeAllObjects];
                DVNW->mSendingBuffer = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"null",@"null", nil];
                [DVNW SendTcpDataWithMode:-1 WithModeID:BtSendEventSubscribeActive WithData:nil WithEnctyp:YES];
                
                /*
                 *空调管理事件
                 *eventType 05
                 *productName 设备名称
                 *productReset 设备修改网络配置 0设备不重启 1设备准备重启
                 */
                if ([[vaildArr objectForKey:@"eventType"] isEqualToString:@"05"])
                {
                    if ([[vaildArr objectForKey:@"productReset"] isEqualToString:@"0"])
                    {
                        TheData->DeviceName = [vaildArr objectForKey:@"productName"];
                    }
                    else
                    {
                        //重新刷新列表？？？还是等待设备离线？？？
                    }
                    return nil;
                }
                /*
                 *设备状态推送事件
                 *eventType02:设备状态改变
                 *Status_Number如果事件消息来的num不连续，则全查状态
                 */
                else if([[vaildArr objectForKey:@"eventType"] isEqualToString:@"02"])
                {
                    /*
                     *如果sendingbuffer有数据，肯定是没发送完，没发完，不处理事件帧
                     */
                    NSMutableDictionary *dic = [DVNW->mSendingBufferOverflow objectForKey:sHostMac];
                    if (dic)
                    {
                        [DVNW->mSendingBufferOverflow removeObjectForKey:sHostMac];
                        //~暂时无法判断是否在发送期间接收到的事件帧，故无法不处理事件帧~
                        //continue;
                    }

                    
                    int statusNum = [[vaildArr valueForKey:@"Status_Number"] intValue];
                    TSLog(@"Status_Number2222 %d",statusNum);
                    
                     if((TheData.Status_Number + 1)%256 == statusNum)
                     {
                         TheData.Status_Number = statusNum;
                     }
                     else if((statusNum - TheData.Status_Number +256)%256 < 128 &&statusNum!= TheData.Status_Number)//全查
                     {
                         if (_statusSearchBlock)//发送设备状态查询
                         {
                             TSLog(@"%@",[NSString stringWithFormat:@"%@",@"eventType02:设备状态改变 tcp"]);
                             _statusSearchBlock(sHostMac);
                         }
                         return nil;
                     }
                     else
                     {
                        GELog(@"%@",[NSString stringWithFormat:@"Status_Number:%d TheData.Status_Number:%ld",statusNum,(long)TheData.Status_Number]);
                        return nil;
                     }
                }
                
            }
            /*
             *说明：只有控制数据才涉及切换数据库，故只需在对应的帧前切换即可
             *     提取控制公共数据及模式私有数据
             *     以下仅限用于空调设备
             * DB数组格式为[arr1,arr2]，现在的空调分两个数据库，但也可能只有一个数据库的其他设备，需要区分
             * arr1为public数据，arr2为mode数据
             * lookupType字段理解错误，无需
             */
    //        NSArray *DB = [DeviceRulesFactory MatchDBWithSigns:[vaildArr objectForKey:@"lookupType"] AndDeviceMAC:sHostMac];//根据设备类型，提取设备控制数据数据库
    //        [vaildArr removeObjectForKey:@"lookupType"];//使用完后即可删除，避免后续搜索key值浪费内存
            NSArray *DB = [DeviceRulesFactory MatchDBWithSigns:@"01" AndDeviceMAC:sHostMac];//根据设备类型，提取设备控制数据数据库
            
            NSArray *keys = [vaildArr allKeys];
            
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"OriginalAndNewDescriptionForACState" ofType:@"plist"];
            NSDictionary *plistDic = [[NSDictionary alloc]initWithContentsOfFile:filePath];//取出网络协议与本地键值对照表
            NSMutableArray *sleepArr = [[NSMutableArray alloc] initWithObjects:@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
            float temperature = 0.0f;
            float decimal = 0.0f;
            for (id key in keys)
            {
                NSString *nwMapLocKey = [plistDic valueForKey:key];//取出网络协议对应的本地键值
                int value = (int)strtoul([[vaildArr objectForKey:key] UTF8String], 0, 16);

                /***/
                if ([key isEqualToString:@"Timer_off_week"] || [key isEqualToString:@"Timer_on_week"])//广域网此key对应了本软件多个key，需要对应到本地key
                {
                    /*
                     *字典中的定时key数据顺序为：日、一、二、三、四、五、六
                     *取出plist里面的本地array，关键字"Timer_off_week"内含多个key
                     */
                    NSArray *keyArrs = [plistDic valueForKey:key];
                    NSLog(@"keyArrs %@",keyArrs);
                    
                    /*
                     *从网络协议数据取出值
                     *内容为：0xxxxxxx ，依次为 无、六、五、四、三、二、一、日
                     *更改：去除0xxxxxxx方式，改用二进制
                     */
//                    NSString *nwValueData = [vaildArr valueForKey:key];
                    
//                    int timeChip = (int)strtoul([nwValueData UTF8String], 0, 16);
                    
                    for (int i = 0;i < [keyArrs count];i++)
                    {
                        unsigned char ttemp = (value>>i)&0x01;
//                        因为DB的空调顺序为：[公共数据，私有数据],定时为公共数据，所以此处直接setobject即可
                        [[DB objectAtIndex:0] setObject:[NSNumber numberWithInt:ttemp] forKey:[keyArrs objectAtIndex:i]];//公共数据中改变定时关工作日相关值
                    }
                }
                else if ([key isEqualToString:@"Sleep_mode3_temp1"] || [key isEqualToString:@"Sleep_mode3_temp2"] || [key isEqualToString:@"Sleep_mode3_temp3"] || [key isEqualToString:@"Sleep_mode3_temp4"] || [key isEqualToString:@"Sleep_mode3_temp5"] || [key isEqualToString:@"Sleep_mode3_temp6"] || [key isEqualToString:@"Sleep_mode3_temp7"] || [key isEqualToString:@"Sleep_mode3_temp8"])
                {
                    value = [[vaildArr objectForKey:key] intValue] * 10;
                    
                    //因为键值可能不是按temp1、2、3进入，所以需要排序
                    unichar sleepChip = [key characterAtIndex:([key length]-1)];
                    int sleepChipData = sleepChip - 49;//根据key值最后的数字排序..数组下标
                    NSLog(@"key %@ , sleepChipData %d",key,sleepChipData);
                    [sleepArr replaceObjectAtIndex:sleepChipData withObject:[NSNumber numberWithInt:value]];
                    
                    [[DB objectAtIndex:1] setObject:sleepArr forKey:@"KDefaultSleepModeDIY"];//模式私有数据
                }
                else if ([key isEqualToString:@"Set_temp_integer"] || [key isEqualToString:@"Set_temp_decimal"])
                {
    //                int value = [[vaildArr objectForKey:key] intValue];//此处为16进制数
                    
//                    int value = (int)strtoul([[vaildArr objectForKey:key] UTF8String], 0, 16);
                    
                    float tep = [[[DB objectAtIndex:1] objectForKey:@"KDefaultTemperature"] floatValue];//模式私有数据
                    temperature = tep;//放错机制导致某些字段不存在
                    
                    if ([key isEqualToString:@"Set_temp_integer"])//整数
                    {
                        temperature = value;
                    }
                    else//小数
                    {
                        decimal += value*0.1;
                    }
                    
                    [[DB objectAtIndex:1] setObject:[NSNumber numberWithFloat:(temperature + decimal)] forKey:@"KDefaultTemperature"];//模式私有数据
                    
                }
                else if ([key isEqualToString:@"Noise_setting_cold"] || [key isEqualToString:@"Noise_setting_heat"])
                {
//                    int value = (int)strtoul([[vaildArr objectForKey:key] UTF8String], 0, 16);
                    
                    if ([key isEqualToString:@"Noise_setting_cold"])
                    {
                        [[DB objectAtIndex:0] setObject:[NSNumber numberWithInt:value] forKey:@"KDefaultCoolNoise"];//公共数据中改变定时关工作日相关值
                    }
                    else
                    {
                        [[DB objectAtIndex:0] setObject:[NSNumber numberWithInt:value] forKey:@"KDefaultHotNoise"];//公共数据中改变定时关工作日相关值
                    }
                }
                else if ([key isEqualToString:@"Up_down_swing"] || [key isEqualToString:@"Left_right_swing"])
                {
//                    int value = (int)strtoul([[vaildArr objectForKey:key] UTF8String], 0, 16);
                    
                    [[DB objectAtIndex:1] setObject:[NSNumber numberWithInt:value] forKey:nwMapLocKey];//模式私有数据
                }
                else
                {
                    //其它非特殊键值均在此处理
                    for (int j = 0; j < [DB count]; j ++)
                    {
                        for (id value in [[DB objectAtIndex:j] allKeys])//枚举空调公共数据及私有数据键值
                        {
                            if ([value isEqualToString:nwMapLocKey])
                            {
                                id nwValue = [vaildArr objectForKey:key];//从网络协议数据中读取数据
                                [[DB objectAtIndex:j] setObject:nwValue forKey:nwMapLocKey];//采用的是深复制，所以不需替换，只需修改值即可。
                                [vaildArr removeObjectForKey:key];//删除已找到的，避免浪费工作
                                break;
                            }
                            //
                        }
                    }
                    
                }
            }
            
            
            AirCondDB *mAcDB = [DVNW->mACDeviceContrDB objectForKey:sHostMac]; //控制数据，需要将数据库切换到对应device
            
            [mAcDB->mModeDefaultDB setValuesForKeysWithDictionary:[DB objectAtIndex:0]];
            
            NSNumber *mode = [mAcDB->mModeDefaultDB objectForKey:@"KDefaultMode"];//根据模式，将模式中的数据提取出来
            NSString *modeKeyWord = [mAcDB->mModeKey objectAtIndex:[mode intValue]];
            [mAcDB->mModeDefaultDB setObject:[DB objectAtIndex:1] forKey:modeKeyWord];
            [mAcDB UpDataForCurrentDB];//更新数据库内容
            [mAcDB UpDataDBDidEnd];//通知界面修改
            
            /*
             *需求变更1（连续发送情况）：同一设备连续发送时，不更新界面，但后台数据需改变直到最后一包到来。
             *
             */
            if ([DVNW.NetWorkDelegate respondsToSelector:@selector(RoomRenew:)])
            {
                [DVNW.NetWorkDelegate performSelector:@selector(RoomRenew:) withObject:nil];//状态查询到后，更新设备列表
            }
        }
        /*事件消息帧03.
         *（空调上线）
         *eventType=02
         *eventContent=01
         *slaveDeviceUUID:xxxx
         *(状态反馈，内机状态)
         *
         *"On_off"="1"
         *"Run_mode"="1"
         */
    //    else if ([Funcod isEqualToString:kDevicesEventMessageFC])
    //    {
    //        NSLog(@"收到事件帧回复");
    //        
    //    }
        /*
         *管理帧0D
         *不知回什么
         */
        else if ([Funcod isEqualToString:kDevicesManageRespondFC])
        {
            NSLog(@"收到管理帧回复");

        }
        
        
        //文件传输相关
        else if ([Funcod isEqualToString:kFileTransferActivateRespondFC]){
            
        }

    }
    return nil;
}

/*
 *返回YES则代表“需处理这包数据”
 *只有控制应答帧需要处理
 */
-(NSMutableDictionary *)queueHandler:(NSDictionary *)jsonDic
{
    BOOL need = NO;
    
    NSString *seqc = [jsonDic objectForKey:@"sequenceNumber"];

    NSString *uuid = [jsonDic objectForKey:@"originDeviceUUID"];
    NSMutableDictionary *dic = [DVNW->mSendingBufferOverflow objectForKey:uuid];
//    if (!dic) {
//        return [jsonDic objectForKey:@"validData"];
//    }
    
    NSDictionary *value = [dic objectForKey:seqc];
    //如果发现控制帧中间丢失，则不认为断网，但状态需全查!
    if([dic count] >= 1)
    {
        for (NSString *tempseqc in [dic allKeys])
        {
            //~~~~需要考虑翻转情况~~~~
            //如果存在中间seqc丢了，但statusNum仍然连续的情况，需全查。
            if( ([seqc intValue] - [tempseqc intValue] +256)%256 < 128 &&[seqc intValue]!= [tempseqc intValue])//全查
            {
                need = YES;
                [dic removeObjectForKey:tempseqc];//小于的全删除，因为后面的数据收到代表OK
            }
        }
        
        if (need)
        {
            [dic removeObjectForKey:seqc];

            if (_statusSearchBlock)//发送设备状态查询
            {
                TSLog(@"%@",[NSString stringWithFormat:@"%@",@"HandlerXXXX:设备状态改变 tcp"]);
                _statusSearchBlock(uuid);
            }
            return nil;//全查了就不用处理这包了
        }
     
    }
    
    if (value)
    {
        [dic removeObjectForKey:seqc];
        
        TSLog(@"disOverflow11 %@\n",dic);
        TSLog(@"disOverflow %@\n",DVNW->mSendingBufferOverflow);
        
        /*
         *判断对应值是否一致
         *是否键值不匹配即可认为是不一样？
         *匹配不一致时需要将一致的数据去除，只同步不一致数据~
         */
//        if ([[value allKeys] count] == [[[jsonDic objectForKey:@"validData"] allKeys] count])
        {
            for (NSString *keyname in [value allKeys])
            {
//                if ([[value objectForKey:keyname] isEqualToString:[[jsonDic objectForKey:@"validData"] objectForKey:keyname]])
                if([[value objectForKey:keyname]intValue] == [[[jsonDic objectForKey:@"validData"] objectForKey:keyname] intValue])
                {
                    [[jsonDic objectForKey:@"validData"] removeObjectForKey:keyname];
                }
                else
                {
                    need = YES;
                }
            }
            
            if (need)
            {
                return [jsonDic objectForKey:@"validData"];
            }
            else
            {
                return nil;
            }
        }
//        else
//        {
//            //键值对都不匹配，则认为是不一样的，需要处理数据
//            return nil;
//        }
        
    }

    //    [deviceDic setObject:kDevicesControlsFC forKey:[NSString stringWithFormat:@"%d",(DVNW->DeviceSeqc+1)]];
//    [DVNW->mSendingBufferOverflow setObject:deviceDic forKey:DVNW->objectUUID];
    return [jsonDic objectForKey:@"validData"];
}

#pragma mark --- maths
- (NSString *)stringFromHexString:(NSString *)hexString { //
    
    char *myBuffer = (char *)malloc((int)[hexString length] / 2 + 1);
    bzero(myBuffer, [hexString length] / 2 + 1);
    for (int i = 0; i < [hexString length] - 1; i += 2) {
        unsigned int anInt;
        NSString * hexCharStr = [hexString substringWithRange:NSMakeRange(i, 2)];
        NSScanner * scanner = [[NSScanner alloc] initWithString:hexCharStr];
        [scanner scanHexInt:&anInt];
        myBuffer[i / 2] = (char)anInt;
    }
    NSString *unicodeString = [NSString stringWithCString:myBuffer encoding:4];
    NSLog(@"------字符串=======%@",unicodeString);
    return unicodeString;
}

#pragma mark --- ControlMaths

-(id)BtSendSearchDeviceForEnctyp:(BOOL)enctyp//广播搜索帧
{
    DVNW->objectUUID = @"0";
    return [self getValidDataFromBufferWithFunctionCode:kDevicesSearchingFC];
}

-(id)BtSendSearchDeviceStatusForEnctyp:(BOOL)enctyp//状态查询帧
{
    return [self getValidDataFromBufferWithFunctionCode:kDevicesStatusFC];
}

-(id)BtSendControlForEnctyp:(BOOL)enctyp
{
    DVNW->objectUUID = DVNW->currentMac;//目标地址为空调
    
    //控制帧数据需添加statusNumber
    DeviceData *TheData = [DVNW->mDeviceDBDic objectForKey:DVNW->objectUUID];
    [DVNW->mSendingBuffer setObject:[NSString stringWithFormat:@"%d",(int)TheData->Status_Number] forKey:@"Status_Number"];
    
    /*
     *添加队列控制
     */
    NSMutableDictionary *deviceDic = [NSMutableDictionary dictionaryWithDictionary:[DVNW->mSendingBufferOverflow objectForKey:DVNW->objectUUID]];
    if ([deviceDic count] >= 3)
    {
        //关闭用户接口
        AppDelegate *app = [[UIApplication sharedApplication] delegate];
        app.window.userInteractionEnabled = NO;
        
        return nil;
    }
    
    NSMutableDictionary *tempDic = [NSMutableDictionary dictionaryWithDictionary:DVNW->mSendingBuffer];
    [deviceDic setObject:tempDic forKey:[NSString stringWithFormat:@"%d",(DVNW->DeviceSeqc+1)%256]];
    [DVNW->mSendingBufferOverflow setObject:deviceDic forKey:DVNW->objectUUID];
    TSLog(@"sendingOverflow %@\n",DVNW->mSendingBufferOverflow);
    
    return [self getValidDataFromBufferWithFunctionCode:kDevicesControlsFC];
}

-(id)BtSendFileTransferCommand:(BOOL)enctyp//启动文件传输请求帧
{
    DVNW->objectUUID = DVNW->currentMac;//目标地址为空调
    return [self getValidDataFromBufferWithFunctionCode:kFileTransferActivateFC];
}

-(id)BtSendManageCommand:(BOOL)enctyp//管理帧
{
    DVNW->objectUUID = DVNW->currentMac;//目标地址为空调
    return [self getValidDataFromBufferWithFunctionCode:kDevicesManageFC];
}

-(id)BtSendDeviceLoginCommand:(BOOL)enctyp//设备登陆帧
{
    DVNW->objectUUID = DVNW->currentMac;//目标地址为空调
    return [self getValidDataFromBufferWithFunctionCode:kDeviceLoginFC];
}

-(id)BtSendEventSubscribeActiveForEnctyp:(BOOL)enctyp//事件消息响应
{
    return [self getValidDataFromBufferWithFunctionCode:kDevicesEventMessageRespondFC];
}

-(id)BtFileTransferSendFilePropertyForEnctyp:(BOOL)enctyp //向空调发送文件属性
{
    DVNW->objectUUID = DVNW->currentMac;//目标地址为空调
    return [self getValidDataFromBufferWithFunctionCode:kFileTransferPropertyFC];
}

- (id)BtFileTransferAllFileDataSentForEnctyp:(BOOL)enctyp //向空调发送文件传输完毕帧
{
    DVNW->objectUUID = DVNW->currentMac;//目标地址为空调
    return [self getValidDataFromBufferWithFunctionCode:kFileTransferDoneFC];
}

- (id)BtFileTransferFilePropertyRespondForEnctyp:(BOOL)enctyp //向空调发送文件属性应答帧
{
    DVNW->objectUUID = DVNW->currentMac;//目标地址为空调
    return [self getValidDataFromBufferWithFunctionCode:kFileTransferPropertyRespondFC];
}

- (id)BtFileTransferAllFileDataSentRespondForEnctyp:(BOOL)enctyp
{
    DVNW->objectUUID = DVNW->currentMac;//目标地址为空调
    return [self getValidDataFromBufferWithFunctionCode:kFileTransferDoneRespond];
}

-(NSArray*)getValidDataFromBufferWithFunctionCode:(NSString*)functionCode
{
    NSString *seqnum;
//    DeviceNetWork *NW = [DeviceNetWork shareInstance];
    if ([functionCode isEqualToString:kDevicesEventMessageRespondFC])
    {
        DeviceData *TheData = [DVNW->mDeviceDBDic objectForKey:DVNW->currentMac];

        seqnum = TheData->tempSeqNum;
        TSLog(@"seccccc %@",seqnum);
    }else
    {
        DVNW->DeviceSeqc = (DVNW->DeviceSeqc + 1)%256;
        
        seqnum = [NSString stringWithFormat:@"%d",DVNW->DeviceSeqc];
    }
    
    NSArray *jsonArray = [JSONProcessing JSONWithValidData:DVNW->mSendingBuffer functionCode:functionCode sequenceNum:seqnum];
    
    NSLog(@"近程");
    return jsonArray;
}

@end
