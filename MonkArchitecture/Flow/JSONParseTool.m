//
//  JSONParseTool.m
//  Remote
//
//  Created by Zhang Hanying on 14-9-20.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "JSONParseTool.h"

@implementation JSONParseTool

+ (NSDictionary *)dictionaryParsedOfJson:(NSData*)json
{
//    NSDictionary *dic = [[NSDictionary alloc]init];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:json options:kNilOptions error:nil];
    return dic;
}

@end
