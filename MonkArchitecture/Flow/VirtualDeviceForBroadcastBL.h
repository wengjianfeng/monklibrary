//
//  VirtualDeviceForBroadcast.h
//  NewworkDemo
//
//  Created by gree's apple on 25/8/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VirtualDeviceProtocol.h"

typedef void (^BCBLBlock)(void);

@interface VirtualDeviceForBroadcastBL : NSObject <VirtualDeviceProtocol,GEUDPSocketDelegate>

@property (nonatomic, strong) GENetworking *_nw;
@property (nonatomic, strong) Command *_command;
@property (nonatomic, strong) GEWiFiContext *_context;


-(instancetype)initWithType:(protocolType)type;

-(void)sendMessage:(BLMessageEvt)msg;

-(GEWiFiContext *)configContextWithDictionary:(NSDictionary *)dic;

- (void)broadcast:(void (^)(void))success fail:(void (^)(void))fail;

- (void)creatUdpWithIpAndPortContext:(NSDictionary *)context;

@end
