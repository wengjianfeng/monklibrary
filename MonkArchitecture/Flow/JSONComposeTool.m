//
//  JSONCompose.m
//  Remote
//
//  Created by Zhang Hanying on 14-9-20.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "JSONComposeTool.h"

@implementation JSONComposeTool

+ (id)JSONComposedWithDictionary:(NSDictionary *)dictionary
{
    NSData *jsonData;
    if ([NSJSONSerialization isValidJSONObject:dictionary]) {
        NSError *error;
        jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
        //NSLog(@"打包好的Json是 --- %@",[[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
    return jsonData;
}

+ (id)JSONStringComposedWithDictionary:(NSDictionary *)dictionary
{
    NSString *JSONString;
    if ([NSJSONSerialization isValidJSONObject:dictionary]) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
        //NSLog(@"打包好的Json是 --- %@",[[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding]);
        JSONString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return JSONString;
}

+ (id)JSONStringComposedWithArray:(NSArray *)array
{
    NSString *JSONString;
    if ([NSJSONSerialization isValidJSONObject:array]) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
        //NSLog(@"打包好的Json是 --- %@",[[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding]);
        JSONString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return JSONString;
}

@end
