//
//  LoginHelper.m
//  G-Life_NewUI
//
//  Created by gree's apple on 21/10/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "LoginHelper.h"
#import "VerifyHelper.h"
#import "WrapperHelper.h"
#import "ControlJsonFormatLists.h"
#import "NotificationLists.h"
#import "CommunicateErrorData.h"
#import "VirtualServerBL.h"
#import "GEDeviceBLRepository.h"
#import "AppContext.h"
#import "NSObject+Helper.h"

@implementation LoginHelper
{
    NSString *_name;
    NSString *_psw;
    
    __block NSString *_info;
    __block NSError *_error;
    
    loginVerifyBlock _block;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        NSLog(@"LoginHelper Init");
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(DispatchNotification:)
                                                     name:Flow_serverFeedbackNormalFrame
                                                   object:nil];
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:Flow_serverFeedbackNormalFrame
                                                  object:nil];
    NSLog(@"LoginHelper dealloc **");
}

-(NSString *)loginUserNameKeyFromChain:(NSString *)userInfo
{
    VerifyHelper *vh = [[VerifyHelper alloc] init];
    __block NSString *key = @"";
    [vh verifyWithName:userInfo withVerifyBlock:^(NSString *verifyInfo, NSError *error) {
        if (!error) {
            key = verifyInfo;
        } else {
            _block(_info,_error);
            _block = nil;
        }
    }];

    if ([key isEqualToString:@"phoneNumberValid"]) {
        return @"userMobile";
    } else if ([key isEqualToString:@"eMailAddressValid"]) {
        return @"userMail";
    } else if ([key isEqualToString:@"userNameValid"]) {
        return @"userName";
    } else {
        return @"userName";
    }
    
    return @"";
}

- (void)loginVerifyHelperName:(NSString *)name password:(NSString *)psw withblock:(loginVerifyBlock)block
{
    _info = nil;
    _error = nil;
    _block = block;
    
    [AppContext shareAppContext].loginSuccess = NO;
    
    VerifyHelper *vh = [[VerifyHelper alloc] init];
    [vh verifyWithName:name password:psw withVerifyBlock:^(NSString *verifyInfo, NSError *error) {
        _info = verifyInfo;
        _error = error;
        
        _name = name;
        _psw = psw;
        WrapperHelper *helper = [[WrapperHelper alloc] init];
        
        if (!error) {
            _error = nil;
            //发送登录帧
            NSLog(@"Login _psw %@ name:%@",_psw,name);
            [helper wrapperName:_name Password:_psw]; // 保存

            VirtualServerBL *bl = (VirtualServerBL *)[[GEDeviceBLRepository shareInstance]
                                                      findVirtualDeviceByMac:[[AppContext shareAppContext] servermac]];
            [bl autoAccountLoginStuff]; // 发送
            
        } else {
            //底层提示错误，根据具体需要展示界面(如果需要知道更详细的信息，那么底层需要修改一下返回数据)
            [helper clearWrapper];
            [AppContext shareAppContext].loginSuccess = NO;

            _block(_info,_error);
            _block = nil;
        }
    }];
}

-(void)logOutWithBlock:(loginVerifyBlock)block
{
    _block = block;
    if ([AppContext shareAppContext].loginSuccess == YES) {
        VirtualServerBL *bl = (VirtualServerBL *)[[GEDeviceBLRepository shareInstance]
                                                  findVirtualDeviceByMac:[[AppContext shareAppContext] servermac]];
        [bl stuffFormatMutableDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"null",@"null", nil]];
        [bl excuteWithTcpCmd:KJsonUserLogoutRequestFC];
        [AppContext shareAppContext].loginSuccess = NO;

    } else {
        _error = [NSError errorWithDomain:@"LogoutError" code:0 userInfo:nil];
        [AppContext shareAppContext].loginSuccess = NO;

        _block(@"登出失败",_error);
        _block = nil;
    }
}

#pragma mark - receiveHandler
// 收到服务器信息之后进行分发
-(void)DispatchNotification:(NSNotification *)notification
{
    NSArray *arr = [notification object];
    if ([[arr objectAtIndex:0] isEqualToString:KJsonUserLoginRespondFC]) {
        [self Rm_LoginSucceed:arr];
    }
}

//“登录请求响应”处理
-(void)Rm_LoginSucceed:(id)info
{
    NSArray *data = info;
    
    NSString *userEamil = @"";
    NSString *userMobile = @"";
    NSString *message = @"";
    
    NSString *statusCode = [data objectAtIndex:1];   //状态码
    CommunicateErrorData *errorData  = [[CommunicateErrorData alloc] init]; //服务器返回数据
    NSString *statusCode_accountOnlyCombinePhone = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocAccountNumberOnlyCombined", @"账户只有手机号码")]; //@“21”
    NSString *statusCode_accountOnlyCombineMail = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocAccountMailOnlyCombined", @"账号只有邮箱")]; //@"22"
    NSString *statusCode_accountCombineMailAndPhone = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocAccountBothNumberMailCombined", @"账号手机号码和邮箱都存在")]; //@"23"
    NSString *statusCode_accountNotActivatedByPhone = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocNotActivatedByPhone", @"未使用手机激活")]; //@"17"
    NSString *statusCode_accountNotActivatedByEMail = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocNotActivatedByEMail", @"未使用邮箱激活")]; //@"18"
    
    if ([statusCode isEqualToString:statusCode_accountOnlyCombinePhone] || [statusCode isEqualToString:statusCode_accountOnlyCombineMail] || [statusCode isEqualToString:statusCode_accountCombineMailAndPhone])
    {
        NSLog(@"登录成功");
        message = @"登录成功";

        if (data.count > 2) {//使用手机或者邮箱登录时，登录响应帧返回除功能码和状态码之外的第三个数据：ValidData。此处的判断为了防止使用用户名登录时导致的数组越界
            _name = [data objectAtIndex:2][@"userName"];
            userEamil = [data objectAtIndex:2][@"userEamil"];
            userMobile = [data objectAtIndex:2][@"userMobile"];
        }
        
        if([statusCode isEqualToString:statusCode_accountNotActivatedByPhone]){ //未使用手机激活
            message = [errorData errorDetailWithStatusCode:statusCode];
            _error = [NSError errorWithDomain:@"error" code:0 userInfo:nil];
        } else if ([statusCode isEqualToString:statusCode_accountNotActivatedByEMail]){ //未使用邮箱激活
            message = [errorData errorDetailWithStatusCode:statusCode];
            _error = [NSError errorWithDomain:@"error" code:0 userInfo:nil];
        }
        
        //将绑定过的手机和邮箱信息置为非空，以便以后点击侧边栏“(修改)绑定{手机，邮箱}”时做判断
        if ([statusCode isEqualToString:statusCode_accountOnlyCombinePhone]){ //只绑定了手机
            
        }else if ([statusCode isEqualToString:statusCode_accountOnlyCombineMail]){ //只绑定了邮箱
            
        }else if ([statusCode isEqualToString:statusCode_accountCombineMailAndPhone]){ //手机邮箱均已绑定
            
        }
        [AppContext shareAppContext].userName = _name;
        [AppContext shareAppContext].userTele = userMobile;
        [AppContext shareAppContext].userMail = userEamil;
        [AppContext shareAppContext].loginSuccess = YES;

    } else if ([statusCode isEqualToString:statusCode_accountNotActivatedByPhone]) {
        //账户未激活
        //需要判断输入的内容是手机号还是用户名或者邮箱
        LoginHelper *h = [[LoginHelper alloc] init];
        NSString *key = [h loginUserNameKeyFromChain:_name];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_name,key, nil];

        //重新发送校验码请求(登录后的激活，服务器不会发送校验码给手机号，所以需要手动添加)
        VerifyHelper *vh = [[VerifyHelper alloc] init];
        [vh verifyWithUserInfoMutaDictionary:dic withCmd:KJsonRequestCheckCodeFC withVerifyBlock:^(NSString *verifyInfo, NSError *error) {
            
        }];
        
        message = @"ActivateBySMS";
        _error = [NSError errorWithDomain:@"error" code:0 userInfo:nil];

    } else {
        message = [errorData errorDetailWithStatusCode:statusCode];
        _error = [NSError errorWithDomain:@"error" code:0 userInfo:nil];
        
        [AppContext shareAppContext].loginSuccess = NO;
    }
    
    _block(message,_error);
    _block = nil;
}
@end
