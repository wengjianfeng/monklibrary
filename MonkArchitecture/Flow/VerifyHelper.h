//
//  VerifyHelper.h
//  G-Life_NewUI
//
//  Created by gree's apple on 21/10/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^verifyHelperBlock)(NSString *verifyInfo,NSError *error);

@interface VerifyHelper : NSObject

//登录用
- (void)verifyWithName:(NSString *)name
              password:(NSString *)psw
       withVerifyBlock:(verifyHelperBlock)block;

//注册用
- (void)verifyWithEmailORPhone:(NSString *)ep
                      userName:(NSString *)name
               withVerifyBlock:(verifyHelperBlock)block;

- (void)verifyWithPassword:(NSString *)psw
           comparePassword:(NSString *)cPsw
           withVerifyBlock:(verifyHelperBlock)block;

//检测账号名称类型
- (void)verifyWithName:(NSString *)name
       withVerifyBlock:(verifyHelperBlock)block;

//注册用于发送接收响应
-(void)verifyWithUserInfoMutaDictionary:(NSMutableDictionary *)infoMdic
                                withCmd:(NSString *)cmd
                        withVerifyBlock:(verifyHelperBlock)block;
//验证验证码
- (void)verifyWithActivateCode:(NSString *)ac
               withVerifyBlock:(verifyHelperBlock)block;
//修改密码验证
- (void)verifyWithOriginPassword:(NSString *)op
                     newPassword:(NSString *)np
                 comparePassword:(NSString *)cp
                 withVerifyBlock:(verifyHelperBlock)block;

@end
