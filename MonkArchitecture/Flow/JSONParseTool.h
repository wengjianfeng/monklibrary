//
//  JSONParseTool.h
//  Remote
//
//  Created by Zhang Hanying on 14-9-20.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JSONParseTool : NSObject

+(NSDictionary*)dictionaryParsedOfJson:(id)json;

@end
