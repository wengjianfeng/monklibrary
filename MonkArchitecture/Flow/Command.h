//
//  Command.h
//  NewworkDemo
//
//  Created by gree's apple on 25/8/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GEFormatFactory.h"

@protocol VirtualDeviceProtocol;

@interface Command : NSObject

/*
 打包数据帧：适用于需要读取设备数据才能构建数据包的需求
 @param identify 根据不同format格式，数据对应的identify
 @param HEX Factory
 @param bl "设备数据单元"，用于打包format所需的信息
 
 @return 待发送的数据包
 */
- (NSData *)excuteWithIdentify:(NSString *)identify
                   withFactory:(id <GEFormatFactory>)factory
                        withBL:(id <VirtualDeviceProtocol>)bl;

/*
 打包数据帧：适用于需要读取设备数据才能构建数据包的需求
 @param identify 根据不同format格式，数据对应的identify
 @param JSON Factory
 @param ctx 无需查询数据库信息，外部构建控制数据
 
 @return 待发送的数据包
 */
- (NSData *)excuteWithIdentify:(NSString *)identify
                   withFactory:(id <GEFormatFactory>)factory
                   withContext:(NSMutableDictionary *)ctx;

/*
 解析数据帧：解析通过socket获取的数据帧
 @param data socket获取的数据帧
 @param factory  HEX or JSON Factory
 @param <VirtualDeviceProtocol>
 @warning: 统一VirtualDeviceProtocol协议
 
 @return 一般情况下都是void，特别情况返回dictionary!看具体类的使用
 */
- (id)parseWithData:(NSData *)data
         withFactory:(id <GEFormatFactory>)factory
              withBL:(id<VirtualDeviceProtocol>)bl;


@end
