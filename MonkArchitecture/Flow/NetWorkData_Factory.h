//
//  NetWorkData_Factory.h
//  Remote
//
//  Created by gree's apple on 18/9/14.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <Foundation/Foundation.h>

//Json相关
#import "JSONComposeTool.h"
#import "FrameData.h"
#import "PackDictionary.h"
#import "JSONProcessing.h"
typedef enum
{
    BtMasterDeclare = 0,//宣告入网
    BtMasterQuit,//手机退网
    
    BtSendRegister,//注册
    BtSendActive,//激活
    BtSendActiveMail,
    BtSendVerify,//验证码校验
    BtResendIdentifyCode, // 重新发送注册验证码
    
    BtSendLogin,//登录
    BtSendSearchDevice,//搜索帧(设备注册表请求) 0x42~0x43 登录之后通过此帧可以获取在线、不在线的设备，以及具体设备网络信息
    
    BtSendSearchDeviceStatus,//状态查询帧 0x07~0x08  搜索到设备后，需要请求状态查询帧
    BtSendControl,//控制帧 0x05~0x06
    //Rm_SendSearchDeviceCommand,//设备搜索，0x62~0x0B多余的？？？？ 讨论删除
    ////-(void)Rm_GetDeviceDescriptionFile;//获取设备描述文件 返回数据是一台的还是多台的？多台时如何处理？ 0x08    走WIFI单芯片检测板升级协议
    //
    BtSendLogout,//账号退出 0x44~0x45
    BtSendUserPwChange,//修改密码 0x4E~0x4F
    BtSendUserPwFind,//找回密码 0x50~？？？？？？？？？？？
    BtSendUserPwChangeVerify,//验证码校验（密码找回）0x50~0xB5???????
    BtSendUserPwchangeNew,//发送新密码 0x16~0xB6
    BtResendUserPwdIdentifyCode, //重新发送验证码（密码找回）
    //
    BtSendBindTelp,//绑定手机请求
    BtSendBindTelpChange,//修改绑定手机请求
    BtSendBindTelpCodeRequest,//绑定手机获取验证码请求
    BtSendBindTelpCodeVerify,//验证码校验
    //
    BtSendBindEmail,//绑定邮箱
    BtSendBindEmailChange, // 修改绑定邮箱
    //
    BtSendBindDevice,//用户绑定 0x5A~0x5B
    BtSendBindDeviceCancel,//用户解绑 0x5C~0x5D
    //
    BtSendEventSubscribe,//事件订阅0x03~0x...
    BtSendEventSubscribeActive,//事件消息响应0x04~0x...
    //
    BtSendRSAExchangeCommand,//请求服务器公钥RSA
    BtSendAESExchangeCommand,//交互AES
    
    BtSendFileTransferCommand,//启动文件传输请求帧 A0
    
    BtSendManageCommand,//管理帧
    
    BtSendDeviceLoginCommand,//设备登陆帧
    
    BtFileTransferFetchLatestVersion,//向服务器请求最新的升级文件版本 A6
    
    BtFileTransferWhetherUpgradeByServer, //向服务器回应是否由服务器升级空调 A9
    
    BtFileTransferSendFileProperty, //传输文件属性 A2
    
    BtFileTransferAllFileDataSent, //文件发送完毕确认 A4
    
    BtFileTransferFilePropertyRespond, //文件属性应发 A3
    
    BtFileTransferAllFileDadaSentRespond, //文件传输完毕确认 A5
    
}typeID;

@interface NetWorkData_Factory : NSObject
{
    @private
    typeID NWid;
}

-(id)DataForID:(int)foo WithData:(id)data WithEnctyp:(BOOL)enctyp;
-(id)DataFormJSON:(id)json;

/*
 *以下方法均能通过DataForID:WithData:WithEnctyp:调用，此处提供单独方法调用。
 *如果不在父类添加以下方法，在子类中则需要做大量重复工作
 *"以下方法不建议直接调用"
 */
-(id)BtMasterDeclare:(BOOL)enctyp;//宣告入网 0x30 UDP方式？？？一直发？？？

-(id)BtMasterQuit:(BOOL)enctyp;//手机退网 0x31~0x UDP方式？？？

-(id)BtSendRegisterForEnctyp:(BOOL)enctyp;//注册 0x46~0x47

-(id)BtResendIdentifyCodeForEnctyp:(BOOL)enctyp; // 重新发送注册验证码

-(id)BtSendActiveForEnctyp:(BOOL)enctyp;//激活 0x48~0x49

-(id)BtSendActiveMailForEnctyp:(BOOL)enctyp; //邮箱激活

-(id)BtSendVerifyForEnctyp:(BOOL)enctyp;//验证码校验 0x4C~0x4D

-(id)BtSendLoginForEnctyp:(BOOL)enctyp;//登录 0x40~0x41

-(id)BtSendSearchDeviceForEnctyp:(BOOL)enctyp;//搜索帧(设备注册表请求) 0x42~0x43 获得的是设备信息，设备状态呢？？设备描述呢？？

-(id)BtSendSearchDeviceStatusForEnctyp:(BOOL)enctyp;//状态查询帧 0x07~0x08

-(id)BtSendControlForEnctyp:(BOOL)enctyp;//控制帧 0x05~0x06
//-(void)Rm_SendSearchDeviceCommandForEnctyp:(BOOL)enctyp;//设备搜索，0x62~0x0B多余的？？？？ 讨论删除
//-(void)Rm_GetDeviceDescriptionFile;//获取设备描述文件 返回数据是一台的还是多台的？多台时如何处理？ 0x08    走WIFI单芯片检测板升级协议

-(id)BtSendLogoutForEnctyp:(BOOL)enctyp;//账号退出 0x44~0x45

-(id)BtSendUserPwChangeForEnctyp:(BOOL)enctyp;//修改密码 0x4E~0x4F

-(id)BtSendUserPwFindForEnctyp:(BOOL)enctyp;//找回密码 0x50~？？？？？？？？？？？

-(id)BtSendSesetPwdForEnctyp:(BOOL)enctyp;

-(id)BtSendUserPwChangeVerifyForEnctyp:(BOOL)enctyp;//发送验证码（密码找回）0x50~0xB5???????

-(id)BtSendUserPwchangeNewForEnctyp:(BOOL)enctyp;//发送新密码 0x16~0xB6

-(id)BtSendBindTelpForEnctyp:(BOOL)enctyp;//绑定手机号

-(id)BtSendBindTelpChangeForEnctyp:(BOOL)enctyp;//修改绑定手机

-(id)BtSendBindTelpCodeRequestForEnctyp:(BOOL)enctyp;// 请求绑定手机验证码

-(id)BtSendBindTelpCodeVerifyForEnctyp:(BOOL)enctyp;//绑定手机验证码校验

-(id)BtSendBindEmailForEnctyp:(BOOL)enctyp;//绑定邮箱 0x58~0x55???????

-(id)BtSendBindEmailChangeForEnctyp:(BOOL)enctyp; // 修改绑定邮箱

-(id)BtSendBindDeviceForEnctyp:(BOOL)enctyp;//用户绑定 0x5A~0x5B

-(id)BtSendBindDeviceCancelForEnctyp:(BOOL)enctyp;//用户解绑 0x5C~0x5D

-(id)BtSendEventSubscribe:(BOOL)enctyp;//事件订阅0x03~0x...

-(id)BtSendEventSubscribeActiveForEnctyp:(BOOL)enctyp;//事件消息响应0x04~0x...

-(id)BtSendRSAExchangeCommand:(NSString *)data Enctyp:(BOOL)enctyp;//请求服务器公钥

-(id)BtSendAESExchangeCommand:(NSString *)data Enctyp:(BOOL)enctyp;//AES交互

-(id)BtSendFileTransferCommand:(BOOL)enctyp;//启动文件传输请求帧

-(id)BtSendManageCommand:(BOOL)enctyp;//管理帧

-(id)BtSendDeviceLoginCommand:(BOOL)enctyp;//设备登陆帧

-(id)BtResendUserPwdIdentifyCodeForEnctyp:(BOOL)enctyp; // 重新发送验证码（密码找回）

-(id)BtFileTransferFetchLatestVersionForEnctyp:(BOOL)enctyp; //向服务器请求最新的升级文件版本

-(id)BtFileTransferWhetherUpgradeByServerForEnctyp:(BOOL)enctyp; //向服务器回应是否由服务器升级空调

-(id)BtFileTransferSendFilePropertyForEnctyp:(BOOL)enctyp; //向空调发送文件属性

-(id)BtFileTransferAllFileDataSentForEnctyp:(BOOL)enctyp; //向空调发送文件发送完毕帧

-(id)BtFileTransferFilePropertyRespondForEnctyp:(BOOL)enctyp; //向空调发送文件属性应答

-(id)BtFileTransferAllFileDataSentRespondForEnctyp:(BOOL)enctyp; //向空调发送文件传输完毕应答

@end
