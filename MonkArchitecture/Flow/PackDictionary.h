//
//  Frame.h
//  Remote
//
//  Created by Zhang Hanying on 14-9-20.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FrameData.h"

@interface PackDictionary : NSObject

-(NSString*)getPhoneUUID;
-(void)switchUUIDsofCommunicationFrame:(FrameData*)frame;

+(NSDictionary*)dictionaryWithFrameData:(FrameData*)frameData;

@end
