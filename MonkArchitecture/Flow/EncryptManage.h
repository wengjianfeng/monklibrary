//
//  EncryptManage.h
//  NewworkDemo
//
//  Created by gree's apple on 18/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, EncryptType) {
    EncryptType_NONE = 0,
    EncryptType_RSA,
    EncryptType_AES,
};

@interface EncryptManage : NSObject

//加密
+ (NSMutableArray *)encryptFrame:(NSMutableArray *)frame type:(EncryptType)type;

//解密
+ (NSData *)decryptFrame:(NSData *)data;

@end
