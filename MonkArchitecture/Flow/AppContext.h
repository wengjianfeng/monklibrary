//
//  GetIpV4.h
//  RemoteController
//
//  Created by WJF-Monk  330694 on 13/11/12.
//  Copyright (c) 2012 WJF-Monk  330694. All rights reserved.
//
//  Use for get IP from system,firstly ,you must connet the wifi in setting

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface AppContext : NSObject

@property (nonatomic, copy, readonly) NSString *lh;             //本地HOST
@property (nonatomic, readonly) NSInteger hexlp;                //本地PORT(HEX)
@property (nonatomic, readonly) NSInteger jslp;                 //本地PORT(JSON)
@property (nonatomic, copy, readonly) NSString *bch;            //广播HOST
@property (nonatomic, readonly) NSInteger bcp;                  //HEX广播PORT
@property (nonatomic, readonly) NSInteger bcJSp;                //JSON格式广播PORT
@property (nonatomic, readonly) NSInteger slp;                   //近程Port

@property (nonatomic, readonly) NSString *sh;                   //服务器HOST
@property (nonatomic, readonly) NSInteger sp;                   //服务器Port
@property (nonatomic, readonly) NSString *servermac;            //服务器mac

@property (nonatomic, readonly) NSString *appv;                 //软件版本

@property (nonatomic, copy, readonly) NSString *osv;            //系统版本
@property (nonatomic, copy, readonly) NSString *osn;            //系统名称

@property (nonatomic, copy, readonly) NSString *teln;           //手机名称
@property (nonatomic, copy, readonly) NSString *uuid;

@property (nonatomic, readonly) NSInteger networkStatus;        //网络状态

@property (nonatomic, copy, readonly) NSString *currentMac;    /*
                                                                 1.当前界面代表的设备mac值。
                                                                 2.此mac仅与当前设备界面挂钩。
                                                                 3.不存在界面的mac通信，使用临时mac。
                                                                 */
@property (nonatomic, copy, readwrite) NSString *tempMac;       //用于非界面对应的mac
@property (nonatomic, copy, readwrite) NSString *uiIdentify;    //用于界面对应的mac

@property (nonatomic, strong, readwrite) NSString *userName;    //登录用户名
@property (nonatomic, strong, readwrite) NSString *userTele;    //用户手机
@property (nonatomic, strong, readwrite) NSString *userMail;    //用户邮箱
@property (nonatomic, readwrite) BOOL connectSuccess;           //服务器连接状态
@property (nonatomic, readwrite) BOOL loginSuccess;             //登录状态

+ (AppContext *)shareAppContext;
- (void)clearUserInfo;  // 清除用户信息

@end
