//
//  VirtualDeviceForBroadcast.m
//  NewworkDemo
//
//  Created by gree's apple on 25/8/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "VirtualDeviceForBroadcastBL.h"
#import "GEHexFactory.h"
#import "GEJsonFactory.h"
#import "GEDeviceBLRepository.h"
#import "VirtualDeviceBL.h"
#import "ControlHexFormatLists.h"
#import "ControlJsonFormatLists.h"
#import "AppContext.h"
#import "NotificationLists.h"

#import "OfflineDetect.h"
#define repeatTime 5

@implementation VirtualDeviceForBroadcastBL
{
    GEUDPSocket *udp;
    RTUDPDAO *udpDao;
    
    BCBLBlock _bcSuccessBlock;
    BCBLBlock _bcFailBlock;
    
    protocolType _mType;
    NSDictionary *_dic;
    
    OfflineDetect *_detect;
    
    BOOL isStartBroadcast;
}
@synthesize _command,_nw,_context;

#pragma mark-
#pragma mark life cycle
-(instancetype)init
{
    self = [super init];
    if (self) {
        _nw = [GENetworking shareInstance];
        udpDao = [RTUDPDAO shareInstance];
        _context = [[GEWiFiContext alloc] init];
        _command = [[Command alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(detectDeviceOfflineNotificate:)
                                                     name:Flow_deviceOffline
                                                   object:nil];
    }
    return self;
}

-(instancetype)initWithType:(protocolType)type
{
    self = [self init];
    if (self) {
        _mType = type;
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:Flow_deviceOffline object:nil];
}

- (void)creatUdpWithIpAndPortContext:(NSDictionary *)context
{
    [_nw set_udpDao:udpDao];
    
    udp = [_nw creatUdpBroadCastWithDAO:udpDao withIpAndPortContext:context withDelegate:self];
}

#pragma mark-
#pragma mark Command
//Command
-(void)broadcast:(void (^)(void))success fail:(void (^)(void))fail
{
    isStartBroadcast = YES;
    
    _bcSuccessBlock = success;
    _bcFailBlock = fail;
    NSData *data = nil;
    
    id<GEFormatFactory> fac;
    if (_mType == hex) {
        fac = [[GEHexFactory alloc] init];
        data = [_command excuteWithIdentify:KHexDataBroadcast withFactory:fac withBL:nil];
    }
        
    [udp writeData:data withRepeat:repeatTime];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[NSRunLoop currentRunLoop] runMode:NSRunLoopCommonModes beforeDate:[NSDate distantFuture]];
            //设置超时，未完成必须关闭
            if (udp._status == GEUDPStatus_NONE) {
                udp._status = GEUDPStatus_Fail;
            }
        });
        
        //如果不关闭的话，CPU将满负载
        while (udp._status == GEUDPStatus_NONE){
            [[NSRunLoop currentRunLoop] runMode:NSRunLoopCommonModes beforeDate:[NSDate distantFuture]];
        }
        udp._status == GEUDPStatus_Success?_bcSuccessBlock():_bcFailBlock();
    });
}

#pragma mark-
#pragma mark message
-(void)sendMessage:(BLMessageEvt)msg
{
    NSString *mac = [NSString stringWithFormat:@"%s",_context.mWifiInfo.mac];
    
    if (msg == BLMessageEvt_BroadcastDone) {
      /****************创建设备*****************/
        GEDeviceBLRepository *rp = [GEDeviceBLRepository shareInstance];
        id<VirtualDeviceProtocol> obj = [rp findVirtualDeviceByMac:mac];
        //STEP1:设备创建及TCP Socket连接及之后的状态查询
        if (!obj) {
            NSLog(@"2222222222222");
            VirtualDeviceBL *vdbl = [[VirtualDeviceBL alloc] initWithType:_mType];
            //deep copy
            [vdbl._context setValuesForKeysWithDictionary:_dic];
            vdbl.identify = mac;    //需要手动设置关键的MAC

            [rp addVirtualDeviceObj:vdbl];   //加入repository进行管理//创建设备应该是在tcp连接完的情况下吧？？
            
            [vdbl creatTcpWithIpAndPortContext:@{
                                                 @"IP"  : [NSString stringWithFormat:@"%s",_context.mWifiInfo.ip],
                                                 @"Port": [NSNumber numberWithInteger:[AppContext shareAppContext].slp],    //协议规定
                                                 }];
            [[NSNotificationCenter defaultCenter] postNotificationName:Flow_updataSuccess object:nil];
        }else {
            //采用json格式时需要区分远程搜索及近程搜索共用数据问题
            //json格式逻辑：当近程存在空调时，无论远程是否有，都走近程。那么就存在时序问题，如果远程先到，则需要处理一下数据
            GEWiFiContext *ctx = [obj _context];
            //多次搜索，避免添加多余
            if ( (_context.mWiFiNetWorkStatus.isLAN != ctx.mWiFiNetWorkStatus.isLAN) && (isStartBroadcast == YES)) {
                NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:YES],@"mWiFiNetWorkStatus.isLAN", nil];
                [obj._context setValuesForKeysWithDictionary:dic];

                [obj creatTcpWithIpAndPortContext:@{
                                                     @"IP"  : [NSString stringWithFormat:@"%s",_context.mWifiInfo.ip],
                                                     @"Port": [NSNumber numberWithInteger:[AppContext shareAppContext].sp]    //协议规定
                                                     }];
                [[NSNotificationCenter defaultCenter] postNotificationName:Flow_updataSuccess object:nil];
            }
        }
        isStartBroadcast = NO;  // 关闭搜索通道
      /****************创建设备*****************/
        
        //STEP2:断网检测；如果秒内（检测板20/s发一次）没收到则认为设备丢失！**仅对近程(JSON)有效**
        if (_mType == json) {
//            [self detectDeviceOfflineWithMac:mac];
        }
        
    }
}

#pragma mark-
#pragma mark GEWiFiContext
-(GEWiFiContext *)configContextWithDictionary:(NSDictionary *)dic
{
    _dic = [NSDictionary dictionaryWithDictionary:dic];

    [_context setValuesForKeysWithDictionary:dic];
    
    return _context;    
}

#pragma mark-
#pragma mark UdpDelegate
- (BOOL)onUdpSocket:(GEUDPSocket *)sock didReceiveData:(NSData *)data fromHost:(NSString *)host port:(UInt16)port
{
    /*
     *因为是业务层啊，所以应当在这里处理hex or json AND Encrypt
     */
    unsigned char *buf;
    buf = (unsigned char *)[data bytes];
    id<GEFormatFactory> fac;
    if (buf[0] == 0x7E && buf[1] == 0x7E) {
        fac = [[GEHexFactory alloc] init];
    }else{
        //可能是加密的json or other
        fac = [[GEJsonFactory alloc] init];
    }
    
    [_command parseWithData:data withFactory:fac withBL:self];

    return YES;
}

#pragma mark-
#pragma mark deviceOffline
- (void)detectDeviceOfflineWithMac:(NSString *)mac {
    if (!_detect) {
        _detect = [[OfflineDetect alloc] init];
    }
    [_detect startDetectOfflineWithMac:mac];
}

- (void)detectDeviceOfflineNotificate:(NSNotification *)noti
{
    NSString *mac = [noti object];
    
    [[GEDeviceBLRepository shareInstance] removeByMac:mac];
    [[NSNotificationCenter defaultCenter] postNotificationName:Flow_updataSuccess object:nil];
}

@end
