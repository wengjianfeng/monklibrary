//
//  GEFormatFactory.h
//  NewworkDemo
//
//  Created by gree's apple on 26/8/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>

//@protocol VirtualDeviceProtocol;
//@class GEAirCDeviceInfoBL;
@protocol VirtualDeviceProtocol;

@protocol GEFormatFactory <NSObject>

@optional
- (NSData *)creatWithIdentify:(NSString *)identify withBL:(id <VirtualDeviceProtocol>)bl;

- (NSData *)creatWithIdentify:(NSString *)identify withDictionary:(NSMutableDictionary *)dic;

- (id)parseWithData:(NSData *)data withBL:(id)bl;

@end