//
//  CommunicatErrorData.m
//  Remote
//
//  Created by Zhang Hanying on 14-9-20.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "CommunicateErrorData.h"

@implementation CommunicateErrorData
{
    NSDictionary *errorDetail;
}

-(id)init
{
    if (self = [super init]) {
        errorDetail = [[NSDictionary alloc]initWithObjectsAndKeys:
                            NSLocalizedString(@"locCommunicateFailure",@"通信失败"),@"0",
                            NSLocalizedString(@"locCommunicateSucess",@"通信成功"),@"1",
                            NSLocalizedString(@"locAccountExisted",@"账号已存在"),@"2",
                            NSLocalizedString(@"LocPhoneNumberExisted",@"手机已存在"),@"3",
                            NSLocalizedString(@"LocEmailExisted", @"邮箱已存在"),@"4",
                            NSLocalizedString(@"LocPhoneCheckCodeSent", @"手机验证码已发送"),@"5",
                            NSLocalizedString(@"LocEmailActivationLinkSent", @"邮箱激活链接已发送"),@"6",
                            NSLocalizedString(@"LocCheckCodeIncorrect", @"验证码错误"),@"7",
                            NSLocalizedString(@"LocCheckCodeOutOfDate", @"验证码已失效，请重新获取验证码"),@"8",
                            NSLocalizedString(@"LocPhoneActivationFailure", @"手机激活失败"),@"9",
                            NSLocalizedString(@"LocCheckCodeSendFailure", @"验证码发送失败"),@"10",
                            NSLocalizedString(@"LocEmailSendFailure", @"邮件激活链接发送失败"),@"11",
                            NSLocalizedString(@"LocBothUsernameAndUserTelExist", @"用户名和手机号都存在"),@"12",
                            NSLocalizedString(@"LocBothUsernameAndUserMailExist", @"用户名和邮箱都存在"),@"13",
                            NSLocalizedString(@"LocAccountNotExist", @"账号不存在"),@"16",
                            NSLocalizedString(@"LocNotActivatedByPhone", @"未使用手机激活"),@"17",
                            NSLocalizedString(@"LocNotActivatedByEMail", @"未使用邮箱激活"),@"18",
                            NSLocalizedString(@"LocAccountPwdNotMatch", @"账号密码不匹配"),@"19",
                            NSLocalizedString(@"LocAccountNotActivate", @"账号未激活"),@"20",
                            NSLocalizedString(@"LocAccountNumberOnlyCombined", @"账户只有手机号码"),@"21",
                            NSLocalizedString(@"LocAccountMailOnlyCombined", @"账号只有邮箱"),@"22",
                            NSLocalizedString(@"LocAccountBothNumberMailCombined", @"账号手机号码和邮箱都存在"),@"23",
                            NSLocalizedString(@"LocPhoneNumberNotBind", @"未绑定手机号"),@"32",
                            NSLocalizedString(@"LocAccountNotCombineEMail", @"账号未绑定邮箱"),@"33",
                            NSLocalizedString(@"LocRandomCodeIncorrect", @"随机码错误"),@"34",
//                            NSLocalizedString(@"LocUserSubmittedDataIncorrect", @"用户提交的数据有误"),@"35",
                            NSLocalizedString(@"LocCombineACFailure", @"绑定空调失败"),@"48",
                            NSLocalizedString(@"LocCancelCombineACFailure", @"解除绑定空调失败"),@"50",
                            NSLocalizedString(@"LocUserNotLoggedIn", @"用户未登录"),@"62",
                            NSLocalizedString(@"LocServerThrowException", @"服务器程序抛出异常"),@"64",
                            NSLocalizedString(@"LocServerDataBaseOperationFailure", @"服务器端数据库操作失败"),@"65",
                            NSLocalizedString(@"LocUserSubmittedInfoIncorrect", @"用户信息输入有误"),@"66",
                            NSLocalizedString(@"LocObjectDeviceOfflineOrNotExist", @"目标设备不在线或者不存在"),@"67",
                            NSLocalizedString(@"locDataPackageWithUnknownFunctionCode", @"未知功能码的数据包"),@"68",
                            NSLocalizedString(@"locUserNotLoggedIn", @"用户未登录"),@"69",
                            NSLocalizedString(@"locReLogInError", @"重复登录错误"),@"70",
                            NSLocalizedString(@"locNewVersionOfApp", @"G-Life有新的版本"),@"71",
                            NSLocalizedString(@"locControlDeviceFirstConnetServer", @"控制设备第一次连接服务器"),@"72",
                            NSLocalizedString(@"locAppUpdateAndControlDeviceFirstConnetServer", @"软件有更新，并且控制设备第一次连接服务器"),@"73",
                            nil];
    }
    return self;
}

-(NSString *)errorDetailWithStatusCode:(NSString *)statusCode
{
    return  [errorDetail valueForKey:statusCode];
}

-(NSString*)statusCodeWithErrorDetail:(NSString*)errorDetails
{
    return [[errorDetail allKeysForObject:errorDetails] firstObject];
}

@end
