//
//  AccountHelper.m
//  G-Life_NewUI
//
//  Created by gree's apple on 1/12/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "AccountHelper.h"
#import "AppContext.h"
#import "VirtualServerBL.h"
#import "GEDeviceBLRepository.h"
#import "ControlJsonFormatLists.h"
#import "VerifyHelper.h"
#import "WrapperHelper.h"
#import "NSObject+Helper.h"
#import "NotificationLists.h"
#import "CommunicateErrorData.h"
@implementation AccountHelper
{
    __block NSString *_info;
    __block NSError *_error;
    
    AccountBlock _block;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(DispatchNotification:)
                                                     name:Flow_serverFeedbackNormalFrame
                                                   object:nil];
    }
    
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:Flow_serverFeedbackNormalFrame
                                                  object:nil];
    NSLog(@"AccountHelper dealloc **");
}

- (void)logoutWithBlock:(AccountBlock)block    //退出账号
{
    _block = block;
    if ([AppContext shareAppContext].loginSuccess == YES) {
        VirtualServerBL *bl = (VirtualServerBL *)[[GEDeviceBLRepository shareInstance]
                                                  findVirtualDeviceByMac:[[AppContext shareAppContext] servermac]];
        [bl stuffFormatMutableDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"null",@"null", nil]];
        [bl excuteWithTcpCmd:KJsonUserLogoutRequestFC];
    } else {
        _error = [NSError errorWithDomain:@"LogoutError" code:0 userInfo:nil];
        _block(@"退出账号错误",_error);
        _block = nil;
    }
}

- (void)changePasswordWithOriginPassword:(NSString *)opsw
                             newPassword:(NSString *)npsw
                         comparePassword:(NSString *)cpsw
                               withBlock:(AccountBlock)block //修改密码
{
     _info = nil;
     _error = nil;
     _block = block;
      
     VerifyHelper *vh = [[VerifyHelper alloc] init];
     [vh verifyWithPassword:npsw comparePassword:cpsw withVerifyBlock:^(NSString *verifyInfo, NSError *error) {
         _info = verifyInfo;
         _error = error;

         if (!error) {
             _error = nil;
             NSString *op = [NSObject md5FromString:opsw];
             NSString *cp = [NSObject md5FromString:cpsw];
             
             VirtualServerBL *bl = (VirtualServerBL *)[[GEDeviceBLRepository shareInstance]
                                                       findVirtualDeviceByMac:[[AppContext shareAppContext] servermac]];
             [bl stuffFormatMutableDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:[AppContext shareAppContext].userName,@"userName",op,@"userPassword",cp,@"userNewPassword", nil]];
             [bl excuteWithTcpCmd:KJsonPasswordChangeRequestFC];
         } else {
             _block(_info,_error);
             _block = nil;
         }
     }];
}

#pragma mark - notification
#pragma mark - receiveHandler
// 收到服务器信息之后进行分发
-(void)DispatchNotification:(NSNotification *)notification
{
    NSArray *arr = [notification object];
    if ([[arr objectAtIndex:0] isEqualToString:KJsonUserLogoutRespondFC]) { //用户登出
        [self Rm_LogoutSucceed:arr];
    } else if ([[arr objectAtIndex:0] isEqualToString:KJsonPasswordChangeRespondFC]) { //更改密码
        [self Rm_ChangePassword:arr];
    }
}

-(void)Rm_LogoutSucceed:(id)info
{
    NSArray *data = info;
    NSString *statusCode = [data objectAtIndex:1];
    if ([statusCode isEqualToString:@"1"]) {
        _error = nil;
        [AppContext shareAppContext].loginSuccess = NO;
        //退出成功清除储存的账号密码
        WrapperHelper *helper = [[WrapperHelper alloc] init];
        [helper clearWrapper];
        _info = @"退出登录成功";
    }else{ // 退出登录失败
        CommunicateErrorData *errorData = [[CommunicateErrorData alloc] init];
        _info = [errorData errorDetailWithStatusCode:statusCode];
        _error = [NSError errorWithDomain:@"LogoutError" code:0 userInfo:nil];
    }
    
    _block(_info,_error);
    _block = nil;
}

-(void)Rm_ChangePassword:(id)info
{
    NSArray *data = info;
    NSString *statusCode = [data objectAtIndex:1];
    if ([statusCode isEqualToString:@"1"]) {
        _info = @"修改成功";
        _error = nil;
    }else{ // 退出登录失败
        CommunicateErrorData *errorData = [[CommunicateErrorData alloc] init];
        _info = [errorData errorDetailWithStatusCode:statusCode];
        _error = [NSError errorWithDomain:@"LogoutError" code:0 userInfo:nil];
    }
    
    _block(_info,_error);
    _block = nil;
}
@end
