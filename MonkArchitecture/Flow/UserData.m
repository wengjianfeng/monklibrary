//
//  UserData.m
//  Remote
//
//  Created by Zhang Hanying on 14-9-20.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "UserData.h"

@implementation UserData

// 四种情况：正常登录 用邮箱注册 用手机号码注册 修改密码
// 1.正常登录
-(id)initWithUsername:(NSString *)userName password:(NSString *)password
{
    if (self = [super init]) {
        self.userName = userName;
        self.userPassword = password;
    }
    return  self;
    
}
// 2.用邮箱注册
- (id)initWithUsername:(NSString *)userName password:(NSString *)password eMailAddress:(NSString *)emailAddress
{
    if (self = [super init]) {
        self.userName = userName;
        self.userPassword = password;
        self.userMail = emailAddress;
    }
    return  self;
}
// 3.用手机号码注册
- (id)initWithUsername:(NSString *)userName password:(NSString *)password phoneNumber:(NSString *)phoneNumber
{
    if (self = [super init]) {
        self.userName = userName;
        self.userPassword = password;
        self.userMobile = phoneNumber;
    }
    return  self;
}

// 4.修改密码
- (id)initWithUsername:(NSString *)userName password:(NSString *)password userNewPassword:(NSString *)userNewPassword
{
    if (self = [super init]) {
        self.userName = userName;
        self.userPassword = password;
        self.userNewPassword = userNewPassword;
    }
    return  self;
}

@end
