//
//  VirtualServerBL.m
//  NewworkDemo
//
//  Created by gree's apple on 15/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "VirtualServerBL.h"
#import "GEDeviceBLRepository.h"
#import "ControlHexFormatLists.h"
#import "ControlJsonFormatLists.h"
#import "GEHexFactory.h"
#import "GEJsonFactory.h"
#import "AppContext.h"
#import "VirtualDeviceBL.h"

#import "RSACryptor.h"
#import "AESCrypt.h"
#import "EncryptManage.h"

#import "UnpackTool.h"
#import "NSTimer+Helper.h"
#import "NSObject+Helper.h"

#import "KeychainItemWrapper.h"
#import "LoginHelper.h"
#import "NotificationLists.h"

@interface VirtualServerBL()
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userPsw;
@property (nonatomic) LoginHelper *helper;
@property (nonatomic, weak) NSTimer *timer;

@end

@implementation VirtualServerBL
{
    GETCPSocket *tcp;
    GEUDPSocket *udp;
    RTTCPDAO *tcpDao;
    RTUDPDAO *udpDao;
    
    BOOL isAESExchangeComplete;
    BOOL isRSAExchangeComplete;
    
    NSString *_mToken;    //推送Token
    BOOL isTokenSended;  //是否发送完毕
}

@synthesize _context,_nw,_command,identify,sendingBuffer;

#pragma mark-
#pragma mark Lift cycle
-(instancetype)initWithType:(protocolType)type
{
    self = [self init];
    if (self) {
        self._mType = type;
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        _nw = [GENetworking shareInstance];
        _context = [[GEWiFiContext alloc] init];
        _command = [[Command alloc] init];
        
        tcpDao = [RTTCPDAO shareInstance];
        udpDao = [RTUDPDAO shareInstance];
        
        identify = [[AppContext shareAppContext] servermac];
        
        sendingBuffer = [[NSMutableDictionary alloc] initWithCapacity:1];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(creatToken:) name:@"pushTokenNotification" object:nil];  //注册推送通知
//        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(tokenError:) name:@"pushTokenNotificationError" object:nil];//获取Token失败通知
    }
    
    return self;
}

-(void)dealloc
{
    [_timer invalidate];
    _timer = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushTokenNotification" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pushTokenNotificationError" object:nil];
    NSLog(@"******************************************\n %s %@ dealloc",__FILE__,self);
}

#pragma mark-
#pragma mark Message
-(void)sendMessage:(BLMessageEvt)msg
{
//    if (msg == BLMessageEvt_DeviceInterrupt) {         //此处一般都是网络类
//        //通知到mediator
//    }
//    [[NSNotificationCenter defaultCenter] postNotificationName:updataNotification
//                                                        object:[NSString stringWithFormat:@"%d",msg]];
}

#pragma mark-
#pragma mark GEWiFiContext
-(GEWiFiContext *)configContextWithDictionary:(NSDictionary *)dic
{
    [_context setValuesForKeysWithDictionary:dic];
    
    return _context;
}

#pragma mark-
#pragma mark Network
-(void)creatTcpWithIpAndPortContext:(NSDictionary *)context
{
    [_nw set_tcpDao:tcpDao];
    _nw.identify = identify;
    
    tcp = [_nw creatTcpWithDAO:tcpDao
          withIpAndPortContext:context
                      withType:GETcpTypeOfPTS
                  withDelegate:self];
    
    tcp._isAlive = NO;
}

-(void)creatUdpWithIpAndPortContext:(NSDictionary *)context
{

}
#pragma mark-
#pragma mark Frame
-(NSData *)creatFrameDataWithCmd:(NSString *)cmd
{
    [AppContext shareAppContext].tempMac = identify; //设置mac
    
    id<GEFormatFactory> fac;
    NSData *data = nil;
    
    fac = [[GEJsonFactory alloc] init];
    //自动添加uuid
    if (![[sendingBuffer allKeys] containsObject:@"objectUUID"]) {
        [sendingBuffer setValue:identify forKey:@"objectUUID"];
    }
    
    data = [_command excuteWithIdentify:cmd withFactory:fac withContext:sendingBuffer];
    
    return data;
}

#pragma mark-
#pragma mark Command
-(void)excuteWithTcpCmd:(NSString *)cmd
{
    NSData *data = [self creatFrameDataWithCmd:cmd];
    
    NSMutableArray *encryptArr = [[NSMutableArray alloc] initWithCapacity:1];
    
    //交互都完成进行加密处理
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:data, nil];
    //加密
    encryptArr = [EncryptManage encryptFrame:array type:EncryptType_AES];

    for (int i = 0 ; i < [encryptArr count]; i++) {
        NSData *d = [encryptArr objectAtIndex:i];
        
        if (d == nil) {
            return;
        }
        [tcp writeData:d];
    }
    
}
-(void)excuteWithUdpCmd:(NSString *)cmd
{

}

#pragma mark-
#pragma mark Sutff
- (void)stuffFormatMutableDictionary:(NSMutableDictionary *)dictionary
{
    [sendingBuffer removeAllObjects];
    sendingBuffer = dictionary;
}

#pragma mark-
#pragma mark TcpDelegate
- (BOOL)GEonSocketWillConnect:(GETCPSocket *)sock
{
    NSLog(@"%s sock WillConnect",__FILE__);
    return YES;
}

- (void)GEonSocket:(GETCPSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    NSLog(@"%s sock didConnect",__FILE__);
    [AppContext shareAppContext].connectSuccess = YES;
    /********创建设备实体********/
    GEDeviceBLRepository *rp = [GEDeviceBLRepository shareInstance];
    //搜索到只需创建一次，之后都是修改其内容
    VirtualServerBL *obj = (VirtualServerBL *)[rp findVirtualDeviceByMac:[self identify]];
    if (!obj) {
        //保存BL
        [rp addVirtualDeviceObj:self];
    }
    /********创建设备实体********/
    
    /********************************************/
    /* Step1 : 交互RSA密钥，只需交互一次            */
    /********************************************/
    [self RSAExchange];
    //延迟一次，若还不行则放弃
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"ExchangeComple"]){
            [self RSAExchange];
        }
    });
}

- (void)GEonSocket:(GETCPSocket *)sock didReadData:(NSData *)data
{
    unsigned char *buf;
    buf = (unsigned char *)[data bytes];
    
//    NSString *c = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//    NSLog(@"Server RC : %@",c);
//    NSString *str = @"{   \"
//    \""deviceType":	"20",\"
//    "protocolEdition":	"64",
//    "originDeviceUUID":	"123456788765432112345678a0b40023",
//    "objectDeviceUUID":	"3A0E8796BF9B4BB3ACBA1E6869C1C80D",
//    "subPackage":	"0023",
//    "functionCode":	"06",
//    "sequenceNumber":	"1",
//    "dataLength":	"2",
//    "validData":	{
//        "FrzRmSG":	"2",
//        "Status_Number":	"30"
//    },
//    "sign":	"f2927fc0db5b4a1c"
//}{
//    "deviceType":	"20",
//    "protocolEdition":	"64",
//    "originDeviceUUID":	"123456788765432112345678a0b40023",
//    "objectDeviceUUID":	"3A0E8796BF9B4BB3ACBA1E6869C1C80D",
//    "subPackage":	"0023",
//    "functionCode":	"06",
//    "sequenceNumber":	"1",
//    "dataLength":	"2",
//    "validData":	{
//        "FrzRmSG":	"3",
//        "Status_Number":	"30"
//    },
//    "sign":	"c06dce0f07f0c900"
//}{
//    "deviceType":	"20",
//    "protocolEdition":	"64",
//    "originDeviceUUID":	"123456788765432112345678a0b40023",
//    "objectDeviceUUID":	"3A0E8796BF9B4BB3ACBA1E6869C1C80D",
//    "subPackage":	"0023",
//    "functionCode":	"06",
//    "sequenceNumber":	"1",
//    "dataLength":	"2",
//    "validData":	{
//        "FrzRmSG":	"4",
//        "Status_Number":	"30"
//    },
//    "sign":	"c8390cef75c98a83"
//}";
    if (buf[0] == 0x7E && buf[1] == 0x7E) {
        [NSException exceptionWithName:@"receiveError" reason:@"7e7e" userInfo:nil];
    }else{
        //处理可能的粘包
        NSString *s = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"解密前的数据: %@",s);

        NSMutableArray *arr = [self unPack:data];

        GEJsonFactory *fac = [[GEJsonFactory alloc] init];

        for (NSData *d in arr) {
            //解密
            NSData *data = [EncryptManage decryptFrame:d];
            
            NSString *l = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"解密后的数据: %@",l);

            //拦截分发(涉及流程的并且无需解包的，直接拦截！Example:RSA/AES)
            BOOL b = [self dispatchEvents:data];
            if (!b) {
                return;
            }
            //处理数据
            [_command parseWithData:data withFactory:fac withBL:self];//涉及与服务器的交流使用self
        }
    }
}

- (void)GEonSocketDidDisconnect:(GETCPSocket *)sock
{
    [[AppContext shareAppContext] clearUserInfo];
    [AppContext shareAppContext].connectSuccess = NO;
    NSLog(@"%s sock didDisconnect",__FILE__);
}

#pragma mark-
#pragma mark UdpDelegate
- (BOOL)onUdpSocket:(GEUDPSocket *)sock didReceiveData:(NSData *)data fromHost:(NSString *)host port:(UInt16)port
{
    NSLog(@"Udp didReceiveData %@",data);
    return YES;
}

#pragma mark- 处理token通知
//正常获取时生成Token
-(void) creatToken:(NSNotification *)notification
{
    NSDictionary *theToken = [notification userInfo];
    
    isTokenSended = NO;
     _mToken = [theToken objectForKey:@"myToken"];
//    发送Token
    [self sendToken];
}
////获取错误时返回0000
//-(void) tokenError:(NSNotification *)notification
//{
//    _mToken = @"0000";    //无法获取时发送0000
//}

//发送推送Token
-(void) sendToken
{
        NSLog(@"发送Token:%@",_mToken);
    if (isTokenSended == NO) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:_mToken,@"deviceToek" ,nil];
        [self packageBuilder:dic type:EncryptType_AES cmd:KJsonEventMessageFC];
        isTokenSended = YES;
    }
}

#pragma mark-
#pragma mark Exchange Methods
- (void)RSAExchange
{
    //若已经交互过了，则直接交互AES
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ExchangeComple"]){
        /********************************************/
        /* Step2 : 交互AES密钥，每次启动应用都要交互     */
        /********************************************/
        [self AESRequest];
        return;
    }
    
    RSACryptor *r = [RSACryptor shareInstance];
    
    long randomData = random();
    NSString *random = [NSString stringWithFormat:@"%ld",randomData];
    
    [[NSUserDefaults standardUserDefaults] setValue:random forKey:@"random1"];
    
    NSString *pubStr = [NSString stringWithFormat:@"%@",r._RSAKeyPair.publicKey];

    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:pubStr,@"pubkey",random,@"identifyCode",identify,@"objectUUID",nil];
//    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:pubStr,@"pubkey",random,@"identifyCode",identify,@"objectUUID",_mToken,@"deviceToek",nil];

    
    [self packageBuilder:dic type:EncryptType_RSA cmd:KJsonClientPostPubKey];
}



//请求会话密钥
- (void)AESRequest
{
    long randomData = random();
    NSString *random = [NSString stringWithFormat:@"%ld",randomData];
    
    [[NSUserDefaults standardUserDefaults] setValue:random forKey:@"random2"];
    
    NSString *str = [NSString stringWithFormat:@"%@",random];
    //因为这个值不需要key，所以用undefine，打包会过滤掉。
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] initWithObjectsAndKeys:str,@"undefine",identify,@"objectUUID", nil];
    
    [self packageBuilder:dic type:EncryptType_RSA cmd:KJsonClientRequestSessionKey];
}

//服务器心跳
- (void)keepAlive
{
    NSLog(@"keepAlive");
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"null",@"null",identify,@"objectUUID", nil];

    [self packageBuilder:dic type:EncryptType_AES cmd:KJsonPhoneInNetworkAnnouncementFC];
}

//自动登录
- (void)autoAccountLogin
{
    KeychainItemWrapper *wrapper = [[KeychainItemWrapper alloc]initWithIdentifier:@"password" accessGroup:nil];
    
    NSString *name = [wrapper objectForKey:(__bridge id)kSecAttrAccount];
    NSString *password = [wrapper objectForKey:(__bridge id)kSecValueData];
    _userName = name;
    _userPsw = password;
}

- (void)autoAccountLoginStuff
{
    [self autoAccountLogin];
    NSLog(@"Step3 自动登录 name:%@ , pw:%@",_userName,_userPsw);
    LoginHelper *h = [[LoginHelper alloc] init];
    NSString *key = [h loginUserNameKeyFromChain:_userName];
    [self stuffFormatMutableDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:_userName,key,[NSObject md5FromString:_userPsw],@"userPassword",identify,@"objectUUID", nil]];
    [self packageBuilder:sendingBuffer type:EncryptType_AES cmd:KJsonUserLoginRequestFC];
}

/********************************************/
/* Step4 :  注册表查询                         */
/********************************************/
- (void)statusRequest
{
    NSLog(@"step4 : 注册表查询");
    NSMutableDictionary *dic =  [[NSMutableDictionary alloc] initWithObjectsAndKeys:@"null",@"null",identify,@"objectUUID", nil];
    
    [self packageBuilder:dic type:EncryptType_AES cmd:KJsonRequestDeviceRegistryFC];
}


#pragma mark-
#pragma mark UnPack
- (NSMutableArray *)unPack:(NSData *)data
{
    //粘包拆包

    UnpackTool *unpack = [UnpackTool shareInstance];
    NSMutableArray *store = [unpack unpackWithData:data];
    
    return store;
}

#pragma mark-
#pragma mark RSA/AES PackageBuilder
- (void)packageBuilder:(NSMutableDictionary *)dic type:(EncryptType)type cmd:(NSString *)cmd
{
    //产生数据
    GEJsonFactory *fac = [[GEJsonFactory alloc] init];
    NSData *data = [_command excuteWithIdentify:cmd withFactory:fac withContext:dic];
    NSMutableArray *array = [[NSMutableArray alloc] initWithObjects:data, nil];
    //加密
    NSMutableArray *muarr = [EncryptManage encryptFrame:array type:type];
    //发送
    for (int i = 0; i<[muarr count]; i++) {
        [tcp writeData:[muarr objectAtIndex:i]];
    }
}


#pragma mark-
#pragma mark Dispatch Events
/*
    收码处理
    @return 返回为NO时，无需进行解包处理
 */
- (BOOL)dispatchEvents:(NSData *)events
{
    return YES;
}

#pragma mark-
#pragma mark setter and getter

#pragma mark - loginHelper
- (void)loginHelperb
{
    [self.helper loginVerifyHelperName:_userName
                         password:_userPsw
                        withblock:^(NSString *info, NSError *error) {
                            if (!error) {
                                //注册表查询
                                [self statusRequest];
                            } else {
                                //提示错误
                                NSLog(@"Login errorInfo: %@",info);
                            }
                            self.helper = nil;
                        }];
}


@end
