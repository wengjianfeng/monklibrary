//
//  GEDeviceBLRepository.h
//  NewworkDemo
//
//  Created by gree's apple on 8/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol VirtualDeviceProtocol;
@protocol GEDeviceInfoProtocol;

/************************************************************************************/
/* 控制业务逻辑的地方，client想获取任何数据都可通过此                                       */
/************************************************************************************/

@interface GEDeviceBLRepository : NSObject
{
    //这两个库在此，可以比较数目是否一样，不一样则认为需要重发或查询或提示
    @private
    NSMutableDictionary *virtualDeviceRepository;//虚拟设备业务层
    NSMutableDictionary *DeviceInfoDBRepository;//设备数据业务层
}
//移植到APPContext中
//@property (nonatomic, strong) NSString *excuteMac;    //当前mac，界面or后台需要发送数据的mac

+ (GEDeviceBLRepository *)shareInstance;

- (BOOL) removeByMac:(NSString *)mac;

- (BOOL)clear;

-(BOOL)clearWithOutObjectName:(NSString *)name;         //remove without "server"

/*********************************************/
/*       这是包括wifi之类的信息  */
/*********************************************/
- (void) addVirtualDeviceObj:(id<VirtualDeviceProtocol>)obj;

- (BOOL) modifyVirtualDeviceObj:(id<VirtualDeviceProtocol>)obj byMac:(NSString *)mac;

- (id<VirtualDeviceProtocol>) findVirtualDeviceByMac:(NSString *)mac;

- (NSMutableDictionary*) findAllVirtualDevice;

/*********************************************/
/*       这是设备控制状态信息  */
/*********************************************/
- (void) addDeviceInfoDBObj:(id<GEDeviceInfoProtocol>)obj byMac:(NSString *)mac;

- (BOOL) modifyDeviceInfoDBObj:(id<GEDeviceInfoProtocol>)obj byMac:(NSString *)mac;

- (id<GEDeviceInfoProtocol>) findDeviceInfoDBByMac:(NSString *)mac;

- (NSMutableDictionary*) findAllDeviceInfoDBDevice;

- (BOOL) removeDeviceInfoByMac:(NSString *)mac;

@end
