//
//  NetWorkData_Factory.m
//  Remote
//
//  Created by gree's apple on 18/9/14.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "NetWorkData_Factory.h"
#import "DeviceNetWork.h"
#import "RSACryptor.h"
#import "AESCrypt.h"

@implementation NetWorkData_Factory

-(id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

-(id)DataForID:(int)foo WithData:(id)data WithEnctyp:(BOOL)enctyp;
{
    NWid = foo;
    /*
     *"0"无加密，"1"RSA加密，"2"AES加密
     */
    NSString *EncryptType = @"0";
    if (enctyp == YES)
    {
        EncryptType = @"2";
    }
    
    NSMutableArray *PutData;
    
    switch (NWid) {
        case BtMasterDeclare:
        {
            PutData =  [self BtMasterDeclare:enctyp];
        }
            break;
        case BtMasterQuit:
        {
            [self BtMasterQuit:enctyp];
        }
            break;
        case BtSendRegister:
        {
            PutData = [self BtSendRegisterForEnctyp:enctyp];
        }
            break;
        case BtSendActive:
        {
            PutData = [self BtSendActiveForEnctyp:enctyp];
        }
            break;
        case BtSendActiveMail:
        {
            PutData = [self BtSendActiveMailForEnctyp:enctyp];
        }
            break;
        case BtSendVerify:
        {
            PutData = [self BtSendVerifyForEnctyp:enctyp];
        }
            break;
        case BtSendLogin:
        {
            PutData = [self BtSendLoginForEnctyp:enctyp];
        }
            break;
        case BtSendSearchDevice:
        {
            PutData = [self BtSendSearchDeviceForEnctyp:enctyp];
        }
            break;
        case BtSendSearchDeviceStatus:
        {
            PutData = [self BtSendSearchDeviceStatusForEnctyp:enctyp];
        }
            break;
        case BtSendControl:
        {
            PutData = [self BtSendControlForEnctyp:enctyp];
        }
            break;
        case BtSendLogout:
        {
            PutData = [self BtSendLogoutForEnctyp:enctyp];
        }
            break;
        case BtSendUserPwChange:
        {
            PutData =  [self BtSendUserPwChangeForEnctyp:enctyp];
        }
            break;
        case BtSendUserPwFind:
        {
            PutData =  [self BtSendUserPwFindForEnctyp:enctyp];
        }
            break;
        case BtSendUserPwChangeVerify:
        {
            PutData =  [self BtSendUserPwChangeVerifyForEnctyp:enctyp];
        }
            break;
        case BtSendUserPwchangeNew:
        {
            PutData =  [self BtSendUserPwchangeNewForEnctyp:enctyp];
        }
            break;
        case BtSendBindTelp:
        {
            PutData =  [self BtSendBindTelpForEnctyp:enctyp];
        }
            break;
        case BtSendBindTelpChange:
        {
            PutData =  [self BtSendBindTelpChangeForEnctyp:enctyp];
        }
            break;
        case BtSendBindTelpCodeRequest:
        {
            PutData =  [self BtSendBindTelpCodeRequestForEnctyp:enctyp];
        }
            break;
        case BtSendBindTelpCodeVerify:
        {
            PutData =  [self BtSendBindTelpCodeVerifyForEnctyp:enctyp];
        }
            break;
        case BtSendBindEmail:
        {
            PutData =  [self BtSendBindEmailForEnctyp:enctyp];
        }
            break;
        case BtSendBindEmailChange:
        {
            PutData =  [self BtSendBindEmailChangeForEnctyp:enctyp];
        }
            break;
        case BtSendBindDevice:
        {
            PutData =  [self BtSendBindDeviceForEnctyp:enctyp];
        }
            break;
        case BtSendBindDeviceCancel:
        {
            PutData =  [self BtSendBindDeviceCancelForEnctyp:enctyp];
        }
            break;
        case BtSendEventSubscribe:
        {
            PutData = [self BtSendEventSubscribe:enctyp];
        }
            break;
        case BtSendEventSubscribeActive:
        {
            PutData =  [self BtSendEventSubscribeActiveForEnctyp:enctyp];
        }
            break;
        case BtSendRSAExchangeCommand:
        {
            PutData =  [self BtSendRSAExchangeCommand:data Enctyp:enctyp];
            EncryptType = @"1";
        }
            break;
        case BtSendAESExchangeCommand:
        {
            PutData = [self BtSendAESExchangeCommand:data Enctyp:enctyp];
            EncryptType = @"1";
        }
            break;
        case BtSendFileTransferCommand:
        {
            PutData = [self BtSendFileTransferCommand:enctyp];
        }
            break;
        case BtSendManageCommand:
        {
            PutData = [self BtSendManageCommand:enctyp];
            
        }
            break;
        case BtSendDeviceLoginCommand:
        {
            PutData = [self BtSendDeviceLoginCommand:enctyp];
        }
            break;
        case BtResendIdentifyCode:
        {
            PutData = [self BtResendIdentifyCodeForEnctyp:enctyp];
        }
            break;
        case BtResendUserPwdIdentifyCode:
        {
            PutData = [self BtResendUserPwdIdentifyCodeForEnctyp:enctyp];
        }
            break;
        case BtFileTransferFetchLatestVersion:
        {
            PutData = [self BtFileTransferFetchLatestVersionForEnctyp:enctyp];
        }
            break;
        case BtFileTransferWhetherUpgradeByServer:
        {
            PutData = [self BtFileTransferWhetherUpgradeByServerForEnctyp:enctyp];
        }
            break;
        case BtFileTransferSendFileProperty:
        {
            PutData = [self BtFileTransferSendFilePropertyForEnctyp:enctyp];
        }
            break;
        case BtFileTransferAllFileDataSent:
        {
            PutData = [self BtFileTransferAllFileDataSentForEnctyp:enctyp];
        }
            break;
        case BtFileTransferFilePropertyRespond:
        {
            PutData = [self BtFileTransferFilePropertyRespondForEnctyp:enctyp];
        }
            break;
        case BtFileTransferAllFileDadaSentRespond:
        {
            PutData = [self BtFileTransferAllFileDataSentRespondForEnctyp:enctyp];
        }

        default:
            break;
    }
    NSData *aa = [PutData objectAtIndex:0];
    NSString *bb = [[NSString alloc] initWithData:aa encoding:NSUTF8StringEncoding];
    TSLog(@"%@",[NSString stringWithFormat:@"原始发送数据 %@",bb]);
   
    PutData = [self NWKFrame:PutData encryptType:EncryptType];
    
    return PutData;

}

-(id)NWKFrame:(NSMutableArray *)frame encryptType:(NSString *)type
{
    if ([type isEqualToString:@"1"])
    {//RSA加密,密钥传输阶段
        for (int i = 0; i < [frame count]; i++)
        {
            NSData *stringdata = [frame objectAtIndex:i];
            //加密
            RSACryptor *r = [RSACryptor shareInstance];
            NSString *RSAEncryptString = [r RSA_EncryptUsingServerPublicKeyWithData:stringdata];
            //组装包形式
            NSString *frameString = [NSString stringWithFormat:@"<<<%@%@%@>>>",@"100",kOriginDeviceUUID,RSAEncryptString];
            //转换形式
            NSData *encryptData = [frameString dataUsingEncoding:NSUTF8StringEncoding];
//            TSLog(@"准备发送的数据 EncryptStr %@",frameString);
            [frame replaceObjectAtIndex:i withObject:encryptData];
        }
    }
    else if([type isEqualToString:@"2"])
    {//AES加密
        DeviceNetWork *NW =  [DeviceNetWork shareInstance];

        for (int i = 0 ; i < [frame count]; i++)
        {
            //接口输出是nsdata，需要转换一下
            NSString *stringdata = [[NSString alloc] initWithData:[frame objectAtIndex:i] encoding:NSUTF8StringEncoding];
            //加密
            NSString *encryptString = [AESCrypt encryptAES128:stringdata forKey:NW.RmSessKey forIv:nil];
            //组装包形式
            NSString *frameString = [NSString stringWithFormat:@"<<<%@%@%@>>>",@"200",kOriginDeviceUUID,encryptString];
            //转换形式
            NSData *encryptData = [frameString dataUsingEncoding:NSUTF8StringEncoding];
            [frame replaceObjectAtIndex:i withObject:encryptData];
//            TSLog(@"%@",[NSString stringWithFormat:@"Remote 会话密钥 encryptString %@",frameString]);
        }
    }
    return frame;
}

-(id)DataFormJSON:(id)json
{
    return nil;
}


/*
 *以下方法不做任何处理
 */
-(id)BtMasterDeclare:(BOOL)enctyp//宣告入网 0x30 UDP方式？？？一直发？？？
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtMasterQuit:(BOOL)enctyp//手机退网 0x31~0x UDP方式？？？
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendRegisterForEnctyp:(BOOL)enctyp//注册 0x46~0x47
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendActiveForEnctyp:(BOOL)enctyp//激活 0x48~0x49
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendActiveMailForEnctyp:(BOOL)enctyp // 激活邮件
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendVerifyForEnctyp:(BOOL)enctyp//验证码校验 0x4C~0x4D
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendLoginForEnctyp:(BOOL)enctyp//登录 0x40~0x41
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendSearchDeviceForEnctyp:(BOOL)enctyp//搜索帧(设备注册表请求) 0x42~0x43 获得的是设备信息，设备状态呢？？设备描述呢？？
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendSearchDeviceStatusForEnctyp:(BOOL)enctyp//状态查询帧 0x07~0x08
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendControlForEnctyp:(BOOL)enctyp//控制帧 0x05~0x06
{
    NSAssert(1, @"null Handling");
    return nil;
}
//-(void)Rm_SendSearchDeviceCommandForEnctyp:(BOOL)enctyp;//设备搜索，0x62~0x0B多余的？？？？ 讨论删除
//-(void)Rm_GetDeviceDescriptionFile;//获取设备描述文件 返回数据是一台的还是多台的？多台时如何处理？ 0x08    走WIFI单芯片检测板升级协议

-(id)BtSendLogoutForEnctyp:(BOOL)enctyp//账号退出 0x44~0x45
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendUserPwChangeForEnctyp:(BOOL)enctyp//修改密码 0x4E~0x4F
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendUserPwFindForEnctyp:(BOOL)enctyp//找回密码 0x50~？？？？？？？？？？？
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendSesetPwdForEnctyp:(BOOL)enctyp
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendUserPwChangeVerifyForEnctyp:(BOOL)enctyp//发送验证码（密码找回）0x50~0xB5???????
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendUserPwchangeNewForEnctyp:(BOOL)enctyp//发送新密码 0x16~0xB6
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendBindTelpForEnctyp:(BOOL)enctyp//绑定手机号 0x54~0x55
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendBindTelpChangeForEnctyp:(BOOL)enctyp//修改绑定手机 0x56~0x57
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendBindTelpCodeRequestForEnctyp:(BOOL)enctyp//请求绑定手机验证码 0x60~0x61????????
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendBindTelpCodeVerifyForEnctyp:(BOOL)enctyp//绑定校验 0x60~0x61???????
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendBindEmailForEnctyp:(BOOL)enctyp//绑定邮箱 0x58~0x55???????
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendBindEmailChangeForEnctyp:(BOOL)enctyp // 修改绑定邮箱
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendBindDeviceForEnctyp:(BOOL)enctyp//用户绑定 0x5A~0x5B
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendBindDeviceCancelForEnctyp:(BOOL)enctyp//用户解绑 0x5C~0x5D
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendEventSubscribe:(BOOL)enctyp//事件订阅0x03~0x...
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendEventSubscribeActiveForEnctyp:(BOOL)enctyp//事件消息响应0x04~0x...
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendRSAExchangeCommand:(NSString *)data Enctyp:(BOOL)enctyp//请求服务器公钥
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendAESExchangeCommand:(NSString *)data Enctyp:(BOOL)enctyp
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendFileTransferCommand:(BOOL)enctyp//启动文件传输请求帧
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendManageCommand:(BOOL)enctyp//管理帧
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtSendDeviceLoginCommand:(BOOL)enctyp//设备登陆帧
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtResendIdentifyCodeForEnctyp:(BOOL)enctyp // 重新发送注册验证码帧
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtResendUserPwdIdentifyCodeForEnctyp:(BOOL)enctyp
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtFileTransferFetchLatestVersionForEnctyp:(BOOL)enctyp
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtFileTransferWhetherUpgradeByServerForEnctyp:(BOOL)enctyp
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtFileTransferSendFilePropertyForEnctyp:(BOOL)enctyp
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtFileTransferAllFileDataSentForEnctyp:(BOOL)enctyp
{
    NSAssert(1, @"null Handling");
    return nil;
}

-(id)BtFileTransferFilePropertyRespondForEnctyp:(BOOL)enctyp
{
    NSAssert(1, @"null Handling");
    return nil;
}

@end
