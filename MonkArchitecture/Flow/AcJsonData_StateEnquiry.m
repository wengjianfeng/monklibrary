//
//  AcJsonData_Encrypt.m
//  NewworkDemo
//
//  Created by gree's apple on 18/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "AcJsonData_StateEnquiry.h"
#import "ControlJsonFormatLists.h"
#import "VirtualDeviceBL.h"

#import "GEDeviceBLRepository.h"
//#import "GEAirCDeviceInfoBL.h"
//#import "GEAirCPriFeed.h"
//#import "GEAirCPubFeed.h"
//
//#import "GEFeedDAO.h"
//#import "GEAirCFeedDAO.h"
//#import "GERFFeedDAO.h"

@implementation AcJsonData_StateEnquiry
{
    VirtualDeviceBL *_bl;
    
//    id<GEFeedDAO>_daoFather;
//    GEAirCFeedDAO *_ACdao;
//    GERFFeedDAO *_RFdao;
////    GEAirCDeviceInfoBL *_deviceInfoBL;/////XXXXXYYYY
//    id<GEDeviceInfoProtocol>_deviceInfoBL;
}

-(id)initWithBL:(id<VirtualDeviceProtocol>)BL
{
    self = [super init];
    if (self) {
        _bl = (VirtualDeviceBL *)BL;
    }
    return self;
}

-(void)parseFromJson:(NSDictionary *)json
{
    NSMutableDictionary *vaildArr = [json objectForKey:@"validData"];
    NSString *Funcod = [json objectForKey:@"functionCode"];
    
    if ([[vaildArr allKeys] containsObject:@"statusCode"])
    {
        NSLog(@"解析数据错误");
        return;
    }
    
    /*
     *队列处理
     */
    if ([Funcod isEqualToString:KJsonControlRespondFC])
    {
        vaildArr = [self crHandler:json];
        
        if (vaildArr == nil)
        {
            NSLog(@"控制不连续");
//            return;
        }
    }
    
    /*
     *状态查询及事件需要判断Status_Number
     */
    if ([Funcod isEqualToString:KJsonStateEnquiryRespondFC])
    {
        [self serHandler:(NSMutableDictionary *)vaildArr];
    }
    /*
     *事件消息帧
     */
    else if ([Funcod isEqualToString:KJsonEventMessageFC])
    {
        BOOL b = [self emHandler:(NSMutableDictionary *)json];
        if (b == YES) {
            NSLog(@"事件不连续");
//            return;
        }
    }
    
    [self dataHandler:vaildArr];
    
    [_bl sendMessage:BLMessageEvt_UpdataSuccess];
}

#pragma mark - ControlRespond Handler
- (NSMutableDictionary *) crHandler:(NSDictionary *)jsonDic
{
    NSMutableDictionary *vaildArr = [self queueHandler:jsonDic];
    
    int statusNum = [[vaildArr valueForKey:@"Status_Number"] intValue];
    [_bl configContextWithDictionary:@{
                                      @"statusNumber" : [NSNumber numberWithInt:statusNum],
                                      }];
    
    return vaildArr;
}

/*
 *返回YES则代表“需处理这包数据”
 *只有控制应答帧需要处理
 */
-(NSMutableDictionary *)queueHandler:(NSDictionary *)jsonDic
{
    BOOL need = NO;
    
    NSString *seqc = [jsonDic objectForKey:@"sequenceNumber"];
    
    //如果发现控制帧中间丢失，则不认为断网，但状态需全查!
    if([_bl._context.mSendingBufferOverflow count] >= 1)
    {
        for (NSString *tempseqc in [_bl._context.mSendingBufferOverflow allKeys])
        {
            //~~~~需要考虑翻转情况~~~~
            //如果存在中间seqc丢了，但statusNum仍然连续的情况，需全查。
            if( ([seqc intValue] - [tempseqc intValue] +256)%256 < 128 &&[seqc intValue]!= [tempseqc intValue])//全查
            {
                need = YES;
                [_bl._context.mSendingBufferOverflow removeAllObjects];//小于的全删除，因为后面的数据收到代表OK
                break;
            }
        }
        
        if (need)
        {
            [_bl stuffFormatMutableDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"01",@"lookupType", nil]];
            [_bl excuteWithTcpCmd:KJsonQueryDeviceStatus];//查询设备状态
            
            return nil;//全查了就不用处理这包了
        }
        
    }
    
    NSDictionary *value = [_bl._context.mSendingBufferOverflow objectForKey:seqc];
    if (value)
    {
        [_bl._context.mSendingBufferOverflow removeObjectForKey:seqc];
        
        /*
         *判断对应值是否一致
         *是否键值不匹配即可认为是不一样？
         *匹配不一致时需要将一致的数据去除，只同步不一致数据~
         */
        for (NSString *keyname in [value allKeys])
        {
            if([[value objectForKey:keyname]intValue] == [[[jsonDic objectForKey:@"validData"] objectForKey:keyname] intValue])
            {
                [[jsonDic objectForKey:@"validData"] removeObjectForKey:keyname];
            }
            else
            {
                need = YES;
            }
        }
        
        if (need)
        {
            return [jsonDic objectForKey:@"validData"];
        }
        else
        {
            return nil;
        }
    }
    
    return [jsonDic objectForKey:@"validData"];
}

#pragma mark - StateEnquiryRespond Handler
- (void) serHandler:(NSMutableDictionary *)vaildArr
{   //来自validData
    [_bl configContextWithDictionary:@{
                                      @"statusNumber" : [NSNumber numberWithInt:[[vaildArr valueForKey:@"Status_Number"] intValue]],
                                      }];
}
#pragma mark - EventMessage Handler
- (BOOL) emHandler:(NSMutableDictionary *)vaildArr
{
    /*
     *事件消息回复04
     */
    [_bl configContextWithDictionary:@{
                                      @"sequenceNumber" : [NSNumber numberWithInt:[[vaildArr valueForKey:@"sequenceNumber"] intValue]],
                                      }];
    
    [_bl stuffFormatMutableDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"null",@"null", nil]];
    [_bl excuteWithTcpCmd:KJsonDevicesEventMessageRespondFC];
    
    NSDictionary *vd = [vaildArr objectForKey:@"validData"];
    /*
     *空调管理事件
     *eventType 05
     *productName 设备名称
     *productReset 设备修改网络配置 0设备不重启 1设备准备重启
     */
    if ([[vd objectForKey:@"eventType"] isEqualToString:@"05"])
    {
        if ([[vaildArr objectForKey:@"productReset"] isEqualToString:@"0"])
        {
            [_bl configContextWithDictionary:@{
                                              @"deviceName" : [vd valueForKey:@"productName"],
                                              }];
        }
        else
        {
            //重新刷新列表？？？还是等待设备离线？？？
        }
        return YES;
    }
    /*
     *设备状态推送事件
     *eventType02:设备状态改变
     *Status_Number如果事件消息来的num不连续，则全查状态
     */
    else if([[vd objectForKey:@"eventType"] isEqualToString:@"02"])
    {
        /*
         *如果sendingbuffer有数据，肯定是没发送完，没发完，不处理事件帧**(如果不处理会出错)
         */
        [_bl._context.mSendingBufferOverflow removeAllObjects];
        
        int statusNum = [[vd valueForKey:@"Status_Number"] intValue];
        
        if((_bl._context.statusNumber + 1)%256 == statusNum) {
            _bl._context.statusNumber = statusNum;
        } else if((statusNum - _bl._context.statusNumber +256)%256 < 128 && statusNum!= _bl._context.statusNumber) {
            //状态码不连续，需要重新查询状态
            [_bl stuffFormatMutableDictionary:[NSMutableDictionary dictionaryWithObjectsAndKeys:@"01",@"lookupType", nil]];
            [_bl excuteWithTcpCmd:KJsonQueryDeviceStatus];//查询设备状态
            
            return YES;
        } else {
            
            return YES;
        }
    }
    
    /*
     *空调上线帧(服务器)
     *eventType:04空调上下线通知帧
     *eventContent:01上线 02下线
     */
    if ([[vd objectForKey:@"eventType"] isEqualToString:@"04"])
    {
        NSString *mac = [vaildArr objectForKey:@"originDeviceUUID"];
        /*
         *一般情况下，进入这里基本证明在注册表返回过程已经有此数据。但不排除异常情况(有漏洞，BUG)，后续需要完善
         */
//        GEDeviceBLRepository *rp = [GEDeviceBLRepository shareInstance];
//        _deviceInfoBL = [rp findDeviceInfoDBByMac:mac];
//        if (_deviceInfoBL == nil)
//        {
//            return YES;//如果不存在则离开，防错。
//        }
        if ([[vd objectForKey:@"eventContent"] isEqualToString:@"01"])
        {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithCapacity:2];
            [dic setValue:[NSNumber numberWithBool:YES] forKey:@"mWiFiNetWorkStatus.isOnline"];
            [dic setValue:[NSNumber numberWithBool:YES] forKey:@"isBind"];
            
            [_bl configContextWithDictionary:dic];
            [_bl sendMessage:BLMessageEvt_DeviceCreatDone];
        }
        else//下线
        {
            /*
                @end BL
            */
            GEDeviceBLRepository *rp = [GEDeviceBLRepository shareInstance];
            VirtualDeviceBL *bl = (VirtualDeviceBL *)[rp findVirtualDeviceByMac:mac];
            if (bl._context.mWiFiNetWorkStatus.isLAN == NO) {
                //如果在近程搜索到设备后，即使远程提示下线，近程也不能移除
                [rp removeDeviceInfoByMac:mac];
            }
        }
        [_bl sendMessage:BLMessageEvt_UpdataSuccess];

        return YES;
    }
    /*
     *空调状态反馈(近程)服务器是否也会回？？？
     *
     */
    else
    {
        //待处理
    }
    return NO;
}
#pragma mark - Data Handler
- (void) dataHandler:(NSMutableDictionary *)vaildArr
{
    GEDeviceBLRepository *rp = [GEDeviceBLRepository shareInstance];
//    _deviceInfoBL = [rp findDeviceInfoDBByMac:_bl.identify];
//    if (_deviceInfoBL) {
//        //得到需要的mac储存库
//        _daoFather = [_deviceInfoBL findByMac:[_bl identify]];
//    } else {
//        NSLog(@"对象不存在");
//    }
//    
//    //******后续看需要移植，否则此对象将越来越大******
//    if ([_bl._context.deviceType isEqualToString:kACDeviceType]) {  //空调
//        //根据当前模式，更新数据库对象
//        _ACdao = (GEAirCFeedDAO *)_daoFather;
//        [_ACdao findByMode:_ACdao._currentMode];
//        
//        [self ACPropertyHandler:vaildArr];
//    } else if ([_bl._context.deviceType isEqualToString:kRFDeviceType]) {  //冰箱
//        _RFdao = (GERFFeedDAO *)_daoFather;
//        [_RFdao findByMode:KRFControlMode];
//        
//        [self RFPropertyHandler:vaildArr];
//    }
    
}

- (void)ACPropertyHandler:(NSMutableDictionary *)vaildArr
{
//    NSArray *keyarr = @[KAirCAutoMode,
//                        KAirCCoolMode,
//                        KAirCDryMode,
//                        KAirCFanMode,
//                        KAirCHotMode,
//                        ];
//    
//    /*
//     *说明：只有控制数据才涉及切换数据库，故只需在对应的帧前切换即可
//     *     提取控制公共数据及模式私有数据
//     *     以下仅限用于空调设备
//     */
//    NSArray *keys = [vaildArr allKeys];
//    
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"ACJsonAndHexDescription" ofType:@"plist"];
//    NSDictionary *plistDic = [[NSDictionary alloc]initWithContentsOfFile:filePath];//取出网络协议与本地键值对照表
//    NSMutableArray *sleepArr = [[NSMutableArray alloc] initWithObjects:@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
//    
//    if ([[vaildArr allKeys] containsObject:@"Aircondition_mode"]) {
//        [_ACdao findByMode:[keyarr objectAtIndex:[[vaildArr objectForKey:@"Aircondition_mode"] intValue]]];       ////根据设备类型，提取设备控制数据数据库
//        [_ACdao setStuff:[vaildArr objectForKey:@"Aircondition_mode"] forKey:KAirCCurrentMode];
//    }
//    
//    for (id key in keys)  {
//        NSString *nwMapLocKey = [plistDic valueForKey:key];//取出网络协议对应的本地键值
//        int value = (int)strtoul([[vaildArr objectForKey:key] UTF8String], 0, 16);
//        
//        if ([key isEqualToString:@"Set_temp_integer"] || [key isEqualToString:@"Set_temp_decimal"]) {
//            float inte = (int)strtoul([[vaildArr objectForKey:@"Set_temp_integer"] UTF8String], 0, 16);
//            int de = (int)strtoul([[vaildArr objectForKey:@"Set_temp_decimal"] UTF8String], 0, 16);
//            inte = inte + de*0.1;
//            
//            [_ACdao setStuff:[NSNumber numberWithFloat:inte] forKey:KAircTemperature];
//        }
//        else  if ([key isEqualToString:@"Timer_off_week"] || [key isEqualToString:@"Timer_on_week"])  {
//            /*
//             *字典中的定时key数据顺序为：日、一、二、三、四、五、六
//             *取出plist里面的本地array，关键字"Timer_off_week"内含多个key
//             */
//            NSArray *keyArrs = [plistDic valueForKey:key];
//            /*
//             *从网络协议数据取出值
//             *内容为：0xxxxxxx ，依次为 无、六、五、四、三、二、一、日
//             *更改：去除0xxxxxxx方式，改用二进制
//             */
//            for (int i = 0;i < [keyArrs count];i++)
//            {
//                unsigned char ttemp = (value>>i)&0x01;
//                [_ACdao setStuff:[NSNumber numberWithInt:ttemp] forKey:[keyArrs objectAtIndex:i]];
//            }
//        }
//        else  if ([key isEqualToString:@"Sleep_mode3_temp1"] || [key isEqualToString:@"Sleep_mode3_temp2"] || [key isEqualToString:@"Sleep_mode3_temp3"] || [key isEqualToString:@"Sleep_mode3_temp4"] || [key isEqualToString:@"Sleep_mode3_temp5"] || [key isEqualToString:@"Sleep_mode3_temp6"] || [key isEqualToString:@"Sleep_mode3_temp7"] || [key isEqualToString:@"Sleep_mode3_temp8"]) {
//            value = [[vaildArr objectForKey:key] intValue] * 10;
//            
//            //因为键值可能不是按temp1、2、3进入，所以需要排序
//            unichar sleepChip = [key characterAtIndex:([key length]-1)];
//            int sleepChipData = sleepChip - 49;//根据key值最后的数字排序..数组下标
//            [sleepArr replaceObjectAtIndex:sleepChipData withObject:[NSNumber numberWithInt:value]];
//            
//            [_ACdao setStuff:sleepArr forKey:KAircSleepModeData];
//        }
//        else if ([key isEqualToString:@"Up_down_swing"] || [key isEqualToString:@"Left_right_swing"]) {
//            BOOL status = YES;
//            int l = 0;
//            int r = 0;
//            switch (value) {
//                case 0x00:
//                    status = NO;
//                    l = 3;
//                    r = 3;
//                    break;
//                case 0x01:
//                    l = 1;
//                    r = 5;
//                    break;
//                case 0x02:
//                    status = NO;
//                    l = 1;
//                    r = 1;
//                    break;
//                case 0x03:
//                    status = NO;
//                    l = 2;
//                    r = 2;
//                    break;
//                case 0x04:
//                    status = NO;
//                    l = 3;
//                    r = 3;
//                    break;
//                case 0x05:
//                    status = NO;
//                    l = 4;
//                    r = 4;
//                    break;
//                case 0x06:
//                    status = NO;
//                    l = 5;
//                    r = 5;
//                    break;
//                case 0x07:
//                    l = 3;
//                    r = 5;
//                    break;
//                case 0x08:
//                    l = 2;
//                    r = 5;
//                    break;
//                case 0x09:
//                    l = 2;
//                    r = 4;
//                    break;
//                case 0x0a:
//                    l = 1;
//                    r = 4;
//                    break;
//                case 0x0b:
//                    l = 1;
//                    r = 3;
//                    break;
//                default:
//                    break;
//            }
//            if([key isEqualToString:@"Up_down_swing"]) {
//                [_ACdao setStuff:[NSNumber numberWithInt:status] forKey:KAircUDSwingON];
//                [_ACdao setStuff:[NSNumber numberWithInt:l] forKey:KAircSwingModeIndex_udStr];
//                [_ACdao setStuff:[NSNumber numberWithInt:r] forKey:KAircSwingModeIndex_udEnd];
//            } else {
//                [_ACdao setStuff:[NSNumber numberWithInt:status] forKey:KAircLRSwingON];
//                [_ACdao setStuff:[NSNumber numberWithInt:l] forKey:KAircSwingModeIndex_lrStr];
//                [_ACdao setStuff:[NSNumber numberWithInt:r] forKey:KAircSwingModeIndex_lrEnd];
//            }
//        }
//        else {
//            [_ACdao setStuff:[NSNumber numberWithInt:value] forKey:nwMapLocKey];
//        }
//    }
}

@end
