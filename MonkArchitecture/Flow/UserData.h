//
//  UserData.h
//  Remote
//
//  Created by Zhang Hanying on 14-9-20.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserData : NSObject

// 用户登录注册相关信息
@property (nonatomic,strong) NSString *userName;         // 用户名
@property (nonatomic,strong) NSString *userPassword;     // 密码
@property (nonatomic,strong) NSString *userNewPassword;  // 新密码
@property (nonatomic,strong) NSString *userMail;         // 邮件地址
@property (nonatomic,strong) NSString *userMobile;       // 手机号码
@property (nonatomic,strong) NSString *userCheckCode;    // 注册短信验证码

// 四种情况：正常登录 用邮箱注册 用手机号码注册 修改密码
-(id)initWithUsername:(NSString*)userName password:(NSString*)password;
-(id)initWithUsername:(NSString*)userName password:(NSString*)password eMailAddress:(NSString*)emailAddress;
-(id)initWithUsername:(NSString*)userName password:(NSString*)password phoneNumber:(NSString*)phoneNumber;
-(id)initWithUsername:(NSString*)userName password:(NSString*)password userNewPassword:(NSString*)userNewPassword;

@end
