//
//  EncryptManage.m
//  NewworkDemo
//
//  Created by gree's apple on 18/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "EncryptManage.h"
#import "RSACryptor.h"
#import "AESCrypt.h"

@implementation EncryptManage

//加密
+ (NSMutableArray *)encryptFrame:(NSMutableArray *)frame type:(EncryptType)type
{
    if (type == EncryptType_RSA) {
        //RSA加密!包括上传公钥及交互AES
        for (int i = 0; i < [frame count]; i++)
        {
            NSData *stringdata = [frame objectAtIndex:i];
            NSLog(@"RSA加密前的数据 %@",[[NSString alloc] initWithData:stringdata encoding:NSUTF8StringEncoding]);

            //加密
            RSACryptor *r = [RSACryptor shareInstance];
            NSString *RSAEncryptString = [r RSA_EncryptUsingServerPublicKeyWithData:stringdata];
            //组装包形式
            NSString *frameString = [NSString stringWithFormat:@"<<<%@%@%@>>>",@"100",kOriginDeviceUUID,RSAEncryptString];
            //转换形式
            NSData *encryptData = [frameString dataUsingEncoding:NSUTF8StringEncoding];
            //            TSLog(@"准备发送的数据 EncryptStr %@",frameString);
            [frame replaceObjectAtIndex:i withObject:encryptData];
        }
    }else if (type == EncryptType_AES){
        //AES加密
        
        for (int i = 0 ; i < [frame count]; i++)
        {
            //接口输出是nsdata，需要转换一下
            NSString *stringdata = [[NSString alloc] initWithData:[frame objectAtIndex:i] encoding:NSUTF8StringEncoding];
            NSLog(@"AES加密前的数据 %@",stringdata);

            //加密
            NSString *sesskey = [[NSUserDefaults standardUserDefaults] valueForKey:@"sess"];
            
            NSString *encryptString = [AESCrypt encryptAES128:stringdata forKey:sesskey forIv:nil];
            //组装包形式
            NSString *frameString = [NSString stringWithFormat:@"<<<%@%@%@>>>",@"200",kOriginDeviceUUID,encryptString];
            //转换形式
            NSData *encryptData = [frameString dataUsingEncoding:NSUTF8StringEncoding];
            [frame replaceObjectAtIndex:i withObject:encryptData];
            //            TSLog(@"%@",[NSString stringWithFormat:@"Remote 会话密钥 encryptString %@",frameString]);
        }
    }
    
    return frame;
}

//解密
+ (NSData *)decryptFrame:(NSData *)data
{
    NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSString *subStr = [str substringFromIndex:3];
    NSData *d;
    
    if ([[subStr substringToIndex:1] isEqualToString:@"1"])//RSA加密，后续需比对版本之类的
    {
        NSString *frameData = [subStr substringFromIndex:35];//去除3+32
        RSACryptor *r = [RSACryptor shareInstance];
        NSData *base64FromString = [frameData dataUsingEncoding:NSUTF8StringEncoding];
        NSString *s = [r RSA_DecryptUsingPrivateKeyWithData:base64FromString];
        d = [s dataUsingEncoding:NSUTF8StringEncoding];
    }
    else if ([[subStr substringToIndex:1] isEqualToString:@"2"])//AES
    {
        NSString *frameData = [subStr substringFromIndex:35];//去除3+32
        
        NSString *sesskey = [[NSUserDefaults standardUserDefaults] valueForKey:@"sess"];
        
        NSString *AES_DeCryptString = [AESCrypt decryptAES128:frameData forKey:sesskey forIv:nil];
        d = [AES_DeCryptString dataUsingEncoding:NSUTF8StringEncoding];
    }
    else//未加密
    {
        d = [str dataUsingEncoding:NSUTF8StringEncoding];
    }
    
    return d;
}

@end
