//
//  AccountHelper.h
//  G-Life_NewUI
//
//  Created by gree's apple on 1/12/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^AccountBlock)(NSString *info, NSError *error);

@interface AccountHelper : NSObject

- (void)logoutWithBlock:(AccountBlock)block;    //退出账号
- (void)changePasswordWithOriginPassword:(NSString *)opsw
                   newPassword:(NSString *)npsw
                   comparePassword:(NSString *)cpsw
                         withBlock:(AccountBlock)block; //修改密码

@end
