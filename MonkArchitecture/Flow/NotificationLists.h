//
//  NotificationLists.h
//  NewworkDemo
//
//  Created by gree's apple on 24/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#ifndef NotificationLists_h
#define NotificationLists_h

static NSString *Flow_networkNotification = @"01";       // 自定义网络状态变化通知

static NSString *Flow_updataSuccess = @"02";             // 数据更新完毕

static NSString *Flow_serverFeedbackNormalFrame = @"03"; // 什么登录啊、注册啊、绑定手机啊...

static NSString *Flow_becomeActive = @"04";              // 进入前台

static NSString *Flow_resignActive = @"05";              // 进入后台

static NSString *Flow_remoteSearchDeviceDone = @"06";    // 远程搜索设备完毕

static NSString *Flow_deviceOffline = @"07";             // 近程设备断开连接

static NSString *Flow_searchDeviceStatus = @"08";                 // 搜索完毕

static NSString *Flow_deviceLoginDone = @"09";           // 设备登录

static NSString *Flow_userFeedback = @"0a";              // 用户反馈

#endif