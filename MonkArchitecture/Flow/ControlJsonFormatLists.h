//
//  ControlJsonFormatLists.h
//  NewworkDemo
//
//  Created by gree's apple on 27/8/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//


#ifndef ControlJsonFormatLists_h
#define ControlJsonFormatLists_h

static NSString *KJsonData = @"JsonData";

/*************JSON 近程功能码*****Start*******/
static NSString *KJsonInLocalAnnouncement = @"11";                  // 设备入网宣告帧
static NSString *KJsonQuitLocalAnnouncement = @"12";                // 设备退网宣告帧
static NSString *KJsonSearchDevice = @"13";                         // 设备搜索帧
static NSString *KJsonSearchDeviceResponse = @"14";                 // 设备搜索应答帧

static NSString *KJsonDeviceLoginFC = @"23";                        // 设备登录帧 手机->设备
static NSString *KJsonDeviceLoginRespondFC = @"24";                 // 设备登陆应答帧 设备->手机

static NSString *KJsonDevicesDiscriptionFC = @"01";                 // 设备描述文件请求帧 手机->设备
static NSString *KJsonDevicesDiscriptionRespondFC = @"02";          // 设备描述文件请求应答帧 设备->手机
static NSString *KJsonDevicesEventMessageFC = @"03";                // 事件消息帧 设备->手机
static NSString *KJsonDevicesEventMessageRespondFC = @"04";         // 事件消息应答帧 手机->设备
static NSString *KJsonDevicesControlsFC = @"05";                    // 控制帧 手机->设备
static NSString *KJsonDevicesControlsRespondFC = @"06";             // 控制应答帧  设备->手机
static NSString *KJsonQueryDeviceStatus = @"07";                    // 状态查询帧 手机->设备
static NSString *KJsonDevicesStatusRespondFC = @"08";               // 状态查询应答帧  设备->手机
static NSString *KJsonDevicesEventSubscribeFC = @"09";              // 事件订阅帧 手机->设备
static NSString *KJsonDevicesEventSubscribeRenewFC = @"0A";         // 事件续订请求帧  手机->设备
static NSString *KJsonDevicesEventSubscribeRespondFC = @"0B";       // 订阅响应帧 设备->手机
static NSString *KJsonDevicesEventSubscribeCancelFC = @"0C";        // 取消订阅请求帧  手机->设备
static NSString *KJsonDevicesManageFC = @"0D";                      // 管理帧 手机->设备
static NSString *KJsonDevicesManageRespondFC = @"0E";               // 管理应答帧 设备->手机

/*************JSON 近程功能码*****End*******/

/*************JSON 远程功能码*****Start********************/
static NSString *KJsonPhoneInNetworkAnnouncementFC = @"17";         // 手机入网宣告帧 手机->服务器
static NSString *KJsonPhoneQuitNetworkAnnouncementFC = @"18";       // 手机退网宣告帧 手机->服务器

static NSString *KJsonDeviceDescriptionFileRequestFC = @"01";       // 获取设备描述文件请求帧 服务器->手机
static NSString *KJsonDeviceDescriptionFileRespondFC = @"02";       // 获取设备描述文件响应帧 手机->服务器
// 登录
static NSString *KJsonUserLoginRequestFC = @"30";                   // 用户登录请求帧 手机->服务器
static NSString *KJsonUserLoginRespondFC = @"31";                   // 用户登录请求应答帧 服务器->手机
static NSString *KJsonUserLogoutRequestFC = @"32";                  // 用户退出登录请求帧
static NSString *KJsonUserLogoutRespondFC = @"33";                  // 用户退出登录请求应答帧

static NSString *KJsonRequestDeviceRegistryFC = @"34";              // 获取设备注册表请求帧 手机->服务器
static NSString *KJsonRequestDeviceRegistryRespondFC = @"35";       // 获取设备注册表请求响应帧 手机->服务器
// 控制
static NSString *KJsonEventMessageFC = @"03";                       // 事件消息帧 服务器->手机
static NSString *KJsonEventMessageRespondFC = @"04";                // 事件消息响应帧 手机->服务器
static NSString *KJsonControlFC = @"05";                            // 控制帧 手机->服务器
static NSString *KJsonControlRespondFC = @"06";                     // 控制响应帧 服务器->手机
static NSString *KJsonStateEnquiryFC = @"07";                       // 状态查询帧 手机->服务器
static NSString *KJsonStateEnquiryRespondFC = @"08";                // 状态查询响应帧 服务器->手机
static NSString *KJsonEventSubscribeRequestFC = @"09";              // 事件订阅请求帧 手机->服务器
static NSString *KJsonEventSubscribeReNewRequestFC = @"0A";         // 事件续订请求帧 手机->服务器
static NSString *KJsonSubscribeRespondFC = @"0B";                   // 订阅响应帧 服务器->手机
static NSString *KJsonCancelSubscribeRequest = @"0C";               // 取消订阅请求帧 手机->服务器
static NSString *KJsonManagementFC = @"0D";                         // 管理帧 手机->服务器
static NSString *KJsonManagementRespondFC = @"0E";                  // 管理应答帧 服务器->手机
// 绑定
static NSString *KJsonNewDeviceCombineAccountRequestFC = @"36";     // 新设备与账号绑定请求帧 手机->服务器
static NSString *KJsonNewDeviceCombineAccountRespondF = @"37";      // 新设备与账号绑定请求应答帧 服务器->手机
// 解绑
static NSString *KJsonDeviceDecombineAccountRequestFC = @"38";      // 设备与账号解绑请求帧 手机->服务器
static NSString *KJsonDeviceDecombineAccountRespondFC = @"39";      // 设备与账号解绑请求应答帧 服务器->手机
// 注册
static NSString *KJsonUserRegisterInfoVerifyRequestFC = @"3A";      // 用户注册信息验证请求帧 手机->服务器  暂时不用14-11-03
static NSString *KJsonUserRegisterInfoVerifyRespondFC = @"3B";      // 用户注册信息验证请求应答帧 服务器->手机 暂时不用14-11-03
static NSString *KJsonRequestCheckCodeFC = @"3C";                   // 发送手机验证码激活请求帧 手机->服务器
static NSString *KJsonRequestCheckCodeRespondFC = @"3D";            // 发送手机验证码激活请求应答帧 服务器->手机
static NSString *KJsonCheckIdentifyCodeRequestFC = @"3E";           // 注册激活短信验证码校验请求帧 手机->服务器
static NSString *KJsonCheckIdentifyCodeRespondFC = @"3F";           // 注册激活短信验证码校验应答帧 服务器->手机
static NSString *KJsonRequestActivateEmailFC = @"40";               // 发送激活邮件请求帧 手机->服务器
static NSString *KJsonRequestActivateEmailRespondFC = @"41";        // 发送激活邮件请求应答帧 服务器->手机
static NSString *KJsonUserRegisterRequestFC = @"42";                // 用户注册请求帧 手机->服务器
static NSString *KJsonUserRegisterRespondFC = @"43";                // 用户注册请求应答帧 服务器->手机
// 密码修改
static NSString *KJsonPasswordChangeRequestFC = @"44";              // 用户修改密码请求帧 手机->服务器
static NSString *KJsonPasswordChangeRespondFC = @"45";              // 用户修改密码请求应答帧 服务器->手机

// 密码找回
static NSString *KJsonRetrievePwdRequestFC = @"46";                 // 用户找回密码请求帧 手机->服务器
static NSString *KJsonRetrievePwdRespondFC = @"47";                 // 用户找回密码请求应答帧 服务器->手机
static NSString *KJsonResetPwdFC = @"48";                           // 用户密码重置请求帧 手机->服务器
static NSString *KJsonResetPwdRespondFC = @"49";                    // 用户密码重置应答帧 服务器->手机

// 更改注册信息
static NSString *KJsonCombinePhoneRequestFC = @"4A";                // 绑定手机请求帧 手机->服务器
static NSString *KJsonCombinePhoneRespondFC = @"4B";                // 绑定手机请求应答帧 服务器->手机
static NSString *KJsonChangeCombinedPhoneRequestFC = @"4C";         // 更改绑定手机请求帧 手机->服务器
static NSString *KJsonChangeCombinedPhoneRespondFC = @"4D";         // 更改绑定手机请求应答帧 服务器->手机
static NSString *KJsonChangeCombinedPhoneVerifyFC = @"4E";          // 发送手机校验码请求帧 手机->服务器
static NSString *KJsonChangeCombinedPhoneVerifyRespondFC = @"4F";   // 发送手机校验码请求应答帧 服务器->手机

static NSString *KJsonCombineEmailRequestFC = @"50";                // 绑定邮箱请求帧 手机->服务器
static NSString *KJsonCombineEmailRespondFC = @"51";                // 绑定邮箱请求应答帧 服务器->手机
static NSString *KJsonChangeEmailRequestFC = @"52";                 // 更改绑定邮箱请求帧 手机->服务器
static NSString *KJsonChangeEmailRespondFC = @"53";                  // 更改绑定邮箱请求应答帧 服务器->手机

static NSString *KJsonRetrivePwdIdentifyCodeRequestFC = @"60";          // 找回密码手机验证码请求帧 手机->服务器
static NSString *KJsonRetrivePwdIdentifyCodeRespondFC = @"61";          // 找回密码手机验证码请求应答帧 服务器->手机
static NSString *KJsonRetrivePwdCheckIdentifyCodeRequestFC = @"62";     // 找回密码短信验证码校验请求帧 手机->服务器
static NSString *KJsonRetrivePwdCheckIdentifyCodeRespondFC = @"63";     // 找回密码短信验证码校验应答帧 服务器->手机
static NSString *KJsonPhoneCombineCheckIdentityCodeRequestFC = @"64";   // 修改/绑定手机短信验证码校验请求帧 手机->服务器
static NSString *KJsonPhoneCombineCheckIdentityCodeResbondFC = @"65";    // 修改/绑定手机短信验证码校验请求应答帧 服务器->手机

static NSString *KJsonUserFeedbackSubmitFC = @"66";                     //用户反馈内容提交帧
static NSString *KJsonUserFeedbackSubmitRespondFC = @"67";              //用户反馈内容应答帧

static NSString *KJsonClientPostPubKey = @"F6";                         // 客户端上报公钥帧
static NSString *KJsonClientPostPubKeyRespond = @"F7";                  // 客户端上报公钥应答帧
static NSString *KJsonClientRequestSessionKey = @"F8";                  // 客户端获取会话密钥帧
static NSString *KJsonClientRequestSessionKeyRespond = @"F9";           // 客户端获取会话密钥应答帧
static NSString *KJsonClientRequestAESKeyFC = @"FA";                    // 客户端获请求单独的AES密钥帧 手机->服务器
static NSString *KJsonClientRequestAESKeyRespondFC = @"FB";             // 客户端获取单独的AES密钥应答帧 服务器->手机

/*************JSON 远程功能码*****End**********************/


//=============================文件传输相关====================================//
static NSString *KJsonFileTransferActivateFC = @"A0";               // 文件传输请求帧 手机->设备
static NSString *KJsonFileTransferActivateRespondFC = @"A1";        // 文件传输请求响应帧 设备->手机
static NSString *KJsonFileTransferPropertyFC = @"A2";               // 发送传输文件属性帧 手机->设备
static NSString *KJsonFileTransferPropertyRespondFC = @"A3";        // 发送传输文件属性应答帧 设备->手机
static NSString *KJsonFileTransferDoneFC = @"A4";                   // 发送文件传输完毕帧 手机->设备
static NSString *KJsonFileTransferDoneRespond = @"A5";              // 发送文件传输完毕应答帧 设备->手机||服务器->手机
//---------------------------作为空调和服务器的中介-----------------------------------//
static NSString *KJsonFileTransferFetchLatestVersionFC = @"A6";         // 向服务器请求最新的升级文件版本 手机->服务器
static NSString *KJsonFileTransferFetchLatestVersionRespondFC = @"A8";  // 向服务器请求最新的升级文件版本响应 服务器->手机
static NSString *KJsonFileTransferExecuteUpgradeFC = @"A9";             // 向服务器发送可升级说明 手机->服务器


#endif