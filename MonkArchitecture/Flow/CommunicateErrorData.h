//
//  CommunicatErrorData.h
//  Remote
//
//  Created by Zhang Hanying on 14-9-20.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommunicateErrorData : NSObject

-(NSString*)errorDetailWithStatusCode:(NSString*)statusCode;
-(NSString*)statusCodeWithErrorDetail:(NSString*)errorDetails;

@end
