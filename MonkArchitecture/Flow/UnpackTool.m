//
//  UnpackTool.m
//  NewworkDemo
//
//  Created by gree's apple on 21/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "UnpackTool.h"
#import "EncryptManage.h"

@implementation UnpackTool
{
    NSString *cutOffString;//被切断的包，临时储存

}

+(id)shareInstance
{
    static UnpackTool *_nw = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _nw = [[self alloc] init];
    });
    return _nw;
}

-(NSMutableArray *)unpackWithData:(NSData *)data
{
    NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:1];
    NSString *receivestr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    NSRange rangeEnd = [receivestr rangeOfString:@">>>"];
    
    if (rangeEnd.location != NSNotFound)//没有粘包(只能说存在完整的包)
    {
        //得到<<<...>>>数据
        NSArray *tempLists = [receivestr componentsSeparatedByString:@">>>"];
        //过滤<<<之前的乱码以及>>>之后的乱码
        for (int i = 0; i<[tempLists count]; i++)
        {
            NSString *str = [tempLists objectAtIndex:i];//分包后存在空格
            if (str == nil || [str isEqualToString:@""]) {
                continue;
            }
            
            NSRange rangebegin = [str rangeOfString:@"<<<"];
            if (rangebegin.location == NSNotFound)//可能是断包的后半截
            {
                if ([cutOffString isEqualToString:@""])//判断之前有无断包
                {
                    continue;
                }
                else
                {
                    NSString *temp = str;
                    str = [NSString stringWithFormat:@"%@%@",cutOffString,temp];
                    cutOffString = @"";
                    rangebegin = [str rangeOfString:@"<<<"];
                }
                //                    continue;
            }
            else//存在"<<<"格式，需要判断后半截有无">>>"
            {
                if (i == ([tempLists count] -1))
                {
                    cutOffString = str;
                    continue;
                }
            }
            NSData *d = [str dataUsingEncoding:NSUTF8StringEncoding];
            [arr addObject:d];
        }
    } else {
        cutOffString = receivestr;
    }
    
    return arr;
}

@end
