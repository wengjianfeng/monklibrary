  //
//  AcData_Control.m
//  Remote
//
//  Created by 翁 健峰 on 13-7-28.
//  Copyright (c) 2013年 WJF-Monk  330694. All rights reserved.
//

#import "AcData_Control.h"
//#import "GEAirCDeviceInfoBL.h"
//#import "GEAirCFeedDAO.h"
//#import "GEAirCPriFeed.h"
//#import "GEAirCPubFeed.h"
#include <arpa/inet.h>
#import "AppContext.h"
#import "VirtualDeviceBL.h"
#import "GEDeviceBLRepository.h"

@interface AcData_Control()
{
    NSArray *unitCData;
    NSArray *unitFData;
    
//    GEAirCFeedDAO *_dao;
//    GEAirCDeviceInfoBL *_deviceInfoBL;
    VirtualDeviceBL *_bl;
}

@end

@implementation AcData_Control
{
    int SetOnValue;
    int SetOffValue;
    BOOL synchronizedToPhone;
    NSDictionary *weekdayOnDic;
    NSDictionary *weekdayOffDic;
}

-(id)init
{
    self = [super init];
    if (self)
    {

    }
    return self;
}

-(id)initWithBL:(id<VirtualDeviceProtocol>)BL
{
    self = [super init];
    if (self)
    {
        _bl = (VirtualDeviceBL *)BL;
//        _deviceInfoBL = (GEAirCDeviceInfoBL *)[[GEDeviceBLRepository shareInstance] findDeviceInfoDBByMac:_bl.identify];
        //更新所有数据
        [self UpDateNewDB];
        
        
        unitCData = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:160],[NSNumber numberWithInt:170],[NSNumber numberWithInt:180],[NSNumber numberWithInt:190],[NSNumber numberWithInt:200],[NSNumber numberWithInt:210],[NSNumber numberWithInt:220],[NSNumber numberWithInt:230],[NSNumber numberWithInt:240],[NSNumber numberWithInt:250],[NSNumber numberWithInt:260],[NSNumber numberWithInt:270],[NSNumber numberWithInt:280],[NSNumber numberWithInt:290],[NSNumber numberWithInt:300], nil];
        unitFData = [[NSArray alloc] initWithObjects:[NSNumber numberWithInt:61],[NSNumber numberWithInt:61],[NSNumber numberWithInt:62],[NSNumber numberWithInt:63],[NSNumber numberWithInt:64],[NSNumber numberWithInt:65],[NSNumber numberWithInt:66],[NSNumber numberWithInt:67],[NSNumber numberWithInt:68],[NSNumber numberWithInt:68],[NSNumber numberWithInt:69],[NSNumber numberWithInt:70],[NSNumber numberWithInt:71],[NSNumber numberWithInt:72],[NSNumber numberWithInt:73],[NSNumber numberWithInt:74],[NSNumber numberWithInt:75],[NSNumber numberWithInt:76],[NSNumber numberWithInt:77],[NSNumber numberWithInt:77],[NSNumber numberWithInt:78],[NSNumber numberWithInt:79],[NSNumber numberWithInt:80],[NSNumber numberWithInt:81],[NSNumber numberWithInt:82],[NSNumber numberWithInt:83],[NSNumber numberWithInt:84],[NSNumber numberWithInt:85],[NSNumber numberWithInt:86],[NSNumber numberWithInt:86], nil];
        
        weekdayOnDic = [[NSDictionary alloc]initWithObjectsAndKeys:@"KDefaultPowerOnTimer_mWeekDay_Timer_Sun",@"1",
                                                                   @"KDefaultPowerOnTimer_mWeekDay_Timer_Mon",@"2",
                                                                   @"KDefaultPowerOnTimer_mWeekDay_Timer_Tue",@"3",
                                                                   @"KDefaultPowerOnTimer_mWeekDay_Timer_Wed",@"4",
                                                                   @"KDefaultPowerOnTimer_mWeekDay_Timer_Thu",@"5",
                                                                   @"KDefaultPowerOnTimer_mWeekDay_Timer_Fri",@"6",
                                                                   @"KDefaultPowerOnTimer_mWeekDay_Timer_Sat",@"7",nil];
        
        weekdayOffDic = [[NSDictionary alloc]initWithObjectsAndKeys:@"KDefaultPowerOffTimer_mWeekDay_Timer_Sun",@"1",
                                                                    @"KDefaultPowerOffTimer_mWeekDay_Timer_Mon",@"2",
                                                                    @"KDefaultPowerOffTimer_mWeekDay_Timer_Tue",@"3",
                                                                    @"KDefaultPowerOffTimer_mWeekDay_Timer_Wed",@"4",
                                                                    @"KDefaultPowerOffTimer_mWeekDay_Timer_Thu",@"5",
                                                                    @"KDefaultPowerOffTimer_mWeekDay_Timer_Fri",@"6",
                                                                    @"KDefaultPowerOffTimer_mWeekDay_Timer_Sat",@"7",nil];
        
    }
    return self;
}

-(int)TcpNwkBuildHeaderFrame:(unsigned char *)Buf
{
    int len = 0;
    unsigned char *Bufptr = Buf;
    
    *Bufptr = 0x7E;//同步字
    len += 1;
    Bufptr ++;
    
    *Bufptr = 0x7E;//同步字
    len += 1;
    Bufptr ++;
    
    *Bufptr = 0;//长度
    len += 1;
    Bufptr ++;
    
    *Bufptr = 0x01;//命令字
    len += 1;
    Bufptr ++;
    
    return len;
}

-(void)UpDateNewDB
{
    //得到需要的mac储存库
//    _dao = [_deviceInfoBL findByMac:[AppContext shareAppContext].currentMac];
//    //根据当前模式，更新数据库对象
//    [_dao findByMode:_dao._currentMode];
}

#pragma mark --- Send
//设置有效数据内容
-(int)TcpNwkBuildPacketFrame:(unsigned char *)Buf
{
    int len = 0;

    return len;
}

#pragma mark --- REC
-(void)HandleRecDate:(NSData *)adata                       //接收空调回复数据
{
}


@end









