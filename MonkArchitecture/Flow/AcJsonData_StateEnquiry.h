//
//  AcJsonData_Encrypt.h
//  NewworkDemo
//
//  Created by gree's apple on 18/9/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

//状态查询及事件 功能码 06/08/03
#import "AcJsonData.h"

@interface AcJsonData_StateEnquiry : AcJsonData

-(id)initWithBL:(id<VirtualDeviceProtocol>)BL;

-(void)parseFromJson:(NSDictionary *)json;

@end
