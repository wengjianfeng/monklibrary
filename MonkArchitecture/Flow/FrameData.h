//
//  ACData2.h
//  Remote
//
//  Created by apple on 14-9-17.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FrameData : NSObject

@property (nonatomic,assign) NSString *deviceType;            // 设备类型
@property (nonatomic,assign) NSString *protocolEdition;       // 协议版本
@property (nonatomic,assign) NSString *functionCode;          // 功能码
@property (nonatomic,assign) NSString *sequenceNumber;        // 流水号
@property (nonatomic,assign) NSString *dataLength;          // 数据长度
@property (nonatomic,assign) NSString *subPackage;          // 分包号
@property (nonatomic,strong) NSString *originDeviceUUID; // 源设备UUID
@property (nonatomic,strong) NSString *objectDeviceUUID; // 目的设备UUID
@property (nonatomic,strong) NSString *sign;             // 数字签名
@property (nonatomic,strong) id       validData;         // 有效数据

- (id)initWithObjectDeviceUUID:(NSString *)objectDeviceUUID functionCode:(NSString*)functionCode sequenceNumber:(NSString*)sequenceNumber;

@end
