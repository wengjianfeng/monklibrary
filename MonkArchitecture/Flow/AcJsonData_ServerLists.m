//
//  AcJsonData_ServerLists.m
//  NewworkDemo
//
//  Created by gree's apple on 14/10/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import "AcJsonData_ServerLists.h"
#import "VirtualServerBL.h"

#import "ControlJsonFormatLists.h"
#import "CommunicateErrorData.h"
#import "NotificationLists.h"

@implementation AcJsonData_ServerLists
{
    id<VirtualDeviceProtocol> _bl;
}
-(id)initWithBL:(id<VirtualDeviceProtocol>)BL
{
    self = [super init];
    if (self) {
        _bl = BL;
    }
    
    return self;
}

-(void)parseFromJson:(NSDictionary *)json
{
    NSMutableDictionary *vaildArr = [json objectForKey:@"validData"];
    NSString *Funcod = [json objectForKey:@"functionCode"];
    
    if([Funcod isEqualToString:KJsonUserFeedbackSubmitRespondFC]){
        NSString *statusCode = [vaildArr valueForKey:@"statusCode"];
        [[NSNotificationCenter defaultCenter] postNotificationName:Flow_userFeedback object:statusCode];
    }
    
    if ([Funcod isEqualToString:KJsonUserRegisterInfoVerifyRespondFC] || [Funcod isEqualToString:@"31"] || [Funcod isEqualToString:@"33"] || [Funcod isEqualToString:@"37"] || [Funcod isEqualToString:@"39"] || [Funcod isEqualToString:@"3D"] || [Funcod isEqualToString:@"41"] || [Funcod isEqualToString:@"45"] || [Funcod isEqualToString:@"47"] || [Funcod isEqualToString:@"49"] || [Funcod isEqualToString:@"4B"] || [Funcod isEqualToString:@"4D"] || [Funcod isEqualToString:@"51"] || [Funcod isEqualToString:@"43"] || [Funcod isEqualToString:@"4F"] || [Funcod isEqualToString:@"3F"] || [Funcod isEqualToString:@"63"] || [Funcod isEqualToString:@"61"] || [Funcod isEqualToString:@"53"] || [Funcod isEqualToString:@"65"] )
    {
        NSString *statusCode = [vaildArr valueForKey:@"statusCode"];
        // 2014-12-02添加，手机号码和邮箱登录时，需要返回用户名
//        NSString *userName = [vaildArr valueForKey:@"userName"];
        NSArray *data = [[NSArray alloc]initWithObjects:Funcod,statusCode,vaildArr, nil];
        CommunicateErrorData *errorData = [[CommunicateErrorData alloc]init];
        NSString *errorInfo = [errorData errorDetailWithStatusCode:[vaildArr valueForKey:@"statusCode"]];
        NSLog(@"%@",[NSString stringWithFormat:@"Remote错误码： %@",errorInfo]);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:Flow_serverFeedbackNormalFrame object:data];
        return;
    }
    
    //文件传输相关，把整个字典传递回去
    // A6 A8 A5
    if ([Funcod isEqualToString:KJsonFileTransferFetchLatestVersionFC] || [Funcod isEqualToString:KJsonFileTransferFetchLatestVersionRespondFC] || [Funcod isEqualToString:KJsonFileTransferDoneRespond]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerFeedBackForFileTransfer" object:json];
    }
    
    NSArray *arr = @[
                      KJsonFileTransferFetchLatestVersionFC,          // 向服务器请求最新的升级文件版本 A6
                      KJsonFileTransferFetchLatestVersionRespondFC,   // 向服务器请求最新的升级文件版本响应 A8
                      KJsonFileTransferActivateRespondFC,             // 文件传输请求响应帧 A1
                      KJsonFileTransferPropertyRespondFC,             // 发送传输文件属性应答帧 A3
                      KJsonFileTransferDoneRespond,                   // 发送文件传输完毕应答帧 A5
                      ];
        //收到文件传输相关帧
        if ([arr containsObject:Funcod]){
            if (![_bl isKindOfClass:[VirtualServerBL class]]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LocFileTransferRespond" object:json];
            } else {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ServerFeedBackForFileTransfer" object:json];
            }
        }
}

@end
