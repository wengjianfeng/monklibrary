//
//  AcJsonData_ServerLists.h
//  NewworkDemo
//
//  Created by gree's apple on 14/10/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

//与服务器的数据，非设备通讯类数据
#import "AcJsonData.h"

@interface AcJsonData_ServerLists : AcJsonData

-(id)initWithBL:(id<VirtualDeviceProtocol>)BL;

-(void)parseFromJson:(NSDictionary *)json;

@end
