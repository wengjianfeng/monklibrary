//
//  VerifyHelper.m
//  G-Life_NewUI
//
//  Created by gree's apple on 21/10/15.
//  Copyright © 2015年 Gree. All rights reserved.
//

#import "VerifyHelper.h"
#import "VirtualServerBL.h"
#import "GEDeviceBLRepository.h"
#import "NotificationLists.h"
#import "CommunicateErrorData.h"
#import "VerifyHelper.h"
#import "AppContext.h"
#import "ControlJsonFormatLists.h"

@implementation VerifyHelper
{
    verifyHelperBlock _blockCopy;
}
-(instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(DispatchNotification:)
                                                     name:Flow_serverFeedbackNormalFrame
                                                   object:nil];
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:Flow_serverFeedbackNormalFrame
                                                  object:nil];
}

- (void)verifyWithName:(NSString *)name
       withVerifyBlock:(verifyHelperBlock)block
{
    _blockCopy = block;
    NSString *info = [[NSString alloc] init];
    NSError *error = nil;
    BOOL phoneNumberValid = [self isValidMobile:name];                // 手机号码合法
    BOOL eMailAddressValid = [self isValidEmail:name];                // 邮箱合法
    BOOL userNameValid = [self isUserNameValidIncludeAllDigit:name] && ![self isAllDigitUserName:name];  // 用户名合法
    
    if (phoneNumberValid == YES) {
        info = @"phoneNumberValid";
    } else if (eMailAddressValid == YES) {
        info = @"eMailAddressValid";
    } else if (userNameValid == YES){
        info = @"userNameValid";
    } else {
        info = @"登录失败";
        error = [NSError errorWithDomain:@"无法判别账号类型 error" code:NSURLErrorUnknown userInfo:nil];
    }
    
    _blockCopy(info,error);
    _blockCopy = nil;
}

-(void)verifyWithName:(NSString *)name
             password:(NSString *)psw
      withVerifyBlock:(verifyHelperBlock)block
{
    _blockCopy = block;
    NSString *info = [[NSString alloc]init];
    NSError *error = nil;
    BOOL phoneNumberValid = [self isValidMobile:name];                // 手机号码合法
    BOOL eMailAddressValid = [self isValidEmail:name];                // 邮箱合法
    BOOL userNameValid = [self isUserNameValidIncludeAllDigit:name] && ![self isAllDigitUserName:name];  // 用户名合法
    BOOL passwordValid = [self isValidPassword:psw];                 // 密码符合要求
    BOOL userValid = (phoneNumberValid || eMailAddressValid || userNameValid);
    
//    phoneNumberValid == YES ? [info appendFormat:@"mobile"]:[info appendFormat:@"email"];

    if (!userValid)
    {
        info = @"用户名或手机号输入有误，请重新输入。";
        error = [NSError errorWithDomain:@"用户名或手机号输入有误，请重新输入。 error" code:NSURLErrorUnknown userInfo:nil];
    }else if (!passwordValid) {
        info = @"密码输入错误，请重新输入。";
        error = [NSError errorWithDomain:@"密码输入错误，请重新输入。 error" code:NSURLErrorUnknown userInfo:nil];
    }
    
    _blockCopy(info,error);
    _blockCopy = nil;
}

- (void)verifyWithEmailORPhone:(NSString *)ep
                      userName:(NSString *)name
               withVerifyBlock:(verifyHelperBlock)block
{
    _blockCopy = block;
    NSString *info = [[NSString alloc] init];
    NSError *error = nil;
    BOOL phoneNumberValid = [self isValidMobile:ep];                // 手机号码合法
    BOOL eMailAddressValid = [self isValidEmail:ep];                // 邮箱合法
    BOOL phoneNumberOreMailAddressValid = (phoneNumberValid || eMailAddressValid); // 手机号码或者邮箱合法
    
    BOOL userNameValid = [self isUserNameValidIncludeAllDigit:name] && ![self isAllDigitUserName:name];  // 用户名合法

    BOOL phoneNumberOrUserNameValid = (phoneNumberOreMailAddressValid || userNameValid); //手机号或用户名合法
    
    if (!phoneNumberOrUserNameValid) {
        info = @"手机号码及用户名不正确，请重新输入。";
        error = [NSError errorWithDomain:@"手机号码及用户名不正确，请重新输入。 error" code:NSURLErrorUnknown userInfo:nil];
    }else
        if (!phoneNumberOreMailAddressValid) {
        info = @"手机号码或邮箱不正确，请重新输入。";
        error = [NSError errorWithDomain:@"手机号码或邮箱不正确，请重新输入。 error" code:NSURLErrorUnknown userInfo:nil];
    }else
        if (!userNameValid) {

        info = @"用户名不正确，请重新输入。";
        error = [NSError errorWithDomain: @"用户名不正确，请输入至少8位且包含字母和数字的用户名。 error" code:NSURLErrorUnknown userInfo:nil];

    }
    if (phoneNumberValid == YES) {
        info = @"phoneNumberValid";
    }else
        if (eMailAddressValid == YES){
        info = @"eMailAddressValid";
    }
    _blockCopy(info,error);
    _blockCopy = nil;
}

- (void)verifyWithPassword:(NSString *)psw
           comparePassword:(NSString *)cPsw
           withVerifyBlock:(verifyHelperBlock)block
{
    _blockCopy = block;
    NSString *info = [[NSString alloc]init];
    NSError *error = nil;
    BOOL passwordValid = [self isValidPassword:psw];                 // 密码符合要求
    BOOL hasTwoSameAndNotEmptyPasswords = (psw && cPsw) && [psw isEqualToString:cPsw];// 密码一致并且非空

    if ([psw isEqualToString:@""] || [cPsw isEqualToString:@""]) {
        info = @"密码不能为空，请输入至少8位包含字母和数字的密码。";
        error = [NSError errorWithDomain:@"密码不能为空，请输入至少8位且包含字母和数字的密码。 error" code:NSURLErrorUnknown userInfo:nil];
    }else
        if (!passwordValid) {
        info = @"密码格式不正确，请重新输入。";
        error = [NSError errorWithDomain:@"密码格式不正确，请重新输入。 error" code:NSURLErrorUnknown userInfo:nil];
    }else
        if (!hasTwoSameAndNotEmptyPasswords) {
        info = @"确认密码输入不正确，请重新输入。";
        error = [NSError errorWithDomain:@"确认密码输入不正确，请重新输入。 error" code:NSURLErrorUnknown userInfo:nil];
    }
    _blockCopy(info,error);
    _blockCopy = nil;
}

-(void)verifyWithUserInfoMutaDictionary:(NSMutableDictionary *)infoMdic
                                withCmd:(NSString *)cmd withVerifyBlock:(verifyHelperBlock)block
{
    _blockCopy = block;
    VirtualServerBL *bl = (VirtualServerBL *)[[GEDeviceBLRepository shareInstance]
                                              findVirtualDeviceByMac:[[AppContext shareAppContext] servermac]];
    [bl stuffFormatMutableDictionary:infoMdic];
    [bl excuteWithTcpCmd:cmd];
}

- (void)verifyWithActivateCode:(NSString *)ac
               withVerifyBlock:(verifyHelperBlock)block
{
    _blockCopy = block;
    NSString *info = nil;
    NSError *error = nil;
    BOOL isActivateCode = [self isIdentifyCodeValid:ac];
    if (isActivateCode) {
        info = @"校验码格式正确";
    } else {
        info = @"校验码格式错误";
        error = [NSError errorWithDomain:@"activateCodeError" code:NSURLErrorUnknown userInfo:nil];
    }
    _blockCopy(info,error);
    _blockCopy = nil;
}

-(void)verifyWithOriginPassword:(NSString *)op
                    newPassword:(NSString *)np
                comparePassword:(NSString *)cp
                withVerifyBlock:(verifyHelperBlock)block
{
    _blockCopy = block;
    NSString *info = nil;
    NSError *error = nil;
    BOOL passwordValid = [self isValidPassword:np]; // 密码符合要求
    BOOL hasTwoSameAndNotEmptyPasswords = [np isEqualToString:cp];// 密码一致并且非空
    BOOL userLoggedIn = [AppContext shareAppContext].loginSuccess;
    BOOL newPwdChanged = ![op isEqualToString:np] && hasTwoSameAndNotEmptyPasswords;

    NSString *message;
    if (!(passwordValid && hasTwoSameAndNotEmptyPasswords && userLoggedIn && newPwdChanged)) {
        if (!passwordValid) {
            message = NSLocalizedString(@"LocUserPasswordIncorrect", @"注册密码格式不符合要求");
        }else if (!hasTwoSameAndNotEmptyPasswords){
            message = NSLocalizedString(@"LocUserPasswordsNotMatch", @"两次输入的注册密码不一致");
        }else if(!newPwdChanged){
            message = NSLocalizedString(@"LocPwdNotChanged", @"新密码与原密码一致");
        }else if(!userLoggedIn){
            message = NSLocalizedString(@"LocUserNotLoggedIn", @"用户尚未登录");
        }
        info = message;
        error = [NSError errorWithDomain:@"changePasswordError" code:0 userInfo:nil];
    }else{
        info = @"信息填写正确";
    }
    _blockCopy(info,error);
    _blockCopy = nil;
}

#pragma mark - verify
// 手机号码是否合法（适用与2014年末国内情况）
-(BOOL)isValidMobile:(NSString *)mobile
{
    NSString *phoneRegex = @"^(\\+86|86)?1(3[0-9]|5[0-35-9]|7[0678]|8[0-9])\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}
// 邮箱地址是否合法
-(BOOL)isValidEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
// 用户名是否合法（包括全部是数字的情况）
-(BOOL)isUserNameValidIncludeAllDigit:(NSString *)userName
{
    NSString *userNameRegex = @"^[a-zA-Z0-9][A-Za-z0-9_]{7,15}";
    NSPredicate *userNameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", userNameRegex];
    return [userNameTest evaluateWithObject:userName];
}
// 用户名全是数字
-(BOOL)isAllDigitUserName:(NSString *)userName
{
    NSString *userNameAllDigitRegex = @"^[0-9]{8,16}";
    NSPredicate *userNameAllDigitTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", userNameAllDigitRegex];
    return [userNameAllDigitTest evaluateWithObject:userName];
}
// 是否是合法的密码
-(BOOL)isValidPassword:(NSString *)password
{
    NSString *passwordRegex = @"^[A-Z0-9a-z_]{8,16}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passwordTest evaluateWithObject:password];
}

// 正则表达式确认验证码是否符合要求
-(BOOL)isIdentifyCodeValid:(NSString *)identifyCode
{
    NSString *identifyCodeRegex = @"^[0-9]{6}$";
    NSPredicate *identifyCodeTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", identifyCodeRegex];
    return [identifyCodeTest evaluateWithObject:identifyCode];
}

#pragma mark - receiveHandler
// 收到服务器信息之后进行分发
-(void)DispatchNotification:(NSNotification *)notification
{
    NSArray *arr = [notification object];
    
    if ([[arr objectAtIndex:0] isEqualToString:KJsonUserRegisterRespondFC])//用户注册信息验证请求应答帧
    {
        [self registerProcess:arr];
    } else if ([[arr objectAtIndex:0] isEqualToString:KJsonCheckIdentifyCodeRespondFC])//注册激活短信验证码校验应答帧
    {
        [self activateProcess:arr];
    } else if ([[arr objectAtIndex:0] isEqualToString:KJsonRequestCheckCodeRespondFC])//发送手机验证码激活请求应答帧
    {
        [self checkCodeProcess:arr];
    }
}

//“登录请求响应”处理
-(void)registerProcess:(id)info
{
    NSString *blockInfo = @"";
    NSError *error = nil;
    
    NSArray *data = info;
    NSString *statusCode = [data objectAtIndex:1];   //状态码
    
    CommunicateErrorData *errorData  = [[CommunicateErrorData alloc] init];
    NSString *statusCode_PhoneCheckCodeSent = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocPhoneCheckCodeSent", @"手机验证码已发送")];        //@“5”
    NSString *statusCode_ActivationLinkSent = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"LocEmailActivationLinkSent", @"邮箱激活链接已发送")]; //@"6"
    
    if ([statusCode isEqualToString:statusCode_PhoneCheckCodeSent] || [statusCode isEqualToString:statusCode_ActivationLinkSent]){ //服务器正确响应要求
        if([statusCode isEqualToString:statusCode_PhoneCheckCodeSent]){ //手机验证码
            //激活界面
            blockInfo = @"ActivateBySMS";
            error = nil;
        }else if ([statusCode isEqualToString:statusCode_ActivationLinkSent]){ //邮箱注册
            blockInfo = [errorData errorDetailWithStatusCode:statusCode];
            error = nil;
        }
    }else{ //服务器返回错误
        blockInfo = [errorData errorDetailWithStatusCode:statusCode];
        error = [NSError errorWithDomain:@" error" code:NSURLErrorUnknown userInfo:nil];
    }
    
    _blockCopy(blockInfo,error);
    _blockCopy = nil;
}

//“激活请求响应”处理
-(void)activateProcess:(id)info
{
    NSString *blockInfo = @"";
    NSError *error = nil;
    
    NSArray *data = info;
    NSString *statusCode = [data objectAtIndex:1];
    
    CommunicateErrorData *errorData  = [[CommunicateErrorData alloc] init];
    NSString *statusCode_communicateSucess = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"locCommunicateSucess",@"通信成功")]; //@"1"
    if([statusCode isEqualToString:statusCode_communicateSucess]){ // 验证码校验成功
        blockInfo = statusCode_communicateSucess;
    }else{
        blockInfo = [errorData errorDetailWithStatusCode:statusCode];
        error = [NSError errorWithDomain:@" error" code:NSURLErrorUnknown userInfo:nil];
    }

    _blockCopy(blockInfo,error);
    _blockCopy = nil;
}

//"验证码请求"处理
- (void)checkCodeProcess:(id)info
{
    NSString *blockInfo = @"";
    NSError *error = nil;
    
    NSArray *data = info;
    NSString *statusCode = [data objectAtIndex:1];
    
    CommunicateErrorData *errorData  = [[CommunicateErrorData alloc] init];
    NSString *statusCode_communicateSucess = [errorData statusCodeWithErrorDetail:NSLocalizedString(@"locCommunicateSucess",@"通信成功")]; //@"1"
    if([statusCode isEqualToString:statusCode_communicateSucess]){ // 验证码校验成功
        blockInfo = statusCode_communicateSucess;
    }else{
        blockInfo = [errorData errorDetailWithStatusCode:statusCode];
        error = [NSError errorWithDomain:@" error" code:NSURLErrorUnknown userInfo:nil];
    }
    _blockCopy(blockInfo,error);
    _blockCopy = nil;
}

@end
