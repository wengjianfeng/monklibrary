//
//  VirtualDevice.h
//  NewworkDemo
//
//  Created by gree's apple on 24/8/15.
//  Copyright (c) 2015年 WJF. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VirtualDeviceProtocol.h"

@interface VirtualDeviceBL : NSObject<GETCPSocketDelegate,GEUDPSocketDelegate,VirtualDeviceProtocol>
{
    NSMutableDictionary *sendingBuffer;
}

@property (nonatomic, strong) GEWiFiContext *_context;
@property (nonatomic, strong) GENetworking *_nw;
@property (nonatomic, strong) Command *_command;
@property (nonatomic, strong) NSString *identify;           //标识 可以认为是MAC
@property (nonatomic, assign) protocolType _mType;          //HEX OR JSON


-(instancetype)initWithType:(protocolType)type;

-(void)sendMessage:(BLMessageEvt)msg;

- (GEWiFiContext *) configContextWithDictionary:(NSDictionary *)dic;

- (void)excuteWithTcpCmd:(NSString *)cmd;

- (void)excuteWithUdpCmd:(NSString *)cmd;

- (void)creatTcpWithIpAndPortContext:(NSDictionary *)context;

- (void)creatUdpWithIpAndPortContext:(NSDictionary *)context;

- (void)stuffFormatMutableDictionary:(NSMutableDictionary *)dictionary;

@end
