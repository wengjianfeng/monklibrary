//
//  NetWorkData_LocFactory.h
//  Remote
//
//  Created by gree's apple on 18/9/14.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "NetWorkData_Factory.h"

typedef void (^LocDeviceSearchComple) (NSString *Mac);//TCP连接，及状态查询
typedef void (^LocDeviceStatusSearchComple) (NSString *Mac);//单纯查询空调运行状态

@interface NetWorkData_LocFactory : NetWorkData_Factory

-(id)DataFormJSON:(id)json;

-(void)setLocDeviceSearchComple:(void (^)(NSString *))deviceSearchCompleBlock;

-(void)setLocDeviceStatusSearchComple:(void (^)(NSString *))deviceSearchCompleBlock;

@end
