//
//  JSONProcessing.m
//  Remote
//
//  Created by Zhang Hanying on 14-9-23.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "JSONProcessing.h"
#import <CommonCrypto/CommonCrypto.h>
#import "ControlJsonFormatLists.h"

#define kPackageLength 1024 //json包的最大长度，超过需要分包

@implementation JSONProcessing

// 通过有效数据的字典和数据帧功能码生成JSON包
+ (NSArray*)JSONWithValidData:(id)validData functionCode:(NSString*)functionCode sequenceNum:(NSString *)seqNum
{
    NSMutableArray *jsonArray = [NSMutableArray array]; // Json数组
    NSString *validDataString;
    // 初始化frameData数据，需要其他数据才能决定的部分此时都为nil
//    DeviceNetWork *NW = [DeviceNetWork shareInstance];
    NSString *uuid = [validData objectForKey:@"objectUUID"];//通过dictionary来传值，达到解耦
    [validData removeObjectForKey:@"objectUUID"];
    
    FrameData *frameData = [[FrameData alloc]initWithObjectDeviceUUID:uuid functionCode:functionCode sequenceNumber:seqNum];
    
    //由于架构修改，如果字典中包含undefine的key，那么就认为发送的是字符串了
    if ([[validData allKeys] containsObject:@"undefine"]) {
        validData = (NSString *)[validData objectForKey:@"undefine"];
    }

    // 确定frameData的其他值
    frameData.validData = validData;
    // 确定dataLength属性
    if ([validData isKindOfClass:[NSDictionary class]]) { // 传进来的是字典
        validDataString = [JSONComposeTool JSONStringComposedWithDictionary:validData];
        frameData.dataLength = [NSString stringWithFormat:@"%lu",(unsigned long)[validDataString length]];
    }else if ([validData isKindOfClass:[NSArray class]]){ // 传进来的是数组
        validDataString = [JSONComposeTool JSONStringComposedWithArray:validData];
        frameData.dataLength = [NSString stringWithFormat:@"%lu",(unsigned long)[validDataString length]];
    }else{  // 字符串
        frameData.dataLength = [NSString stringWithFormat:@"%lu",(unsigned long)[validData length]];
    }
    
    int dataLengthIntValue = [frameData.dataLength intValue];  // validData数据长度的整数值
    // 不需要分包
    if ( (dataLengthIntValue < kPackageLength) && (dataLengthIntValue > 0) ) {
        frameData.subPackage = @"0000";
        // sign字段
        NSDictionary *frameDic = [PackDictionary dictionaryWithFrameData:frameData];
        NSMutableString *signString = [self stringFromDictionary:frameDic];
        frameData.sign = [self signWithString:signString];
        [frameDic setValue:frameData.sign forKey:@"sign"];
        // 数据帧打包成Json
        NSData *frameJson = [JSONComposeTool JSONComposedWithDictionary:frameDic];
        if (frameJson == nil) {
            return nil;
        }
//        NSLog(@"Json Factory SendData %@",[[NSString alloc]initWithData:frameJson encoding:NSUTF8StringEncoding]);
        [jsonArray addObject:frameJson];
    }
    // 分包
#warning 现在对长度很大的数据分包存在问题
    else if (dataLengthIntValue > kPackageLength){
        // 把json分段，按顺序保存在数组中
        NSMutableArray *subJsonsArray = [self splitJsonString:validDataString withLengthLimit:kPackageLength];
        NSLog(@"json分段结果是-----%@",subJsonsArray);
        // 将得到的子Json数组分别打包
        int subPackagesCount = (int)[subJsonsArray count];  // 分包数量
        for (int i = 0; i < subPackagesCount; i++) {
            NSString *subjson = subJsonsArray[i];
            frameData.validData = subjson;
            frameData.subPackage = [self subPackageSerialWithSubPackagesCount:subPackagesCount subPackageindex:i+1];
            frameData.dataLength = [NSString stringWithFormat:@"%lu", (unsigned long)subjson.length];
            // sign字段
            NSDictionary *subDictionary = [PackDictionary dictionaryWithFrameData:frameData];
            NSMutableString *signString = [self stringFromDictionary:subDictionary];
            frameData.sign = [self signWithString:signString];
            // 数据帧字典最后再把sign字段加入
            [subDictionary setValue:frameData.sign forKey:@"sign"];
            NSData *json = [JSONComposeTool JSONComposedWithDictionary:subDictionary];
            NSString *validDataWithoutEscapeCharacter = [[NSString alloc]initWithData:json encoding:NSUTF8StringEncoding];
            validDataWithoutEscapeCharacter = [validDataWithoutEscapeCharacter stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
            validDataWithoutEscapeCharacter = [validDataWithoutEscapeCharacter stringByReplacingOccurrencesOfString:@"\"{" withString:@"{"];
            validDataWithoutEscapeCharacter = [validDataWithoutEscapeCharacter stringByReplacingOccurrencesOfString:@"}\"" withString:@"}"];
            validDataWithoutEscapeCharacter = [validDataWithoutEscapeCharacter stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];
            NSLog(@"Single Json ----------- %@",validDataWithoutEscapeCharacter);
            json = [validDataWithoutEscapeCharacter dataUsingEncoding:NSUTF8StringEncoding];
            [jsonArray addObject:json];
        }
    }
    return jsonArray;
}

// 从Json数组中解析出一个字典
+ (NSDictionary *)dictionaryParsedFromJsonArray:(NSArray*)jsonArray
{
    NSDictionary *dictionary;  // 总的dictionary
    int dataLength = 0;        // 有多包时，返回总的ValidData数据长度
    NSMutableDictionary *validDataParsed = [NSMutableDictionary dictionary];  // 解析出来的validData字典
    NSMutableString *encryptedValidDataString;
    // 接收到的Json数据只有一包
    if (jsonArray.count == 1) {
        NSData *json = [jsonArray firstObject];
        //NSLog(@"=====-------%@",[[NSString alloc]initWithData:json encoding:NSUTF8StringEncoding]);
        dictionary = [JSONParseTool dictionaryParsedOfJson:json];
        //NSLog(@"DICTIONARY-=-=-=%@",dictionary);
        return dictionary;
    }// 接收到的json数据有多包
    else if (jsonArray.count > 1){
        for (int i = 0; i < jsonArray.count; i++) {
            NSData *json = [jsonArray objectAtIndex:i];
            //NSLog(@"JSON IS %@",[[NSString alloc]initWithData:json encoding:NSUTF8StringEncoding]);
            dictionary = [JSONParseTool dictionaryParsedOfJson:json];
            //NSLog(@"DICTIONARY-=-=-=%@",dictionary);
            dataLength += [[dictionary objectForKey:@"dataLength"]intValue]; // 累加每个分包的数据长度
            [dictionary setValue:[NSString stringWithFormat:@"%d",dataLength] forKey:@"dataLength"];
            
            if ([dictionary[@"functionCode"]isEqualToString:KJsonClientPostPubKeyRespond] || [dictionary[@"functionCode"]isEqualToString:KJsonClientRequestSessionKeyRespond]){
                // 把分开的字符串再组合起来
                NSString *subJsonString = [dictionary objectForKey:@"validData"];
                [encryptedValidDataString appendString:subJsonString];
                [validDataParsed setObject:encryptedValidDataString forKey:@"validData"];
            }else{
                NSDictionary *subDictionary = [dictionary objectForKey:@"validData"];
                // 把解析出的字典加入总的validData字典
                [validDataParsed addEntriesFromDictionary:subDictionary];
            }
        }
    }
    // 用解出来的字典替换validData的Json包
    [dictionary setValue:validDataParsed forKey:@"validData"];
    //NSLog(@"FINALLY---%@",dictionary);
    return dictionary;
}

// 根据分包数量和分包的index生成分包序列号
+(NSString *)subPackageSerialWithSubPackagesCount:(int)count subPackageindex:(int)index
{
    NSMutableString *serial = [NSMutableString string];
    if (count < 1 || count > 16384) { // 输入有误
        serial = nil;
    }else if (count == 1) { // 只有一包
        serial = [NSMutableString stringWithString:@"0000"];
    }else{   // 多包
        serial = [NSMutableString stringWithFormat:@"%@",[[NSMutableString alloc]initWithFormat:@"%04x",index]];
        if (index < count) {      // 多包且不是最后一包
            [serial replaceCharactersInRange:NSMakeRange(0, 1) withString:@"8"]; // "8"和"C"的来源请参照数据帧格式文档
        }else if(index == count){ // 多包的最后一包
            [serial replaceCharactersInRange:NSMakeRange(0, 1) withString:@"C"];
        }
    }
    //NSLog(@"serial-------- %@",serial);
    return serial;
}

// 根据长度限制将Json按照键值对分成小的Json包
+(NSMutableArray*)splitJsonString:(NSString*)jsonString withLengthLimit:(int)lengthLimit
{
    NSMutableArray *subJsons = [NSMutableArray array];     // 分好的子Json数组
    NSMutableString *subString = [NSMutableString string]; // 每次获得的子Json字符串
    NSArray *keyValuePairs = [jsonString componentsSeparatedByString:@"},"]; // 原Json被分成的键值对数组
    //NSLog(@"分成的单个键值对是----------%@",keyValuePairs);
//    int keyValuePairsUsed;    // 已经使用了多少个键值对
    NSString *keyValuePair;   // 单个键值对
    for (int i = 0; i < keyValuePairs.count; i++) {
        keyValuePair = keyValuePairs[i];
        if ((subString.length + keyValuePair.length) < lengthLimit-2) { // 未达到单个包长度限制
            [subString appendFormat:@"%@},",keyValuePair];
//            keyValuePairsUsed = i + 1;
        }else{ // 达到每个分包长度限制，注意这里不包含最后一个包的情况
            i--;  // 遍历需要回退一位
            [subString deleteCharactersInRange:NSMakeRange(subString.length-1, 1)]; // 删除最后一个逗号
            [subString appendString:@"]"];      // 在字符串最后添加
            [subJsons addObject:subString];       // 把结果加入到数组
            subString = [NSMutableString string]; // subString置空
            [subString appendString:@"["];        // 置空之后，在最开始加入，这里不包括第一个包的情况
        } //NSLog(@"键值对是-----%@，重新合成的字符串是-------%@，已使用的键值对个数%d",keyValuePairs[i],subString,keyValuePairsUsed);
        //SLog(@"SUBSTRING === %@\n",subString);
    }
    [subString deleteCharactersInRange:NSMakeRange(subString.length-2, 2)]; // 删除最后一个包多余的一个逗号
    [subJsons addObject:subString]; // 把最后一个包加入
    return subJsons;
}

#pragma mark - 把字典或者Json包内的键值对转换成字符串
// 把字典的键值对按顺序转换成字符串
+(NSMutableString*)stringFromDictionary:(NSDictionary*)dictionary
{
    NSMutableString *signString = [NSMutableString string];
    NSArray *keys = [dictionary allKeys];  // 所有的键
    
    // 将所有的键排序，后来不再排序
    //NSArray *sortedKeys = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        //return [obj1 compare:obj2 options:NSNumericSearch];
    //}];
    
    NSString *value;
    for (NSString *key in keys) {
        if ([key isEqualToString:@"validData"] && [[dictionary valueForKey:key] isKindOfClass:[NSDictionary class]]) { //是字典
            NSDictionary *dic = [dictionary valueForKey:key];
            value = [self stringFromValidData:dic];
        }else{ //是字符串
            value = [dictionary valueForKey:key];
        }
        //[signString appendFormat:@"%@:%@",key,value];
        [signString appendFormat:@"%@%@",key,value];
    };
    return signString;
}
//把validData转换成字符串
+(NSString*)stringFromValidData:(NSDictionary*)validData
{
    //将字典转换成字符串
    NSMutableString *signString = [NSMutableString string];
    NSArray *keys = [validData allKeys];  // 所有的键
    for (NSString *key in keys) {
        NSString *value = validData[key];
        [signString appendFormat:@"%@%@",key,value];
    };
    return signString;
}

// 把JSON包按内部内容顺序转换成字符串
//+(NSMutableString*)alphaBeticalStringFromJsonString:(NSString*)jsonString
//{
//    NSMutableString *signString = [NSMutableString string];
//    // 把json解析成字典
//    NSString *string = [jsonString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSData *signData = [string dataUsingEncoding:NSUTF8StringEncoding];
//    NSDictionary *signDic = [JSONParseTool dictionaryParsedOfJson:signData];
//    NSLog(@"signDic-------%@",signDic);
//    // 把字典转换成字符串
//    signString = [self alphaBeticalStringFromDictionary:signDic];
//    NSLog(@"signString-=-=-=%@",signString);
//    return signString;
//}

#pragma mark - md5加密和生成sign字段字符串
// 生成字符串的sign字段
+(NSString*)signWithString:(NSString*)string
{
    NSString *md5 = [self md5FromString:string];        //NSLog(@"字符串转换成md5是：----%@",md5);
    md5 = [md5 substringWithRange:NSMakeRange(5, 16)];  //NSLog(@"md5截取6-21位是：----%@",md5);
    md5 = [self md5FromString:md5];                     //NSLog(@"截取的md5再做MD5以后是：----%@",md5);
    md5 = [md5 substringWithRange:NSMakeRange(4, 16)];  //NSLog(@"得到的结果再取第5到20位是：----%@",md5);
    return md5;
}

// 生成字符串的md5值
+(NSString*)md5FromString:(NSString*)string
{
    const char *cStr = [string UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (unsigned int)strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

@end
