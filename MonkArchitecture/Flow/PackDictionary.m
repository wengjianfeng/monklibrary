//
//  Frame.m
//  Remote
//
//  Created by Zhang Hanying on 14-9-20.
//  Copyright (c) 2014年 WJF-Monk  330694. All rights reserved.
//

#import "PackDictionary.h"
#import "FrameData.h"

@implementation PackDictionary

+(NSDictionary *)dictionaryWithFrameData:(FrameData *)frameData
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:frameData.originDeviceUUID forKey:@"originDeviceUUID"];
    if (frameData.objectDeviceUUID == nil)
    {
        return nil;
    }
    [dic setObject:frameData.objectDeviceUUID forKey:@"objectDeviceUUID"];
    [dic setObject:frameData.validData forKey:@"validData"];
    
    [dic setObject:frameData.dataLength forKey:@"dataLength"];
    [dic setObject:frameData.subPackage forKey:@"subPackage"];
    
    [dic setObject:frameData.functionCode forKey:@"functionCode"];
    [dic setObject:frameData.sequenceNumber forKey:@"sequenceNumber"];
    [dic setObject:frameData.deviceType forKey:@"deviceType"];
    if (frameData.sign) {
        [dic setObject:frameData.sign forKey:@"sign"];
    }
    
    return dic;
}

- (NSString *)getPhoneUUID
{
    NSString *UUID = [[NSUUID UUID]UUIDString];
    return  UUID;
}

- (void)switchUUIDsofCommunicationFrame:(FrameData *)frame
{
    NSString *originalDeviceUUID = frame.originDeviceUUID;
    frame.originDeviceUUID = frame.objectDeviceUUID;
    frame.objectDeviceUUID = originalDeviceUUID;
}

@end
